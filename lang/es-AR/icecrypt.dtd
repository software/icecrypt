<!ENTITY icecrypt.label "Icecrypt">
<!ENTITY icecrypt.openpgp.label "OpenPGP">
<!ENTITY icecrypt.keyUserId.label "Cuenta / ID de usuario">
<!ENTITY icecrypt.keygenTitle.label "Generar clave OpenPGP">
<!ENTITY icecrypt.useForSigning.label "Usar la clave generada para la identidad seleccionada">
<!ENTITY icecrypt.keyComment.label "Comentario">
<!ENTITY icecrypt.keyNoPassphrase.label "Sin frase contrase\u00F1a">
<!ENTITY icecrypt.keyPassphrase.label "Frase contrase\u00F1a">
<!ENTITY icecrypt.keyPassphraseRepeat.label "Frase contrase\u00F1a (repetir)">
<!ENTITY icecrypt.generateKey.tooltip "Genera una nueva clave compatible con OpenPGP para firmar y/o cifrar.">
<!ENTITY icecrypt.cancelKey.label "Cancelar">
<!ENTITY icecrypt.cancelKey.tooltip "Cancelar generaci\u00F3n de la clave.">
<!ENTITY icecrypt.keyGen.expiry.title "Validez de la clave">
<!ENTITY icecrypt.keyGen.expire.label "La clave caduca dentro de">
<!ENTITY icecrypt.keyGen.days.label "d\u00EDas">
<!ENTITY icecrypt.keyGen.months.label "meses">
<!ENTITY icecrypt.keyGen.years.label "a\u00F1os">
<!ENTITY icecrypt.keyGen.noExpiry.label "La clave nunca caduca">
<!ENTITY icecrypt.keyGen.keySize.label "Tama\u00F1o de la clave">
<!ENTITY icecrypt.keyGen.console.label "Consola de generaci\u00F3n de clave">
<!ENTITY icecrypt.keyGen.keyType.label "Tipo de clave">
<!ENTITY icecrypt.keyGen.keyType.dsa "DSA y ElGamal">
<!ENTITY icecrypt.keyGen.keyType.rsa "RSA">
<!ENTITY icecrypt.preferences.label "Preferencias de Icecrypt">
<!ENTITY icecrypt.passwordSettings.label "Configurar frase contrase\u00F1a">
<!ENTITY icecrypt.userNoPassphrase.label "Sin frase contrase\u00F1a para el usuario">
<!ENTITY icecrypt.userNoPassphrase.tooltip "Activar si su clave OpenPGP no est\u00E1 protegida con una frase clave.">
<!ENTITY icecrypt.expertUser.tooltip "Activar si desea ver elementos de men\u00FA y opciones para usuarios avanzados.">
<!ENTITY icecrypt.basicUser.tooltip "Presione si no desea ver elementos de men\u00FA y opciones para usuarios avanzados.">
<!ENTITY icecrypt.prefs.pathsTitle.label "Archivos y directorios">
<!ENTITY icecrypt.prefs.overrideGpg.label "Ignorar con">
<!ENTITY icecrypt.prefs.overrideGpg.tooltip "Pulsar para especificar una ubicaci\u00F3n para GnuPG">
<!ENTITY icecrypt.agentAdditionalParam.label "Par\u00E1metros adicionales para GnuPG">
<!ENTITY icecrypt.agentAdditionalParam.tooltip "Especifique las opciones que usar\u00E1 GnuPG.">
<!ENTITY icecrypt.mime_parts_on_demand.label "Cargar partes MIME bajo demanda (carpetas IMAP)">
<!ENTITY icecrypt.maxIdleMinutesHead.label "Recordar frase contrase\u00F1a durante">
<!ENTITY icecrypt.maxIdleMinutesTail.label "minutos de tiempo ocioso">
<!ENTITY icecrypt.maxIdleMinutes.tooltip "Cachear en memoria las frases clave durante un tiempo limitado.">
<!ENTITY icecrypt.resetPrefsButton.label "Restablecer">
<!ENTITY icecrypt.resetPrefs.tooltip "Volver a poner todas las preferencias con su valor predeterminado.">
<!ENTITY icecrypt.basic.label "B\u00E1sicas">
<!ENTITY icecrypt.sending.label "Enviar">
<!ENTITY icecrypt.keySel.label "Selecci\u00F3n de clave">
<!ENTITY icecrypt.pgpMime.label "PGP/MIME">
<!ENTITY icecrypt.advanced.label "Avanzadas">
<!ENTITY icecrypt.debug.label "Depuraci\u00F3n">
<!ENTITY icecrypt.basicPrefs.label "Opciones b\u00E1sicas">
<!ENTITY icecrypt.moreOptions.label "M\u00E1s opciones">
<!ENTITY icecrypt.alwaysTrustSend.label "Siempre confiar en el ID de usuario">
<!ENTITY icecrypt.alwaysTrustSend.tooltip "No usar la Web de confianza para determinar la validez de las claves">
<!ENTITY icecrypt.useNonDefaultComment.label "A\u00F1adir el comentario de Icecrypt en la firma OpenPGP">
<!ENTITY icecrypt.keyserver.label "Servidor de claves">
<!ENTITY icecrypt.keyserverDlg.label "Seleccionar servidor de claves">
<!ENTITY icecrypt.keyservers.label "Servidor(es) de claves">
<!ENTITY icecrypt.keyservers.sample "Ejemplo: sks.dnsalias.net, pgp.mit.edu, ldap://certserver.pgp.com">
<!ENTITY icecrypt.searchForKey.label "Buscar clave">
<!ENTITY icecrypt.doubleDashSeparator.label "Tratar '--' como separador de firma">
<!ENTITY icecrypt.useGpgAgent.label "Usar agente gpg para gestionar la frase contrase\u00F1a">
<!ENTITY icecrypt.noHushMailSupport.label "Usar '&lt;' y '&gt;' para especificar direcciones de correo electr\u00F3nico">
<!ENTITY icecrypt.doubleDashSeparator.tooltip "Dos guiones indican por s\u00ED mismos un bloque de firma">
<!ENTITY icecrypt.useGpgAgent.tooltip "Usar el agente gpg (parte de GnuPG 2) para toda la introducci\u00F3n de la frase clave">
<!ENTITY icecrypt.noHushMailSupport.tooltip "Use los caracteres '&lt;' y '&gt;' para especificar direcciones de correo electr\u00F3nico para GnuPG. Desactivar si los destinatarios tienen claves antiguas de Hushmail.">
<!ENTITY icecrypt.mime_parts_on_demand.tooltip "Debe desactivar esta opci\u00F3n si est\u00E1 recibiendo mensajes PGP/MIME en carpetas IMAP">
<!ENTITY icecrypt.disableSMIMEui.tooltip "Eliminar el bot\u00F3n S/MIME de la barra de herramientas de la ventana de redacci\u00F3n de mensajes">
<!ENTITY icecrypt.logdirectory.label "Directorio de registro">
<!ENTITY icecrypt.testemail.label "Probar direcci\u00F3n de correo">
<!ENTITY icecrypt.test.label "Probar">
<!ENTITY icecrypt.decryptbutton.label "Descifrar">
<!ENTITY icecrypt.decryptbutton.tip "Descifrar o verificar el mensaje con Icecrypt">
<!ENTITY icecrypt.messengermenu.accesskey "N">
<!ENTITY icecrypt.decryptverify.label "Descifrar/verificar">
<!ENTITY icecrypt.importpublickey.label "Importar clave p\u00FAblica">
<!ENTITY icecrypt.savedecrypted.label "Guardar mensaje descifrado">
<!ENTITY icecrypt.autoDecrypt.label "Descifrar/verificar mensajes autom\u00E1ticamente">
<!ENTITY icecrypt.clearPassphrase.label "Borrar frase contrase\u00F1a guardada">
<!ENTITY icecrypt.editRules.label "Editar reglas por destinatario">
<!ENTITY icecrypt.prefs.label "Preferencias">
<!ENTITY icecrypt.debugMenu.label "Depurar OpenPGP">
<!ENTITY icecrypt.viewconsole.label "Ver consola">
<!ENTITY icecrypt.viewdebuglog.label "Ver archivo de registro">
<!ENTITY icecrypt.generateKey.label "Generar la clave">
<!ENTITY icecrypt.help.label "Ayuda">
<!ENTITY icecrypt.about.title "Acerca de Icecrypt">
<!ENTITY icecrypt.about.label "Acerca de Icecrypt">
<!ENTITY icecrypt.reload.label "Recargar mensaje">
<!ENTITY icecrypt.browse.label "Examinar...">
<!ENTITY icecrypt.sendersKeyMenu.label "Clave del remitente">
<!ENTITY icecrypt.adminSmartCard.label "Administrar tarjeta inteligente...">
<!ENTITY icecrypt.setupWiz.label "Asistente de Configuraci\u00F3n">
<!ENTITY icecrypt.decryptverify.accesskey "D">
<!ENTITY icecrypt.importpublickey.accesskey "I">
<!ENTITY icecrypt.savedecrypted.accesskey "S">
<!ENTITY icecrypt.reload.accesskey "E">
<!ENTITY icecrypt.adminSmartCard.accesskey "M">
<!ENTITY icecrypt.autoDecrypt.accesskey "A">
<!ENTITY icecrypt.debugMenu.accesskey "G">
<!ENTITY icecrypt.clearPassphrase.accesskey "B">
<!ENTITY icecrypt.editRules.accesskey "R">
<!ENTITY icecrypt.setupWiz.accesskey "S">
<!ENTITY icecrypt.prefs.accesskey "P">
<!ENTITY icecrypt.viewconsole.accesskey "C">
<!ENTITY icecrypt.viewdebuglog.accesskey "G">
<!ENTITY icecrypt.help.accesskey "U">
<!ENTITY icecrypt.about.accesskey "O">
<!ENTITY icecrypt.sendersKeyMenu.accesskey "C">
<!ENTITY icecrypt.securitybutton.label "Icecrypt">
<!ENTITY icecrypt.securitybutton.tip "Opciones de seguridad Icecrypt">
<!ENTITY icecrypt.signedsend.label "Firmar mensaje">
<!ENTITY icecrypt.encryptedsend.label "Cifrar mensaje">
<!ENTITY icecrypt.disableRules.label "Ignorar reglas por destinatario">
<!ENTITY icecrypt.composeOptionsMenu.label "Opciones predeterminadas de redacci\u00F3n">
<!ENTITY icecrypt.pgpAccountSettings.label "Opciones de firmado/cifrado...">
<!ENTITY icecrypt.sendOptionsMenu.label "Opciones de env\u00EDo...">
<!ENTITY icecrypt.defaultKeySelOpts.label "Opciones de selecci\u00F3n de clave...">
<!ENTITY icecrypt.sendPGPMime.label "Usar PGP/MIME en este mensaje">
<!ENTITY icecrypt.undoencryption.label "Deshacer cifrado">
<!ENTITY icecrypt.attachkey.label "Adjuntar clave p\u00FAblica...">
<!ENTITY icecrypt.attachmykey.label "Adjuntar Mi clave p\u00FAblica">
<!ENTITY icecrypt.composemenu.accesskey "N">
<!ENTITY icecrypt.signedsend.accesskey "S">
<!ENTITY icecrypt.signedsend.key "S">
<!ENTITY icecrypt.encryptedsend.accesskey "E">
<!ENTITY icecrypt.encryptedsend.key "P">
<!ENTITY icecrypt.disableRules.accesskey "I">
<!ENTITY icecrypt.composeOptionsMenu.accesskey "D">
<!ENTITY icecrypt.sendOptionsMenu.accesskey "S">
<!ENTITY icecrypt.defaultKeySelOpts.accesskey "O">
<!ENTITY icecrypt.pgpAccountSettings.accesskey "E">
<!ENTITY icecrypt.sendPGPMime.accesskey "P">
<!ENTITY icecrypt.undoencryption.accesskey "H">
<!ENTITY icecrypt.attachkey.accesskey "A">
<!ENTITY icecrypt.attachmykey.accesskey "M">
<!ENTITY icecrypt.defaultEncryption.label "Cifrar mensajes por defecto">
<!ENTITY icecrypt.usePGPMimeAlways.label "Siempre usar PGP/MIME">
<!ENTITY icecrypt.advancedPrefsButton.label "Avanzadas...">
<!ENTITY icecrypt.recipientsSelectionOption.label "Comportamiento para seleccionar claves de destinatarios al enviar mensajes">
<!ENTITY icecrypt.defineRules.label "Definir reglas...">
<!ENTITY icecrypt.wrapHtmlBeforeSend.label "Reajustar el texto HTML firmado antes de enviar">
<!ENTITY icecrypt.wrapHtmlBeforeSend.tooltip "Reajustar el texto para asegurarse que la firma permanecer\u00E1 intacta.">
<!ENTITY icecrypt.autoKeyRetrieve.label "Descargar las claves autom\u00E1ticamente del servidor">
<!ENTITY icecrypt.autoKeyRetrieve2.label "del siguiente servidor de claves:">
<!ENTITY icecrypt.resetRememberedValues.label "Restablecer avisos">
<!ENTITY icecrypt.resetRememberedValues.tooltip "Volver a mostrar todos los di\u00E1logos de aviso y preguntas que haya ocultado en el pasado.">
<!ENTITY icecrypt.keygen.desc "<a class='icecryptStrong'>NOTA: Generar la clave puede tardar varios minutos.</a> No salga del programa mientras se est\u00E9 generando la clave. Navegue activamente o realice distintas operaciones que requieran un uso intensivo del disco durante el proceso, repercutir\u00E1n en la aleatoridad y velocidad del mismo. Cuando las claves se hayan generado, se le avisar\u00E1.">
<!ENTITY icecrypt.userSelectionList.label "Seleccionar clave Icecrypt">
<!ENTITY icecrypt.usersNotFound.label "Los destinatarios no son v\u00E1lidos, no son confiables o no se encuentran">
<!ENTITY icecrypt.keyExpiry.label "Expiraci\u00F3n">
<!ENTITY icecrypt.keySelection.label "Selecci\u00F3n">
<!ENTITY icecrypt.keyId.label "ID de clave">
<!ENTITY icecrypt.displayNoLonger.label "No volver a mostrar esta opci\u00F3n de di\u00E1logo si no se puede cifrar">
<!ENTITY icecrypt.importMissingKeys.label "Descargar las claves que falten">
<!ENTITY icecrypt.importMissingKeys.accesskey "I">
<!ENTITY icecrypt.importMissingKeys.tooltip "Intentar importar las claves que falten de un servidor de claves">
<!ENTITY icecrypt.refreshKeys.label "Actualizar lista de claves">
<!ENTITY icecrypt.refreshKeys.accesskey "R">
<!ENTITY icecrypt.perRecipientsOption.label "Crear regla(s) por destinatario">
<!ENTITY icecrypt.icecryptAttachDesc.label "Este mensaje contiene adjuntos. \u00BFC\u00F3mo desea cifrarlos o firmarlos?">
<!ENTITY icecrypt.enigEncryptAttachNone.label "Cifrar/firmar s\u00F3lo el texto del mensaje, no los adjuntos">
<!ENTITY icecrypt.enigEncryptAttachInline.label "Cifrar cada adjunto por separado y enviar el mensaje usando PGP integrado">
<!ENTITY icecrypt.enigEncryptAttachPgpMime.label "Cifrar/firmar el mensaje conjuntamente y enviarlo usando PGP/MIME">
<!ENTITY icecrypt.encryptAttachSkipDlg.label "Usar el m\u00E9todo seleccionado para todos los adjuntos futuros">
<!ENTITY icecrypt.ctxDecryptOpen.label "Descifrar y abrir">
<!ENTITY icecrypt.ctxDecryptSave.label "Descifrar y guardar como...">
<!ENTITY icecrypt.ctxImportKey.label "Importar clave OpenPGP">
<!ENTITY icecrypt.ctxDecryptOpen.accesskey "D">
<!ENTITY icecrypt.ctxDecryptSave.accesskey "C">
<!ENTITY icecrypt.ctxImportKey.accesskey "I">
<!ENTITY icecrypt.detailsHdrButton.label "Detalles">
<!ENTITY icecrypt.amPrefTitle.label "Opciones OpenPGP">
<!ENTITY icecrypt.amPrefDesc.label "Icecrypt proporciona soporte para el cifrado y firmado OpenPGP de mensajes. Necesita tener instalado GnuPG (gpg) para usar estas caracter\u00EDsticas.">
<!ENTITY icecrypt.amPrefEnablePgp.label "Activar el soporte OpenPGP (Icecrypt) para esta identidad">
<!ENTITY icecrypt.amPrefUseFromAddr.label "Usar la direcci\u00F3n de correo de esta identidad para identificar la clave OpenPGP">
<!ENTITY icecrypt.amPrefUseKeyId.label "Usar un ID de clave OpenPGP espec\u00EDfico (0x1234ABCD):">
<!ENTITY icecrypt.amPrefSelectKey.label "Seleccionar clave...">
<!ENTITY icecrypt.amPrefDefaultEncrypt.label "Opciones predeterminadas de redacci\u00F3n de mensajes">
<!ENTITY icecrypt.encryptionDlg.label "Opciones de cifrado y firma Icecrypt">
<!ENTITY icecrypt.encDlgEncrypt.label "Cifrar mensaje">
<!ENTITY icecrypt.encDlgEncrypt.accesskey "E">
<!ENTITY icecrypt.encDlgSign.label "Firmar mensaje">
<!ENTITY icecrypt.encDlgSign.accesskey "S">
<!ENTITY icecrypt.encDlgPgpMime.label "Usar PGP/MIME">
<!ENTITY icecrypt.encDlgPgpMime.accesskey "U">
<!ENTITY icecrypt.amPrefPgpHeader.label "Enviar cabecera 'OpenPGP'">
<!ENTITY icecrypt.amPrefPgpHeader.id.label "Enviar ID de clave OpenPGP">
<!ENTITY icecrypt.amPrefPgpHeader.url.label "Enviar URL para recuperar clave:">
<!ENTITY icecrypt.progressText.label "Progreso:">
<!ENTITY icecrypt.openPgpSecurity.label "Seguridad OpenPGP">
<!ENTITY icecrypt.pgpSecurityInfo.label "Informaci\u00F3n de seguridad Icecrypt...">
<!ENTITY icecrypt.copySecurityInfo.label "Copiar informaci\u00F3n de seguridad Icecrypt">
<!ENTITY icecrypt.showPhoto.label "Ver ID fotogr\u00E1fico OpenPGP">
<!ENTITY icecrypt.signSenderKey.label "Firmar clave...">
<!ENTITY icecrypt.trustSenderKey.label "Establecer confianza en la clave...">
<!ENTITY icecrypt.signSenderKeyPopup.label "Firmar clave del remitente...">
<!ENTITY icecrypt.trustSenderKeyPopup.label "Establecer confianza en la clave del remitente...">
<!ENTITY icecrypt.createRuleFromAddr.label "Crear regla Icecrypt para la direcci\u00F3n...">
<!ENTITY icecrypt.keyManWindow.label "Administraci\u00F3n de claves">
<!ENTITY icecrypt.keyManWindow.accesskey "N">
<!ENTITY icecrypt.singleRcptSettings.label "Icecrypt - Configuraci\u00F3n del destinatario">
<!ENTITY icecrypt.selKeysButton.label "Seleccionar clave(s)...">
<!ENTITY icecrypt.action.label "Acci\u00F3n">
<!ENTITY icecrypt.nextRule.label "Continuar con la regla siguiente para la direcci\u00F3n coincidente">
<!ENTITY icecrypt.nextAddress.label "No comprobar las reglas siguientes para la direcci\u00F3n coincidente">
<!ENTITY icecrypt.useKeys.label "Usar las siguientes claves OpenPGP:">
<!ENTITY icecrypt.selKeysButton.accesskey "S">
<!ENTITY icecrypt.setDefaultsFor.label "Predeterminada para...">
<!ENTITY icecrypt.encryption.label "Cifrar">
<!ENTITY icecrypt.signing.label "Firmar">
<!ENTITY icecrypt.never.label "Nunca">
<!ENTITY icecrypt.always.label "Siempre">
<!ENTITY icecrypt.maybe.label "S\u00ED, si est\u00E1 seleccionada en la redacci\u00F3n del mensaje">
<!ENTITY icecrypt.singleRcptSettings.desc "(NOTA: En caso de conflictos, 'Nunca' precede a 'Siempre')">
<!ENTITY icecrypt.ruleEmail.label "Definir reglas Icecrypt para">
<!ENTITY icecrypt.ruleEmail.tooltip "Rellenar s\u00F3lo direcciones de correo electr\u00F3nico, NO poner los nombres en los destinatarios Ejemplo: 'El nombre <usuario@servidor.net>' no es v\u00E1lido">
<!ENTITY icecrypt.sepratationDesc.label "(Separar las direcciones de correo con espacios)">
<!ENTITY icecrypt.matchDescStart.label "Aplicar regla si el destinatario">
<!ENTITY icecrypt.matchDescEnd.label "una de las direcciones de arriba.">
<!ENTITY icecrypt.matchExact.label "es exactamente">
<!ENTITY icecrypt.matchContains.label "contiene">
<!ENTITY icecrypt.matchBegin.label "comienza con">
<!ENTITY icecrypt.matchEnd.label "acaba en">
<!ENTITY icecrypt.not.label "No">
<!ENTITY icecrypt.rulesEditor.label "Icecrypt - Editor de reglas por destinatario">
<!ENTITY icecrypt.email.label "Correo electr\u00F3nico">
<!ENTITY icecrypt.pgpKeys.label "Clave(s) OpenPGP">
<!ENTITY icecrypt.sign.label "Firmar">
<!ENTITY icecrypt.encrypt.label "Cifrar">
<!ENTITY icecrypt.edit.label "Modificar">
<!ENTITY icecrypt.edit.accesskey "M">
<!ENTITY icecrypt.add.label "Agregar">
<!ENTITY icecrypt.add.accesskey "A">
<!ENTITY icecrypt.delete.label "Eliminar">
<!ENTITY icecrypt.delete.accesskey "E">
<!ENTITY icecrypt.moveUp.label "Mover arriba">
<!ENTITY icecrypt.moveUp.accesskey "O">
<!ENTITY icecrypt.moveDown.label "Mover abajo">
<!ENTITY icecrypt.moveDown.accesskey "V">
<!ENTITY icecrypt.searchRule.label "Ver reglas con direcciones que contengan:">
<!ENTITY icecrypt.clearSearch.label "Borrar">
<!ENTITY icecrypt.clearSearch.accesskey "B">
<!ENTITY icecrypt.searchKeyDlg.label "Descargar claves OpenPGP">
<!ENTITY icecrypt.searchKeyDlgCapt.label "Se encontraron las siguientes claves - Seleccionar para importar">
<!ENTITY icecrypt.created.label "Creada">
<!ENTITY icecrypt.valid.label "V\u00E1lida">
<!ENTITY icecrypt.progress.label "Progreso">
<!ENTITY icecrypt.sKeyDlg.title "Icecrypt - Firmar clave">
<!ENTITY icecrypt.sKeyDlg.signKey.label "Clave que se va a firmar:">
<!ENTITY icecrypt.sKeyDlg.fingerprint.label "Huella digital:">
<!ENTITY icecrypt.sKeyDlg.signWithKey.label "Clave a firmar:">
<!ENTITY icecrypt.sKeyDlg.checked.label "\u00BFCon cu\u00E1nto cuidado ha comprobado que la clave que va a firmar pertenece actualmente a la persona indicada arriba?">
<!ENTITY icecrypt.sKeyDlg.noAnswer.label "No contesto">
<!ENTITY icecrypt.sKeyDlg.notChecked.label "No lo he comprobado en absoluto">
<!ENTITY icecrypt.sKeyDlg.casualCheck.label "He hecho una comprobaci\u00F3n informal">
<!ENTITY icecrypt.sKeyDlg.carefulCheck.label "Lo he comprobado meticulosamente">
<!ENTITY icecrypt.sKeyDlg.createLocal.label "Firma local (no se puede exportar)">
<!ENTITY icecrypt.addUidDlg.title "Icecrypt - Agregar ID de usuario">
<!ENTITY icecrypt.addUidDlg.name.label "Nombre">
<!ENTITY icecrypt.addUidDlg.email.label "Correo electr\u00F3nico">
<!ENTITY icecrypt.addUidDlg.comment.label "Comentario (opcional)">
<!ENTITY icecrypt.keyTrust.title "Icecrypt - Establecer confianza en la clave">
<!ENTITY icecrypt.keyTrust.trustKey.label "Clave en la cual confiar:">
<!ENTITY icecrypt.keyTrust.trustLevel.label "\u00BFCu\u00E1nto conf\u00EDa en la clave?">
<!ENTITY icecrypt.keyTrust.dontKnow.label "No lo s\u00E9 o prefiero no decirlo">
<!ENTITY icecrypt.keyTrust.noTrust.label "NO tengo confianza">
<!ENTITY icecrypt.keyTrust.marginalTrust.label "Conf\u00EDo un poco">
<!ENTITY icecrypt.keyTrust.fullTrust.label "Conf\u00EDo totalmente">
<!ENTITY icecrypt.keyTrust.ultimateTrust.label "Conf\u00EDo absolutamente">
<!ENTITY icecrypt.keyMan.title "Administrar claves Icecrypt">
<!ENTITY icecrypt.keyMan.close.label "Cerrar ventana">
<!ENTITY icecrypt.keyMan.generate.label "Nuevo par de claves">
<!ENTITY icecrypt.keyMan.sign.label "Firmar clave">
<!ENTITY icecrypt.keyMan.setTrust.label "Establecer confianza en la clave">
<!ENTITY icecrypt.keyMan.genRevoke.label "Certificado de revocaci\u00F3n">
<!ENTITY icecrypt.keyMan.ctxGenRevoke.label "Generar y guardar certificado de revocaci\u00F3n">
<!ENTITY icecrypt.keyMan.fileMenu.label "Archivo">
<!ENTITY icecrypt.keyMan.editMenu.label "Editar">
<!ENTITY icecrypt.keyMan.viewMenu.label "Ver">
<!ENTITY icecrypt.keyMan.keyserverMenu.label "Servidor de claves">
<!ENTITY icecrypt.keyMan.generateMenu.label "Generar">
<!ENTITY icecrypt.keyMan.fileMenu.accesskey "A">
<!ENTITY icecrypt.keyMan.editMenu.accesskey "E">
<!ENTITY icecrypt.keyMan.viewMenu.accesskey "V">
<!ENTITY icecrypt.keyMan.keyserverMenu.accesskey "S">
<!ENTITY icecrypt.keyMan.generateMenu.accesskey "G">
<!ENTITY icecrypt.keyMan.importFromFile.label "Importar claves desde un archivo">
<!ENTITY icecrypt.keyMan.importFromServer.label "Buscar claves">
<!ENTITY icecrypt.keyMan.importFromClipbrd.label "Importar claves desde el portapapeles">
<!ENTITY icecrypt.keyMan.exportToFile.label "Exportar claves a un archivo">
<!ENTITY icecrypt.keyMan.sendKeys.label "Enviar claves p\u00FAblicas por correo">
<!ENTITY icecrypt.keyMan.uploadToServer.label "Subir claves p\u00FAblicas">
<!ENTITY icecrypt.keyMan.copyToClipbrd.label "Copiar claves p\u00FAblicas al portapapeles">
<!ENTITY icecrypt.keyMan.ctxExportToFile.label "Exportar claves a un archivo">
<!ENTITY icecrypt.keyMan.ctxUploadToServer.label "Subir claves p\u00FAblicas al servidor de claves">
<!ENTITY icecrypt.keyMan.ctxCopyToClipbrd.label "Copiar claves p\u00FAblicas al portapapeles">
<!ENTITY icecrypt.keyMan.refreshSelKeys.label "Actualizar las claves p\u00FAblicas seleccionadas">
<!ENTITY icecrypt.keyMan.refreshAllKeys.label "Actualizar todas las claves p\u00FAblicas">
<!ENTITY icecrypt.keyMan.ctxRefreshKey.label "Actualizar claves p\u00FAblicas desde el servidor de claves">
<!ENTITY icecrypt.keyMan.reload.label "Recargar cach\u00E9 de claves">
<!ENTITY icecrypt.keyMan.manageUid.label "Administrar IDs de usuario">
<!ENTITY icecrypt.keyMan.changePwd.label "Cambiar frase contrase\u00F1a">
<!ENTITY icecrypt.keyMan.delKey.label "Borrar clave">
<!ENTITY icecrypt.keyMan.revokeKey.label "Revocar clave">
<!ENTITY icecrypt.keyMan.keyProps.label "Propiedades de la clave">
<!ENTITY icecrypt.keyMan.showAllKeys.label "Mostrar por Defecto Todas las Claves">
<!ENTITY icecrypt.keyMan.viewPhoto.label "ID fotogr\u00E1fico">
<!ENTITY icecrypt.keyMan.ctxViewPhoto.label "Ver ID fotogr\u00E1fico">
<!ENTITY icecrypt.keyMan.viewSig.label "Firmas">
<!ENTITY icecrypt.keyMan.ctxViewSig.label "Ver firmas">
<!ENTITY icecrypt.keyMan.userId.label "Nombre">
<!ENTITY icecrypt.keyMan.keyType.label "Tipo">
<!ENTITY icecrypt.keyMan.calcTrust.label "Nivel de confianza">
<!ENTITY icecrypt.keyMan.ownerTrust.label "Confianza del propietario">
<!ENTITY icecrypt.keyMan.stopTransfer.label "Parar transferencia">
<!ENTITY icecrypt.keyMan.fingerprint.label "Huella digital">
<!ENTITY icecrypt.keyMan.selectAll.label "Seleccionar todas las claves">
<!ENTITY icecrypt.keyMan.emptyTree.tooltip "Ingrese los t\u00E9rminos de b\u00FAsqueda en la casilla encima">
<!ENTITY icecrypt.keyMan.nothingFound.tooltip "Ninguna clave cohincide con sus t\u00E9rminos de b\u00FAsqueda">
<!ENTITY icecrypt.keyMan.pleaseWait.tooltip "Por favor espere mientras las claves son cargadas ...">
<!ENTITY icecrypt.keyMan.filter.label "Filtrar por IDs de usuario o de clave que contengan:">
<!ENTITY icecrypt.keyMan.close.accesskey "C">
<!ENTITY icecrypt.keyMan.generate.accesskey "N">
<!ENTITY icecrypt.keyMan.sign.accesskey "F">
<!ENTITY icecrypt.keyMan.setTrust.accesskey "T">
<!ENTITY icecrypt.keyMan.genRevoke.accesskey "R">
<!ENTITY icecrypt.keyMan.delKey.accesskey "B">
<!ENTITY icecrypt.keyMan.revokeKey.accesskey "R">
<!ENTITY icecrypt.keyMan.importFromFile.accesskey "I">
<!ENTITY icecrypt.keyMan.exportToFile.accesskey "E">
<!ENTITY icecrypt.keyMan.importFromServer.accesskey "S">
<!ENTITY icecrypt.keyMan.uploadToServer.accesskey "U">
<!ENTITY icecrypt.keyMan.reload.accesskey "R">
<!ENTITY icecrypt.keyMan.manageUid.accesskey "M">
<!ENTITY icecrypt.keyMan.changePwd.accesskey "O">
<!ENTITY icecrypt.keyMan.viewSig.accesskey "S">
<!ENTITY icecrypt.keyMan.showPhoto.accesskey "F">
<!ENTITY icecrypt.keyMan.importFromClipbrd.accesskey "I">
<!ENTITY icecrypt.keyMan.copyToClipbrd.accesskey "C">
<!ENTITY icecrypt.keyMan.keyDetails.accesskey "P">
<!ENTITY icecrypt.keyMan.showAllKeys.accesskey "D">
<!ENTITY icecrypt.keyMan.enableKey.accesskey "V">
<!ENTITY icecrypt.keyMan.refreshSelKeys.accesskey "R">
<!ENTITY icecrypt.keyMan.refreshAllKeys.accesskey "A">
<!ENTITY icecrypt.keyMan.selectAll.accesskey "A">
<!ENTITY icecrypt.keyMan.sendKeys.accesskey "S">
<!ENTITY icecrypt.keyMan.selectAll.key "A">
<!ENTITY icecrypt.viewKeySigDlg.title "Lista de firmas">
<!ENTITY icecrypt.viewKeySigDlg.sigForKey.label "Firmas para la clave:">
<!ENTITY icecrypt.viewKeySigDlg.sigType.label "Tipo de firma">
<!ENTITY icecrypt.manageUidDlg.title "Cambiar ID de usuario primario">
<!ENTITY icecrypt.manageUidDlg.affectedKey.label "Clave a cambiar:">
<!ENTITY icecrypt.manageUidDlg.availableUid.label "IDs de usuario disponibles:">
<!ENTITY icecrypt.manageUidDlg.addUid.label "Agregar">
<!ENTITY icecrypt.manageUidDlg.deleteUid.label "Borrar">
<!ENTITY icecrypt.manageUidDlg.revokeUid.label "Revocar">
<!ENTITY icecrypt.manageUidDlg.setPrimary.label "Establecer primario">
<!ENTITY icecrypt.keyDetails.title "Propiedades de la clave">
<!ENTITY icecrypt.keyDetails.userId.label "ID de usuario primario">
<!ENTITY icecrypt.keyDetails.keyId.label "ID de clave">
<!ENTITY icecrypt.keyDetails.additionalUid.label "ID de Usuario Adicional">
<!ENTITY icecrypt.keyDetails.ID.label "ID">
<!ENTITY icecrypt.keyDetails.keyType.label "Tipo">
<!ENTITY icecrypt.keyDetails.keyPart.label "Parte de Clave">
<!ENTITY icecrypt.keyDetails.algorithm.label "Algoritmo">
<!ENTITY icecrypt.keyDetails.size.label "Tama\u00F1o">
<!ENTITY icecrypt.keyDetails.created.label "Creada">
<!ENTITY icecrypt.keyDetails.expiry.label "Expiraci\u00F3n">
<!ENTITY icecrypt.keyDetails.calcTrust.label "Nivel de confianza">
<!ENTITY icecrypt.keyDetails.ownerTrust.label "Confianza del propietario">
<!ENTITY icecrypt.keyDetails.fingerprint.label "Huella digital">
<!ENTITY icecrypt.keyDetails.selAction.label "Seleccionar acci\u00F3n ...">
<!ENTITY icecrypt.keyDetails.selAction.accesskey "S">
<!ENTITY icecrypt.cardDetails.title "Detalles de la tarjeta inteligente OpenPGP">
<!ENTITY icecrypt.cardDetails.cardMenu.label "Tarjeta inteligente">
<!ENTITY icecrypt.cardDetails.adminPin.label "Cambiar el PIN">
<!ENTITY icecrypt.cardDetails.genCardKey.label "Generar clave">
<!ENTITY icecrypt.cardDetails.vendor.label "Fabricante">
<!ENTITY icecrypt.cardDetails.serial.label "N\u00FAmero de serie">
<!ENTITY icecrypt.cardDetails.name.label "Apellido, Nombre">
<!ENTITY icecrypt.cardDetails.lang.label "Idioma">
<!ENTITY icecrypt.cardDetails.sex.label "Sexo">
<!ENTITY icecrypt.cardDetails.url.label "URL de la clave p\u00FAblica">
<!ENTITY icecrypt.cardDetails.login.label "Datos de la sesi\u00F3n">
<!ENTITY icecrypt.cardDetails.forcepin.label "Forzar PIN de firma">
<!ENTITY icecrypt.cardDetails.maxpinlen.label "Longitud m\u00E1xima del PIN">
<!ENTITY icecrypt.cardDetails.pinretry.label "Contador de reintentos del PIN">
<!ENTITY icecrypt.cardDetails.sigcount.label "Contador de la firma">
<!ENTITY icecrypt.cardDetails.sigKey.label "Clave de la firma">
<!ENTITY icecrypt.cardDetails.keyCreated.label "Creada">
<!ENTITY icecrypt.cardDetails.encKey.label "Clave de cifrado">
<!ENTITY icecrypt.cardDetails.authKey.label "Clave de autenticaci\u00F3n">
<!ENTITY icecrypt.cardDetails.yes.label "Si">
<!ENTITY icecrypt.cardDetails.no.label "No">
<!ENTITY icecrypt.cardDetails.male.label "Masculino">
<!ENTITY icecrypt.cardDetails.female.label "Femenino">
<!ENTITY icecrypt.cardDetails.editData.label "Editar los datos de la tarjeta">
<!ENTITY icecrypt.cardDetails.save.label "Guardar">
<!ENTITY icecrypt.cardDetails.reset.label "Restablecer">
<!ENTITY icecrypt.cardDetails.closeWindow.label "Cerrar">
<!ENTITY icecrypt.cardDetails.adminPin.accesskey "P">
<!ENTITY icecrypt.cardDetails.genCardKey.accesskey "G">
<!ENTITY icecrypt.cardDetails.cardMenu.accesskey "S">
<!ENTITY icecrypt.cardDetails.editData.accesskey "E">
<!ENTITY icecrypt.genCardKey.title "Generar clave OpenPGP">
<!ENTITY icecrypt.genCardKey.backupKey.label "Guardar copia de respaldo de la clave fuera de la tarjeta">
<!ENTITY icecrypt.genCardKey.desc "<a class='icecryptStrong'>NOTA: La generaci\u00F3n de las claves puede tardar varios minutos.</a> No salga del programa mientras se est\u00E1n generando. Se le avisar\u00E1 cuando las claves se hayan terminado de generar.">
<!ENTITY icecrypt.cardPin.title "Cambiar PIN de la tarjeta inteligente">
<!ENTITY icecrypt.cardPin.action.label "\u00BFQu\u00E9 desea hacer?">
<!ENTITY icecrypt.cardPin.changePin.label "Cambiar PIN">
<!ENTITY icecrypt.cardPin.changeAdmPin.label "Cambiar PIN administrativo">
<!ENTITY icecrypt.cardPin.unblockPin.label "Desbloquear PIN">
<!ENTITY icecrypt.cardPin.currAdmPin.label "PIN administrativo actual">
<!ENTITY icecrypt.cardPin.newAdminPin.label "Nuevo PIN administrativo">
<!ENTITY icecrypt.cardPin.adminPinRepeat.label "Repetir nuevo PIN">
<!ENTITY icecrypt.cardPin.currPin.label "PIN actual">
<!ENTITY icecrypt.cardPin.newPin.label "Nuevo PIN">
<!ENTITY icecrypt.cardPin.pinRepeat.label "Repetir nuevo PIN">
<!ENTITY icecrypt.changePasswd.title "Cambiar frase contrase\u00F1a OpenPGP">
<!ENTITY icecrypt.changePasswd.currPasswd.label "Frase contrase\u00F1a actual">
<!ENTITY icecrypt.changePasswd.newPasswd.label "Nueva frase contrase\u00F1a">
<!ENTITY icecrypt.changePasswd.repeatPasswd.label "Repetir frase contrase\u00F1a">
<!ENTITY icecrypt.setupWiz.title "Asistente de instalaci\u00F3n de Icecrypt">
<!ENTITY icecrypt.setupWiz.yes "Si">
<!ENTITY icecrypt.setupWiz.no "No, gracias">
<!ENTITY icecrypt.setupWiz.pgWelcome.desc "Este asistente le ayuda a empezar a usar Icecrypt directamente. En las pr\u00F3ximas pantallas le haremos algunas preguntas para tener todo configurado.<html:br /><html:br />






Para hacerlo todo sencillo, asumimos ciertos valores sobre la configuraci\u00F3n. Estos valores intentar\u00E1n proporcionar un alto nivel de seguridad para el usuario medio sin crear confusi\u00F3n. Por supuesto, puede cambiar todas estas opciones despu\u00E9s de que termine el asistente. Puede averiguar m\u00E1s acerca de las caracter\u00EDsticas de Icecrypt en el men\u00FA Ayuda o en el



">
<!ENTITY icecrypt.setupWiz.pgWelcome.startNow "\u00BFDesea usar ahora el asistente?">
<!ENTITY icecrypt.setupWiz.pgWelcome.yes "S\u00ED, deseo que me ayude el asistente">
<!ENTITY icecrypt.setupWiz.pgWelcome.no "No, gracias. Prefiero configurar las cosas manualmente">
<!ENTITY icecrypt.setupWiz.pgSelectId.title "Seleccionar identidad">
<!ENTITY icecrypt.setupWiz.pgSelectId.subtitle "Seleccione la cuenta o identidad con la que desee que funcione Icecrypt">
<!ENTITY icecrypt.setupWiz.pgSelectId.desc "Las opciones de Icecrypt son espec\u00EDficas para las cuentas e identidades. Por defecto, Icecrypt se configurar\u00E1 para funcionar en todas las cuentas e identidades. Si no es lo que desea, por favor, seleccione la cuenta espec\u00EDfica o la identidad con la que desee que funcione Icecrypt.">
<!ENTITY icecrypt.setupWiz.pgSelectId.allIds "Me gustar\u00EDa configurar Icecrypt para todas las identidades">
<!ENTITY icecrypt.setupWiz.pgSelectId.selectIds "Me gustar\u00EDa configurar Icecrypt para las siguientes identidades:">
<!ENTITY icecrypt.setupWiz.pgSelectId.note "<a class='icecryptStrong'>NOTA:</a> Icecrypt siempre comprobar\u00E1 las firmas en los correos de cada cuenta o identidad, tanto si est\u00E1 activado como si no">
<!ENTITY icecrypt.setupWiz.pgSign.title "Firma">
<!ENTITY icecrypt.setupWiz.pgSign.subtitle "Firmar digitalmente el correo saliente">
<!ENTITY icecrypt.setupWiz.pgSign.desc "Icecrypt le permite firmar sus correos electr\u00F3nicos digitalmente. Esto es como la versi\u00F3n electr\u00F3nica de firmar una carta, y permite a la gente estar segura de que un correo proviene realmente de usted. Es una buena pr\u00E1ctica de seguridad firmar todo el correo que se env\u00EDa.<html:br /><html:br />






Para verificar su correo firmado, la gente necesita un programa de correo que interprete OpenPGP. Si no tienen un programa que entienda OpenPGP, podr\u00E1n leer su correo, pero la firma se ver\u00E1 como un adjunto o como texto alrededor del mensaje. Esto puede molestar a algunas personas. Necesita elegir si desea firmar todo el correo que env\u00EDe, o si desea evitar correo firmado a algunas personas.">
<!ENTITY icecrypt.setupWiz.pgSign.signAllMsg "\u00BFDesea que por defecto se firme todo el correo saliente?">
<!ENTITY icecrypt.setupWiz.pgSign.yesSign "S\u00ED, deseo firmar todo mi correo">
<!ENTITY icecrypt.setupWiz.pgSign.noSign "No, deseo crear reglas por destinatario para los correos que necesiten estar firmados">
<!ENTITY icecrypt.setupWiz.pgSettings.title "Preferencias">
<!ENTITY icecrypt.setupWiz.pgSettings.subtitle "Cambiar la configuraci\u00F3n del correo para hacer que Icecrypt funcione mejor">
<!ENTITY icecrypt.setupWiz.pgSettings.desc "Este asistente puede cambiar la configuraci\u00F3n de su correo para asegurarse que no hay problemas al firmar y cifrar correo electr\u00F3nico en su computadora. Estos cambios son mayormente t\u00E9cnicos, que no notar\u00E1, aunque un detalle importante es que el correo se redactar\u00E1 en texto plano de manera predeterminada.">
<!ENTITY icecrypt.setupWiz.pgSettings.changePref "\u00BFDesea cambiar unas pocas preferencias predeterminadas para hacer que Icecrypt funcione en su computadora?">
<!ENTITY icecrypt.setupWiz.pgSettings.details "Detalles...">
<!ENTITY icecrypt.setupWiz.pgKeySel.title "Selecci\u00F3n de clave">
<!ENTITY icecrypt.setupWiz.pgKeySel.subtitle "Crear una clave para firmar y cifrar correo electr\u00F3nico">
<!ENTITY icecrypt.setupWiz.pgKeySel.desc "Hemos detectado que ya tiene una clave OpenPGP. Puede bien usar una de sus claves existentes para firmar, cifrar y descifrar correos, o puede crear un nuevo par de claves.">
<!ENTITY icecrypt.setupWiz.pgKeySel.newKey "Deseo crear un nuevo par de claves para firmar y cifrar mi correo electr\u00F3nico">
<!ENTITY icecrypt.setupWiz.pgKeySel.useExistingKey "Deseo seleccionar una de las claves de abajo para firmar y cifrar mi correo:">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.title "Crear clave">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.subtitle "Crear una clave para firmar y cifrar correo electr\u00F3nico">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.desc1 "Necesita tener un 'par de claves' para firmar y cifrar correo, o para leer correos cifrados. Un par de claves tiene dos claves, una p\u00FAblica y una privada.">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.desc2 "Necesita proporcionar su clave p\u00FAblica a todos los de su lista de contactos que quieran verificar su firma, o para enviarle correo cifrado. Mientras tanto, necesita manter su clave privada en secreto. No debe proporcionarla, o dejarla desprotegida. Puede leer todo el correo que la gente cifre y le env\u00EDe. Tambi\u00E9n puede cifrar correo en su nombre. Debido a que es secreto, est\u00E1 protegido con una frase contrase\u00F1a.">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.passRepeat "Por favor, confirme su frase contrase\u00F1a escribi\u00E9ndola de nuevo">
<!ENTITY icecrypt.setupWiz.pgSummary.title "Resumen">
<!ENTITY icecrypt.setupWiz.pgSummary.subtitle "Confirme que el asistente guardar\u00E1 ahora estos cambios">
<!ENTITY icecrypt.setupWiz.pgSummary.desc "\u00A1Ya casi ha terminado! Si pulsa el bot\u00F3n 'Siguiente', el asistente realizar\u00E1 las siguientes acciones:">
<!ENTITY icecrypt.setupWiz.pgKeygen.title "Creaci\u00F3n de clave">
<!ENTITY icecrypt.setupWiz.pgKeygen.subtitle "Su clave se est\u00E1 generando ahora">
<!ENTITY icecrypt.setupWiz.pgComplete.title "Gracias">
<!ENTITY icecrypt.setupWiz.pgComplete.desc "Icecrypt ahora est\u00E1 listo para su uso.<html:br /><html:br />Gracias por usar Icecrypt.">
<!ENTITY icecrypt.setupWiz.details.title "Preferencias">
<!ENTITY icecrypt.setupWiz.details.subtitle "Modificar configuraci\u00F3n del programa">
<!ENTITY icecrypt.setupWiz.details.imapOnDemand "Desactivar la carga de partes IMAP bajo demanda">
<!ENTITY icecrypt.setupWiz.details.changingPrefs "Este asistente modifica las siguientes preferencias:">
<!ENTITY icecrypt.setupWiz.details.noFlowedMsg "Desactivar texto flu\u00EDdo (RFC 2646)">
<!ENTITY icecrypt.setupWiz.details.viewAsPlain "Ver cuerpo del mensaje como texto plano">
<!ENTITY icecrypt.setupWiz.details.8bitEncoding "Usar codificaci\u00F3n de 8 bits para enviar mensajes">
<!ENTITY icecrypt.setupWiz.details.noComposeHTML "No redactar mensajes en HTML">
<!ENTITY icecrypt.setupWiz.pgNoStart.title "Cancelar el asistente">
<!ENTITY icecrypt.setupWiz.pgNoStart.desc "Ha escogido no usar el asistente para configurar Icecrypt.<html:br /><html:br />Gracias por usar Icecrypt.">
<!ENTITY icecrypt.advancedIdentityDlg.title "Configuraci\u00F3n Avanzada de Identidades Icecrypt">
<!ENTITY icecrypt.amPrefPgp.sendKeyWithMsg.label "Adjuntar mi clave p\u00FAblica a los mensajes">
