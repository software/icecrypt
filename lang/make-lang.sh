#!/bin/sh

# make-lang.sh v1.0.10
# this script is used to create a language-specifi XPI for Icecrypt

# if you want to debug this script, set DEBUG to something >0
DEBUG=0

if [ $# -ne 2 ]; then
  echo "Usage: $0 xx-YY version"
  echo "       where: xx-YY   is the language and country code representing the"
  echo "                      translated language"
  echo "              version is the Icecrypt version, e.g. 0.84.1"
  exit 1
fi

ENIGLANG=$1
export ENIGLANG

ENIGVERSION=$2
export ENIGVERSION

LANGDIR=${ENIGLANG}/chrome/locale/${ENIGLANG}/icecrypt
HELPDIR=${LANGDIR}/help
cwd=`pwd`
rm -rf ${LANGDIR} >/dev/null 2>&1
mkdir -p ${LANGDIR}
mkdir -p ${HELPDIR}

LANGHASH=`echo "${ENIGLANG}" | md5sum | awk '{ print substr($0,1,2)}'`
export LANGHASH

# create chrome.manifest for Thunderbird 3.1 and newer
cat > ${ENIGLANG}/chrome.manifest <<EOT
locale      icecrypt    ${ENIGLANG}       jar:chrome/icecrypt-${ENIGLANG}.jar!/locale/${ENIGLANG}/icecrypt/
EOT

# create install.rdf for Thunderbird 1.0 and newer
cat > ${ENIGLANG}/install.rdf <<EOT
<?xml version="1.0"?>

<RDF xmlns="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
     xmlns:em="http://www.mozilla.org/2004/em-rdf#">

  <Description about="urn:mozilla:install-manifest">
    <em:id>icecrypt-${ENIGLANG}@hyperbola.info</em:id>
    <em:version>${ENIGVERSION}</em:version>

    <!-- Target Applications this extension can install into,
        with minimum and maximum supported versions. -->

    <em:targetApplication>
      <!-- Icedove-UXP -->
      <Description>
        <em:id>{3aa07e56-beb0-47a0-b0cb-c735edd25419}</em:id>
        <em:minVersion>52.0</em:minVersion>
        <em:maxVersion>52.*</em:maxVersion>
      </Description>
    </em:targetApplication>

    <em:targetApplication>
      <!-- Iceape-UXP -->
      <Description>
        <em:id>{9184b6fe-4a5c-484d-8b4b-efbfccbfb514}</em:id>
        <em:minVersion>52.0</em:minVersion>
        <em:maxVersion>52.*</em:maxVersion>
      </Description>
    </em:targetApplication>

    <!-- Front End MetaData -->
    <em:name>Icecrypt ${ENIGLANG}</em:name>
    <em:description>Icecrypt ${ENIGLANG} language package</em:description>

    <!-- Author of the package, replace with your name if you like -->
    <em:creator>Hyperbola Project</em:creator>

    <em:homepageURL>https://hyperbola.info</em:homepageURL>

    <!-- Front End Integration Hooks (used by Extension Manager)-->
    <em:optionsURL>chrome://icecrypt/content/pref-icecrypt.xul</em:optionsURL>
    <em:aboutURL>chrome://icecrypt/content/icecryptAbout.xul</em:aboutURL>
    <em:iconURL>chrome://icecrypt/skin/icecrypt-about.png</em:iconURL>

  </Description>
</RDF>
EOT

for f in icecrypt.dtd icecrypt.properties am-enigprefs.properties upgrade_080.html ; do
  cp ${f} ${LANGDIR}
done

if [ -d help ]; then
  cd help
fi
pwd

for f in compose.html editRcptRule.html initError.html messenger.html rulesEditor.html sendingPrefs ; do
  cp ${f} ${cwd}/${HELPDIR}
done

cd ${cwd}/${ENIGLANG}/chrome
zip -r -D icecrypt-${ENIGLANG}.jar locale
cd ..
zip ../icecrypt-${ENIGLANG}-${ENIGVERSION}.xpi install.rdf chrome.manifest chrome/icecrypt-${ENIGLANG}.jar
cd ..

test $DEBUG -eq 0 && rm -rf ${ENIGLANG}
