<!ENTITY icecrypt.label "Icecrypt">
<!ENTITY icecrypt.keyUserId.label "账户 / 个人标识">
<!ENTITY icecrypt.keygenTitle.label "请指定您的备份数据所在的文件。">
<!ENTITY icecrypt.useForSigning.label "将生成的密钥用于选中的身份">
<!ENTITY icecrypt.keyNoPassphrase.label "没有口令">
<!ENTITY icecrypt.keyPassphrase.label "口令">
<!ENTITY icecrypt.keyPassphraseRepeat.label "口令（重复）">
<!ENTITY icecrypt.generateKey.tooltip "创建一个新的可用于加密和/或签名的 OpenPGP 兼容密钥">
<!ENTITY icecrypt.cancelKey.label "取消">
<!ENTITY icecrypt.cancelKey.tooltip "取消密钥生成">
<!ENTITY icecrypt.keyGen.expiry.title "密钥有效期">
<!ENTITY icecrypt.keyGen.expire.label "密钥有效期为">
<!ENTITY icecrypt.keyGen.days.label "天">
<!ENTITY icecrypt.keyGen.months.label "月">
<!ENTITY icecrypt.keyGen.years.label "年">
<!ENTITY icecrypt.keyGen.noExpiry.label "密钥永不过期">
<!ENTITY icecrypt.keyGen.keySize.label "密钥长度">
<!ENTITY icecrypt.keyGen.console.label "密钥生成终端">
<!ENTITY icecrypt.keyGen.keyType.label "密钥类型">
<!ENTITY icecrypt.keyGen.keyType.dsa "DSA &amp; El Gamal">
<!ENTITY icecrypt.keyGen.keyType.rsa "RSA">
<!ENTITY icecrypt.preferences.label "Icecrypt 选项">
<!ENTITY icecrypt.passwordSettings.label "口令设置">
<!ENTITY icecrypt.userNoPassphrase.label "不再询问任何口令">
<!ENTITY icecrypt.userNoPassphrase.tooltip "如果您想用口令保护您的 OpenPGP 密钥就选择">
<!ENTITY icecrypt.expertUserMenus.label "显示专家设置和菜单">
<!ENTITY icecrypt.expertUser.tooltip "如果您想看到面向高级用户的菜单和配置就选此项。">
<!ENTITY icecrypt.basicUserMenus.label "隐藏专家设置和菜单">
<!ENTITY icecrypt.basicUser.tooltip "如果您不想看到面向高级用户的菜单和选项就选此项。">
<!ENTITY icecrypt.prefs.pathsTitle.label "文件和目录">
<!ENTITY icecrypt.prefs.overrideGpg.label "优先使用">
<!ENTITY icecrypt.prefs.overrideGpg.tooltip "点击指定 GnuPG 的路径">
<!ENTITY icecrypt.agentAdditionalParam.label "附加到 GnuPG 的参数">
<!ENTITY icecrypt.agentAdditionalParam.tooltip "指定 GnuPG 所用的选项。">
<!ENTITY icecrypt.mime_parts_on_demand.label "仅在打开时下载附件 (仅 IMAP)">
<!ENTITY icecrypt.maxIdleMinutesHead.label "在空闲">
<!ENTITY icecrypt.maxIdleMinutesTail.label "分钟后清除缓存的口令">
<!ENTITY icecrypt.maxIdleMinutes.tooltip "在内存中缓存口令一段时间。">
<!ENTITY icecrypt.resetPrefsButton.label "重置">
<!ENTITY icecrypt.resetPrefs.tooltip "恢复所有 Icecrypt 设置到默认值">
<!ENTITY icecrypt.backupPrefsButton.label "导出设置和密钥">
<!ENTITY icecrypt.backupPrefs.tooltip "保存 Icecrypt 的所有首选项到文件，包括您的 OpenPGP 密钥。">
<!ENTITY icecrypt.restorePrefsButton.label "恢复设置和密钥">
<!ENTITY icecrypt.restorePrefs.tooltip "从文件恢复 Icecrypt 的所有首选项，包括您的 OpenPGP 密钥。">
<!ENTITY icecrypt.backupRestore.desc "导出（备份）和恢复您的 Icecrypt 设置，包括您的 OpenPGP 密钥。">
<!ENTITY icecrypt.basic.label "基本">
<!ENTITY icecrypt.sending.label "发送">
<!ENTITY icecrypt.keySel.label "选择密钥">
<!ENTITY icecrypt.pgpMime.label "PGP/MIME">
<!ENTITY icecrypt.advanced.label "高级">
<!ENTITY icecrypt.backupRestore.label "备份/恢复">
<!ENTITY icecrypt.basicPrefs.label "基本设置">
<!ENTITY icecrypt.moreOptions.label "更多选项">
<!ENTITY icecrypt.useNonDefaultComment.label "在 OpenPGP 签名中增加 Icecrypt 注释">
<!ENTITY icecrypt.keyserver.label "密钥服务器">
<!ENTITY icecrypt.keyserverDlg.label "选择密钥服务器">
<!ENTITY icecrypt.keyservers.label "指定您的密钥服务器">
<!ENTITY icecrypt.keyservers.sample "示例: sks.dnsalias.net, pgp.mit.edu, ldap://certserver.pgp.com">
<!ENTITY icecrypt.searchForKey.label "搜索密钥">
<!ENTITY icecrypt.doubleDashSeparator.label "将'--'看作邮件正文和签名的分隔符">
<!ENTITY icecrypt.useGpgAgent.label "使用 gpg-agent 处理口令">
<!ENTITY icecrypt.noHushMailSupport.label "使用 '&lt;' 和 '&gt;' 来指定电子邮件地址">
<!ENTITY icecrypt.doubleDashSeparator.tooltip "同一行中两个连续的破折号标志签名区域">
<!ENTITY icecrypt.useGpgAgent.tooltip "所有口令输入都使用 gpg-agent (GnuPG 2 的一部分)">
<!ENTITY icecrypt.noHushMailSupport.tooltip "使用 '&lt;' 和 '&gt;' 字符来指定面向 GnuPG 的电子邮件地址。如果收件人有旧的 Hushmail 密钥就禁用。">
<!ENTITY icecrypt.mime_parts_on_demand.tooltip "如果您通过 IMAP 文件夹接收 PGP/MIME 邮件，您应该关闭此选项。">
<!ENTITY icecrypt.disableSMIMEui.tooltip "移除撰写邮件窗口的工具栏上的 S/MIME 按钮">
<!ENTITY icecrypt.autoKeyServerSelection.label "总是使用首个密钥服务器">
<!ENTITY icecrypt.autoKeyServerSelection.tooltip "默认使用列表上第一个密钥服务器。">
<!ENTITY icecrypt.decryptbutton.label "解密">
<!ENTITY icecrypt.decryptbutton.tip "使用 Icecrypt 解密或验证邮件">
<!ENTITY icecrypt.messengermenu.accesskey "a">
<!ENTITY icecrypt.decryptverify.label "解密/验证">
<!ENTITY icecrypt.importpublickey.label "导入公钥">
<!ENTITY icecrypt.importKey.label "导入密钥">
<!ENTITY icecrypt.autoDecrypt.label "自动解密/验证邮件">
<!ENTITY icecrypt.clearPassphrase.label "清除保存的口令">
<!ENTITY icecrypt.editRules.label "编辑收件人规则">
<!ENTITY icecrypt.decryptToFolder.label "解密到文件夹">
<!ENTITY icecrypt.decryptToFolder.accesskey "D">
<!-- "recent" label for Folder selection submenu -->
<!ENTITY icecrypt.recent.label "最近">
<!ENTITY icecrypt.recent.accesskey "R">
<!ENTITY icecrypt.prefs.label "选项">
<!ENTITY icecrypt.debugMenu.label "调试 OpenPGP">
<!ENTITY icecrypt.viewconsole.label "查看终端">
<!ENTITY icecrypt.viewlog.label "查看日志">
<!ENTITY icecrypt.generateKey.label "生成密钥">
<!ENTITY icecrypt.help.label "帮助">
<!ENTITY icecrypt.about.title "关于 Icecrypt">
<!ENTITY icecrypt.about.label "关于 Icecrypt">
<!ENTITY icecrypt.reload.label "重新载入邮件">
<!ENTITY icecrypt.browse.label "浏览...">
<!ENTITY icecrypt.sendersKeyMenu.label "发件人密钥">
<!ENTITY icecrypt.adminSmartCard.label "管理智能卡...">
<!ENTITY icecrypt.setupWiz.label "安装向导">
<!ENTITY icecrypt.decryptverify.accesskey "D">
<!ENTITY icecrypt.importpublickey.accesskey "I">
<!ENTITY icecrypt.reload.accesskey "E">
<!ENTITY icecrypt.adminSmartCard.accesskey "M">
<!ENTITY icecrypt.autoDecrypt.accesskey "A">
<!ENTITY icecrypt.debugMenu.accesskey "G">
<!ENTITY icecrypt.clearPassphrase.accesskey "l">
<!ENTITY icecrypt.editRules.accesskey "R">
<!ENTITY icecrypt.setupWiz.accesskey "S">
<!ENTITY icecrypt.prefs.accesskey "P">
<!ENTITY icecrypt.viewconsole.accesskey "c">
<!ENTITY icecrypt.viewlog.accesskey "g">
<!ENTITY icecrypt.help.accesskey "H">
<!ENTITY icecrypt.about.accesskey "b">
<!ENTITY icecrypt.sendersKeyMenu.accesskey "K">
<!ENTITY icecrypt.composetoolbar.label "Icecrypt 工具栏">
<!ENTITY icecrypt.composetoolbar.desc "Icecrypt 加密信息">
<!ENTITY icecrypt.signedsend.label "签名邮件">
<!ENTITY icecrypt.encryptedsend.label "加密邮件">
<!ENTITY icecrypt.pgpAccountSettings.label "签名/加密选项...">
<!ENTITY icecrypt.sendOptionsMenu.label "发送选项...">
<!ENTITY icecrypt.defaultKeySelOpts.label "密钥选择选项...">
<!ENTITY icecrypt.usePgpMime.label "协议：PGP/MIME">
<!ENTITY icecrypt.useInline.label "协议：内嵌 PGP">
<!ENTITY icecrypt.tempTrustAllKeys.label "信任所有收件人的密钥">
<!ENTITY icecrypt.undoencryption.label "取消加密">
<!ENTITY icecrypt.attachkey.label "附送公钥...">
<!ENTITY icecrypt.attachmykey.label "附送我的公钥">
<!ENTITY icecrypt.protectHeaders.label "保护主题">
<!ENTITY icecrypt.composemenu.accesskey "n">
<!ENTITY icecrypt.signedsend.accesskey "S">
<!ENTITY icecrypt.signedsend.key "S">
<!ENTITY icecrypt.encryptedsend.accesskey "E">
<!ENTITY icecrypt.encryptedsend.key "P">
<!ENTITY icecrypt.sendOptionsMenu.accesskey "S">
<!ENTITY icecrypt.defaultKeySelOpts.accesskey "K">
<!ENTITY icecrypt.pgpAccountSettings.accesskey "E">
<!ENTITY icecrypt.usePgpMime.accesskey "G">
<!ENTITY icecrypt.useInline.accesskey "I">
<!ENTITY icecrypt.tempTrustAllKeys.accesskey "T">
<!ENTITY icecrypt.undoencryption.accesskey "U">
<!ENTITY icecrypt.attachkey.accesskey "A">
<!ENTITY icecrypt.attachmykey.accesskey "M">
<!-- account specific options -->
<!ENTITY icecrypt.defaultSigning.label "默认签名邮件">
<!ENTITY icecrypt.defaultEncryption.label "默认加密邮件">
<!ENTITY icecrypt.usePGPMimeAlways.label "总是使用 PGP/MIME">
<!ENTITY icecrypt.afterDefaultsAndRules.label "在默认值和规则应用后：">
<!ENTITY icecrypt.finallySignEncrypted.label "签名已加密的邮件">
<!ENTITY icecrypt.finallySignNotEncrypted.label "签名未加密的邮件">
<!ENTITY icecrypt.autoEncryptDrafts.label "保存时加密邮件草稿">
<!ENTITY icecrypt.advancedPrefsButton.label "高级...">
<!ENTITY icecrypt.openpgpPrefsButton.label "Icecrypt 选项...">
<!-- Sending Preferences -->
<!ENTITY icecrypt.SendingPrefs.label "常规发送选项">
<!ENTITY icecrypt.seeAccountSettings.label "另见具体的 Icecrypt 设置（账户设置 -> OpenPGP 安全）">
<!ENTITY icecrypt.encryptionModelConvenient.label "方便的加密设置">
<!ENTITY icecrypt.encryptionModelManually.label "手动加密设置">
<!ENTITY icecrypt.keepCryptoSettingsForReply.label "回复已加密/已签名的邮件时加密/签名">
<!ENTITY icecrypt.keepCryptoSettingsForReply.tooltip "如果可能，回复已加密/已签名的邮件时进行加密/签名">
<!ENTITY icecrypt.autoSendEncryptedOption.label "自动发送加密的">
<!ENTITY icecrypt.autoSendEncryptedNever.label "永不">
<!ENTITY icecrypt.autoSendEncryptedNever.tooltip "不要自动发送加密的，除非触发了收件人规则">
<!ENTITY icecrypt.autoSendEncryptedIfKeys.label "如果可能">
<!ENTITY icecrypt.autoSendEncryptedIfKeys.tooltip "自动发送已加密的，如果每个收件人都有至少一个已知的并可接受的（见上）并且没有收件人规则要求强制不加密。">
<!ENTITY icecrypt.acceptedKeysOption.label "要发送加密的，接受">
<!ENTITY icecrypt.acceptedKeysTrusted.label "仅信任的密钥">
<!ENTITY icecrypt.acceptedKeysTrusted.tooltip "只接受由您或者您信任的人签发的密钥。技术上讲，您只接受“有效的”密钥进行的加密。">
<!ENTITY icecrypt.acceptedKeysAllUsable.label "所有不可用的密钥">
<!ENTITY icecrypt.acceptedKeysAllUsable.tooltip "接受所有并未被您或者签发者废除/停用/过期的密钥。技术上讲，这等于使用 GPG 的 '--trust-model always' 选项。">
<!ENTITY icecrypt.confirmBeforeSendingOption.label "发送前确认">
<!ENTITY icecrypt.confirmBeforeSendingNever.label "永不">
<!ENTITY icecrypt.confirmBeforeSendingNever.tooltip "不要在发送邮件前显示有关签名/加密的信息对话框">
<!ENTITY icecrypt.confirmBeforeSendingAlways.label "总是">
<!ENTITY icecrypt.confirmBeforeSendingAlways.tooltip "总是在发送邮件前显示有关签名/加密的信息对话框">
<!ENTITY icecrypt.confirmBeforeSendingIfEncrypted.label "如果已加密">
<!ENTITY icecrypt.confirmBeforeSendingIfEncrypted.tooltip "在发送加密邮件前显示有关签名/加密的信息对话框">
<!ENTITY icecrypt.confirmBeforeSendingIfNotEncrypted.label "如果未加密">
<!ENTITY icecrypt.confirmBeforeSendingIfNotEncrypted.tooltip "在发送未加密邮件前显示有关签名/加密的信息对话框">
<!ENTITY icecrypt.confirmBeforeSendingIfRules.label "如果默认加密设置的规则被更改">
<!ENTITY icecrypt.confirmBeforeSendingIfRules.tooltip "如果您的有关加密的选项被一个收件人规则更改或者为自动加密，在发送前显示有关签名/加密的对话框。">
<!-- keySel Preferences -->
<!ENTITY icecrypt.recipientsSelectionOption.label "发送邮件时如何确定收件人密钥">
<!ENTITY icecrypt.assignKeysByRules.label "依照收件人规则">
<!ENTITY icecrypt.assignKeysByRules.tooltip "根据收件人规则分配密钥。强制提醒创建新规则，如果这是唯一选中的选项。">
<!ENTITY icecrypt.assignKeysByEmailAddr.label "依照密钥管理器中的电子邮件地址">
<!ENTITY icecrypt.assignKeysByEmailAddr.tooltip "根据密钥管理器中的电子邮件地址分配密钥。">
<!ENTITY icecrypt.assignKeysManuallyIfMissing.label "手动，如果密钥缺失">
<!ENTITY icecrypt.assignKeysManuallyIfMissing.tooltip "如果密钥缺失，打开对话框以手动分配密钥。">
<!ENTITY icecrypt.assignKeysManuallyAlways.label "总是（也）手动">
<!ENTITY icecrypt.assignKeysManuallyAlways.tooltip "总是最终打开手动分配密钥的对话框。">
<!ENTITY icecrypt.defineRules.label "定义规则...">
<!ENTITY icecrypt.wrapHtmlBeforeSend.label "发送前换行已数字签名的 HTML 文本">
<!ENTITY icecrypt.wrapHtmlBeforeSend.tooltip "重排文本以便保证签名能完整保留.">
<!ENTITY icecrypt.autoKeyRetrieve.label "自动从服务器下载密钥">
<!ENTITY icecrypt.autoKeyRetrieve2.label "从以下密钥服务器:">
<!ENTITY icecrypt.resetRememberedValues.label "重置警告">
<!ENTITY icecrypt.resetRememberedValues.tooltip "重新显示您已隐藏过的所有警告对话框和问题。">
<!ENTITY icecrypt.keygen.desc "<a class='icecryptStrong'>注意：密钥生成过程可能会持续数分钟。</a>生成密钥的过程中请不要退出本程序。在此期间浏览网页或者执行能产生大量磁盘操作的程序将帮助填充“随机数池”并能加速密钥创建进程。密钥创建结束时您将收到完成通知。">
<!ENTITY icecrypt.userSelectionList.label "Icecrypt 密钥选择">
<!ENTITY icecrypt.usersNotFound.label "收件人无效、不被信任或者找不到">
<!ENTITY icecrypt.keyExpiry.label "有效期至">
<!ENTITY icecrypt.uidValidity.label "验证">
<!ENTITY icecrypt.keySelection.label "密钥选择">
<!ENTITY icecrypt.keyId.label "密钥标识">
<!ENTITY icecrypt.userSelSendSigned.label "发送已签名的">
<!ENTITY icecrypt.userSelSendSigned.accesskey "S">
<!ENTITY icecrypt.userSelSendEncrypted.label "发送已加密的">
<!ENTITY icecrypt.userSelSendEncrypted.accesskey "E">
<!ENTITY icecrypt.displayNoLonger.label "如果无法加密则不显示此对话框">
<!ENTITY icecrypt.importMissingKeys.label "下载缺少的密钥">
<!ENTITY icecrypt.importMissingKeys.accesskey "D">
<!ENTITY icecrypt.importMissingKeys.tooltip "尝试从一个密钥服务器导入缺少的密钥">
<!ENTITY icecrypt.refreshKeys.label "刷新密钥列表">
<!ENTITY icecrypt.refreshKeys.accesskey "R">
<!ENTITY icecrypt.send.label "发送">
<!ENTITY icecrypt.perRecipientsOption.label "创建收件人规则">
<!ENTITY icecrypt.icecryptAttachDesc.label "这则邮件包含附件。您希望怎样对其进行加密/数字签名？">
<!ENTITY icecrypt.enigEncryptAttachNone.label "仅加密/数字签名邮件，不处理附件">
<!ENTITY icecrypt.enigAttachNoneSign.label "签名邮件文本，但不签名附件">
<!ENTITY icecrypt.enigAttachNoneEncrypt.label "加密邮件文本，但不加密附件">
<!ENTITY icecrypt.enigAttachNoneEncryptAndSign.label "加密并签名邮件文本，但不加密附件">
<!ENTITY icecrypt.enigEncryptAttachInline.label "分别加密每个附件并对邮件使用内嵌式 PGP">
<!ENTITY icecrypt.enigAttachInlineSign.label "分别签名每个附件并使用内嵌 PGP 方式发送该邮件">
<!ENTITY icecrypt.enigAttachInlineEncrypt.label "分别加密每个附件并使用内嵌 PGP 方式发送该邮件">
<!ENTITY icecrypt.enigAttachInlineEncryptAndSign.label "分别加密并签名每个附件并使用内嵌 PGP 方式发送该邮件">
<!ENTITY icecrypt.enigEncryptAttachPgpMime.label "将邮件做为一个整体进行加密/数字签名，并使用 PGP/MIME 发送">
<!ENTITY icecrypt.enigAttachPgpMimeSign.label "整体签名邮件并使用 PGP/MIME 方式发送它">
<!ENTITY icecrypt.enigAttachPgpMimeEncrypt.label "整体加密邮件并使用 PGP/MIME 方式发送它">
<!ENTITY icecrypt.enigAttachPgpMimeEncryptAndSign.label "整体加密并签名邮件并使用 PGP/MIME 方式发送它">
<!ENTITY icecrypt.enigEncryptAttachDontEncryptMsg.label "全部不要加密/签名邮件">
<!ENTITY icecrypt.enigAttachDontEncryptMsgSign.label "全部不要签名邮件">
<!ENTITY icecrypt.enigAttachDontEncryptMsgEncrypt.label "全部不要加密邮件">
<!ENTITY icecrypt.enigAttachDontEncryptMsgEncryptAndSign.label "全部不要加密并签名邮件">
<!ENTITY icecrypt.encryptAttachSkipDlg.label "使用所选择的模式用于所有将来的附件">
<!ENTITY icecrypt.ctxDecryptOpen.label "解密并打开">
<!ENTITY icecrypt.ctxDecryptSave.label "解密并另存为...">
<!ENTITY icecrypt.ctxImportKey.label "导入 OpenPGP 密钥">
<!ENTITY icecrypt.ctxVerifyAtt.label "Verify signature">
<!ENTITY icecrypt.ctxDecryptOpen.accesskey "D">
<!ENTITY icecrypt.ctxDecryptSave.accesskey "C">
<!ENTITY icecrypt.ctxImportKey.accesskey "I">
<!ENTITY icecrypt.ctxVerifyAtt.accesskey "V">
<!ENTITY icecrypt.detailsHdrButton.label "详细信息">
<!ENTITY icecrypt.revealAttachmentsButton.label "已附加文件的名称已隐藏。按“显示”按钮显示原文件名。">
<!ENTITY icecrypt.revealAttachments.button "显示">
<!ENTITY icecrypt.exchangeGarbage.desc "这是一个坏掉的来自 MS-Exchange 的 PGP/MIME 消息。如果您无法访问该附件，或者回复或转发时出现问题，请按“修复消息”按钮修复该消息。">
<!ENTITY icecrypt.exchangeGarbage.fixButton.label "修复信息">
<!ENTITY icecrypt.exchangeGarbage.waitMessage "请稍候...">
<!ENTITY icecrypt.amPrefTitle.label "OpenPGP 选项">
<!ENTITY icecrypt.amPrefDesc.label "Icecrypt 为您提供 OpenPGP 加密和数字签名邮件支持。要使用该功能，您必须安装了 GnuPG (gpg) 程序。">
<!ENTITY icecrypt.amPrefEnablePgp.label "为这个标识打开 OpenPGP 支持 (Icecrypt)">
<!ENTITY icecrypt.amPrefUseFromAddr.label "使用这个标识的电子邮件地址标示 OpenPGP 密钥">
<!ENTITY icecrypt.amPrefUseKeyId.label "使用指定的 OpenPGP 密钥标识 (0x1234ABCD):">
<!ENTITY icecrypt.amPrefSelectKey.label "选择密钥...">
<!ENTITY icecrypt.amPrefDefaultEncrypt.label "邮件撰写默认选项">
<!ENTITY icecrypt.encryptionDlg.label "Icecrypt 加密和数字签名设置">
<!ENTITY icecrypt.encDlgEncrypt.label "加密邮件">
<!ENTITY icecrypt.encDlgEncrypt.accesskey "E">
<!ENTITY icecrypt.encDlgSign.label "数字签名邮件">
<!ENTITY icecrypt.encDlgSign.accesskey "S">
<!ENTITY icecrypt.encDlgPgpMime.label "使用 PGP/MIME">
<!ENTITY icecrypt.encDlgPgpMime.accesskey "U">
<!ENTITY icecrypt.encDlgInlinePgp.label "使用内嵌 PGP">
<!ENTITY icecrypt.encDlgInlinePgp.accesskey "I">
<!ENTITY icecrypt.encDlgReset.label "重置为默认值">
<!ENTITY icecrypt.encDlgReset.accesskey "R">
<!ENTITY icecrypt.amPrefPgpHeader.label "发送 'OpenPGP' 信头">
<!ENTITY icecrypt.amPrefPgpHeader.id.label "发送 OpenPGP 密钥标识">
<!ENTITY icecrypt.amPrefPgpHeader.url.label "发送获取密钥的 URL:">
<!ENTITY icecrypt.progressText.label "进度:">
<!ENTITY icecrypt.openPgpSecurity.label "OpenPGP 安全">
<!ENTITY icecrypt.pgpSecurityInfo.label "Icecrypt 安全信息...">
<!ENTITY icecrypt.copySecurityInfo.label "复制 Icecrypt 安全信息">
<!ENTITY icecrypt.showPhoto.label "查看 OpenPGP 内置相片">
<!ENTITY icecrypt.photoViewer.title "OpenPGP 照片查看器">
<!ENTITY icecrypt.displayKeyProperties.label "查看密钥属性">
<!ENTITY icecrypt.signSenderKey.label "签署密钥...">
<!ENTITY icecrypt.trustSenderKey.label "设置密钥信任级别...">
<!ENTITY icecrypt.signSenderKeyPopup.label "为发件人的签署密钥...">
<!ENTITY icecrypt.trustSenderKeyPopup.label "设置发件人的密钥信任级别...">
<!ENTITY icecrypt.createRuleFromAddr.label "从地址创建 Icecrypt 规则...">
<!ENTITY icecrypt.keyManWindow.label "密钥管理">
<!ENTITY icecrypt.keyManWindow.accesskey "y">
<!ENTITY icecrypt.singleRcptSettings.label "Icecrypt - 密钥规则设置">
<!ENTITY icecrypt.compose.excesslength.label "您的邮件中有太长的行。这应该被处理以避免不良的签名。">
<!ENTITY icecrypt.compose.excesslength_action.label "您想如何处理？">
<!ENTITY icecrypt.wrapSelectionList.label "选择折行">
<!ENTITY icecrypt.compose.wrap_radio.label "自动折行（良好的签名，但可能看上去不美观）">
<!ENTITY icecrypt.compose.sendasis_radio.label "就这样发送（存在签名异常的风险）">
<!ENTITY icecrypt.compose.use_mime_radio.label "使用 PGP/MIME（良好的签名，但有些收件人无法解码它）">
<!ENTITY icecrypt.compose.editmanually.label "手动编辑">
<!ENTITY icecrypt.selKeysButton.label "选择密钥...">
<!ENTITY icecrypt.action.label "动作">
<!ENTITY icecrypt.nextRule.label "如果邮件地址符合条件则测试下一条规则">
<!ENTITY icecrypt.nextAddress.label "如果邮件地址符合条件则不再测试后续规则">
<!ENTITY icecrypt.useKeys.label "使用下列 OpenPGP 密钥进行加密或数字签名:">
<!ENTITY icecrypt.selKeysButton.accesskey "S">
<!ENTITY icecrypt.setDefaultsFor.label "加密或数字签名默认设置...">
<!ENTITY icecrypt.encryption.label "加密">
<!ENTITY icecrypt.signing.label "数字签名">
<!ENTITY icecrypt.never.label "从不">
<!ENTITY icecrypt.always.label "总是">
<!ENTITY icecrypt.maybe.label "在撰写邮件时选择">
<!ENTITY icecrypt.singleRcptSettings.desc "(注意: 在发生冲突的情况下，“从不”将取代“总是”设置)">
<!ENTITY icecrypt.ruleEmail.label "为如下收件人">
<!ENTITY icecrypt.ruleEmail.tooltip "只能使用邮件地址，不要填写收件人姓名 例如 '张三 <zhang.san@address.net>' 是无效的">
<!ENTITY icecrypt.sepratationDesc.label "设置 OpenPGP 规则(请用空格分隔各邮件地址)">
<!ENTITY icecrypt.matchDescStart.label "如果收件人地址">
<!ENTITY icecrypt.matchDescEnd.label "则应用规则">
<!ENTITY icecrypt.matchExact.label "与上述某邮件地址完全相同">
<!ENTITY icecrypt.matchContains.label "包含上述邮件地址中的字符">
<!ENTITY icecrypt.matchBegin.label "以上述某邮件地址开头">
<!ENTITY icecrypt.matchEnd.label "以上述某邮件地址结尾">
<!ENTITY icecrypt.not.label "不在上述邮件地址中">
<!ENTITY icecrypt.rulesEditor.label "Icecrypt - 收件人规则编辑器">
<!ENTITY icecrypt.email.label "电子邮件">
<!ENTITY icecrypt.pgpKeys.label "OpenPGP 密钥">
<!ENTITY icecrypt.sign.label "数字签名">
<!ENTITY icecrypt.encrypt.label "加密">
<!ENTITY icecrypt.edit.label "修改">
<!ENTITY icecrypt.edit.accesskey "M">
<!ENTITY icecrypt.add.label "增加">
<!ENTITY icecrypt.add.accesskey "A">
<!ENTITY icecrypt.delete.label "删除">
<!ENTITY icecrypt.delete.accesskey "E">
<!ENTITY icecrypt.moveUp.label "上移">
<!ENTITY icecrypt.moveUp.accesskey "U">
<!ENTITY icecrypt.moveDown.label "下移">
<!ENTITY icecrypt.moveDown.accesskey "D">
<!ENTITY icecrypt.searchRule.label "显示电子邮件地址中包含如下字符的规则:">
<!ENTITY icecrypt.clearSearch.label "清除">
<!ENTITY icecrypt.clearSearch.accesskey "C">
<!ENTITY icecrypt.searchKeyDlg.label "下载 OpenPGP 密钥">
<!ENTITY icecrypt.searchKeyDlgCapt.label "有符合条件的密钥 - 请选择要导入的密钥">
<!ENTITY icecrypt.searchKeyDlgSelAll.label "全选/全消">
<!ENTITY icecrypt.created.label "创建">
<!ENTITY icecrypt.progress.label "进度">
<!ENTITY icecrypt.sKeyDlg.title "Icecrypt - 签署密钥">
<!ENTITY icecrypt.sKeyDlg.signKey.label "要签署的密钥:">
<!ENTITY icecrypt.sKeyDlg.fingerprint.label "密钥指纹:">
<!ENTITY icecrypt.sKeyDlg.signWithKey.label "使用此密钥签署:">
<!ENTITY icecrypt.sKeyDlg.ownKeyTrust.label "请注意：您必须设置所有者信任，以最终在这里显示您自己的密钥。">
<!ENTITY icecrypt.sKeyDlg.checked.label "您对“您将要签署的密钥确实属于声称拥有它的人”做过何种程度的考察?">
<!ENTITY icecrypt.sKeyDlg.noAnswer.label "我不想回答">
<!ENTITY icecrypt.sKeyDlg.notChecked.label "我还没检查过">
<!ENTITY icecrypt.sKeyDlg.casualCheck.label "我随便检查了一下">
<!ENTITY icecrypt.sKeyDlg.carefulCheck.label "我很仔细的检查过">
<!ENTITY icecrypt.sKeyDlg.createLocal.label "本地签署(将不会被导出)">
<!ENTITY icecrypt.addUidDlg.title "Icecrypt - 增加用户标识">
<!ENTITY icecrypt.addUidDlg.name.label "姓名">
<!ENTITY icecrypt.addUidDlg.email.label "电邮">
<!ENTITY icecrypt.keyTrust.title "Icecrypt - 设置密钥信任级别">
<!ENTITY icecrypt.keyTrust.trustKey.label "要设置信任级别的密钥:">
<!ENTITY icecrypt.keyTrust.trustLevel.label "您在多大的程度上信任这个密钥?">
<!ENTITY icecrypt.keyTrust.dontKnow.label "我不知道">
<!ENTITY icecrypt.keyTrust.noTrust.label "我不信任">
<!ENTITY icecrypt.keyTrust.marginalTrust.label "我半信半疑">
<!ENTITY icecrypt.keyTrust.fullTrust.label "我可以相信">
<!ENTITY icecrypt.keyTrust.ultimateTrust.label "我完全信任">
<!ENTITY icecrypt.keyExpiry.title "Icecrypt - 更改过期时间">
<!ENTITY icecrypt.keyExpiry.expiryKey.label "密钥要更改的过期时间：">
<!ENTITY icecrypt.keyMan.title "Icecrypt 密钥管理">
<!ENTITY icecrypt.keyMan.close.label "关闭窗口">
<!ENTITY icecrypt.keyMan.generate.label "新密钥对">
<!ENTITY icecrypt.keyMan.sign.label "签署密钥">
<!ENTITY icecrypt.keyMan.setTrust.label "设置密钥信任级别">
<!ENTITY icecrypt.keyMan.genRevoke.label "撤销证书">
<!ENTITY icecrypt.keyMan.ctxGenRevoke.label "创建并保存撤销证书">
<!ENTITY icecrypt.keyMan.fileMenu.label "文件">
<!ENTITY icecrypt.keyMan.editMenu.label "编辑">
<!ENTITY icecrypt.keyMan.viewMenu.label "查看">
<!ENTITY icecrypt.keyMan.keyserverMenu.label "密钥服务器">
<!ENTITY icecrypt.keyMan.generateMenu.label "创建">
<!ENTITY icecrypt.keyMan.fileMenu.accesskey "F">
<!ENTITY icecrypt.keyMan.editMenu.accesskey "E">
<!ENTITY icecrypt.keyMan.viewMenu.accesskey "V">
<!ENTITY icecrypt.keyMan.keyserverMenu.accesskey "K">
<!ENTITY icecrypt.keyMan.generateMenu.accesskey "G">
<!ENTITY icecrypt.keyMan.importFromFile.label "从文件导入密钥">
<!ENTITY icecrypt.keyMan.importFromServer.label "搜索密钥">
<!ENTITY icecrypt.keyMan.importFromClipbrd.label "从剪贴板导入密钥">
<!ENTITY icecrypt.keyMan.importFromUrl.label "从网址导入密钥">
<!ENTITY icecrypt.keyMan.exportToFile.label "导出密钥到文件">
<!ENTITY icecrypt.keyMan.sendKeys.label "用邮件发送公钥">
<!ENTITY icecrypt.keyMan.createMail.label "用选中密钥撰写邮件">
<!ENTITY icecrypt.keyMan.uploadToServer.label "上传公钥">
<!ENTITY icecrypt.keyMan.copyToClipbrd.label "复制公钥到剪贴板">
<!ENTITY icecrypt.keyMan.ctxExportToFile.label "导出密钥到文件">
<!ENTITY icecrypt.keyMan.ctxUploadToServer.label "上传公钥到密钥服务器">
<!ENTITY icecrypt.keyMan.ctxCopyToClipbrd.label "复制公钥到剪贴板">
<!ENTITY icecrypt.keyMan.refreshSelKeys.label "更新所选择的公钥">
<!ENTITY icecrypt.keyMan.refreshAllKeys.label "更新所有的公钥">
<!ENTITY icecrypt.keyMan.downloadContactKeys.label "查找所有联系人的密钥">
<!ENTITY icecrypt.keyMan.ctxRefreshKey.label "从密钥服务器更新公钥">
<!ENTITY icecrypt.keyMan.reload.label "刷新密钥缓存">
<!ENTITY icecrypt.keyMan.addPhoto.label "Add Photo">
<!ENTITY icecrypt.keyMan.manageUid.label "管理用户标识">
<!ENTITY icecrypt.keyMan.changeExpiry.label "更改过期时间">
<!ENTITY icecrypt.keyMan.changePwd.label "更改口令">
<!ENTITY icecrypt.keyMan.delKey.label "删除密钥">
<!ENTITY icecrypt.keyMan.revokeKey.label "废除密钥">
<!ENTITY icecrypt.keyMan.keyProps.label "密钥属性">
<!ENTITY icecrypt.keyMan.showAllKeys.label "Display All Keys by Default">
<!ENTITY icecrypt.keyMan.viewPhoto.label "相片">
<!ENTITY icecrypt.keyMan.ctxViewPhoto.label "查看相片">
<!ENTITY icecrypt.keyMan.showInvalidKeys.label "显示无效的密钥">
<!ENTITY icecrypt.keyMan.showUntrustedKeys.label "显示未信任的密钥">
<!ENTITY icecrypt.keyMan.showOthersKeys.label "显示来自其他人的密钥">
<!ENTITY icecrypt.keyMan.userId.label "Name">
<!ENTITY icecrypt.keyMan.keyType.label "类型">
<!ENTITY icecrypt.keyMan.calcTrust.label "推算的信任级">
<!ENTITY icecrypt.keyMan.ownerTrust.label "设定的信任级">
<!ENTITY icecrypt.keyMan.stopTransfer.label "停止传输">
<!ENTITY icecrypt.keyMan.fingerprint.label "指纹">
<!ENTITY icecrypt.keyMan.selectAll.label "选择所有密钥">
<!ENTITY icecrypt.keyMan.addToPRRule.label "添加到收件人规则">
<!ENTITY icecrypt.keyMan.emptyTree.tooltip "请在上面的框输入搜索内容">
<!ENTITY icecrypt.keyMan.nothingFound.tooltip "没有密钥匹配您的搜索内容">
<!ENTITY icecrypt.keyMan.pleaseWait.tooltip "密钥载入中，请稍候…">
<!ENTITY icecrypt.keyMan.filter.label "仅显示用户标识中或密钥标识中包含如下字符的密钥:">
<!ENTITY icecrypt.keyMan.close.accesskey "C">
<!ENTITY icecrypt.keyMan.generate.accesskey "K">
<!ENTITY icecrypt.keyMan.sign.accesskey "S">
<!ENTITY icecrypt.keyMan.setTrust.accesskey "T">
<!ENTITY icecrypt.keyMan.genRevoke.accesskey "R">
<!ENTITY icecrypt.keyMan.delKey.accesskey "D">
<!ENTITY icecrypt.keyMan.revokeKey.accesskey "R">
<!ENTITY icecrypt.keyMan.importFromFile.accesskey "I">
<!ENTITY icecrypt.keyMan.exportToFile.accesskey "E">
<!ENTITY icecrypt.keyMan.importFromServer.accesskey "S">
<!ENTITY icecrypt.keyMan.uploadToServer.accesskey "U">
<!ENTITY icecrypt.keyMan.reload.accesskey "R">
<!ENTITY icecrypt.keyMan.addPhoto.accesskey "P">
<!ENTITY icecrypt.keyMan.manageUid.accesskey "M">
<!ENTITY icecrypt.keyMan.changeExpiry.accesskey "E">
<!ENTITY icecrypt.keyMan.changePwd.accesskey "P">
<!ENTITY icecrypt.keyMan.showInvalidKeys.accesskey "D">
<!ENTITY icecrypt.keyMan.showUntrustedKeys.accesskey "U">
<!ENTITY icecrypt.keyMan.showOthersKeys.accesskey "O">
<!ENTITY icecrypt.keyMan.showPhoto.accesskey "P">
<!ENTITY icecrypt.keyMan.importFromClipbrd.accesskey "I">
<!ENTITY icecrypt.keyMan.importFromUrl.accesskey "U">
<!ENTITY icecrypt.keyMan.copyToClipbrd.accesskey "C">
<!ENTITY icecrypt.keyMan.keyDetails.accesskey "K">
<!ENTITY icecrypt.keyMan.showAllKeys.accesskey "D">
<!ENTITY icecrypt.keyMan.enableKey.accesskey "B">
<!ENTITY icecrypt.keyMan.refreshSelKeys.accesskey "R">
<!ENTITY icecrypt.keyMan.refreshAllKeys.accesskey "A">
<!ENTITY icecrypt.keyMan.selectAll.accesskey "A">
<!ENTITY icecrypt.keyMan.sendKeys.accesskey "S">
<!ENTITY icecrypt.keyMan.createMail.accesskey "C">
<!ENTITY icecrypt.keyMan.downloadContactKeys.accesskey "F">
<!ENTITY icecrypt.keyMan.selectAll.key "A">
<!ENTITY icecrypt.keyMan.keyDetails.key "I">
<!ENTITY icecrypt.keyMan.refreshKey.key "R">
<!ENTITY icecrypt.keySearch.selectAll.key "A">
<!ENTITY icecrypt.manageUidDlg.title "修改默认用户标识">
<!ENTITY icecrypt.manageUidDlg.affectedKey.label "要修改的密钥:">
<!ENTITY icecrypt.manageUidDlg.availableUid.label "可用的用户标识:">
<!ENTITY icecrypt.manageUidDlg.addUid.label "增加">
<!ENTITY icecrypt.manageUidDlg.deleteUid.label "删除">
<!ENTITY icecrypt.manageUidDlg.revokeUid.label "废除">
<!ENTITY icecrypt.manageUidDlg.setPrimary.label "设为默认值">
<!ENTITY icecrypt.keyDetails.title "密钥属性">
<!ENTITY icecrypt.keyDetails.signaturesTab "认证">
<!ENTITY icecrypt.keyDetails.structureTab "结构">
<!ENTITY icecrypt.keyDetails.uidCertifiedCol "用户 ID / 认证由">
<!ENTITY icecrypt.keyDetails.userId.label "默认用户标识">
<!ENTITY icecrypt.keyDetails.ID.label "标识">
<!ENTITY icecrypt.keyDetails.keyType.label "类型">
<!ENTITY icecrypt.keyDetails.keyPart.label "密钥部分">
<!ENTITY icecrypt.keyDetails.algorithm.label "算法">
<!ENTITY icecrypt.keyDetails.size.label "长度">
<!ENTITY icecrypt.keyDetails.created.label "创建于">
<!ENTITY icecrypt.keyDetails.expiry.label "失效于">
<!ENTITY icecrypt.keyDetails.usage.label "使用">
<!ENTITY icecrypt.keyDetails.fingerprint.label "指纹">
<!ENTITY icecrypt.keyDetails.selAction.label "选择操作…">
<!ENTITY icecrypt.keyDetails.selAction.accesskey "S">
<!ENTITY icecrypt.keyDetails.change.label "更改">
<!ENTITY icecrypt.keyDetails.signKey.label "证明">
<!ENTITY icecrypt.keyDetails.trustStatus.label "您依赖于认证">
<!ENTITY icecrypt.keyDetails.keyValidity.label "验证">
<!ENTITY icecrypt.keyDetails.alsoKnown.label "也称为">
<!ENTITY icecrypt.cardDetails.title "OpenPGP 智能卡细节">
<!ENTITY icecrypt.cardDetails.cardMenu.label "智能卡">
<!ENTITY icecrypt.cardDetails.adminPin.label "修改识别码">
<!ENTITY icecrypt.cardDetails.genCardKey.label "创建密钥">
<!ENTITY icecrypt.cardDetails.vendor.label "制造商">
<!ENTITY icecrypt.cardDetails.serial.label "序列号">
<!ENTITY icecrypt.cardDetails.name.label "姓, 名">
<!ENTITY icecrypt.cardDetails.lang.label "语言">
<!ENTITY icecrypt.cardDetails.sex.label "性别">
<!ENTITY icecrypt.cardDetails.url.label "公钥的 URL">
<!ENTITY icecrypt.cardDetails.login.label "登录数据">
<!ENTITY icecrypt.cardDetails.forcepin.label "强制数字签名识别码">
<!ENTITY icecrypt.cardDetails.maxpinlen.label "最大识别码长度">
<!ENTITY icecrypt.cardDetails.pinretry.label "识别码重试次数计数器">
<!ENTITY icecrypt.cardDetails.sigcount.label "数字签名次数计数器">
<!ENTITY icecrypt.cardDetails.sigKey.label "数字签名密钥">
<!ENTITY icecrypt.cardDetails.keyCreated.label "创建于">
<!ENTITY icecrypt.cardDetails.encKey.label "加密密钥">
<!ENTITY icecrypt.cardDetails.authKey.label "认证密钥">
<!ENTITY icecrypt.cardDetails.yes.label "是">
<!ENTITY icecrypt.cardDetails.no.label "否">
<!ENTITY icecrypt.cardDetails.male.label "男">
<!ENTITY icecrypt.cardDetails.female.label "女">
<!ENTITY icecrypt.cardDetails.editData.label "编辑卡数据">
<!ENTITY icecrypt.cardDetails.save.label "保存">
<!ENTITY icecrypt.cardDetails.reset.label "复位">
<!ENTITY icecrypt.cardDetails.closeWindow.label "关闭">
<!ENTITY icecrypt.cardDetails.adminPin.accesskey "P">
<!ENTITY icecrypt.cardDetails.genCardKey.accesskey "G">
<!ENTITY icecrypt.cardDetails.cardMenu.accesskey "S">
<!ENTITY icecrypt.cardDetails.editData.accesskey "E">
<!ENTITY icecrypt.genCardKey.title "生成 OpenPGP 密钥">
<!ENTITY icecrypt.genCardKey.backupKey.label "在卡外部保存密钥备份">
<!ENTITY icecrypt.genCardKey.desc "<a class='icecryptStrong'>注意：密钥生成过程可能会持续数分钟。</a> 生成密钥的过程中请不要退出本程序。密钥生成结束时您将收到完成通知。">
<!ENTITY icecrypt.cardPin.title "修改智能卡识别码">
<!ENTITY icecrypt.cardPin.action.label "您想要做什么">
<!ENTITY icecrypt.cardPin.changePin.label "更改识别码">
<!ENTITY icecrypt.cardPin.changeAdmPin.label "修改管理识别码">
<!ENTITY icecrypt.cardPin.unblockPin.label "识别码解锁">
<!ENTITY icecrypt.cardPin.currAdmPin.label "当前管理识别码">
<!ENTITY icecrypt.cardPin.newAdminPin.label "新管理识别码">
<!ENTITY icecrypt.cardPin.adminPinRepeat.label "再次输入新识别码以确认">
<!ENTITY icecrypt.cardPin.currPin.label "当前识别码">
<!ENTITY icecrypt.cardPin.newPin.label "新识别码">
<!ENTITY icecrypt.cardPin.pinRepeat.label "再次输入新识别码以确认">
<!-- wizard: welcome step -->
<!ENTITY icecrypt.setupWiz.title "Icecrypt 设置向导">
<!ENTITY icecrypt.setupWiz.pgIntro.title "欢迎使用 Icecrypt 安装向导">
<!ENTITY icecrypt.setupWiz.pgIntro.desc "看起来您是第一次在这台计算机上使用 Icecrypt。
为了使用 Icecrypt，您首先需要正确的设置它。
这里将协助您完成初始设置过程。">
<!ENTITY icecrypt.setupWiz.pgIntro.desc2 "您想现在就设置 Icecrypt 吗，或者稍后再设置？">
<!ENTITY icecrypt.setupWiz.pgIntro.startNow "立即开始设置">
<!ENTITY icecrypt.setupWiz.pgIntro.startLater "稍后再配置 Icecrypt">
<!ENTITY icecrypt.setupWiz.pgWelcome.title "您想如何配置 Icecrypt？">
<!ENTITY icecrypt.setupWiz.pgWelcome.manualOrAuto "您想手动配置 Icecrypt，还是希望提供安装协助？">
<!ENTITY icecrypt.setupWiz.pgWelcome.beginnersMode "我更喜欢标准配置（推荐给新手）">
<!ENTITY icecrypt.setupWiz.pgWelcome.advancedMode "我更喜欢扩展配置（推荐给高级用户）">
<!ENTITY icecrypt.setupWiz.pgWelcome.manualMode "我更喜欢手动配置（推荐给专家）">
<!ENTITY icecrypt.setupWiz.pgWelcome.importSettings "我想从以前的安装导入我的设置">
<!-- wizard: select account IDs step -->
<!ENTITY icecrypt.setupWiz.pgSelectId.title "选择身份标识">
<!ENTITY icecrypt.setupWiz.pgSelectId.subtitle "选择 Icecrypt 所工作的帐号或身份标识">
<!ENTITY icecrypt.setupWiz.pgSelectId.desc2 "您想为您的所有电子邮件账户/身份设置 Icecrypt 吗？">
<!ENTITY icecrypt.setupWiz.pgSelectId.selectallIds "我想对我的所有账户/身份启用 Icecrypt">
<!ENTITY icecrypt.setupWiz.pgSelectId.selectedIds "我只想对下列身份启用 Icecrypt：">
<!ENTITY icecrypt.setupWiz.pgSelectId.note "<a class='icecryptStrong'>注意:</a> Icecrypt 将总是验证每个帐号或身份标识邮件的数                                                             字签名，而无论它是否被允许或不">
<!-- wizard: install GnuPG step -->
<!ENTITY icecrypt.setupWiz.pgInstall.title "需要 GnuPG">
<!ENTITY icecrypt.setupWiz.pgInstall.desc "Icecrypt 需要 GnuPG 才能实现密码学处理。
本向导尝试了定位 GnuPG 可执行程序的位置，但未能找到。">
<!ENTITY icecrypt.setupWiz.pgInstall.installDesc1 "Icecrypt 可以下载并为您启动 GnuPG 安装程序。
如果您希望这样做，点击“安装 GnuPG”按钮以下载并安装 GnuPG。">
<!ENTITY icecrypt.setupWiz.pgInstall.installDesc2 "另外，如果您已经安装了 GnuPG，请手动使用“浏览”按钮指定 GnuPG 的路径。">
<!ENTITY icecrypt.setupWiz.pgInstall.locateDesc "请使用“浏览”按钮定位 GnuPG。">
<!ENTITY icecrypt.setupWiz.pgInstall.installButton "安装…">
<!ENTITY icecrypt.setupWiz.pgInstall.locateButton "浏览…">
<!ENTITY icecrypt.setupWiz.pgInstall.downloadLabel "正在下载 GnuPG…">
<!ENTITY icecrypt.setupWiz.pgInstall.installLabel "正在启动安装程序。请遵循 GnuPG 安装程序的向导指示。">
<!ENTITY icecrypt.setupWiz.pgInstall.installDone "安装完成。请继续本向导的下一页面，继续安装过程。">
<!ENTITY icecrypt.setupWiz.pgInstall.gnupgFound "找到了有效的 GnuPG 安装位置。">
<!-- wizard: key selection step -->
<!ENTITY icecrypt.setupWiz.pgKeySel.title "选择密钥">
<!ENTITY icecrypt.setupWiz.pgKeySel.subtitle "创建一个用于数字签名和加密邮件的密钥">
<!ENTITY icecrypt.setupWiz.pgKeySel.desc2 "我们检测到您已经有了一个 OpenPGP 密钥。您可以使用您现有密钥中的任何一个，或者创建一个新的密钥对。">
<!ENTITY icecrypt.setupWiz.pgKeySel.newKey "我想为数字签名和加密邮件创建新的密钥对">
<!ENTITY icecrypt.setupWiz.pgKeySel.useExistingKey "我想为数字签名和加密邮件选择一个密钥:">
<!-- wizard: key creation - password step -->
<!ENTITY icecrypt.setupWiz.pgKeyCreate.title "创建密钥">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.subtitle "创建一个用于数字签名和加密邮件的密钥">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.descTwoKeys "此对话框将创建一对密钥：">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.descPub "您的<html:b>公钥</html:b>是<html:b>给其他人</html:b>用以向您发送加密邮件的东西。您可以将它分发给任何人。">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.descSec1 "您的<html:b>私钥</html:b>是<html:b>仅供您自己使用</html:b>的用来解密这些邮件，以及签名要发出的邮件时使用。
您应该不将它泄露给任何人。">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.descSec2 "您的<html:b>私钥</html:b>是<html:b>仅供您自己使用</html:b>的用来解密这些邮件，以及签名要发出的邮件时使用。
您应该不将它泄露给任何人。
为了您的私钥的安全，您需要在接下来的两个对话框中输入一个口令。">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.descPass "您的<html:b>口令</html:b>是用来保护您的私钥的密码。它用以避免您的私钥被滥用。">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.passContain "该口令应该是总计包含至少8个字符、数字和标点符号的短语。<html:b>不推荐</html:b>使用元音 (e.g. ä, é, ñ) 和特定语言的字符。">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.passRepeat "请再次确认您输入的口令">
<!ENTITY icecrypt.setupWiz.pgKeyCreate.passQuality "口令强度：">
<!-- wizard: no key found step -->
<!ENTITY icecrypt.setupWiz.pgNoKeyFound.title2 "创建或导入 OpenPGP 密钥">
<!ENTITY icecrypt.setupWiz.pgNoKeyFound.desc2 "您似乎还没有任何 OpenPGP 密钥。您想创建一个新密钥或者从已有文件中导入吗？">
<!ENTITY icecrypt.setupWiz.pgNoKeyFound.importKey "我想导入我已有的公钥和私钥">
<!-- wizard: import key page -->
<!ENTITY icecrypt.setupWiz.pgKeyImport.title "导入 OpenPGP 密钥">
<!ENTITY icecrypt.setupWiz.pgKeyImport.subtitle "指定要导入的文件">
<!ENTITY icecrypt.setupWiz.pgKeyImport.desc "请指定包含有要导入的公钥和私钥的文件。如果首个文件同时包含有公钥和私钥，私钥栏可留空。">
<!ENTITY icecrypt.setupWiz.pgKeyImport.pubkeyFile "公钥文件">
<!ENTITY icecrypt.setupWiz.pgKeyImport.seckeyFile "私钥文件">
<!-- wizard: key creation - key generation step -->
<!ENTITY icecrypt.setupWiz.pgKeygen.title "创建密钥">
<!ENTITY icecrypt.setupWiz.pgKeygen.subtitle "正在创建您的密钥">
<!ENTITY icecrypt.setupWiz.pgKeygen.keyGenComplete "您的密钥已生成">
<!ENTITY icecrypt.setupWiz.pgKeygen.revokeCertDesc "如果您的私钥丢失或泄露，您可能需要废除您的公钥
以使其他人不在继续使用您的旧密钥。
对于此目的，您需要进行证书废除。
<html:br/>您将需要为此输入您的密码。">
<!ENTITY icecrypt.setupWiz.pgKeygen.revCertButton "创建证书废除">
<!-- wizard: import settings from previous installation -->
<!ENTITY icecrypt.setupWiz.pgImportSettings.title "从以前的安装导入设置">
<!ENTITY icecrypt.setupWiz.pgImportSettings.desc "Icecrypt 允许转移其所有设置到一台新电脑，包括 OpenPGP 密钥。完成此操作需要两个步骤：
<html:ol>
<html:li>使用 Icecrypt 首选项中的备份向导，从您的旧电脑导出您的数据：</html:li>
<html:li>使用本向导将您的数据导入您的新电脑：</html:li>
</html:ol>">
<!ENTITY icecrypt.setupWiz.pgImportSettings.fileName "请指定包含您的备份数据的文件：">
<!ENTITY icecrypt.setupWiz.pgImportSettings.importing "导入密钥并应用设置">
<!ENTITY icecrypt.setupWiz.pgImportSettings.error "导入时发生错误。您的设置未能应用。">
<!-- wizard: final page -->
<!ENTITY icecrypt.setupWiz.pgComplete.title "谢谢你">
<!ENTITY icecrypt.setupWiz.pgComplete.desc "Icecrypt 已经就绪，可以使用。<html:br></html:br><html:br></html:br>感谢您使用 Icecrypt。">
<!-- wizard: manual setup page -->
<!ENTITY icecrypt.setupWiz.pgExpert.title "手动配置 Icecrypt">
<!ENTITY icecrypt.setupWiz.pgExpert.desc "您选择不使用 Icecrypt 安装向导来进行配置。">
<!ENTITY icecrypt.setupWiz.pgExpert.keyManager "Icecrypt 密钥管理器在这里可用。">
<!ENTITY icecrypt.setupWiz.pgExpert.expertSettings "导出设置在这里可用。">
<!ENTITY icecrypt.advancedIdentityDlg.title "高级 Icecrypt 身份设置">
<!ENTITY icecrypt.amPrefPgp.sendKeyWithMsg.label "附加我的公钥到邮件">
<!ENTITY icecrypt.addPhoto.question.label "您是否想添加下列图片到下列密钥？">
<!ENTITY icecrypt.addPhoto.title "添加照片到密钥">
<!ENTITY icecrypt.msgViewColumn.label "OpenPGP">
<!ENTITY icecrypt.msgViewColumn.tooltip "按照 OpenPGP 状态排序">
<!ENTITY icecrypt.addToRule.title "添加密钥到收件人规则">
<!ENTITY icecrypt.addToRule.useRuleButton.label "添加密钥到选中规则">
<!ENTITY icecrypt.addToRule.useRuleButton.accesskey "A">
<!ENTITY icecrypt.addToRule.newRuleButton.label "创建新规则">
<!ENTITY icecrypt.addToRule.newRuleButton.accesskey "N">
<!ENTITY icecrypt.savelogfile.label "保存日志到文件...">
<!ENTITY icecrypt.filterEditor.moveError.label "不支持在“永久解密”操作后再进行操作。">
<!-- Export wizard -->
<!ENTITY icecrypt.exportWiz.title "导出您的 Icecrypt 设置">
<!ENTITY icecrypt.exportWiz.pgIntro.title "欢迎使用导出设置的向导">
<!ENTITY icecrypt.exportWiz.pgIntro.desc1 "Icecrypt 允许转移其所有设置到一台新电脑，包括 OpenPGP 密钥。完成此操作需要两个步骤：
<html:ol>
<html:li>使用本向导从您的旧电脑导出您的数据：</html:li>
<html:li>使用本向导将您的数据导入您的新电脑：</html:li>
</html:ol>">
<!ENTITY icecrypt.exportWiz.pgIntro.specifyFile "请指定导出文件：">
<!ENTITY icecrypt.exportWiz.pgExport.title "正在导出 Icecrypt 设置">
<!ENTITY icecrypt.exportWiz.pgExport.exporting "正在保存。您可能被询问您的密钥的密码。">
<!ENTITY icecrypt.exportWiz.pgExport.done "您的 OpenPGP 密钥和您的 Icecrypt 设置现在已保存。

请将生成的文件传输到新的电脑，启动 Icedove-UXP 并从 Icecrypt 启动设置向导。">
<!ENTITY icecrypt.exportWiz.pgExport.failed "导出时发生错误。设置未能导出。">
