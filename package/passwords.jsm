/*global Components: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

var EXPORTED_SYMBOLS = ["IcecryptPassword"];

const Cu = Components.utils;

Cu.import("resource://icecrypt/lazy.jsm"); /*global IcecryptLazy: false */
Cu.import("resource://icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */
Cu.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Cu.import("resource://icecrypt/subprocess.jsm"); /*global subprocess: false */


const gpgAgent = IcecryptLazy.loader("icecrypt/gpgAgent.jsm", "IcecryptGpgAgent");
const getDialog = IcecryptLazy.loader("icecrypt/dialog.jsm", "IcecryptDialog");
const getLocale = IcecryptLazy.loader("icecrypt/locale.jsm", "IcecryptLocale");

const IcecryptPassword = {
  /*
   * Get GnuPG command line options for receiving the password depending
   * on the various user and system settings (gpg-agent/no passphrase)
   *
   * @return: Array the GnuPG command line options
   */
  command: function() {
    if (gpgAgent().useGpgAgent()) {
      return ["--use-agent"];
    }
    else {
      if (!IcecryptPrefs.getPref("noPassphrase")) {
        return ["--passphrase-fd", "0", "--no-use-agent"];
      }
    }
    return [];
  },

  getMaxIdleMinutes: function() {
    try {
      return IcecryptPrefs.getPref("maxIdleMinutes");
    }
    catch (ex) {}

    return 5;
  },

  clearPassphrase: function(win) {
    // clear all passphrases from gpg-agent by reloading the config
    if (!IcecryptCore.getService()) return;

    if (!gpgAgent().useGpgAgent()) {
      return;
    }

    let exitCode = -1;
    let isError = 0;

    const proc = {
      command: gpgAgent().connGpgAgentPath,
      arguments: [],
      charset: null,
      environment: IcecryptCore.getEnvList(),
      stdin: function(pipe) {
        pipe.write("RELOADAGENT\n");
        pipe.write("/bye\n");
        pipe.close();
      },
      stdout: function(data) {
        if (data.search(/^ERR/m) >= 0) {
          ++isError;
        }
      },
      done: function(result) {
        exitCode = result.exitCode;
      }
    };

    try {
      subprocess.call(proc).wait();
    }
    catch (ex) {}

    if (isError === 0) {
      getDialog().alert(win, getLocale().getString("passphraseCleared"));
    }
    else {
      getDialog().alert(win, getLocale().getString("cannotClearPassphrase"));
    }
  }
};
