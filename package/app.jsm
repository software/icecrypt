/*global Components: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

var EXPORTED_SYMBOLS = ["IcecryptApp"];

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;

Cu.import("resource://gre/modules/AddonManager.jsm"); /*global AddonManager: false */
Cu.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */

const DIR_SERV_CONTRACTID = "@mozilla.org/file/directory_service;1";
const ENIG_EXTENSION_GUID = "{809acbe0-5423-4c50-a516-4b669cec108d}";
const SEAMONKEY_ID = "{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}";
const XPCOM_APPINFO = "@mozilla.org/xre/app-info;1";

const IcecryptApp = {
  /**
   * Platform application name (e.g. Thunderbird)
   */
  getName: function() {
    return Cc[XPCOM_APPINFO].getService(Ci.nsIXULAppInfo).name;
  },

  /**
   * Return the directory holding the current profile as nsIFile object
   */
  getProfileDirectory: function() {
    let ds = Cc[DIR_SERV_CONTRACTID].getService(Ci.nsIProperties);
    return ds.get("ProfD", Ci.nsIFile);
  },

  isSuite: function() {
    // return true if Seamonkey, false otherwise
    return Cc[XPCOM_APPINFO].getService(Ci.nsIXULAppInfo).ID == SEAMONKEY_ID;
  },

  getVersion: function() {
    IcecryptLog.DEBUG("app.jsm: getVersion\n");
    IcecryptLog.DEBUG("app.jsm: installed version: " + IcecryptApp.version + "\n");
    return IcecryptApp.version;
  },

  getInstallLocation: function() {
    return IcecryptApp.installLocation;
  },

  setVersion: function(version) {
    IcecryptApp.version = version;
  },

  setInstallLocation: function(location) {
    IcecryptApp.installLocation = location;
  },

  registerAddon: function(addon) {
    IcecryptApp.setVersion(addon.version);
    IcecryptApp.setInstallLocation(addon.getResourceURI("").QueryInterface(Ci.nsIFileURL).file);
  },

  initAddon: function() {
    AddonManager.getAddonByID(ENIG_EXTENSION_GUID, IcecryptApp.registerAddon);
  }
};

IcecryptApp.initAddon();
