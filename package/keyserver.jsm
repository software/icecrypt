/*global Components: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

var EXPORTED_SYMBOLS = ["IcecryptKeyServer"];

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;

Cu.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Cu.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Cu.import("resource://icecrypt/httpProxy.jsm"); /*global IcecryptHttpProxy: false */
Cu.import("resource://icecrypt/gpg.jsm"); /*global IcecryptGpg: false */
Cu.import("resource://icecrypt/gpgAgent.jsm"); /*global IcecryptGpgAgent: false */
Cu.import("resource://icecrypt/files.jsm"); /*global IcecryptFiles: false */
Cu.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */
Cu.import("resource://icecrypt/subprocess.jsm"); /*global subprocess: false */
Cu.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */

const nsIIcecrypt = Ci.nsIIcecrypt;

const IcecryptKeyServer = {
  /**
   * search, download or upload key on, from or to a keyserver
   *
   * @actionFlags: Integer - flags (bitmap) to determine the required action
   *                         (see nsIIcecrypt - Keyserver action flags for details)
   * @keyserver:   String  - keyserver URL (optionally incl. protocol)
   * @searchTerms: String  - space-separated list of search terms or key IDs
   * @listener:    Object  - execStart Listener Object. See execStart for details.
   * @errorMsgObj: Object  - object to hold error message in .value
   *
   * @return:      Subprocess object, or null in case process could not be started
   */
  access: function(actionFlags, keyserver, searchTerms, listener, errorMsgObj) {
    IcecryptLog.DEBUG("keyserver.jsm: access: " + searchTerms + "\n");

    if (!keyserver) {
      errorMsgObj.value = IcecryptLocale.getString("failNoServer");
      return null;
    }

    if (!searchTerms && !(actionFlags & nsIIcecrypt.REFRESH_KEY)) {
      errorMsgObj.value = IcecryptLocale.getString("failNoID");
      return null;
    }

    const proxyHost = IcecryptHttpProxy.getHttpProxy(keyserver);
    let args = IcecryptGpg.getStandardArgs(true);

    if (actionFlags & nsIIcecrypt.SEARCH_KEY) {
      args = IcecryptGpg.getStandardArgs(false).
      concat(["--command-fd", "0", "--fixed-list", "--with-colons"]);
    }
    if (proxyHost) {
      args = args.concat(["--keyserver-options", "http-proxy=" + proxyHost]);
    }
    args = args.concat(["--keyserver", keyserver.trim()]);

    //     if (actionFlags & nsIIcecrypt.SEARCH_KEY | nsIIcecrypt.DOWNLOAD_KEY | nsIIcecrypt.REFRESH_KEY) {
    //       args = args.concat(["--command-fd", "0", "--fixed-list", "--with-colons"]);
    //     }

    let inputData = null;
    const searchTermsList = searchTerms.split(" ");

    if (actionFlags & nsIIcecrypt.DOWNLOAD_KEY) {
      args.push("--recv-keys");
      args = args.concat(searchTermsList);
    }
    else if (actionFlags & nsIIcecrypt.REFRESH_KEY) {
      args.push("--refresh-keys");
    }
    else if (actionFlags & nsIIcecrypt.SEARCH_KEY) {
      args.push("--search-keys");
      args = args.concat(searchTermsList);
      inputData = "quit\n";
    }
    else if (actionFlags & nsIIcecrypt.UPLOAD_KEY) {
      args.push("--send-keys");
      args = args.concat(searchTermsList);
    }

    const isDownload = actionFlags & (nsIIcecrypt.REFRESH_KEY | nsIIcecrypt.DOWNLOAD_KEY);

    IcecryptLog.CONSOLE("icecrypt> " + IcecryptFiles.formatCmdLine(IcecryptGpgAgent.agentPath, args) + "\n");

    let proc = null;
    let exitCode = null;

    try {
      proc = subprocess.call({
        command: IcecryptGpgAgent.agentPath,
        arguments: args,
        environment: IcecryptCore.getEnvList(),
        charset: null,
        stdin: inputData,
        stdout: function(data) {
          listener.stdout(data);
        },
        stderr: function(data) {
          if (data.search(/^\[GNUPG:\] ERROR/m) >= 0) {
            exitCode = 4;
          }
          listener.stderr(data);
        },
        done: function(result) {
          try {
            if (result.exitCode === 0 && isDownload) {
              IcecryptKeyRing.clearCache();
            }
            if (exitCode === null) {
              exitCode = result.exitCode;
            }
            listener.done(exitCode);
          }
          catch (ex) {}
        },
        mergeStderr: false
      });
    }
    catch (ex) {
      IcecryptLog.ERROR("keyserver.jsm: access: subprocess.call failed with '" + ex.toString() + "'\n");
      throw ex;
    }

    if (!proc) {
      IcecryptLog.ERROR("keyserver.jsm: access: subprocess failed due to unknown reasons\n");
      return null;
    }

    return proc;
  }
};
