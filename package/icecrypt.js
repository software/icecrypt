/*global Components: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

const Cu = Components.utils;

Cu.import("resource://gre/modules/XPCOMUtils.jsm"); /*global XPCOMUtils: false */
Cu.import("resource://icecrypt/subprocess.jsm"); /*global subprocess: false */
Cu.import("resource://icecrypt/pipeConsole.jsm"); /*global IcecryptConsole: false */
Cu.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Cu.import("resource://icecrypt/gpgAgent.jsm"); /*global IcecryptGpgAgent: false */
Cu.import("resource://icecrypt/encryption.jsm"); /*global IcecryptEncryption: false */
Cu.import("resource://icecrypt/decryption.jsm"); /*global IcecryptDecryption: false */
Cu.import("resource://icecrypt/protocolHandler.jsm"); /*global IcecryptProtocolHandler: false */
Cu.import("resource://icecrypt/rules.jsm"); /*global IcecryptRules: false */
Cu.import("resource://icecrypt/filters.jsm"); /*global IcecryptFilters: false */
Cu.import("resource://icecrypt/armor.jsm"); /*global IcecryptArmor: false */
Cu.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Cu.import("resource://icecrypt/os.jsm"); /*global IcecryptOS: false */
Cu.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Cu.import("resource://icecrypt/commandLine.jsm"); /*global IcecryptCommandLine: false */
Cu.import("resource://icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */
Cu.import("resource://icecrypt/uris.jsm"); /*global IcecryptURIs: false */
Cu.import("resource://icecrypt/verify.jsm"); /*global IcecryptVerifyAttachment: false */
Cu.import("resource://icecrypt/mimeVerify.jsm"); /*global IcecryptVerify: false */
Cu.import("resource://icecrypt/windows.jsm"); /*global IcecryptWindows: false */
Cu.import("resource://icecrypt/dialog.jsm"); /*global IcecryptDialog: false */
Cu.import("resource://icecrypt/configure.jsm"); /*global IcecryptConfigure: false */
Cu.import("resource://icecrypt/app.jsm"); /*global IcecryptApp: false */

/* Implementations supplied by this module */
const NS_ENIGMAIL_CONTRACTID = "@mozdev.org/icecrypt/icecrypt;1";

const NS_ENIGMAIL_CID =
  Components.ID("{847b3a01-7ab1-11d4-8f02-006008948af5}");

// Contract IDs and CIDs used by this module
const NS_OBSERVERSERVICE_CONTRACTID = "@mozilla.org/observer-service;1";

const Cc = Components.classes;
const Ci = Components.interfaces;

// Interfaces
const nsISupports = Ci.nsISupports;
const nsIObserver = Ci.nsIObserver;
const nsIEnvironment = Ci.nsIEnvironment;
const nsIIcecrypt = Ci.nsIIcecrypt;

const NS_XPCOM_SHUTDOWN_OBSERVER_ID = "xpcom-shutdown";

///////////////////////////////////////////////////////////////////////////////
// Icecrypt encryption/decryption service
///////////////////////////////////////////////////////////////////////////////

function getLogDirectoryPrefix() {
  try {
    return IcecryptPrefs.getPrefBranch().getCharPref("logDirectory") || "";
  }
  catch (ex) {
    return "";
  }
}

function initializeLogDirectory() {
  const prefix = getLogDirectoryPrefix();
  if (prefix) {
    IcecryptLog.setLogLevel(5);
    IcecryptLog.setLogDirectory(prefix);
    IcecryptLog.DEBUG("icecrypt.js: Logging debug output to " + prefix + "/enigdbug.txt\n");
  }
}

function initializeLogging(env) {
  const nspr_log_modules = env.get("NSPR_LOG_MODULES");
  const matches = nspr_log_modules.match(/icecrypt.js:(\d+)/);

  if (matches && (matches.length > 1)) {
    IcecryptLog.setLogLevel(Number(matches[1]));
    IcecryptLog.WARNING("icecrypt.js: Icecrypt: LogLevel=" + matches[1] + "\n");
  }
}

function initializeSubprocessLogging(env) {
  const nspr_log_modules = env.get("NSPR_LOG_MODULES");
  const matches = nspr_log_modules.match(/subprocess:(\d+)/);

  subprocess.registerLogHandler(function(txt) {
    IcecryptLog.ERROR("subprocess.jsm: " + txt);
  });

  if (matches && matches.length > 1 && matches[1] > 2) {
    subprocess.registerDebugHandler(function(txt) {
      IcecryptLog.DEBUG("subprocess.jsm: " + txt);
    });
  }
}

function initializeAgentInfo() {
  if (IcecryptGpgAgent.useGpgAgent() && (!IcecryptOS.isDosLike())) {
    if (!IcecryptGpgAgent.isDummy()) {
      IcecryptCore.addToEnvList("GPG_AGENT_INFO=" + IcecryptGpgAgent.gpgAgentInfo.envStr);
    }
  }
}

function failureOn(ex, status) {
  status.initializationError = IcecryptLocale.getString("icecryptNotAvailable");
  IcecryptLog.ERROR("icecrypt.js: Icecrypt.initialize: Error - " + status.initializationError + "\n");
  IcecryptLog.DEBUG("icecrypt.js: Icecrypt.initialize: exception=" + ex.toString() + "\n");
  throw Components.results.NS_ERROR_FAILURE;
}

function getEnvironment(status) {
  try {
    return Cc["@mozilla.org/process/environment;1"].getService(nsIEnvironment);
  }
  catch (ex) {
    failureOn(ex, status);
  }
}

function initializeEnvironment(env) {
  // Initialize global environment variables list
  const passEnv = ["GNUPGHOME", "GPGDIR", "ETC",
    "ALLUSERSPROFILE", "APPDATA", "BEGINLIBPATH",
    "COMMONPROGRAMFILES", "COMSPEC", "DBUS_SESSION_BUS_ADDRESS", "DISPLAY",
    "ENIGMAIL_PASS_ENV", "ENDLIBPATH",
    "GTK_IM_MODULE",
    "HOME", "HOMEDRIVE", "HOMEPATH",
    "LANG", "LANGUAGE", "LC_ALL", "LC_COLLATE", "LC_CTYPE",
    "LC_MESSAGES", "LC_MONETARY", "LC_NUMERIC", "LC_TIME",
    "LOCPATH", "LOGNAME", "LD_LIBRARY_PATH", "MOZILLA_FIVE_HOME",
    "NLSPATH", "PATH", "PATHEXT", "PROGRAMFILES", "PWD",
    "QT_IM_MODULE",
    "SHELL", "SYSTEMDRIVE", "SYSTEMROOT",
    "TEMP", "TMP", "TMPDIR", "TZ", "TZDIR", "UNIXROOT",
    "USER", "USERPROFILE", "WINDIR", "XAUTHORITY",
    "XMODIFIERS"
  ];

  const passList = env.get("ENIGMAIL_PASS_ENV");
  if (passList) {
    const passNames = passList.split(":");
    for (var k = 0; k < passNames.length; k++) {
      passEnv.push(passNames[k]);
    }
  }

  IcecryptCore.initEnvList();
  for (var j = 0; j < passEnv.length; j++) {
    const envName = passEnv[j];
    const envValue = env.get(envName);
    if (envValue) {
      IcecryptCore.addToEnvList(envName + "=" + envValue);
    }
  }

  IcecryptLog.DEBUG("icecrypt.js: Icecrypt.initialize: Ec.envList = " + IcecryptCore.getEnvList() + "\n");
}

function initializeObserver(on) {
  // Register to observe XPCOM shutdown
  const obsServ = Cc[NS_OBSERVERSERVICE_CONTRACTID].getService().
  QueryInterface(Ci.nsIObserverService);
  obsServ.addObserver(on, NS_XPCOM_SHUTDOWN_OBSERVER_ID, false);
}

function Icecrypt() {
  this.wrappedJSObject = this;
}

Icecrypt.prototype = {
  classDescription: "Icecrypt",
  classID: NS_ENIGMAIL_CID,
  contractID: NS_ENIGMAIL_CONTRACTID,

  initialized: false,
  initializationAttempted: false,
  initializationError: "",

  _xpcom_factory: {
    createInstance: function(aOuter, iid) {
      // Icecrypt is a service -> only instanciate once
      return IcecryptCore.ensuredIcecryptService(function() {
        return new Icecrypt();
      });
    },
    lockFactory: function(lock) {}
  },
  QueryInterface: XPCOMUtils.generateQI([nsIIcecrypt, nsIObserver, nsISupports]),

  observe: function(aSubject, aTopic, aData) {
    IcecryptLog.DEBUG("icecrypt.js: Icecrypt.observe: topic='" + aTopic + "' \n");

    if (aTopic == NS_XPCOM_SHUTDOWN_OBSERVER_ID) {
      // XPCOM shutdown
      this.finalize();

    }
    else {
      IcecryptLog.DEBUG("icecrypt.js: Icecrypt.observe: no handler for '" + aTopic + "'\n");
    }
  },


  finalize: function() {
    IcecryptLog.DEBUG("icecrypt.js: Icecrypt.finalize:\n");
    if (!this.initialized) return;

    IcecryptGpgAgent.finalize();
    IcecryptLog.onShutdown();

    IcecryptLog.setLogLevel(3);
    this.initializationError = "";
    this.initializationAttempted = false;
    this.initialized = false;
  },


  initialize: function(domWindow, version) {
    this.initializationAttempted = true;

    IcecryptLog.DEBUG("icecrypt.js: Icecrypt.initialize: START\n");

    if (this.initialized) return;

    initializeLogDirectory();

    IcecryptCore.setIcecryptService(this);

    this.environment = getEnvironment(this);

    initializeLogging(this.environment);
    initializeSubprocessLogging(this.environment);
    initializeEnvironment(this.environment);

    try {
      IcecryptConsole.write("Initializing Icecrypt service ...\n");
    }
    catch (ex) {
      failureOn(ex, this);
    }

    IcecryptGpgAgent.setAgentPath(domWindow, this);
    IcecryptGpgAgent.detectGpgAgent(domWindow, this);

    initializeAgentInfo();

    initializeObserver(this);

    this.initialized = true;

    IcecryptLog.DEBUG("icecrypt.js: Icecrypt.initialize: END\n");
  },

  reinitialize: function() {
    this.initialized = false;
    this.initializationAttempted = true;

    IcecryptConsole.write("Reinitializing Icecrypt service ...\n");
    IcecryptGpgAgent.setAgentPath(null, this);
    this.initialized = true;
  },

  getService: function(holder, win, startingPreferences) {
    if (!win) {
      win = IcecryptWindows.getBestParentWin();
    }

    IcecryptLog.DEBUG("icecrypt.js: svc = " + holder.svc + "\n");

    if (!holder.svc.initialized) {
      const firstInitialization = !holder.svc.initializationAttempted;

      try {
        // Initialize icecrypt
        IcecryptCore.init(IcecryptApp.getVersion());
        holder.svc.initialize(win, IcecryptApp.getVersion());

        try {
          // Reset alert count to default value
          IcecryptPrefs.getPrefBranch().clearUserPref("initAlert");
        }
        catch (ex) {}
      }
      catch (ex) {
        if (firstInitialization) {
          // Display initialization error alert
          const errMsg = (holder.svc.initializationError ? holder.svc.initializationError : IcecryptLocale.getString("accessError")) +
            "\n\n" + IcecryptLocale.getString("initErr.howToFixIt");

          const checkedObj = {
            value: false
          };
          if (IcecryptPrefs.getPref("initAlert")) {
            const r = IcecryptDialog.longAlert(win, "Icecrypt: " + errMsg,
              IcecryptLocale.getString("dlgNoPrompt"),
              null, IcecryptLocale.getString("initErr.setupWizard.button"),
              null, checkedObj);
            if (r >= 0 && checkedObj.value) {
              IcecryptPrefs.setPref("initAlert", false);
            }
            if (r == 1) {
              // start setup wizard
              IcecryptWindows.openSetupWizard(win, false);
              return Icecrypt.getService(holder, win);
            }
          }
          if (IcecryptPrefs.getPref("initAlert")) {
            holder.svc.initializationAttempted = false;
            holder.svc = null;
          }
        }

        return null;
      }

      const configuredVersion = IcecryptPrefs.getPref("configuredVersion");

      IcecryptLog.DEBUG("icecryptCommon.jsm: getService: " + configuredVersion + "\n");

      if (firstInitialization && holder.svc.initialized &&
        IcecryptGpgAgent.agentType === "pgp") {
        IcecryptDialog.alert(win, IcecryptLocale.getString("pgpNotSupported"));
      }

      if (holder.svc.initialized && (IcecryptApp.getVersion() != configuredVersion)) {
        IcecryptConfigure.configureIcecrypt(win, startingPreferences);
      }
    }

    return holder.svc.initialized ? holder.svc : null;
  }
}; // Icecrypt.prototype


IcecryptArmor.registerOn(Icecrypt.prototype);
IcecryptDecryption.registerOn(Icecrypt.prototype);
IcecryptEncryption.registerOn(Icecrypt.prototype);
IcecryptRules.registerOn(Icecrypt.prototype);
IcecryptURIs.registerOn(Icecrypt.prototype);
IcecryptVerifyAttachment.registerOn(Icecrypt.prototype);
IcecryptVerify.registerContentTypeHandler();

// This variable is exported implicitly and should not be refactored or removed
const NSGetFactory = XPCOMUtils.generateNSGetFactory([Icecrypt, IcecryptProtocolHandler, IcecryptCommandLine.Handler]);

IcecryptFilters.registerAll();
