/*global Components: false, IcecryptCore: false, XPCOMUtils: false, IcecryptData: false, IcecryptLog: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

var EXPORTED_SYMBOLS = ["IcecryptProtocolHandler"];

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");
Components.utils.import("resource://icecrypt/core.jsm");
Components.utils.import("resource://icecrypt/data.jsm");
Components.utils.import("resource://icecrypt/log.jsm");
Components.utils.import("resource://icecrypt/streams.jsm"); /*global IcecryptStreams: false */
Components.utils.import("resource://icecrypt/uris.jsm"); /*global IcecryptURIs: false */
Components.utils.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */

const NS_SIMPLEURI_CONTRACTID = "@mozilla.org/network/simple-uri;1";
const NS_ENIGMAILPROTOCOLHANDLER_CONTRACTID = "@mozilla.org/network/protocol;1?name=icecrypt";
const NS_ENIGMAILPROTOCOLHANDLER_CID = Components.ID("{847b3a11-7ab1-11d4-8f02-006008948af5}");
const ASS_CONTRACTID = "@mozilla.org/appshell/appShellService;1";
const WMEDIATOR_CONTRACTID = "@mozilla.org/appshell/window-mediator;1";

const Cc = Components.classes;
const Ci = Components.interfaces;

const nsIProtocolHandler = Ci.nsIProtocolHandler;

var EC = IcecryptCore;

const gDummyPKCS7 =
  'Content-Type: multipart/mixed;\r\n boundary="------------060503030402050102040303\r\n\r\nThis is a multi-part message in MIME format.\r\n--------------060503030402050102040303\r\nContent-Type: application/x-pkcs7-mime\r\nContent-Transfer-Encoding: 8bit\r\n\r\n\r\n--------------060503030402050102040303\r\nContent-Type: application/x-icecrypt-dummy\r\nContent-Transfer-Encoding: 8bit\r\n\r\n\r\n--------------060503030402050102040303--\r\n';


function IcecryptProtocolHandler() {}

IcecryptProtocolHandler.prototype = {
  classDescription: "Icecrypt Protocol Handler",
  classID: NS_ENIGMAILPROTOCOLHANDLER_CID,
  contractID: NS_ENIGMAILPROTOCOLHANDLER_CONTRACTID,
  scheme: "icecrypt",
  defaultPort: -1,
  protocolFlags: nsIProtocolHandler.URI_INHERITS_SECURITY_CONTEXT |
    nsIProtocolHandler.URI_LOADABLE_BY_ANYONE |
    nsIProtocolHandler.URI_NORELATIVE |
    nsIProtocolHandler.URI_NOAUTH |
    nsIProtocolHandler.URI_OPENING_EXECUTES_SCRIPT,

  QueryInterface: XPCOMUtils.generateQI([nsIProtocolHandler]),

  newURI: function(aSpec, originCharset, aBaseURI) {
    IcecryptLog.DEBUG("icecrypt.js: IcecryptProtocolHandler.newURI: aSpec='" + aSpec + "'\n");

    // cut of any parameters potentially added to the URI; these cannot be handled
    if (aSpec.substr(0, 14) == "icecrypt:dummy") aSpec = "icecrypt:dummy";

    var uri = Cc[NS_SIMPLEURI_CONTRACTID].createInstance(Ci.nsIURI);
    uri.spec = aSpec;

    return uri;
  },

  newChannel: function(aURI) {
    IcecryptLog.DEBUG("icecrypt.js: IcecryptProtocolHandler.newChannel: URI='" + aURI.spec + "'\n");

    var messageId = IcecryptData.extractMessageId(aURI.spec);
    var mimeMessageId = IcecryptData.extractMimeMessageId(aURI.spec);
    var contentType, contentCharset, contentData;

    if (messageId) {
      // Handle icecrypt:message/...

      if (!EC.getIcecryptService()) {
        throw Components.results.NS_ERROR_FAILURE;
      }

      if (IcecryptURIs.getMessageURI(messageId)) {
        var messageUriObj = IcecryptURIs.getMessageURI(messageId);

        contentType = messageUriObj.contentType;
        contentCharset = messageUriObj.contentCharset;
        contentData = messageUriObj.contentData;

        IcecryptLog.DEBUG("icecrypt.js: IcecryptProtocolHandler.newChannel: messageURL=" + messageUriObj.originalUrl + ", content length=" + contentData.length + ", " + contentType + ", " +
          contentCharset + "\n");

        // do NOT delete the messageUriObj now from the list, this will be done once the message is unloaded (fix for bug 9730).

      }
      else if (mimeMessageId) {
        this.handleMimeMessage(mimeMessageId);
      }
      else {

        contentType = "text/plain";
        contentCharset = "";
        contentData = "Icecrypt error: invalid URI " + aURI.spec;
      }

      let channel = IcecryptStreams.newStringChannel(aURI, contentType, "UTF-8", contentData);

      return channel;
    }

    if (aURI.spec.indexOf(aURI.scheme + "://photo/") === 0) {
      // handle photo ID
      contentType = "image/jpeg";
      contentCharset = "";
      let keyId = aURI.spec.substr(17);
      let exitCodeObj = {};
      let errorMsgObj = {};
      let f = IcecryptKeyRing.getPhotoFile(keyId, 0, exitCodeObj, errorMsgObj);
      if (exitCodeObj.value === 0) {
        let channel = IcecryptStreams.newFileChannel(aURI, f, "image/jpeg", true);
        return channel;
      }

      return null;
    }

    if (aURI.spec == aURI.scheme + ":dummy") {
      // Dummy PKCS7 content (to access mimeEncryptedClass)
      return IcecryptStreams.newStringChannel(aURI, "message/rfc822", "", gDummyPKCS7);
    }

    var winName, spec;
    if (aURI.spec == "about:" + aURI.scheme) {
      // About Icecrypt
      //            winName = "about:"+icecrypt;
      winName = "about:icecrypt";
      spec = "chrome://icecrypt/content/icecryptAbout.xul";

    }
    else if (aURI.spec == aURI.scheme + ":console") {
      // Display icecrypt console messages
      winName = "icecrypt:console";
      spec = "chrome://icecrypt/content/icecryptConsole.xul";

    }
    else if (aURI.spec == aURI.scheme + ":keygen") {
      // Display icecrypt key generation console
      winName = "icecrypt:keygen";
      spec = "chrome://icecrypt/content/icecryptKeygen.xul";

    }
    else {
      // Display Icecrypt about page
      winName = "about:icecrypt";
      spec = "chrome://icecrypt/content/icecryptAbout.xul";
    }

    var windowManager = Cc[WMEDIATOR_CONTRACTID].getService(Ci.nsIWindowMediator);

    var winEnum = windowManager.getEnumerator(null);
    var recentWin = null;
    while (winEnum.hasMoreElements() && !recentWin) {
      var thisWin = winEnum.getNext();
      if (thisWin.location.href == spec) {
        recentWin = thisWin;
      }
    }

    if (recentWin) {
      recentWin.focus();
    }
    else {
      var appShellSvc = Cc[ASS_CONTRACTID].getService(Ci.nsIAppShellService);
      var domWin = appShellSvc.hiddenDOMWindow;

      domWin.open(spec, "_blank", "chrome,menubar,toolbar,resizable");
    }

    throw Components.results.NS_ERROR_FAILURE;
  },

  handleMimeMessage: function(messageId) {
    //        IcecryptLog.DEBUG("icecrypt.js: IcecryptProtocolHandler.handleMimeMessage: messageURL="+messageUriObj.originalUrl+", content length="+contentData.length+", "+contentType+", "+contentCharset+"\n");
    IcecryptLog.DEBUG("icecrypt.js: IcecryptProtocolHandler.handleMimeMessage: messageURL=, content length=, , \n");
  },

  allowPort: function(port, scheme) {
    // non-standard ports are not allowed
    return false;
  }
};
