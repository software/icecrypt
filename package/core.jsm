/*global Components: false, Icecrypt: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

var EXPORTED_SYMBOLS = ["IcecryptCore"];

const Cc = Components.classes;
const Ci = Components.interfaces;

const icecryptHolder = {
  svc: null
}; // Global Icecrypt Service
let envList = null; // currently filled from icecrypt.js

function lazy(importName, name) {
  let holder = null;
  return function(f) {
    if (!holder) {
      if (f) {
        holder = f();
      }
      else {
        const result = {};
        Components.utils.import("resource://icecrypt/" + importName, result);
        holder = result[name];
      }
    }
    return holder;
  };
}

const IcecryptCore = {
  version: "",

  init: function(icecryptVersion) {
    this.version = icecryptVersion;
  },

  /**
   * get and or initialize the Icecrypt service,
   * including the handling for upgrading old preferences to new versions
   *
   * @win:                - nsIWindow: parent window (optional)
   * @startingPreferences - Boolean: true - called while switching to new preferences
   *                        (to avoid re-check for preferences)
   */
  getService: function(win, startingPreferences) {
    // Lazy initialization of Icecrypt JS component (for efficiency)

    if (icecryptHolder.svc) {
      return icecryptHolder.svc.initialized ? icecryptHolder.svc : null;
    }

    try {
      icecryptHolder.svc = Cc["@mozdev.org/icecrypt/icecrypt;1"].createInstance(Ci.nsIIcecrypt);
      return icecryptHolder.svc.wrappedJSObject.getService(icecryptHolder, win, startingPreferences);
    }
    catch (ex) {
      return null;
    }

  },

  getIcecryptService: function() {
    return icecryptHolder.svc;
  },

  setIcecryptService: function(v) {
    icecryptHolder.svc = v;
  },

  ensuredIcecryptService: function(f) {
    if (!icecryptHolder.svc) {
      IcecryptCore.setIcecryptService(f());
    }
    return icecryptHolder.svc;
  },

  getKeyRing: lazy("keyRing.jsm", "IcecryptKeyRing"),

  /**
   * obtain a list of all environment variables
   *
   * @return: Array of Strings with the following structrue
   *          variable_name=variable_content
   */
  getEnvList: function() {
    return envList;
  },

  addToEnvList: function(str) {
    IcecryptCore.getEnvList().push(str);
  },

  initEnvList: function() {
    envList = [];
  }
};
