/*global Components: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

var EXPORTED_SYMBOLS = ["IcecryptVerifyAttachment"];

const Cu = Components.utils;

Cu.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Cu.import("resource://icecrypt/files.jsm"); /*global IcecryptFiles: false */
Cu.import("resource://icecrypt/gpgAgent.jsm"); /*global IcecryptGpgAgent: false */
Cu.import("resource://icecrypt/gpg.jsm"); /*global IcecryptGpg: false */
Cu.import("resource://icecrypt/execution.jsm"); /*global IcecryptExecution: false */
Cu.import("resource://icecrypt/time.jsm"); /*global IcecryptTime: false */
Cu.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Cu.import("resource://icecrypt/decryption.jsm"); /*global IcecryptDecryption: false */

const Ci = Components.interfaces;

const nsIIcecrypt = Ci.nsIIcecrypt;

const IcecryptVerifyAttachment = {
  attachment: function(parent, verifyFile, sigFile, statusFlagsObj, errorMsgObj) {
    IcecryptLog.DEBUG("verify.jsm: IcecryptVerifyAttachment.attachment:\n");

    const verifyFilePath = IcecryptFiles.getEscapedFilename(IcecryptFiles.getFilePathReadonly(verifyFile.QueryInterface(Ci.nsIFile)));
    const sigFilePath = IcecryptFiles.getEscapedFilename(IcecryptFiles.getFilePathReadonly(sigFile.QueryInterface(Ci.nsIFile)));

    const args = IcecryptGpg.getStandardArgs(true).
    concat(["--verify", sigFilePath, verifyFilePath]);

    const listener = IcecryptExecution.newSimpleListener();

    const proc = IcecryptExecution.execStart(IcecryptGpgAgent.agentPath, args, false, parent, listener, statusFlagsObj);

    if (!proc) {
      return -1;
    }

    proc.wait();

    const retObj = {};
    IcecryptDecryption.decryptMessageEnd(listener.stderrData, listener.exitCode, 1, true, true, nsIIcecrypt.UI_INTERACTIVE, retObj);

    if (listener.exitCode === 0) {
      const detailArr = retObj.sigDetails.split(/ /);
      const dateTime = IcecryptTime.getDateTime(detailArr[2], true, true);
      const msg1 = retObj.errorMsg.split(/\n/)[0];
      const msg2 = IcecryptLocale.getString("keyAndSigDate", ["0x" + retObj.keyId.substr(-8, 8), dateTime]);
      errorMsgObj.value = msg1 + "\n" + msg2;
    }
    else {
      errorMsgObj.value = retObj.errorMsg;
    }

    return listener.exitCode;
  },

  registerOn: function(target) {
    target.verifyAttachment = IcecryptVerifyAttachment.attachment;
  }
};
