/*global Components: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

var EXPORTED_SYMBOLS = ["IcecryptEvents"];

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;

Cu.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Cu.import("resource://icecrypt/timer.jsm"); /*global IcecryptTimer: false */

/****
 * DEPRECATED - Use timer.jsm instead
 */

const IcecryptEvents = {
  /**
   * dispatch event aynchronously to the main thread
   *
   * @callbackFunction: Function - any function specification
   * @sleepTimeMs:      Number - optional number of miliseconds to delay
   *                             (0 if not specified)
   * @arrayOfArgs:      Array - arguments to pass to callbackFunction
   */
  dispatchEvent: function(callbackFunction, sleepTimeMs = 0, arrayOfArgs) {
    IcecryptLog.DEBUG("icecryptCommon.jsm: dispatchEvent f=" + callbackFunction.name + "\n");

    return IcecryptTimer.setTimeout(function _f() {
      callbackFunction(arrayOfArgs);
    }, sleepTimeMs);
  }
};
