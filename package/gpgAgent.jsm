/*global Components: false, unescape: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

var EXPORTED_SYMBOLS = ["IcecryptGpgAgent"];

const Cu = Components.utils;

Cu.import("resource://gre/modules/ctypes.jsm"); /*global ctypes: false */
Cu.import("resource://icecrypt/subprocess.jsm"); /*global subprocess: false */
Cu.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Cu.import("resource://icecrypt/files.jsm"); /*global IcecryptFiles: false */
Cu.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Cu.import("resource://icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */
Cu.import("resource://icecrypt/os.jsm"); /*global IcecryptOS: false */
Cu.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Cu.import("resource://icecrypt/dialog.jsm"); /*global IcecryptDialog: false */
Cu.import("resource://icecrypt/windows.jsm"); /*global IcecryptWindows: false */
Cu.import("resource://icecrypt/app.jsm"); /*global IcecryptApp: false */
Cu.import("resource://icecrypt/gpg.jsm"); /*global IcecryptGpg: false */
Cu.import("resource://icecrypt/execution.jsm"); /*global IcecryptExecution: false */
Cu.import("resource://icecrypt/passwords.jsm"); /*global IcecryptPassword: false */
Cu.import("resource://icecrypt/system.jsm"); /*global IcecryptSystem: false */
Cu.import("resource://icecrypt/data.jsm"); /*global IcecryptData: false */

const Cc = Components.classes;
const Ci = Components.interfaces;

const nsIIcecrypt = Ci.nsIIcecrypt;

const NS_LOCAL_FILE_CONTRACTID = "@mozilla.org/file/local;1";
const DIR_SERV_CONTRACTID = "@mozilla.org/file/directory_service;1";
const NS_LOCALFILEOUTPUTSTREAM_CONTRACTID = "@mozilla.org/network/file-output-stream;1";

const DEFAULT_FILE_PERMS = 0x180; // equals 0600

// Making this a var makes it possible to test windows things on linux
var nsIWindowsRegKey = Ci.nsIWindowsRegKey;

var gIsGpgAgent = -1;

const DUMMY_AGENT_INFO = "none";

function cloneOrNull(v) {
  if (v && typeof v.clone === "function") {
    return v.clone();
  }
  else {
    return v;
  }
}

function extractAgentInfo(fullStr) {
  if (fullStr) {
    return fullStr.
    replace(/[\r\n]/g, "").
    replace(/^.*=/, "").
    replace(/;.*$/, "");
  }
  else {
    return "";
  }
}

function getHomedirFromParam(param) {
  let i = param.search(/--homedir/);
  if (i >= 0) {
    param = param.substr(i + 9);

    let m = param.match(/^(\s*)([^\\]".+[^\\]")/);
    if (m && m.length > 2) {
      param = m[2].substr(1);
      let j = param.search(/[^\\]"/);
      return param.substr(1, j);
    }

    m = param.match(/^(\s*)([^\\]'.+[^\\]')/);
    if (m && m.length > 2) {
      param = m[2].substr(1);
      let j = param.search(/[^\\]'/);
      return param.substr(1, j);
    }

    m = param.match(/^(\s*)(\S+)/);
    if (m && m.length > 2) {
      return m[2];
    }
  }

  return null;
}

var IcecryptGpgAgent = {
  agentType: "",
  agentPath: null,
  connGpgAgentPath: null,
  gpgconfPath: null,
  gpgAgentInfo: {
    preStarted: false,
    envStr: ""
  },
  gpgAgentProcess: null,
  gpgAgentIsOptional: true,

  isDummy: function() {
    return IcecryptGpgAgent.gpgAgentInfo.envStr === DUMMY_AGENT_INFO;
  },

  useGpgAgent: function() {
    let useAgent = false;

    try {
      if (IcecryptOS.isDosLike() && !IcecryptGpg.getGpgFeature("supports-gpg-agent")) {
        useAgent = false;
      }
      else {
        // gpg version >= 2.0.16 launches gpg-agent automatically
        if (IcecryptGpg.getGpgFeature("autostart-gpg-agent")) {
          useAgent = true;
          IcecryptLog.DEBUG("icecrypt.js: Setting useAgent to " + useAgent + " for gpg2 >= 2.0.16\n");
        }
        else {
          useAgent = (IcecryptGpgAgent.gpgAgentInfo.envStr.length > 0 || IcecryptPrefs.getPrefBranch().getBoolPref("useGpgAgent"));
        }
      }
    }
    catch (ex) {}
    return useAgent;
  },

  resetGpgAgent: function() {
    IcecryptLog.DEBUG("gpgAgent.jsm: resetGpgAgent\n");
    gIsGpgAgent = -1;
  },

  isCmdGpgAgent: function(pid) {
    IcecryptLog.DEBUG("gpgAgent.jsm: isCmdGpgAgent:\n");

    const environment = Cc["@mozilla.org/process/environment;1"].getService(Ci.nsIEnvironment);
    let ret = false;

    let path = environment.get("PATH");
    if (!path || path.length === 0) {
      path = "/bin:/usr/bin:/usr/local/bin";
    }

    const psCmd = IcecryptFiles.resolvePath("ps", path, false);

    const proc = {
      command: psCmd,
      arguments: ["-o", "comm", "-p", pid],
      environment: IcecryptCore.getEnvList(),
      charset: null,
      done: function(result) {
        IcecryptLog.DEBUG("gpgAgent.jsm: isCmdGpgAgent: got data: '" + result.stdout + "'\n");
        var data = result.stdout.replace(/[\r\n]/g, " ");
        if (data.search(/gpg-agent/) >= 0) {
          ret = true;
        }
      }
    };

    try {
      subprocess.call(proc).wait();
    }
    catch (ex) {}

    return ret;

  },

  isAgentTypeGpgAgent: function() {
    // determine if the used agent is a gpg-agent

    IcecryptLog.DEBUG("gpgAgent.jsm: isAgentTypeGpgAgent:\n");

    // to my knowledge there is no other agent than gpg-agent on Windows
    if (IcecryptOS.getOS() == "WINNT") return true;

    if (gIsGpgAgent >= 0) {
      return gIsGpgAgent == 1;
    }

    let pid = -1;
    let exitCode = -1;
    if (!IcecryptCore.getService()) return false;

    const proc = {
      command: IcecryptGpgAgent.connGpgAgentPath,
      arguments: [],
      charset: null,
      environment: IcecryptCore.getEnvList(),
      stdin: function(pipe) {
        pipe.write("/subst\n");
        pipe.write("/serverpid\n");
        pipe.write("/echo pid: ${get serverpid}\n");
        pipe.write("/bye\n");
        pipe.close();
      },
      done: function(result) {
        exitCode = result.exitCode;
        const data = result.stdout.replace(/[\r\n]/g, "");
        if (data.search(/^pid: [0-9]+$/) === 0) {
          pid = data.replace(/^pid: /, "");
        }
      }
    };

    try {
      subprocess.call(proc).wait();
      if (exitCode) pid = -2;
    }
    catch (ex) {}

    IcecryptLog.DEBUG("gpgAgent.jsm: isAgentTypeGpgAgent: pid=" + pid + "\n");

    IcecryptGpgAgent.isCmdGpgAgent(pid);
    let isAgent = false;

    try {
      isAgent = IcecryptGpgAgent.isCmdGpgAgent(pid);
      gIsGpgAgent = isAgent ? 1 : 0;
    }
    catch (ex) {}

    return isAgent;
  },

  getAgentMaxIdle: function() {
    IcecryptLog.DEBUG("gpgAgent.jsm: getAgentMaxIdle:\n");
    let maxIdle = -1;

    if (!IcecryptCore.getService()) return maxIdle;

    const DEFAULT = 7;
    const CFGVALUE = 9;

    const proc = {
      command: IcecryptGpgAgent.gpgconfPath,
      arguments: ["--list-options", "gpg-agent"],
      charset: null,
      environment: IcecryptCore.getEnvList(),
      done: function(result) {
        const lines = result.stdout.split(/[\r\n]/);

        for (let i = 0; i < lines.length; i++) {
          IcecryptLog.DEBUG("gpgAgent.jsm: getAgentMaxIdle: line: " + lines[i] + "\n");

          if (lines[i].search(/^default-cache-ttl:/) === 0) {
            const m = lines[i].split(/:/);
            if (m[CFGVALUE].length === 0) {
              maxIdle = Math.round(m[DEFAULT] / 60);
            }
            else {
              maxIdle = Math.round(m[CFGVALUE] / 60);
            }

            break;
          }
        }
      }
    };

    subprocess.call(proc).wait();
    return maxIdle;
  },

  setAgentMaxIdle: function(idleMinutes) {
    IcecryptLog.DEBUG("gpgAgent.jsm: setAgentMaxIdle:\n");
    if (!IcecryptCore.getService()) return;

    const RUNTIME = 8;

    const proc = {
      command: IcecryptGpgAgent.gpgconfPath,
      arguments: ["--runtime", "--change-options", "gpg-agent"],
      environment: IcecryptCore.getEnvList(),
      charset: null,
      mergeStderr: true,
      stdin: function(pipe) {
        pipe.write("default-cache-ttl:" + RUNTIME + ":" + (idleMinutes * 60) + "\n");
        pipe.write("max-cache-ttl:" + RUNTIME + ":" + (idleMinutes * 600) + "\n");
        pipe.close();
      },
      stdout: function(data) {
        IcecryptLog.DEBUG("gpgAgent.jsm: setAgentMaxIdle.stdout: " + data + "\n");
      },
      done: function(result) {
        IcecryptLog.DEBUG("gpgAgent.jsm: setAgentMaxIdle.stdout: gpgconf exitCode=" + result.exitCode + "\n");
      }
    };

    try {
      subprocess.call(proc);
    }
    catch (ex) {
      IcecryptLog.DEBUG("gpgAgent.jsm: setAgentMaxIdle: exception: " + ex.toString() + "\n");
    }
  },

  getMaxIdlePref: function(win) {
    let maxIdle = IcecryptPrefs.getPref("maxIdleMinutes");

    try {
      if (IcecryptCore.getService(win)) {
        if (IcecryptGpgAgent.gpgconfPath &&
          IcecryptGpgAgent.connGpgAgentPath) {

          if (IcecryptGpgAgent.isAgentTypeGpgAgent()) {
            const m = IcecryptGpgAgent.getAgentMaxIdle();
            if (m > -1) maxIdle = m;
          }
        }
      }
    }
    catch (ex) {}

    return maxIdle;
  },

  setMaxIdlePref: function(minutes) {
    IcecryptPrefs.setPref("maxIdleMinutes", minutes);

    if (IcecryptGpgAgent.isAgentTypeGpgAgent()) {
      try {
        IcecryptGpgAgent.setAgentMaxIdle(minutes);
      }
      catch (ex) {}
    }
  },

  /**
   * Determine the "gpg home dir", i.e. the directory where gpg.conf and the keyring are
   * stored
   *
   * @return String - directory name, or NULL (in case the command did not succeed)
   */
  getGpgHomeDir: function() {


    let param = IcecryptPrefs.getPref("agentAdditionalParam");

    if (param) {
      let hd = getHomedirFromParam(param);

      if (hd) return hd;
    }

    if (IcecryptGpgAgent.gpgconfPath === null) return null;

    const command = IcecryptGpgAgent.gpgconfPath;
    let args = ["--list-dirs"];

    let exitCode = -1;
    let outStr = "";
    IcecryptLog.DEBUG("icecrypt.js: Icecrypt.setAgentPath: calling subprocess with '" + command.path + "'\n");

    IcecryptLog.CONSOLE("icecrypt> " + IcecryptFiles.formatCmdLine(command, args) + "\n");

    const proc = {
      command: command,
      arguments: args,
      environment: IcecryptCore.getEnvList(),
      charset: null,
      done: function(result) {
        exitCode = result.exitCode;
        outStr = result.stdout;
      },
      mergeStderr: false
    };

    try {
      subprocess.call(proc).wait();
    }
    catch (ex) {
      IcecryptLog.ERROR("icecrypt.js: Icecrypt.getGpgHomeDir: subprocess.call failed with '" + ex.toString() + "'\n");
      IcecryptLog.DEBUG("  icecrypt> DONE with FAILURE\n");
      throw ex;
    }

    let m = outStr.match(/^(homedir:)(.*)$/mi);
    if (m && m.length > 2) {
      return IcecryptData.convertGpgToUnicode(unescape(m[2]));
    }

    return null;
  },

  setAgentPath: function(domWindow, esvc) {
    let agentPath = "";
    try {
      agentPath = IcecryptPrefs.getPrefBranch().getCharPref("agentPath");
    }
    catch (ex) {}

    var agentType = "gpg";
    var agentName = "";

    IcecryptGpgAgent.resetGpgAgent();

    if (IcecryptOS.isDosLike()) {
      agentName = "gpg2.exe;gpg.exe;gpg1.exe";
    }
    else {
      agentName = "gpg2;gpg;gpg1";
    }


    if (agentPath) {
      // Locate GnuPG executable

      // Append default .exe extension for DOS-Like systems, if needed
      if (IcecryptOS.isDosLike() && (agentPath.search(/\.\w+$/) < 0)) {
        agentPath += ".exe";
      }

      try {
        let pathDir = Cc[NS_LOCAL_FILE_CONTRACTID].createInstance(Ci.nsIFile);

        if (!IcecryptFiles.isAbsolutePath(agentPath, IcecryptOS.isDosLike())) {
          // path relative to Mozilla installation dir
          const ds = Cc[DIR_SERV_CONTRACTID].getService();
          const dsprops = ds.QueryInterface(Ci.nsIProperties);
          pathDir = dsprops.get("CurProcD", Ci.nsIFile);

          const dirs = agentPath.split(new RegExp(IcecryptOS.isDosLike() ? "\\\\" : "/"));
          for (let i = 0; i < dirs.length; i++) {
            if (dirs[i] != ".") {
              pathDir.append(dirs[i]);
            }
          }
          pathDir.normalize();
        }
        else {
          // absolute path
          IcecryptFiles.initPath(pathDir, agentPath);
        }
        if (!(pathDir.isFile() /* && pathDir.isExecutable()*/ )) {
          throw Components.results.NS_ERROR_FAILURE;
        }
        agentPath = pathDir.QueryInterface(Ci.nsIFile);

      }
      catch (ex) {
        esvc.initializationError = IcecryptLocale.getString("gpgNotFound", [agentPath]);
        IcecryptLog.ERROR("icecrypt.js: Icecrypt.initialize: Error - " + esvc.initializationError + "\n");
        throw Components.results.NS_ERROR_FAILURE;
      }
    }
    else {
      // Resolve relative path using PATH environment variable
      const envPath = esvc.environment.get("PATH");
      agentPath = IcecryptFiles.resolvePath(agentName, envPath, IcecryptOS.isDosLike());

      if (!agentPath && IcecryptOS.isDosLike()) {
        // DOS-like systems: search for GPG in c:\gnupg, c:\gnupg\bin, d:\gnupg, d:\gnupg\bin
        let gpgPath = "c:\\gnupg;c:\\gnupg\\bin;d:\\gnupg;d:\\gnupg\\bin";
        agentPath = IcecryptFiles.resolvePath(agentName, gpgPath, IcecryptOS.isDosLike());
      }

      if ((!agentPath) && IcecryptOS.isWin32) {
        // Look up in Windows Registry
        const installDir = ["Software\\GNU\\GNUPG", "Software\\GNUPG"];
        try {
          for (let i = 0; i < installDir.length && !agentPath; i++) {
            let gpgPath = IcecryptOS.getWinRegistryString(installDir[i], "Install Directory", nsIWindowsRegKey.ROOT_KEY_LOCAL_MACHINE);

            agentPath = IcecryptFiles.resolvePath(agentName, gpgPath, IcecryptOS.isDosLike());
            if (!agentPath) {
              gpgPath += "\\bin";
              agentPath = IcecryptFiles.resolvePath(agentName, gpgPath, IcecryptOS.isDosLike());
            }
          }
        }
        catch (ex) {}

        if (!agentPath) {
          // try to determine the default PATH from the registry
          // (that's how gpg4win sets up the path)
          try {
            let winPath = IcecryptOS.getWinRegistryString("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment", "Path", nsIWindowsRegKey.ROOT_KEY_LOCAL_MACHINE);
            agentPath = IcecryptFiles.resolvePath(agentName, winPath, IcecryptOS.isDosLike());
          }
          catch (ex) {}
        }

        if (!agentPath) {
          // default for gpg4win 3.0
          let gpgPath = "C:\\Program Files\\GnuPG\\bin;C:\\Program Files (x86)\\GnuPG\\bin";
          agentPath = IcecryptFiles.resolvePath(agentName, gpgPath, IcecryptOS.isDosLike());
        }
      }

      if (!agentPath && !IcecryptOS.isDosLike()) {
        // Unix-like systems: check /usr/bin and /usr/local/bin
        let gpgPath = "/usr/bin:/usr/local/bin";
        agentPath = IcecryptFiles.resolvePath(agentName, gpgPath, IcecryptOS.isDosLike());
      }

      if (!agentPath) {
        esvc.initializationError = IcecryptLocale.getString("gpgNotInPath");
        IcecryptLog.ERROR("icecrypt.js: Icecrypt: Error - " + esvc.initializationError + "\n");
        throw Components.results.NS_ERROR_FAILURE;
      }
      agentPath = agentPath.QueryInterface(Ci.nsIFile);
    }

    IcecryptLog.CONSOLE("IcecryptAgentPath=" + IcecryptFiles.getFilePathDesc(agentPath) + "\n\n");

    IcecryptGpgAgent.agentType = agentType;
    IcecryptGpgAgent.agentPath = agentPath;
    IcecryptGpg.setAgentPath(agentPath);
    IcecryptExecution.agentType = agentType;

    const command = agentPath;
    let args = [];
    if (agentType == "gpg") {
      args = ["--version", "--version", "--batch", "--no-tty", "--charset", "utf-8", "--display-charset", "utf-8"];
    }

    let exitCode = -1;
    let outStr = "";
    let errStr = "";
    IcecryptLog.DEBUG("icecrypt.js: Icecrypt.setAgentPath: calling subprocess with '" + command.path + "'\n");

    IcecryptLog.CONSOLE("icecrypt> " + IcecryptFiles.formatCmdLine(command, args) + "\n");

    const proc = {
      command: command,
      arguments: args,
      environment: IcecryptCore.getEnvList(),
      charset: null,
      done: function(result) {
        exitCode = result.exitCode;
        outStr = result.stdout;
        errStr = result.stderr;
      },
      mergeStderr: false
    };

    try {
      subprocess.call(proc).wait();
    }
    catch (ex) {
      IcecryptLog.ERROR("icecrypt.js: Icecrypt.setAgentPath: subprocess.call failed with '" + ex.toString() + "'\n");
      IcecryptLog.DEBUG("  icecrypt> DONE with FAILURE\n");
      throw ex;
    }
    IcecryptLog.DEBUG("  icecrypt> DONE\n");

    outStr = IcecryptSystem.convertNativeToUnicode(outStr);

    if (exitCode !== 0) {
      IcecryptLog.ERROR("icecrypt.js: Icecrypt.setAgentPath: gpg failed with exitCode " + exitCode + " msg='" + outStr + " " + errStr + "'\n");
      throw Components.results.NS_ERROR_FAILURE;
    }

    IcecryptLog.CONSOLE(outStr + "\n");

    // detection for Gpg4Win wrapper
    if (outStr.search(/^gpgwrap.*;/) === 0) {
      const outLines = outStr.split(/[\n\r]+/);
      const firstLine = outLines[0];
      outLines.splice(0, 1);
      outStr = outLines.join("\n");
      agentPath = firstLine.replace(/^.*;[ \t]*/, "");

      IcecryptLog.CONSOLE("gpg4win-gpgwrapper detected; IcecryptAgentPath=" + agentPath + "\n\n");
    }

    const versionParts = outStr.replace(/[\r\n].*/g, "").replace(/ *\(gpg4win.*\)/i, "").split(/ /);
    const gpgVersion = versionParts[versionParts.length - 1];

    IcecryptLog.DEBUG("icecrypt.js: detected GnuPG version '" + gpgVersion + "'\n");
    IcecryptGpg.agentVersion = gpgVersion;

    if (!IcecryptGpg.getGpgFeature("version-supported")) {
      if (!domWindow) {
        domWindow = IcecryptWindows.getBestParentWin();
      }
      IcecryptDialog.alert(domWindow, IcecryptLocale.getString("oldGpgVersion14", [gpgVersion]));
      throw Components.results.NS_ERROR_FAILURE;
    }

    IcecryptGpgAgent.gpgconfPath = IcecryptGpgAgent.resolveToolPath("gpgconf");
    IcecryptGpgAgent.connGpgAgentPath = IcecryptGpgAgent.resolveToolPath("gpg-connect-agent");

    IcecryptLog.DEBUG("icecrypt.js: Icecrypt.setAgentPath: gpgconf found: " + (IcecryptGpgAgent.gpgconfPath ? "yes" : "no") + "\n");
  },

  // resolve the path for GnuPG helper tools
  resolveToolPath: function(fileName) {
    if (IcecryptOS.isDosLike()) {
      fileName += ".exe";
    }

    let filePath = cloneOrNull(IcecryptGpgAgent.agentPath);

    if (filePath) {
      // try to get the install directory of gpg/gpg2 executable
      filePath.normalize();
      filePath = filePath.parent;
    }

    if (filePath) {
      filePath.append(fileName);
      if (filePath.exists()) {
        filePath.normalize();
        return filePath;
      }
    }

    const foundPath = IcecryptFiles.resolvePath(fileName, IcecryptCore.getIcecryptService().environment.get("PATH"), IcecryptOS.isDosLike());
    if (foundPath) {
      foundPath.normalize();
    }
    return foundPath;
  },

  detectGpgAgent: function(domWindow, esvc) {
    IcecryptLog.DEBUG("icecrypt.js: detectGpgAgent\n");

    var gpgAgentInfo = esvc.environment.get("GPG_AGENT_INFO");
    if (gpgAgentInfo && gpgAgentInfo.length > 0) {
      IcecryptLog.DEBUG("icecrypt.js: detectGpgAgent: GPG_AGENT_INFO variable available\n");
      // env. variable suggests running gpg-agent
      IcecryptGpgAgent.gpgAgentInfo.preStarted = true;
      IcecryptGpgAgent.gpgAgentInfo.envStr = gpgAgentInfo;
      IcecryptGpgAgent.gpgAgentIsOptional = false;
    }
    else {
      IcecryptLog.DEBUG("icecrypt.js: detectGpgAgent: no GPG_AGENT_INFO variable set\n");
      IcecryptGpgAgent.gpgAgentInfo.preStarted = false;

      var command = null;
      var outStr = "";
      var errorStr = "";
      var exitCode = -1;
      IcecryptGpgAgent.gpgAgentIsOptional = false;
      if (IcecryptGpg.getGpgFeature("autostart-gpg-agent")) {
        IcecryptLog.DEBUG("icecrypt.js: detectGpgAgent: gpg 2.0.16 or newer - not starting agent\n");
      }
      else {
        if (IcecryptGpgAgent.connGpgAgentPath && IcecryptGpgAgent.connGpgAgentPath.isExecutable()) {
          // try to connect to a running gpg-agent

          IcecryptLog.DEBUG("icecrypt.js: detectGpgAgent: gpg-connect-agent is executable\n");

          IcecryptGpgAgent.gpgAgentInfo.envStr = DUMMY_AGENT_INFO;

          command = IcecryptGpgAgent.connGpgAgentPath.QueryInterface(Ci.nsIFile);

          IcecryptLog.CONSOLE("icecrypt> " + command.path + "\n");

          try {
            subprocess.call({
              command: command,
              environment: IcecryptCore.getEnvList(),
              stdin: "/echo OK\n",
              charset: null,
              done: function(result) {
                IcecryptLog.DEBUG("detectGpgAgent detection terminated with " + result.exitCode + "\n");
                exitCode = result.exitCode;
                outStr = result.stdout;
                errorStr = result.stderr;
                if (result.stdout.substr(0, 2) == "OK") exitCode = 0;
              },
              mergeStderr: false
            }).wait();
          }
          catch (ex) {
            IcecryptLog.ERROR("icecrypt.js: detectGpgAgent: " + command.path + " failed\n");
            IcecryptLog.DEBUG("  icecrypt> DONE with FAILURE\n");
            exitCode = -1;
          }
          IcecryptLog.DEBUG("  icecrypt> DONE\n");

          if (exitCode === 0) {
            IcecryptLog.DEBUG("icecrypt.js: detectGpgAgent: found running gpg-agent\n");
            return;
          }
          else {
            IcecryptLog.DEBUG("icecrypt.js: detectGpgAgent: no running gpg-agent. Output='" + outStr + "' error text='" + errorStr + "'\n");
          }

        }

        // and finally try to start gpg-agent
        var commandFile = IcecryptGpgAgent.resolveToolPath("gpg-agent");
        var agentProcess = null;

        if ((!commandFile) || (!commandFile.exists())) {
          commandFile = IcecryptGpgAgent.resolveToolPath("gpg-agent2");
        }

        if (commandFile && commandFile.exists()) {
          command = commandFile.QueryInterface(Ci.nsIFile);
        }

        if (command === null) {
          IcecryptLog.ERROR("icecrypt.js: detectGpgAgent: gpg-agent not found\n");
          IcecryptDialog.alert(domWindow, IcecryptLocale.getString("gpgAgentNotStarted", [IcecryptGpg.agentVersion]));
          throw Components.results.NS_ERROR_FAILURE;
        }
      }

      if ((!IcecryptOS.isDosLike()) && (!IcecryptGpg.getGpgFeature("autostart-gpg-agent"))) {

        // create unique tmp file
        var ds = Cc[DIR_SERV_CONTRACTID].getService();
        var dsprops = ds.QueryInterface(Ci.nsIProperties);
        var tmpFile = dsprops.get("TmpD", Ci.nsIFile);
        tmpFile.append("gpg-wrapper.tmp");
        tmpFile.createUnique(tmpFile.NORMAL_FILE_TYPE, DEFAULT_FILE_PERMS);
        let args = [command.path,
          tmpFile.path,
          "--sh", "--no-use-standard-socket",
          "--daemon",
          "--default-cache-ttl", (IcecryptPassword.getMaxIdleMinutes() * 60).toString(),
          "--max-cache-ttl", "999999"
        ]; // ca. 11 days

        try {
          var process = Cc["@mozilla.org/process/util;1"].createInstance(Ci.nsIProcess);
          var exec = IcecryptApp.getInstallLocation().clone();
          exec.append("wrappers");
          exec.append("gpg-agent-wrapper.sh");
          process.init(exec);
          process.run(true, args, args.length);

          if (!tmpFile.exists()) {
            IcecryptLog.ERROR("icecrypt.js: detectGpgAgent no temp file created\n");
          }
          else {
            outStr = IcecryptFiles.readFile(tmpFile);
            tmpFile.remove(false);
            exitCode = 0;
          }
        }
        catch (ex) {
          IcecryptLog.ERROR("icecrypt.js: detectGpgAgent: failed with '" + ex + "'\n");
          exitCode = -1;
        }

        if (exitCode === 0) {
          IcecryptGpgAgent.gpgAgentInfo.envStr = extractAgentInfo(outStr);
          IcecryptLog.DEBUG("icecrypt.js: detectGpgAgent: started -> " + IcecryptGpgAgent.gpgAgentInfo.envStr + "\n");
          IcecryptGpgAgent.gpgAgentProcess = IcecryptGpgAgent.gpgAgentInfo.envStr.split(":")[1];
        }
        else {
          IcecryptLog.ERROR("icecrypt.js: detectGpgAgent: gpg-agent output: " + outStr + "\n");
          IcecryptDialog.alert(domWindow, IcecryptLocale.getString("gpgAgentNotStarted", [IcecryptGpg.agentVersion]));
          throw Components.results.NS_ERROR_FAILURE;
        }
      }
      else {
        IcecryptGpgAgent.gpgAgentInfo.envStr = DUMMY_AGENT_INFO;
        var envFile = Components.classes[NS_LOCAL_FILE_CONTRACTID].createInstance(Ci.nsIFile);
        IcecryptFiles.initPath(envFile, IcecryptGpgAgent.determineGpgHomeDir(esvc));
        envFile.append("gpg-agent.conf");

        var data = "default-cache-ttl " + (IcecryptPassword.getMaxIdleMinutes() * 60) + "\n";
        data += "max-cache-ttl 999999";
        if (!envFile.exists()) {
          try {
            var flags = 0x02 | 0x08 | 0x20;
            var fileOutStream = Cc[NS_LOCALFILEOUTPUTSTREAM_CONTRACTID].createInstance(Ci.nsIFileOutputStream);
            fileOutStream.init(envFile, flags, 384, 0); // 0600
            fileOutStream.write(data, data.length);
            fileOutStream.flush();
            fileOutStream.close();
          }
          catch (ex) {} // ignore file write errors
        }
      }
    }
    IcecryptLog.DEBUG("icecrypt.js: detectGpgAgent: GPG_AGENT_INFO='" + IcecryptGpgAgent.gpgAgentInfo.envStr + "'\n");
  },

  determineGpgHomeDir: function(esvc) {
    let homeDir = esvc.environment.get("GNUPGHOME");

    if (!homeDir && IcecryptOS.isWin32) {
      homeDir = IcecryptOS.getWinRegistryString("Software\\GNU\\GNUPG", "HomeDir", nsIWindowsRegKey.ROOT_KEY_CURRENT_USER);

      if (!homeDir) {
        homeDir = esvc.environment.get("USERPROFILE") || esvc.environment.get("SystemRoot");

        if (homeDir) homeDir += "\\Application Data\\GnuPG";
      }

      if (!homeDir) homeDir = "C:\\gnupg";
    }

    if (!homeDir) homeDir = esvc.environment.get("HOME") + "/.gnupg";

    return homeDir;
  },

  finalize: function() {
    if (IcecryptGpgAgent.gpgAgentProcess) {
      IcecryptLog.DEBUG("gpgAgent.jsm: IcecryptGpgAgent.finalize: stopping gpg-agent\n");
      try {
        const proc = {
          command: IcecryptGpgAgent.connGpgAgentPath,
          arguments: [],
          charset: null,
          environment: IcecryptCore.getEnvList(),
          stdin: function(pipe) {
            pipe.write("killagent\n");
            pipe.write("/bye\n");
            pipe.close();
          }
        };

        subprocess.call(proc).wait();
      }
      catch (ex) {
        IcecryptLog.ERROR("gpgAgent.jsm: IcecryptGpgAgent.finalize ERROR: " + ex + "\n");
      }
    }
  }
};
