/*global Components: false, IcecryptDecryptPermanently: false, IcecryptCore: false, IcecryptLog: false, IcecryptLocale: false, IcecryptDialog: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

var EXPORTED_SYMBOLS = ["IcecryptFilters"];

Components.utils.import("resource://icecrypt/core.jsm");
Components.utils.import("resource://icecrypt/decryptPermanently.jsm");
Components.utils.import("resource://icecrypt/log.jsm");
Components.utils.import("resource://icecrypt/locale.jsm");
Components.utils.import("resource://icecrypt/dialog.jsm");

const Cc = Components.classes;
const Ci = Components.interfaces;

/********************************************************************************
 Filter actions for decrypting messages permanently
 ********************************************************************************/

/**
 * filter action for creating a decrypted version of the mail and
 * deleting the original mail at the same time
 */

const filterActionMoveDecrypt = {
  id: "icecrypt@icecrypt.net#filterActionMoveDecrypt",
  name: IcecryptLocale.getString("filter.decryptMove.label"),
  value: "movemessage",
  apply: function(aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {

    IcecryptLog.DEBUG("icecrypt.js: filterActionMoveDecrypt: Move to: " + aActionValue + "\n");

    var msgHdrs = [];

    for (var i = 0; i < aMsgHdrs.length; i++) {
      msgHdrs.push(aMsgHdrs.queryElementAt(i, Ci.nsIMsgDBHdr));
    }

    IcecryptDecryptPermanently.dispatchMessages(msgHdrs, aActionValue, true, true);

    return;
  },

  isValidForType: function(type, scope) {
    return true;
  },

  validateActionValue: function(value, folder, type) {
    IcecryptDialog.alert(null, IcecryptLocale.getString("filter.decryptMove.warnExperimental"));

    if (value === "") {
      return IcecryptLocale.getString("filter.folderRequired");
    }

    return null;
  },

  allowDuplicates: false,
  isAsync: false,
  needsBody: true
};

/**
 * filter action for creating a decrypted copy of the mail, leaving the original
 * message untouched
 */
const filterActionCopyDecrypt = {
  id: "icecrypt@icecrypt.net#filterActionCopyDecrypt",
  name: IcecryptLocale.getString("filter.decryptCopy.label"),
  value: "copymessage",
  apply: function(aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
    IcecryptLog.DEBUG("icecrypt.js: filterActionCopyDecrypt: Copy to: " + aActionValue + "\n");

    var msgHdrs = [];

    for (var i = 0; i < aMsgHdrs.length; i++) {
      msgHdrs.push(aMsgHdrs.queryElementAt(i, Ci.nsIMsgDBHdr));
    }

    IcecryptDecryptPermanently.dispatchMessages(msgHdrs, aActionValue, false, true);
    return;
  },

  isValidForType: function(type, scope) {
    return true;
  },

  validateActionValue: function(value, folder, type) {
    if (value === "") {
      return IcecryptLocale.getString("filter.folderRequired");
    }

    return null;
  },

  allowDuplicates: false,
  isAsync: false,
  needsBody: true
};

const IcecryptFilters = {
  registerAll: function() {
    var filterService = Cc["@mozilla.org/messenger/services/filters;1"].getService(Ci.nsIMsgFilterService);
    filterService.addCustomAction(filterActionMoveDecrypt);
    filterService.addCustomAction(filterActionCopyDecrypt);
  }
};
