/*global Components: false, IcecryptCore: false, IcecryptLog: false, IcecryptPrefs: false, IcecryptApp: false, IcecryptLocale: false, IcecryptDialog: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

var EXPORTED_SYMBOLS = ["IcecryptEncryption"];

Components.utils.import("resource://icecrypt/core.jsm");
Components.utils.import("resource://icecrypt/data.jsm"); /*global IcecryptData: false */
Components.utils.import("resource://icecrypt/log.jsm");
Components.utils.import("resource://icecrypt/prefs.jsm");
Components.utils.import("resource://icecrypt/app.jsm");
Components.utils.import("resource://icecrypt/locale.jsm");
Components.utils.import("resource://icecrypt/dialog.jsm");
Components.utils.import("resource://icecrypt/gpgAgent.jsm"); /*global IcecryptGpgAgent: false */
Components.utils.import("resource://icecrypt/gpg.jsm"); /*global IcecryptGpg: false */
Components.utils.import("resource://icecrypt/errorHandling.jsm"); /*global IcecryptErrorHandling: false */
Components.utils.import("resource://icecrypt/execution.jsm"); /*global IcecryptExecution: false */
Components.utils.import("resource://icecrypt/files.jsm"); /*global IcecryptFiles: false */
Components.utils.import("resource://icecrypt/passwords.jsm"); /*global IcecryptPassword: false */
Components.utils.import("resource://icecrypt/funcs.jsm"); /*global IcecryptFuncs: false */
Components.utils.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */

const Cc = Components.classes;
const Ci = Components.interfaces;
const nsIIcecrypt = Ci.nsIIcecrypt;

var EC = IcecryptCore;

const gMimeHashAlgorithms = [null, "sha1", "ripemd160", "sha256", "sha384", "sha512", "sha224", "md5"];

const ENC_TYPE_MSG = 0;
const ENC_TYPE_ATTACH_BINARY = 1;
const ENC_TYPE_ATTACH_ASCII = 2;

const GPG_COMMENT_OPT = "Using GnuPG.";


const IcecryptEncryption = {
  getEncryptCommand: function(fromMailAddr, toMailAddr, bccMailAddr, hashAlgorithm, sendFlags, isAscii, errorMsgObj) {
    IcecryptLog.DEBUG("encryption.jsm: getEncryptCommand: hashAlgorithm=" + hashAlgorithm + "\n");

    try {
      fromMailAddr = IcecryptFuncs.stripEmail(fromMailAddr);
      toMailAddr = IcecryptFuncs.stripEmail(toMailAddr);
      bccMailAddr = IcecryptFuncs.stripEmail(bccMailAddr);

    }
    catch (ex) {
      errorMsgObj.value = IcecryptLocale.getString("invalidEmail");
      return null;
    }

    var defaultSend = sendFlags & nsIIcecrypt.SEND_DEFAULT;
    var signMsg = sendFlags & nsIIcecrypt.SEND_SIGNED;
    var encryptMsg = sendFlags & nsIIcecrypt.SEND_ENCRYPTED;
    var usePgpMime = sendFlags & nsIIcecrypt.SEND_PGP_MIME;

    var useDefaultComment = false;
    try {
      useDefaultComment = IcecryptPrefs.getPref("useDefaultComment");
    }
    catch (ex) {}

    var hushMailSupport = false;
    try {
      hushMailSupport = IcecryptPrefs.getPref("hushMailSupport");
    }
    catch (ex) {}

    var detachedSig = (usePgpMime || (sendFlags & nsIIcecrypt.SEND_ATTACHMENT)) && signMsg && !encryptMsg;

    var toAddrList = toMailAddr.split(/\s*,\s*/);
    var bccAddrList = bccMailAddr.split(/\s*,\s*/);
    var k;

    var encryptArgs = IcecryptGpg.getStandardArgs(true);

    if (!useDefaultComment)
      encryptArgs = encryptArgs.concat(["--comment", GPG_COMMENT_OPT.replace(/%s/, IcecryptApp.getName())]);

    var angledFromMailAddr = ((fromMailAddr.search(/^0x/) === 0) || hushMailSupport) ?
      fromMailAddr : "<" + fromMailAddr + ">";
    angledFromMailAddr = angledFromMailAddr.replace(/(["'`])/g, "\\$1");

    if (signMsg && hashAlgorithm) {
      encryptArgs = encryptArgs.concat(["--digest-algo", hashAlgorithm]);
    }

    if (encryptMsg) {
      switch (isAscii) {
        case ENC_TYPE_MSG:
          encryptArgs.push("-a");
          encryptArgs.push("-t");
          break;
        case ENC_TYPE_ATTACH_ASCII:
          encryptArgs.push("-a");
      }

      encryptArgs.push("--encrypt");

      if (signMsg)
        encryptArgs.push("--sign");

      if (sendFlags & nsIIcecrypt.SEND_ALWAYS_TRUST) {
        encryptArgs.push("--trust-model");
        encryptArgs.push("always");
      }
      if ((sendFlags & nsIIcecrypt.SEND_ENCRYPT_TO_SELF) && fromMailAddr)
        encryptArgs = encryptArgs.concat(["--encrypt-to", angledFromMailAddr]);

      for (k = 0; k < toAddrList.length; k++) {
        toAddrList[k] = toAddrList[k].replace(/'/g, "\\'");
        if (toAddrList[k].length > 0) {
          encryptArgs.push("-r");
          if (toAddrList[k].search(/^GROUP:/) === 0) {
            // groups from gpg.conf file
            encryptArgs.push(toAddrList[k].substr(6));
          }
          else {
            encryptArgs.push((hushMailSupport || (toAddrList[k].search(/^0x/) === 0)) ? toAddrList[k] : "<" + toAddrList[k] + ">");
          }
        }
      }

      for (k = 0; k < bccAddrList.length; k++) {
        bccAddrList[k] = bccAddrList[k].replace(/'/g, "\\'");
        if (bccAddrList[k].length > 0) {
          encryptArgs.push("--hidden-recipient");
          encryptArgs.push((hushMailSupport || (bccAddrList[k].search(/^0x/) === 0)) ? bccAddrList[k] : "<" + bccAddrList[k] + ">");
        }
      }

    }
    else if (detachedSig) {
      encryptArgs = encryptArgs.concat(["-s", "-b"]);

      switch (isAscii) {
        case ENC_TYPE_MSG:
          encryptArgs = encryptArgs.concat(["-a", "-t"]);
          break;
        case ENC_TYPE_ATTACH_ASCII:
          encryptArgs.push("-a");
      }

    }
    else if (signMsg) {
      encryptArgs = encryptArgs.concat(["-t", "--clearsign"]);
    }

    if (fromMailAddr) {
      encryptArgs = encryptArgs.concat(["-u", angledFromMailAddr]);
    }

    return encryptArgs;
  },

  /**
   * Determine if the sender key ID or user ID can be used for signing and/or encryption
   *
   * @param sendFlags:    Number  - the send Flags; need to contain SEND_SIGNED and/or SEND_ENCRYPTED
   * @param fromMailAddr: String  - the sender email address or key ID
   *
   * @return Object:
   *         - keyId:    String - the found key ID, or null if fromMailAddr is not valid
   *         - errorMsg: String - the erorr message if key not valid, or null if key is valid
   */
  determineOwnKeyUsability: function(sendFlags, fromMailAddr) {
    IcecryptLog.DEBUG("encryption.jsm: determineOwnKeyUsability: sendFlags=" + sendFlags + ", sender=" + fromMailAddr + "\n");

    let keyList = [];
    let ret = {
      keyId: null,
      errorMsg: null
    };

    let sign = (sendFlags & nsIIcecrypt.SEND_SIGNED ? true : false);
    let encrypt = (sendFlags & nsIIcecrypt.SEND_ENCRYPTED ? true : false);

    if (fromMailAddr.search(/^(0x)?[A-Z0-9]+$/) === 0) {
      // key ID specified
      let key = IcecryptKeyRing.getKeyById(fromMailAddr);
      keyList.push(key);
    }
    else {
      // email address specified
      keyList = IcecryptKeyRing.getKeysByUserId(fromMailAddr);
    }

    if (keyList.length === 0) {
      ret.errorMsg = IcecryptLocale.getString("errorOwnKeyUnusable", fromMailAddr);
      return ret;
    }

    if (sign) {
      keyList = keyList.reduce(function _f(p, keyObj) {
        if (keyObj.getSigningValidity().keyValid) p.push(keyObj);
        return p;
      }, []);
    }

    if (encrypt) {
      keyList = keyList.reduce(function _f(p, keyObj) {
        if (keyObj && keyObj.getEncryptionValidity().keyValid) p.push(keyObj);
        return p;
      }, []);
    }

    if (keyList.length === 0) {
      if (sign) {
        ret.errorMsg = IcecryptErrorHandling.determineInvSignReason(fromMailAddr);
      }
      else {
        ret.errorMsg = IcecryptErrorHandling.determineInvRcptReason(fromMailAddr);
      }
    }
    else {
      // TODO: use better algorithm
      ret.keyId = keyList[0].fpr;
    }

    return ret;
  },

  encryptMessageStart: function(win, uiFlags, fromMailAddr, toMailAddr, bccMailAddr,
    hashAlgorithm, sendFlags, listener, statusFlagsObj, errorMsgObj) {
    IcecryptLog.DEBUG("encryption.jsm: encryptMessageStart: uiFlags=" + uiFlags + ", from " + fromMailAddr + " to " + toMailAddr + ", hashAlgorithm=" + hashAlgorithm + " (" + IcecryptData.bytesToHex(
      IcecryptData.pack(sendFlags, 4)) + ")\n");

    let keyUseability = this.determineOwnKeyUsability(sendFlags, fromMailAddr);

    if (!keyUseability.keyId) {
      IcecryptLog.DEBUG("encryption.jsm: encryptMessageStart: own key invalid\n");
      errorMsgObj.value = keyUseability.errorMsg;
      statusFlagsObj.value = nsIIcecrypt.INVALID_RECIPIENT | nsIIcecrypt.NO_SECKEY | nsIIcecrypt.DISPLAY_MESSAGE;

      return null;
    }
    // TODO: else - use the found key ID

    var pgpMime = uiFlags & nsIIcecrypt.UI_PGP_MIME;

    var hashAlgo = gMimeHashAlgorithms[IcecryptPrefs.getPref("mimeHashAlgorithm")];

    if (hashAlgorithm) {
      hashAlgo = hashAlgorithm;
    }

    errorMsgObj.value = "";

    if (!sendFlags) {
      IcecryptLog.DEBUG("encryption.jsm: encryptMessageStart: NO ENCRYPTION!\n");
      errorMsgObj.value = IcecryptLocale.getString("notRequired");
      return null;
    }

    if (!IcecryptCore.getService(win)) {
      IcecryptLog.ERROR("encryption.jsm: encryptMessageStart: not yet initialized\n");
      errorMsgObj.value = IcecryptLocale.getString("notInit");
      return null;
    }

    var encryptArgs = IcecryptEncryption.getEncryptCommand(fromMailAddr, toMailAddr, bccMailAddr, hashAlgo, sendFlags, ENC_TYPE_MSG, errorMsgObj);
    if (!encryptArgs)
      return null;

    var signMsg = sendFlags & nsIIcecrypt.SEND_SIGNED;

    var proc = IcecryptExecution.execStart(IcecryptGpgAgent.agentPath, encryptArgs, signMsg, win, listener, statusFlagsObj);

    if (statusFlagsObj.value & nsIIcecrypt.MISSING_PASSPHRASE) {
      IcecryptLog.ERROR("encryption.jsm: encryptMessageStart: Error - no passphrase supplied\n");

      errorMsgObj.value = "";
    }

    if (pgpMime && errorMsgObj.value) {
      IcecryptDialog.alert(win, errorMsgObj.value);
    }

    return proc;
  },

  encryptMessageEnd: function(fromMailAddr, stderrStr, exitCode, uiFlags, sendFlags, outputLen, retStatusObj) {
    IcecryptLog.DEBUG("encryption.jsm: encryptMessageEnd: uiFlags=" + uiFlags + ", sendFlags=" + IcecryptData.bytesToHex(IcecryptData.pack(sendFlags, 4)) + ", outputLen=" + outputLen + "\n");

    var pgpMime = uiFlags & nsIIcecrypt.UI_PGP_MIME;
    var defaultSend = sendFlags & nsIIcecrypt.SEND_DEFAULT;
    var signMsg = sendFlags & nsIIcecrypt.SEND_SIGNED;
    var encryptMsg = sendFlags & nsIIcecrypt.SEND_ENCRYPTED;

    retStatusObj.statusFlags = 0;
    retStatusObj.errorMsg = "";
    retStatusObj.blockSeparation = "";

    if (!IcecryptCore.getService().initialized) {
      IcecryptLog.ERROR("encryption.jsm: encryptMessageEnd: not yet initialized\n");
      retStatusObj.errorMsg = IcecryptLocale.getString("notInit");
      return -1;
    }

    IcecryptErrorHandling.parseErrorOutput(stderrStr, retStatusObj);

    exitCode = IcecryptExecution.fixExitCode(exitCode, retStatusObj);
    if ((exitCode === 0) && !outputLen) {
      exitCode = -1;
    }

    if (exitCode !== 0 && (signMsg || encryptMsg)) {
      // GnuPG might return a non-zero exit code, even though the message was correctly
      // signed or encryped -> try to fix the exit code

      var correctedExitCode = 0;
      if (signMsg) {
        if (!(retStatusObj.statusFlags & nsIIcecrypt.SIG_CREATED)) correctedExitCode = exitCode;
      }
      if (encryptMsg) {
        if (!(retStatusObj.statusFlags & nsIIcecrypt.END_ENCRYPTION)) correctedExitCode = exitCode;
      }
      exitCode = correctedExitCode;
    }

    IcecryptLog.DEBUG("encryption.jsm: encryptMessageEnd: command execution exit code: " + exitCode + "\n");

    if (retStatusObj.statusFlags & nsIIcecrypt.DISPLAY_MESSAGE) {
      if (retStatusObj.extendedStatus.search(/\bdisp:/) >= 0) {
        retStatusObj.errorMsg = retStatusObj.statusMsg;
      }
      else {
        if (fromMailAddr.search(/^0x/) === 0) {
          fromMailAddr = fromMailAddr.substr(2);
        }
        if (fromMailAddr.search(/^[A-F0-9]{8,40}$/i) === 0) {
          fromMailAddr = "[A-F0-9]+" + fromMailAddr;
        }

        let s = new RegExp("^(\\[GNUPG:\\] )?INV_(RECP|SGNR) [0-9]+ (\\<|0x)?" + fromMailAddr + "\\>?", "m");
        if (retStatusObj.statusMsg.search(s) >= 0) {
          retStatusObj.errorMsg += "\n\n" + IcecryptLocale.getString("keyError.resolutionAction");
        }
        else if (retStatusObj.statusMsg.length > 0) {
          retStatusObj.errorMsg = retStatusObj.statusMsg;
        }
      }
    }
    else if (retStatusObj.statusFlags & nsIIcecrypt.INVALID_RECIPIENT) {
      retStatusObj.errorMsg = retStatusObj.statusMsg;
    }
    else if (exitCode !== 0) {
      retStatusObj.errorMsg = IcecryptLocale.getString("badCommand");
    }

    return exitCode;
  },

  encryptMessage: function(parent, uiFlags, plainText, fromMailAddr, toMailAddr, bccMailAddr, sendFlags,
    exitCodeObj, statusFlagsObj, errorMsgObj) {
    IcecryptLog.DEBUG("icecrypt.js: Icecrypt.encryptMessage: " + plainText.length + " bytes from " + fromMailAddr + " to " + toMailAddr + " (" + sendFlags + ")\n");

    exitCodeObj.value = -1;
    statusFlagsObj.value = 0;
    errorMsgObj.value = "";

    if (!plainText) {
      IcecryptLog.DEBUG("icecrypt.js: Icecrypt.encryptMessage: NO ENCRYPTION!\n");
      exitCodeObj.value = 0;
      IcecryptLog.DEBUG("  <=== encryptMessage()\n");
      return plainText;
    }

    var defaultSend = sendFlags & nsIIcecrypt.SEND_DEFAULT;
    var signMsg = sendFlags & nsIIcecrypt.SEND_SIGNED;
    var encryptMsg = sendFlags & nsIIcecrypt.SEND_ENCRYPTED;

    if (encryptMsg) {
      // First convert all linebreaks to newlines
      plainText = plainText.replace(/\r\n/g, "\n");
      plainText = plainText.replace(/\r/g, "\n");

      // we need all data in CRLF according to RFC 4880
      plainText = plainText.replace(/\n/g, "\r\n");
    }

    var inspector = Cc["@mozilla.org/jsinspector;1"].createInstance(Ci.nsIJSInspector);

    var listener = IcecryptExecution.newSimpleListener(
      function _stdin(pipe) {
        pipe.write(plainText);
        pipe.close();
      },
      function _done(exitCode) {
        // unlock wait
        if (inspector.eventLoopNestLevel > 0) {
          inspector.exitNestedEventLoop();
        }
      });


    var proc = IcecryptEncryption.encryptMessageStart(parent, uiFlags,
      fromMailAddr, toMailAddr, bccMailAddr,
      null, sendFlags,
      listener, statusFlagsObj, errorMsgObj);
    if (!proc) {
      exitCodeObj.value = -1;
      IcecryptLog.DEBUG("  <=== encryptMessage()\n");
      return "";
    }

    // Wait for child pipes to close
    inspector.enterNestedEventLoop(0);

    var retStatusObj = {};
    exitCodeObj.value = IcecryptEncryption.encryptMessageEnd(fromMailAddr, IcecryptData.getUnicodeData(listener.stderrData), listener.exitCode,
      uiFlags, sendFlags,
      listener.stdoutData.length,
      retStatusObj);

    statusFlagsObj.value = retStatusObj.statusFlags;
    statusFlagsObj.statusMsg = retStatusObj.statusMsg;
    errorMsgObj.value = retStatusObj.errorMsg;


    if ((exitCodeObj.value === 0) && listener.stdoutData.length === 0)
      exitCodeObj.value = -1;

    if (exitCodeObj.value === 0) {
      // Normal return
      IcecryptLog.DEBUG("  <=== encryptMessage()\n");
      return IcecryptData.getUnicodeData(listener.stdoutData);
    }

    // Error processing
    IcecryptLog.DEBUG("icecrypt.js: Icecrypt.encryptMessage: command execution exit code: " + exitCodeObj.value + "\n");
    return "";
  },

  encryptAttachment: function(parent, fromMailAddr, toMailAddr, bccMailAddr, sendFlags, inFile, outFile,
    exitCodeObj, statusFlagsObj, errorMsgObj) {
    IcecryptLog.DEBUG("encryption.jsm: IcecryptEncryption.encryptAttachment infileName=" + inFile.path + "\n");

    statusFlagsObj.value = 0;
    sendFlags |= nsIIcecrypt.SEND_ATTACHMENT;

    let asciiArmor = false;
    try {
      asciiArmor = IcecryptPrefs.getPrefBranch().getBoolPref("inlineAttachAsciiArmor");
    }
    catch (ex) {}

    const asciiFlags = (asciiArmor ? ENC_TYPE_ATTACH_ASCII : ENC_TYPE_ATTACH_BINARY);
    let args = IcecryptEncryption.getEncryptCommand(fromMailAddr, toMailAddr, bccMailAddr, "", sendFlags, asciiFlags, errorMsgObj);

    if (!args) {
      return null;
    }

    const signMessage = (sendFlags & nsIIcecrypt.SEND_SIGNED);

    if (signMessage) {
      args = args.concat(IcecryptPassword.command());
    }

    const inFilePath = IcecryptFiles.getEscapedFilename(IcecryptFiles.getFilePathReadonly(inFile.QueryInterface(Ci.nsIFile)));
    const outFilePath = IcecryptFiles.getEscapedFilename(IcecryptFiles.getFilePathReadonly(outFile.QueryInterface(Ci.nsIFile)));

    args = args.concat(["--yes", "-o", outFilePath, inFilePath]);

    let cmdErrorMsgObj = {};

    const msg = IcecryptExecution.execCmd(IcecryptGpgAgent.agentPath, args, "", exitCodeObj, statusFlagsObj, {}, cmdErrorMsgObj);
    if (exitCodeObj.value !== 0) {
      if (cmdErrorMsgObj.value) {
        errorMsgObj.value = IcecryptFiles.formatCmdLine(IcecryptGpgAgent.agentPath, args);
        errorMsgObj.value += "\n" + cmdErrorMsgObj.value;
      }
      else {
        errorMsgObj.value = "An unknown error has occurred";
      }

      return "";
    }

    return msg;
  },

  registerOn: function(target) {
    target.encryptMessage = IcecryptEncryption.encryptMessage;
    target.encryptAttachment = IcecryptEncryption.encryptAttachment;
  }
};
