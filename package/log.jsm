/*global Components: false, IcecryptConsole: false, dump: false, IcecryptFiles: false, IcecryptOS: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

var EXPORTED_SYMBOLS = ["IcecryptLog"];

Components.utils.import("resource://icecrypt/pipeConsole.jsm");
Components.utils.import("resource://icecrypt/files.jsm");
Components.utils.import("resource://icecrypt/os.jsm");

const Cc = Components.classes;
const Ci = Components.interfaces;

const XPCOM_APPINFO = "@mozilla.org/xre/app-info;1";
const NS_IOSERVICE_CONTRACTID = "@mozilla.org/network/io-service;1";


const IcecryptLog = {
  level: 3,
  data: null,
  directory: null,
  fileStream: null,

  setLogLevel: function(newLogLevel) {
    IcecryptLog.level = newLogLevel;
  },

  getLogLevel: function() {
    return IcecryptLog.level;
  },

  setLogDirectory: function(newLogDirectory) {
    IcecryptLog.directory = newLogDirectory + (IcecryptOS.isDosLike() ? "\\" : "/");
    IcecryptLog.createLogFiles();
  },

  createLogFiles: function() {
    if (IcecryptLog.directory && (!IcecryptLog.fileStream) && IcecryptLog.level >= 5) {
      IcecryptLog.fileStream = IcecryptFiles.createFileStream(IcecryptLog.directory + "enigdbug.txt");
    }
  },

  onShutdown: function() {
    if (IcecryptLog.fileStream) {
      IcecryptLog.fileStream.close();
    }
    IcecryptLog.fileStream = null;
  },

  getLogData: function(version, prefs) {
    let ioServ = Cc[NS_IOSERVICE_CONTRACTID].getService(Ci.nsIIOService);

    let oscpu = "";
    let platform = "";

    try {
      let httpHandler = ioServ.getProtocolHandler("http");
      httpHandler = httpHandler.QueryInterface(Ci.nsIHttpProtocolHandler);
      oscpu = httpHandler.oscpu;
      platform = httpHandler.platform;
    }
    catch (ex) {}

    let data = "Icecrypt version " + version + "\n" +
      "OS/CPU=" + oscpu + "\n" +
      "Platform=" + platform + "\n" +
      "Non-default preference values:\n";

    let p = prefs.getPrefBranch().getChildList("");

    for (let i in p) {
      if (prefs.getPrefBranch().prefHasUserValue(p[i])) {
        data += p[i] + ": " + prefs.getPref(p[i]) + "\n";
      }
    }

    let otherPref = ["dom.workers.maxPerDomain"];
    let root = prefs.getPrefRoot();
    for (let op of otherPref) {
      try {
        data += op + ": " + root.getIntPref(op) + "\n";
      }
      catch (ex) {
        data += ex.toString() + "\n";
      }
    }
    return data + "\n" + IcecryptLog.data;
  },

  WRITE: function(str) {
    function withZeroes(val, digits) {
      return ("0000" + val.toString()).substr(-digits);
    }

    var d = new Date();
    var datStr = d.getFullYear() + "-" + withZeroes(d.getMonth() + 1, 2) + "-" + withZeroes(d.getDate(), 2) + " " + withZeroes(d.getHours(), 2) + ":" + withZeroes(d.getMinutes(), 2) + ":" +
      withZeroes(d.getSeconds(), 2) + "." + withZeroes(d.getMilliseconds(), 3) + " ";
    if (IcecryptLog.level >= 4)
      dump(datStr + str);

    if (IcecryptLog.data === null) {
      IcecryptLog.data = "";
      let appInfo = Cc[XPCOM_APPINFO].getService(Ci.nsIXULAppInfo);
      IcecryptLog.WRITE("Mozilla Platform: " + appInfo.name + " " + appInfo.version + "\n");
    }
    // truncate first part of log data if it grow too much
    if (IcecryptLog.data.length > 5120000) {
      IcecryptLog.data = IcecryptLog.data.substr(-400000);
    }

    IcecryptLog.data += datStr + str;

    if (IcecryptLog.fileStream) {
      IcecryptLog.fileStream.write(datStr, datStr.length);
      IcecryptLog.fileStream.write(str, str.length);
    }
  },

  DEBUG: function(str) {
    IcecryptLog.WRITE("[DEBUG] " + str);
  },

  WARNING: function(str) {
    IcecryptLog.WRITE("[WARN] " + str);
    IcecryptConsole.write(str);
  },

  ERROR: function(str) {
    try {
      var consoleSvc = Cc["@mozilla.org/consoleservice;1"].getService(Ci.nsIConsoleService);
      var scriptError = Cc["@mozilla.org/scripterror;1"].createInstance(Ci.nsIScriptError);
      scriptError.init(str, null, null, 0, 0, scriptError.errorFlag, "Icecrypt");
      consoleSvc.logMessage(scriptError);
    }
    catch (ex) {}

    IcecryptLog.WRITE("[ERROR] " + str);
  },

  CONSOLE: function(str) {
    if (IcecryptLog.level >= 3) {
      IcecryptLog.WRITE("[CONSOLE] " + str);
    }

    IcecryptConsole.write(str);
  },

  /**
   *  Log an exception including the stack trace
   *
   *  referenceInfo: String - arbitraty text to write before the exception is logged
   *  ex:            exception object
   */
  writeException: function(referenceInfo, ex) {
    IcecryptLog.ERROR(referenceInfo + ": caught exception: " +
      ex.name + "\n" +
      "Message: '" + ex.message + "'\n" +
      "File:    " + ex.fileName + "\n" +
      "Line:    " + ex.lineNumber + "\n" +
      "Stack:   " + ex.stack + "\n");
  }
};
