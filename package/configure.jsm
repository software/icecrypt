/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*global Components: false */

"use strict";

var EXPORTED_SYMBOLS = ["IcecryptConfigure"];

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;


/*global IcecryptLog: false, IcecryptPrefs: false, IcecryptTimer: false, IcecryptApp: false, IcecryptLocale: false, IcecryptDialog: false, IcecryptWindows: false */

Cu.import("resource://icecrypt/log.jsm");
Cu.import("resource://icecrypt/prefs.jsm");
Cu.import("resource://icecrypt/timer.jsm");
Cu.import("resource://icecrypt/app.jsm");
Cu.import("resource://icecrypt/locale.jsm");
Cu.import("resource://icecrypt/dialog.jsm");
Cu.import("resource://icecrypt/windows.jsm");

function upgradeRecipientsSelection() {
  // Upgrade perRecipientRules and recipientsSelectionOption to
  // new recipientsSelection

  var keySel = IcecryptPrefs.getPref("recipientsSelectionOption");
  var perRecipientRules = IcecryptPrefs.getPref("perRecipientRules");

  var setVal = 2;

  /*
   1: rules only
   2: rules & email addresses (normal)
   3: email address only (no rules)
   4: manually (always prompt, no rules)
   5: no rules, no key selection
   */

  switch (perRecipientRules) {
    case 0:
      switch (keySel) {
        case 0:
          setVal = 5;
          break;
        case 1:
          setVal = 3;
          break;
        case 2:
          setVal = 4;
          break;
        default:
          setVal = 2;
      }
      break;
    case 1:
      setVal = 2;
      break;
    case 2:
      setVal = 1;
      break;
    default:
      setVal = 2;
  }

  // set new pref
  IcecryptPrefs.setPref("recipientsSelection", setVal);

  // clear old prefs
  IcecryptPrefs.getPrefBranch().clearUserPref("perRecipientRules");
  IcecryptPrefs.getPrefBranch().clearUserPref("recipientsSelectionOption");
}

function upgradePrefsSending() {
  IcecryptLog.DEBUG("icecryptCommon.jsm: upgradePrefsSending()\n");

  var cbs = IcecryptPrefs.getPref("confirmBeforeSend");
  var ats = IcecryptPrefs.getPref("alwaysTrustSend");
  var ksfr = IcecryptPrefs.getPref("keepSettingsForReply");
  IcecryptLog.DEBUG("icecryptCommon.jsm: upgradePrefsSending cbs=" + cbs + " ats=" + ats + " ksfr=" + ksfr + "\n");

  // Upgrade confirmBeforeSend (bool) to confirmBeforeSending (int)
  switch (cbs) {
    case false:
      IcecryptPrefs.setPref("confirmBeforeSending", 0); // never
      break;
    case true:
      IcecryptPrefs.setPref("confirmBeforeSending", 1); // always
      break;
  }

  // Upgrade alwaysTrustSend (bool)   to acceptedKeys (int)
  switch (ats) {
    case false:
      IcecryptPrefs.setPref("acceptedKeys", 0); // valid
      break;
    case true:
      IcecryptPrefs.setPref("acceptedKeys", 1); // all
      break;
  }

  // if all settings are default settings, use convenient encryption
  if (cbs === false && ats === true && ksfr === true) {
    IcecryptPrefs.setPref("encryptionModel", 0); // convenient
    IcecryptLog.DEBUG("icecryptCommon.jsm: upgradePrefsSending() encryptionModel=0 (convenient)\n");
  }
  else {
    IcecryptPrefs.setPref("encryptionModel", 1); // manually
    IcecryptLog.DEBUG("icecryptCommon.jsm: upgradePrefsSending() encryptionModel=1 (manually)\n");
  }

  // clear old prefs
  IcecryptPrefs.getPrefBranch().clearUserPref("confirmBeforeSend");
  IcecryptPrefs.getPrefBranch().clearUserPref("alwaysTrustSend");
}


function upgradeHeadersView() {
  // all headers hack removed -> make sure view is correct
  var hdrMode = null;
  try {
    hdrMode = IcecryptPrefs.getPref("show_headers");
  }
  catch (ex) {}

  if (!hdrMode) hdrMode = 1;
  try {
    IcecryptPrefs.getPrefBranch().clearUserPref("show_headers");
  }
  catch (ex) {}

  IcecryptPrefs.getPrefRoot().setIntPref("mail.show_headers", hdrMode);
}

function upgradeCustomHeaders() {
  try {
    var extraHdrs = " " + IcecryptPrefs.getPrefRoot().getCharPref("mailnews.headers.extraExpandedHeaders").toLowerCase() + " ";

    var extraHdrList = [
      "x-icecrypt-version",
      "content-transfer-encoding",
      "openpgp",
      "x-mimeole",
      "x-bugzilla-reason",
      "x-php-bug"
    ];

    for (let hdr in extraHdrList) {
      extraHdrs = extraHdrs.replace(" " + extraHdrList[hdr] + " ", " ");
    }

    extraHdrs = extraHdrs.replace(/^ */, "").replace(/ *$/, "");
    IcecryptPrefs.getPrefRoot().setCharPref("mailnews.headers.extraExpandedHeaders", extraHdrs);
  }
  catch (ex) {}
}

/**
 * Change from global PGP/MIME setting to per-identity setting
 */
function upgradeOldPgpMime() {
  var pgpMimeMode = false;
  try {
    pgpMimeMode = (IcecryptPrefs.getPref("usePGPMimeOption") == 2);
  }
  catch (ex) {
    return;
  }

  try {
    var accountManager = Cc["@mozilla.org/messenger/account-manager;1"].getService(Ci.nsIMsgAccountManager);
    for (var i = 0; i < accountManager.allIdentities.length; i++) {
      var id = accountManager.allIdentities.queryElementAt(i, Ci.nsIMsgIdentity);
      if (id.getBoolAttribute("enablePgp")) {
        id.setBoolAttribute("pgpMimeMode", pgpMimeMode);
      }
    }

    IcecryptPrefs.getPrefBranch().clearUserPref("usePGPMimeOption");
  }
  catch (ex) {}
}

/**
 * Change the default to PGP/MIME for all accounts, except nntp
 */
function defaultPgpMime() {
  let accountManager = Cc["@mozilla.org/messenger/account-manager;1"].getService(Ci.nsIMsgAccountManager);
  let changedSomething = false;

  for (let acct = 0; acct < accountManager.accounts.length; acct++) {
    let ac = accountManager.accounts.queryElementAt(acct, Ci.nsIMsgAccount);
    if (ac.incomingServer.type.search(/(pop3|imap|movemail)/) >= 0) {

      for (let i = 0; i < ac.identities.length; i++) {
        let id = ac.identities.queryElementAt(i, Ci.nsIMsgIdentity);
        if (id.getBoolAttribute("enablePgp") && !id.getBoolAttribute("pgpMimeMode")) {
          changedSomething = true;
        }
        id.setBoolAttribute("pgpMimeMode", true);
      }
    }
  }

  if (IcecryptPrefs.getPref("advancedUser") && changedSomething) {
    IcecryptDialog.alert(null,
      IcecryptLocale.getString("preferences.defaultToPgpMime"));
  }
}

const IcecryptConfigure = {
  configureIcecrypt: function(win, startingPreferences) {
    IcecryptLog.DEBUG("configure.jsm: configureIcecrypt\n");
    let oldVer = IcecryptPrefs.getPref("configuredVersion");

    let vc = Cc["@mozilla.org/xpcom/version-comparator;1"].getService(Ci.nsIVersionComparator);
    if (oldVer === "") {
      IcecryptWindows.openSetupWizard(win, false);
    }
    else {
      if (oldVer < "0.95") {
        try {
          upgradeHeadersView();
          upgradeOldPgpMime();
          upgradeRecipientsSelection();
        }
        catch (ex) {}
      }
      if (vc.compare(oldVer, "1.0") < 0) {
        upgradeCustomHeaders();
      }
      if (vc.compare(oldVer, "1.7a1pre") < 0) {
        // 1: rules only
        //     => assignKeysByRules true; rest false
        // 2: rules & email addresses (normal)
        //     => assignKeysByRules/assignKeysByEmailAddr/assignKeysManuallyIfMissing true
        // 3: email address only (no rules)
        //     => assignKeysByEmailAddr/assignKeysManuallyIfMissing true
        // 4: manually (always prompt, no rules)
        //     => assignKeysManuallyAlways true
        // 5: no rules, no key selection
        //     => assignKeysByRules/assignKeysByEmailAddr true

        upgradePrefsSending();
      }
      if (vc.compare(oldVer, "1.7") < 0) {
        // open a modal dialog. Since this might happen during the opening of another
        // window, we have to do this asynchronously
        IcecryptTimer.setTimeout(
          function _cb() {
            var doIt = IcecryptDialog.confirmDlg(win,
              IcecryptLocale.getString("icecryptCommon.versionSignificantlyChanged"),
              IcecryptLocale.getString("icecryptCommon.checkPreferences"),
              IcecryptLocale.getString("dlg.button.close"));
            if (!startingPreferences && doIt) {
              // same as:
              // - IcecryptWindows.openPrefWindow(window, true, 'sendingTab');
              // but
              // - without starting the service again because we do that right now
              // - and modal (waiting for its end)
              win.openDialog("chrome://icecrypt/content/pref-icecrypt.xul",
                "_blank", "chrome,resizable=yes,modal", {
                  'showBasic': true,
                  'clientType': 'thunderbird',
                  'selectTab': 'sendingTab'
                });
            }
          }, 100);
      }

      if (vc.compare(oldVer, "1.9a2pre") < 0) {
        defaultPgpMime();
      }
    }


    IcecryptPrefs.setPref("configuredVersion", IcecryptApp.getVersion());
    IcecryptPrefs.savePrefs();
  }
};
