/*global do_load_module: false, do_get_file: false, do_get_cwd: false, testing: false, test: false, Assert: false, resetting: false, JSUnit: false, do_test_pending: false */
/*global do_test_finished: false, component: false, Cc: false, Ci: false, setupTestAccounts: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

do_load_module("file://" + do_get_cwd().path + "/testHelper.js"); /*global withIcecrypt: false, withTestGpgHome: false */

testing("expiry.jsm"); /*global IcecryptExpiry: false */
component("icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */
component("icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */
component("icecrypt/locale.jsm"); /*global IcecryptLocale: false */

/*global Math: false, Date: false, uniqueKeyList: false, DAY: false */

setupTestAccounts();

test(function shouldCheckKeyExpiry() {

  IcecryptKeyRing.clearCache();
  let keyListObj = IcecryptKeyRing.getAllKeys();

  let now = Math.floor(Date.now() / 1000);

  let a = [{
    keyId: "123"
  }, {
    keyId: "456"
  }, {
    keyId: "123"
  }, {
    keyId: "763"
  }, {
    keyId: "456"
  }];
  let b = uniqueKeyList(a);
  Assert.equal(b.length, 3);

  keyListObj.keySortList.push(1); // ensure that key list is not reloaded
  keyListObj.keyList.push(createKeyObj("ABCDEF0123456789", "user1@icecrypt-test.net", now + DAY * 5, true));
  keyListObj.keyList.push(createKeyObj("DBCDEF0123456789", "user2@icecrypt-test.net", now - DAY * 5, true));
  keyListObj.keyList.push(createKeyObj("EBCDEF0123456789", "user2@icecrypt-test.net", now + DAY * 100, true));
  keyListObj.keyList.push(createKeyObj("CBCDEF0123456789", "user3@icecrypt-test.net", 0, true));
  keyListObj.keyList.push(createKeyObj("BBCDEF0123456789", "user4@icecrypt-test.net", now - DAY * 5, true));
  keyListObj.keyList.push(createKeyObj("FBCDEF0123456789", "user5@icecrypt-test.net", now - DAY * 5, true));
  keyListObj.keyList.push(createKeyObj("ACCDEF0123456789", "user5@icecrypt-test.net", now + DAY * 5, true));

  IcecryptKeyRing.rebuildKeyIndex();

  let k = IcecryptExpiry.getExpiryForKeySpec([], 10);
  Assert.equal(k.length, 0);

  k = IcecryptExpiry.getExpiryForKeySpec(["0xABCDEF0123456789", "BBCDEF0123456789", "CBCDEF0123456789"], 10);
  Assert.equal(k.map(getKeyId).join(" "), "ABCDEF0123456789");

  k = IcecryptExpiry.getExpiryForKeySpec(["user1@icecrypt-test.net", "user2@icecrypt-test.net", "user5@icecrypt-test.net"], 10);
  Assert.equal(k.map(getKeyId).join(" "), "ABCDEF0123456789 ACCDEF0123456789");
});

test(function shouldCheckKeySpecs() {
  let a = IcecryptExpiry.getKeysSpecForIdentities();
  Assert.equal(a.join(" "), "ABCDEF0123456789 user2@icecrypt-test.net user4@icecrypt-test.net");
});

test(function shouldGetNewlyExpiredKeys() {
  IcecryptPrefs.setPref("keyCheckResult", "");
  IcecryptPrefs.setPref("warnKeyExpiryNumDays", 10);
  let a = IcecryptExpiry.getNewlyExpiredKeys();
  Assert.equal(a.map(getKeyId).join(" "), "ABCDEF0123456789");

  IcecryptPrefs.setPref("warnKeyExpiryNumDays", 101);
  a = IcecryptExpiry.getNewlyExpiredKeys();
  Assert.equal(a, null);

  let keyCheckResult = JSON.parse(IcecryptPrefs.getPref("keyCheckResult", ""));
  keyCheckResult.lastCheck = Date.now() - 86401000;
  IcecryptPrefs.setPref("keyCheckResult", JSON.stringify(keyCheckResult));

  a = IcecryptExpiry.getNewlyExpiredKeys();
  Assert.equal(a.map(getKeyId).join(" "), "EBCDEF0123456789");

  keyCheckResult = JSON.parse(IcecryptPrefs.getPref("keyCheckResult", ""));
  keyCheckResult.lastCheck = Date.now() - 86401000;
  IcecryptPrefs.setPref("keyCheckResult", JSON.stringify(keyCheckResult));

  a = IcecryptExpiry.getNewlyExpiredKeys();
  Assert.equal(a.length, 0);
});

test(function shouldDoKeyExpiryCheck() {

  IcecryptPrefs.setPref("keyCheckResult", "");
  IcecryptPrefs.setPref("warnKeyExpiryNumDays", 101);

  let str = IcecryptExpiry.keyExpiryCheck();
  Assert.equal(str, IcecryptLocale.getString("expiry.keysExpireSoon", [101, '- "user1@icecrypt-test.net" (key ID 123456781234567812345678ABCDEF0123456789)\n' +
    '- "user2@icecrypt-test.net" (key ID 123456781234567812345678EBCDEF0123456789)\n'
  ]));


  let keyCheckResult = JSON.parse(IcecryptPrefs.getPref("keyCheckResult", ""));
  keyCheckResult.lastCheck = Date.now() - 86401000;
  IcecryptPrefs.setPref("keyCheckResult", JSON.stringify(keyCheckResult));

  IcecryptPrefs.setPref("warnKeyExpiryNumDays", 10);
  str = IcecryptExpiry.keyExpiryCheck();
  Assert.equal(str, "");
});

function getKeyId(key) {
  return key.keyId;
}

function createKeyObj(keyId, userId, expiryDate, hasSecretKey) {
  return {
    keyId: keyId,
    userId: userId,
    fpr: "123456781234567812345678" + keyId,
    expiryTime: expiryDate,
    keyUseFor: "escESC",
    secretAvailable: hasSecretKey,
    keyTrust: "u",
    type: "pub",
    userIds: [{
      userId: userId,
      type: "uid",
      keyTrust: "u"
    }],
    subKeys: [],
    signatures: [],
    getKeyExpiry: function() {
      if (this.expiryTime === 0) return Number.MAX_VALUE;
      return this.expiryTime;
    },
    get fprFormatted() {
      return this.fpr;
    }
  };
}
