/*global do_load_module: false, do_get_file: false, do_get_cwd: false, testing: false, test: false, Assert: false, resetting: false, JSUnit: false, do_test_pending: false, do_test_finished: false, withTestGpgHome:false */
/*global IcecryptCore: false, Icecrypt: false, component: false, Cc: false, Ci: false, withEnvironment: false, nsIIcecrypt: false, nsIEnvironment: false, Ec: false, IcecryptPrefs: false, IcecryptOS: false, IcecryptArmor: false */
/*jshint -W120 */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

do_load_module("file://" + do_get_cwd().path + "/testHelper.js");

testing("icecrypt.js");

function newIcecrypt(f) {
  var oldIcecrypt = IcecryptCore.getIcecryptService();
  try {
    var icecrypt = new Icecrypt();
    IcecryptCore.setIcecryptService(icecrypt);
    f(icecrypt);
  }
  finally {
    IcecryptCore.setIcecryptService(oldIcecrypt);
  }
}

// testing: initialize
test(function initializeWillPassEnvironmentIfAskedTo() {
  var window = JSUnit.createStubWindow();
  withEnvironment({
    "ENIGMAIL_PASS_ENV": "STUFF:BLARG",
    "STUFF": "testing"
  }, function() {
    newIcecrypt(function(icecrypt) {
      icecrypt.initialize(window, "");
      Assert.assertArrayContains(IcecryptCore.getEnvList(), "STUFF=testing");
    });
  });
});

test(function initializeWillNotPassEnvironmentsNotAskedTo() {
  var window = JSUnit.createStubWindow();
  var environment = Cc["@mozilla.org/process/environment;1"].getService(nsIEnvironment);
  environment.set("ENIGMAIL_PASS_ENV", "HOME");
  environment.set("STUFF", "testing");
  newIcecrypt(function(icecrypt) {
    icecrypt.initialize(window, "");
    Assert.assertArrayNotContains(IcecryptCore.getEnvList(), "STUFF=testing");
  });
});

test(function initializeWillNotSetEmptyEnvironmentValue() {
  var window = JSUnit.createStubWindow();
  var environment = Cc["@mozilla.org/process/environment;1"].getService(nsIEnvironment);
  environment.set("APPDATA", "");
  newIcecrypt(function(icecrypt) {
    icecrypt.initialize(window, "");
    Assert.assertArrayNotContains(IcecryptCore.getEnvList(), "APPDATA=");
  });
});
