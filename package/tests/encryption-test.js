/*global do_load_module: false, do_get_file: false, do_get_cwd: false, testing: false, test: false, Assert: false, resetting: false, JSUnit: false, do_test_pending: false, do_test_finished: false, component: false, Cc: false, Ci: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

do_load_module("file://" + do_get_cwd().path + "/testHelper.js"); /*global withIcecrypt: false, withTestGpgHome: false */

testing("encryption.jsm"); /*global IcecryptEncryption: false, nsIIcecrypt: false */
component("icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: fales */
component("icecrypt/armor.jsm"); /*global IcecryptArmor: fales */
component("icecrypt/Locale.jsm"); /*global IcecryptLocale: fales */

test(withTestGpgHome(withIcecrypt(function shouldSignMessage() {
  const secretKey = do_get_file("resources/dev-strike.sec", false);
  const revocationCert = do_get_file("resources/dev-strike.rev", false);
  const errorMsgObj = {};
  const importedKeysObj = {};
  IcecryptKeyRing.importKeyFromFile(secretKey, errorMsgObj, importedKeysObj);
  const parentWindow = JSUnit.createStubWindow();
  const plainText = "Hello there!";
  const strikeAccount = "strike.devtest@gmail.com";
  const exitCodeObj = {};
  const statusFlagObj = {};
  const encryptResult = IcecryptEncryption.encryptMessage(parentWindow,
    nsIIcecrypt.UI_TEST,
    plainText,
    strikeAccount,
    strikeAccount,
    "",
    nsIIcecrypt.SEND_TEST | nsIIcecrypt.SEND_SIGNED,
    exitCodeObj,
    statusFlagObj,
    errorMsgObj
  );
  Assert.equal(0, exitCodeObj.value);
  Assert.equal(0, errorMsgObj.value);
  Assert.equal(true, (statusFlagObj.value == nsIIcecrypt.SIG_CREATED));
  const blockType = IcecryptArmor.locateArmoredBlock(encryptResult, 0, "", {}, {}, {});
  Assert.equal("SIGNED MESSAGE", blockType);

  let r = IcecryptEncryption.determineOwnKeyUsability(nsIIcecrypt.SEND_SIGNED, "strike.devtest@gmail.com");
  Assert.equal(r.keyId, "65537E212DC19025AD38EDB2781617319CE311C4");

  IcecryptKeyRing.importKeyFromFile(revocationCert, errorMsgObj, importedKeysObj);
  r = IcecryptEncryption.determineOwnKeyUsability(nsIIcecrypt.SEND_SIGNED, "0x65537E212DC19025AD38EDB2781617319CE311C4");
  Assert.equal(r.errorMsg, IcecryptLocale.getString("keyRing.pubKeyRevoked", ["anonymous strike <strike.devtest@gmail.com>", "0x781617319CE311C4"]));
})));

test(withTestGpgHome(withIcecrypt(function shouldEncryptMessage() {
  const publicKey = do_get_file("resources/dev-strike.asc", false);
  const errorMsgObj = {};
  const importedKeysObj = {};
  IcecryptKeyRing.importKeyFromFile(publicKey, errorMsgObj, importedKeysObj);
  const parentWindow = JSUnit.createStubWindow();
  const plainText = "Hello there!";
  const strikeAccount = "strike.devtest@gmail.com";
  const exitCodeObj = {};
  const statusFlagObj = {};
  const encryptResult = IcecryptEncryption.encryptMessage(parentWindow,
    nsIIcecrypt.UI_TEST,
    plainText,
    strikeAccount,
    strikeAccount,
    "",
    nsIIcecrypt.SEND_TEST | nsIIcecrypt.SEND_ENCRYPTED | nsIIcecrypt.SEND_ALWAYS_TRUST,
    exitCodeObj,
    statusFlagObj,
    errorMsgObj
  );
  Assert.equal(0, exitCodeObj.value);
  Assert.equal(0, errorMsgObj.value);
  Assert.equal(true, (statusFlagObj.value & nsIIcecrypt.END_ENCRYPTION) !== 0);
  const blockType = IcecryptArmor.locateArmoredBlock(encryptResult, 0, "", {}, {}, {});
  Assert.equal("MESSAGE", blockType);

  let r = IcecryptEncryption.determineOwnKeyUsability(nsIIcecrypt.SEND_ENCRYPTED, "strike.devtest@gmail.com");
  Assert.equal(r.keyId, "65537E212DC19025AD38EDB2781617319CE311C4");
})));

test(withTestGpgHome(withIcecrypt(function shouldGetErrorReason() {
  let r = IcecryptEncryption.determineOwnKeyUsability(nsIIcecrypt.SEND_SIGNED, "strike.devtest@gmail.com");
  let expected = IcecryptLocale.getString("keyRing.noSecretKey", ["anonymous strike <strike.devtest@gmail.com>", "0x781617319CE311C4"]) + "\n";
  Assert.equal(r.errorMsg, expected);

  r = IcecryptEncryption.determineOwnKeyUsability(nsIIcecrypt.SEND_SIGNED | nsIIcecrypt.SEND_ENCRYPTED, "nobody@notfound.net");
  expected = IcecryptLocale.getString("errorOwnKeyUnusable", "nobody@notfound.net");
  Assert.equal(r.errorMsg, expected);

})));
