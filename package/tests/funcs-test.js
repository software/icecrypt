/*global do_load_module: false, do_get_file: false, do_get_cwd: false, testing: false, test: false, Assert: false, resetting: false, IcecryptApp: false */
/*global IcecryptFuncs: false, rulesListHolder: false, EC: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

do_load_module("file://" + do_get_cwd().path + "/testHelper.js");

testing("funcs.jsm");

var IcecryptFuncsTests = {
  testStripEmail(str, res) {
    let addr;
    addr = IcecryptFuncs.stripEmail(str);
    Assert.equal(addr, res);
  }
};

test(function stripEmail() {
  IcecryptFuncsTests.testStripEmail("some stuff <a@b.de> some stuff",
    "a@b.de");

  IcecryptFuncsTests.testStripEmail("\"some stuff\" a@b.de",
    "a@b.de");

  IcecryptFuncsTests.testStripEmail("\"some, stuff\" a@b.de",
    "a@b.de");

  IcecryptFuncsTests.testStripEmail("some stuff <a@b.de> some stuff, xyz<xy@a.xx>xyc",
    "a@b.de,xy@a.xx");

  IcecryptFuncsTests.testStripEmail(" a@b.de , <aa@bb.de>",
    "a@b.de,aa@bb.de");

  IcecryptFuncsTests.testStripEmail("    ,,,,;;;; , ; , ;",
    "");

  IcecryptFuncsTests.testStripEmail(";",
    "");


  IcecryptFuncsTests.testStripEmail("    ,,oneRule,;;; , ;",
    "oneRule");

  IcecryptFuncsTests.testStripEmail("    ,,,nokey,;;;; , nokey2 ; , ;",
    "nokey,nokey2");

  IcecryptFuncsTests.testStripEmail(",,,newsgroupa ",
    "newsgroupa");

  // test invalid email addresses:
  Assert.throws(
    function() {
      IcecryptFuncs.stripEmail(" a@b.de , <aa@bb.de> <aa@bb.dd>");
    }
  );
  Assert.throws(
    function() {
      IcecryptFuncs.stripEmail("\"some stuff a@b.de");
    }
  );

});

test(function compareMimePartLevel() {
  Assert.throws(
    function() {
      IcecryptFuncs.compareMimePartLevel("1.2.e", "1.2");
    }
  );

  let e = IcecryptFuncs.compareMimePartLevel("1.1", "1.1.2");
  Assert.equal(e, -2);

  e = IcecryptFuncs.compareMimePartLevel("1.1", "1.2.2");
  Assert.equal(e, -1);

  e = IcecryptFuncs.compareMimePartLevel("1", "2");
  Assert.equal(e, -1);

  e = IcecryptFuncs.compareMimePartLevel("1.2", "1.1.2");
  Assert.equal(e, 1);

  e = IcecryptFuncs.compareMimePartLevel("1.2.2", "1.2");
  Assert.equal(e, 2);

  e = IcecryptFuncs.compareMimePartLevel("1.2.2", "1.2.2");
  Assert.equal(e, 0);

});
