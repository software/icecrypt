/*global do_load_module: false, do_get_file: false, do_get_cwd: false, testing: false, test: false, Assert: false, resetting: false, JSUnit: false, do_test_pending: false, do_test_finished: false */
/*global TestHelper: false, withEnvironment: false, nsIWindowsRegKey: true */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

do_load_module("file://" + do_get_cwd().path + "/testHelper.js");
/*global TestHelper: false, withEnvironment: false, withIcecrypt: false, component: false,
  withTestGpgHome: false, osUtils: false, IcecryptFiles */

testing("gpgAgent.jsm"); /*global IcecryptGpgAgent: false, IcecryptOS: false, getHomedirFromParam: false */
component("icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */
component("icecrypt/gpg.jsm"); /*global IcecryptGpg: false */

// testing: determineGpgHomeDir
//   environment: GNUPGHOME
//   isWin32:
//     registry Software\GNU\GNUPG\HomeDir
//     environment: USERPROFILE + \Application Data\GnuPG
//     environment: SystemRoot + \Application Data\GnuPG
//     c:\gnupg
//   environment: HOME + .gnupg

test(function determineGpgHomeDirReturnsGNUPGHOMEIfExists() {
  withEnvironment({
    "GNUPGHOME": "stuffResult1"
  }, function(e) {
    var icecrypt = {
      environment: e
    };
    Assert.equal("stuffResult1", IcecryptGpgAgent.determineGpgHomeDir(icecrypt));
  });
});

// this test cannot be reliably performed on Windows
if (JSUnit.getOS() != "WINNT") {
  test(function determineGpgHomeDirReturnsHomePlusGnupgForNonWindowsIfNoGNUPGHOMESpecificed() {
    withEnvironment({
      "HOME": "/my/little/home"
    }, function(e) {
      e.set("GNUPGHOME", null);
      var icecrypt = {
        environment: e
      };
      Assert.equal("/my/little/home/.gnupg", IcecryptGpgAgent.determineGpgHomeDir(icecrypt));
    });
  });
}

test(function determineGpgHomeDirReturnsRegistryValueForWindowsIfExists() {
  withEnvironment({}, function(e) {
    e.set("GNUPGHOME", null);
    resetting(IcecryptOS, 'getWinRegistryString', function(a, b, c) {
      if (a === "Software\\GNU\\GNUPG" && b === "HomeDir" && c === "foo bar") {
        return "\\foo\\bar\\gnupg";
      }
      else {
        return "\\somewhere\\else";
      }
    }, function() {
      resetting(IcecryptOS, 'isWin32', true, function() {
        var icecrypt = {
          environment: e
        };
        nsIWindowsRegKey = {
          ROOT_KEY_CURRENT_USER: "foo bar"
        };
        Assert.equal("\\foo\\bar\\gnupg", IcecryptGpgAgent.determineGpgHomeDir(icecrypt));
      });
    });
  });
});

test(function determineGpgHomeDirReturnsUserprofileIfItExists() {
  withEnvironment({
    "USERPROFILE": "\\bahamas"
  }, function(e) {
    e.set("GNUPGHOME", null);
    resetting(IcecryptOS, 'getWinRegistryString', function(a, b, c) {}, function() {
      resetting(IcecryptOS, 'isWin32', true, function() {
        var icecrypt = {
          environment: e
        };
        nsIWindowsRegKey = {
          ROOT_KEY_CURRENT_USER: "foo bar"
        };
        Assert.equal("\\bahamas\\Application Data\\GnuPG", IcecryptGpgAgent.determineGpgHomeDir(icecrypt));
      });
    });
  });
});

test(function determineGpgHomeDirReturnsSystemrootIfItExists() {
  withEnvironment({
    "SystemRoot": "\\tahiti",
    "USERPROFILE": null
  }, function(e) {
    e.set("GNUPGHOME", null);
    resetting(IcecryptOS, 'getWinRegistryString', function(a, b, c) {}, function() {
      resetting(IcecryptOS, 'isWin32', true, function() {
        var icecrypt = {
          environment: e
        };
        nsIWindowsRegKey = {
          ROOT_KEY_CURRENT_USER: "foo bar"
        };
        Assert.equal("\\tahiti\\Application Data\\GnuPG", IcecryptGpgAgent.determineGpgHomeDir(icecrypt));
      });
    });
  });
});

test(function determineGpgHomeDirReturnsDefaultForWin32() {
  withEnvironment({
    "SystemRoot": null,
    "USERPROFILE": null
  }, function(e) {
    e.set("GNUPGHOME", null);
    resetting(IcecryptOS, 'getWinRegistryString', function(a, b, c) {}, function() {
      resetting(IcecryptOS, 'isWin32', true, function() {
        var icecrypt = {
          environment: e
        };
        nsIWindowsRegKey = {
          ROOT_KEY_CURRENT_USER: "foo bar"
        };
        Assert.equal("C:\\gnupg", IcecryptGpgAgent.determineGpgHomeDir(icecrypt));
      });
    });
  });
});


// // testing: useGpgAgent
// // useGpgAgent depends on several values:
// //   IcecryptOS.isDosLike()
// //   Gpg.getGpgFeature("supports-gpg-agent")
// //   Gpg.getGpgFeature("autostart-gpg-agent")
// //   IcecryptGpgAgent.gpgAgentInfo.envStr.length>0
// //   IcecryptPrefs.getPrefBranch().getBoolPref("useGpgAgent")

function asDosLike(f) {
  resetting(IcecryptOS, 'isDosLikeVal', true, f);
}

function notDosLike(f) {
  resetting(IcecryptOS, 'isDosLikeVal', false, f);
}

function withGpgFeatures(features, f) {
  resetting(IcecryptGpg, 'getGpgFeature', function(feature) {
    return features.indexOf(feature) != -1;
  }, f);
}

function mockPrefs(prefs) {
  return {
    getBoolPref: function(name) {
      return prefs[name];
    }
  };
}

test(function useGpgAgentIsFalseIfIsDosLikeAndDoesntSupportAgent() {
  asDosLike(function() {
    withGpgFeatures([], function() {
      Assert.ok(!IcecryptGpgAgent.useGpgAgent());
    });
  });
});

test(function useGpgAgentIsTrueIfIsDosLikeAndSupportsAgentAndAutostartsAgent() {
  asDosLike(function() {
    withGpgFeatures(["supports-gpg-agent", "autostart-gpg-agent"], function() {
      Assert.ok(IcecryptGpgAgent.useGpgAgent());
    });
  });
});

test(function useGpgAgentIsTrueIfIsDosLikeAndSupportsAgentAndThereExistsAnAgentString() {
  asDosLike(function() {
    withGpgFeatures(["supports-gpg-agent"], function() {
      IcecryptGpgAgent.gpgAgentInfo.envStr = "blarg";
      Assert.ok(IcecryptGpgAgent.useGpgAgent());
    });
  });
});

test(function useGpgAgentIsFalseIfIsDosLikeAndSupportsAgentButNoAgentInfoAvailable() {
  asDosLike(function() {
    withGpgFeatures(["supports-gpg-agent"], function() {
      IcecryptGpgAgent.gpgAgentInfo.envStr = "";
      Assert.ok(!IcecryptGpgAgent.useGpgAgent());
    });
  });
});

test(function useGpgAgentIsTrueIfIsDosLikeAndSupportsAgentAndPrefIsSet() {
  asDosLike(function() {
    withGpgFeatures(["supports-gpg-agent"], function() {
      resetting(IcecryptPrefs, 'getPrefBranch', function() {
        return mockPrefs({
          useGpgAgent: true
        });
      }, function() {
        Assert.ok(IcecryptGpgAgent.useGpgAgent());
      });
    });
  });
});


test(function useGpgAgentIsTrueIfNotDosLikeAndSupportsAgentAndAutostartsAgent() {
  notDosLike(function() {
    withGpgFeatures(["supports-gpg-agent", "autostart-gpg-agent"], function() {
      Assert.ok(IcecryptGpgAgent.useGpgAgent());
    });
  });
});

test(function useGpgAgentIsTrueIfNotDosLikeAndSupportsAgentAndThereExistsAnAgentString() {
  notDosLike(function() {
    withGpgFeatures(["supports-gpg-agent"], function() {
      IcecryptGpgAgent.gpgAgentInfo.envStr = "blarg";
      Assert.ok(IcecryptGpgAgent.useGpgAgent());
    });
  });
});

test(function useGpgAgentIsFalseIfNotDosLikeAndSupportsAgentButNoAgentInfoAvailable() {
  notDosLike(function() {
    withGpgFeatures(["supports-gpg-agent"], function() {
      IcecryptGpgAgent.gpgAgentInfo.envStr = "";
      Assert.ok(!IcecryptGpgAgent.useGpgAgent());
    });
  });
});

test(function useGpgAgentIsTrueIfNotDosLikeAndSupportsAgentAndPrefIsSet() {
  notDosLike(function() {
    withGpgFeatures(["supports-gpg-agent"], function() {
      resetting(IcecryptPrefs, 'getPrefBranch', function() {
        return mockPrefs({
          useGpgAgent: true
        });
      }, function() {
        Assert.ok(IcecryptGpgAgent.useGpgAgent());
      });
    });
  });
});

// // setAgentPath

test(withIcecrypt(function setAgentPathDefaultValues(icecrypt) {
  withEnvironment({}, function(e) {
    icecrypt.environment = e;
    IcecryptGpgAgent.setAgentPath(JSUnit.createStubWindow(), icecrypt);
    Assert.equal("gpg", IcecryptGpgAgent.agentType);
    Assert.equal("gpg", IcecryptGpgAgent.agentPath.leafName.substr(0, 3));
    Assert.equal("gpgconf", IcecryptGpgAgent.gpgconfPath.leafName.substr(0, 7));
    Assert.equal("gpg-connect-agent", IcecryptGpgAgent.connGpgAgentPath.leafName.substr(0, 17));
    // Basic check to test if GnuPG version was properly extracted
    Assert.ok(IcecryptGpg.agentVersion.search(/^[2-9]\.[0-9]+(\.[0-9]+)?/) === 0);
  });
}));

// // resolveToolPath

test(withIcecrypt(function resolveToolPathDefaultValues(icecrypt) {
  withEnvironment({}, function(e) {
    resetting(IcecryptGpgAgent, 'agentPath', "/usr/bin/gpg-agent", function() {
      icecrypt.environment = e;
      var result = IcecryptGpgAgent.resolveToolPath("zip");
      Assert.equal("zip", result.leafName.substr(0, 3));
    });
  });
}));

// route cannot be tested reliably on non-Unix systems
// test(withIcecrypt(function resolveToolPathFromPATH(icecrypt) {
//     withEnvironment({PATH: "/sbin"}, function(e) {
//         resetting(IcecryptGpgAgent, 'agentPath', "/usr/bin/gpg-agent", function() {
//             icecrypt.environment = e;
//             var result = IcecryptGpgAgent.resolveToolPath("route");
//             Assert.equal("/sbin/route", result.path);
//         });
//     });
// }));

// detectGpgAgent
test(withIcecrypt(function detectGpgAgentSetsAgentInfoFromEnvironmentVariable(icecrypt) {
  withEnvironment({
    GPG_AGENT_INFO: "a happy agent"
  }, function(e) {
    icecrypt.environment = e;
    IcecryptGpgAgent.detectGpgAgent(JSUnit.createStubWindow(), icecrypt);

    Assert.ok(IcecryptGpgAgent.gpgAgentInfo.preStarted);
    Assert.equal("a happy agent", IcecryptGpgAgent.gpgAgentInfo.envStr);
    Assert.ok(!IcecryptGpgAgent.gpgAgentIsOptional);
  });
}));


test(withIcecrypt(function detectGpgAgentWithNoAgentInfoInEnvironment(icecrypt) {
  withEnvironment({}, function(e) {
    icecrypt.environment = e;
    IcecryptGpgAgent.detectGpgAgent(JSUnit.createStubWindow(), icecrypt);

    Assert.ok(!IcecryptGpgAgent.gpgAgentInfo.preStarted);
    Assert.ok(!IcecryptGpgAgent.gpgAgentIsOptional);
  });
}));

test(withIcecrypt(function detectGpgAgentWithAutostartFeatureWillDoNothing(icecrypt) {
  withEnvironment({}, function(e) {
    withGpgFeatures(["autostart-gpg-agent"], function() {
      icecrypt.environment = e;
      IcecryptGpgAgent.detectGpgAgent(JSUnit.createStubWindow(), icecrypt);
      Assert.equal("none", IcecryptGpgAgent.gpgAgentInfo.envStr);
    });
  });
}));

//getGpgHomeDir
test(withTestGpgHome(withIcecrypt(function shouldGetGpgHomeDir() {
  let homedirExpected = osUtils.OS.Path.join(IcecryptFiles.getTempDir(), ".gnupgTest");

  let homeDir = IcecryptGpgAgent.getGpgHomeDir();
  Assert.equal(homedirExpected, homeDir);
})));

// getHomedirFromParam
test(function shouldGetHomedirFromParam() {
  let hd = getHomedirFromParam('--homedir /some1/path');
  Assert.equal(hd, "/some1/path");

  hd = getHomedirFromParam('--opt1 --homedir /some2/path --opt2');
  Assert.equal(hd, "/some2/path");

  hd = getHomedirFromParam('--opt1 --homedir   "C:\\My Path\\is\\Very \\"long 1\\"" --opt2');
  Assert.equal(hd, 'C:\\My Path\\is\\Very \\"long 1\\"');

  hd = getHomedirFromParam('--opt1 --homedir "C:\\My Path\\is\\Very \\"long 2\\"" --opt2 "Some \\"more\\" fun"');
  Assert.equal(hd, 'C:\\My Path\\is\\Very \\"long 2\\"');
});
