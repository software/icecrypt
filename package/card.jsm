/*global Components: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

var EXPORTED_SYMBOLS = ["IcecryptCard"];

const Cu = Components.utils;

Cu.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Cu.import("resource://icecrypt/execution.jsm"); /*global IcecryptExecution: false */
Cu.import("resource://icecrypt/gpg.jsm"); /*global IcecryptGpg: false */

const IcecryptCard = {
  getCardStatus: function(exitCodeObj, errorMsgObj) {
    IcecryptLog.DEBUG("card.jsm: IcecryptCard.getCardStatus\n");
    const args = IcecryptGpg.getStandardArgs(false).
    concat(["--status-fd", "2", "--fixed-list-mode", "--with-colons", "--card-status"]);
    const statusMsgObj = {};
    const statusFlagsObj = {};

    const outputTxt = IcecryptExecution.execCmd(IcecryptGpg.agentPath, args, "", exitCodeObj, statusFlagsObj, statusMsgObj, errorMsgObj);

    if ((exitCodeObj.value === 0) && !outputTxt) {
      exitCodeObj.value = -1;
      return "";
    }

    return outputTxt;
  }
};
