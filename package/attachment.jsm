/*global Components: false, escape: false, unescape: false */
/*jshint -W097 */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

var EXPORTED_SYMBOLS = ["IcecryptAttachment"];

const Cu = Components.utils;

Cu.import("resource://icecrypt/execution.jsm"); /*global IcecryptExecution: false */
Cu.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Cu.import("resource://icecrypt/gpgAgent.jsm"); /*global IcecryptGpgAgent: false */
Cu.import("resource://icecrypt/passwords.jsm"); /*global IcecryptPassword: false */
Cu.import("resource://icecrypt/gpg.jsm"); /*global IcecryptGpg: false */
Cu.import("resource://icecrypt/data.jsm"); /*global IcecryptData: false */

const IcecryptAttachment = {
  getFileName: function(parent, byteData) {
    IcecryptLog.DEBUG("attachment.jsm: getFileName\n");

    const args = IcecryptGpg.getStandardArgs(true).
    concat(IcecryptPassword.command()).
    concat(["--decrypt"]);

    const listener = IcecryptExecution.newSimpleListener(
      function _stdin(pipe) {
        IcecryptLog.DEBUG("attachment.jsm: getFileName: _stdin\n");
        pipe.write(byteData);
        pipe.write("\n");
        pipe.close();
      });

    listener.stdout = function(data) {};

    const proc = IcecryptExecution.execStart(IcecryptGpgAgent.agentPath, args, false, parent, listener, {});

    if (!proc) {
      return null;
    }

    proc.wait();

    const matches = listener.stderrData.match(/^(\[GNUPG:\] PLAINTEXT [0-9]+ [0-9]+ )(.*)$/m);
    if (matches && (matches.length > 2)) {
      var filename = matches[2];
      if (filename.indexOf(" ") > 0) {
        filename = filename.replace(/ .*$/, "");
      }
      return IcecryptData.convertToUnicode(unescape(filename), "utf-8");
    }
    else {
      return null;
    }
  }
};
