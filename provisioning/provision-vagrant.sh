#!/usr/bin/env bash
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

echo "Provisioning ..."
apt-get update
apt-get install -y zip thunderbird xvfb gnupg2
apt-get install -y xfce4 virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11
apt-get install -y ntp
apt-get install -y rng-tools haveged
apt-get upgrade -y
wget -O /tmp/jsunit-0.1.xpi https://www.enigmail.net/jsunit/jsunit-0.1.xpi
rm -rf /usr/lib/thunderbird-addons/extensions/jsunit@icecrypt.net
unzip /tmp/jsunit-0.1.xpi -d /usr/lib/thunderbird-addons/extensions/jsunit@icecrypt.net
rm -rf '/usr/lib/thunderbird-addons/extensions/{809acbe0-5423-4c50-a516-4b669cec108d}'
echo "/icecrypt-src/build/dist" > '/usr/lib/thunderbird-addons/extensions/{809acbe0-5423-4c50-a516-4b669cec108d}'
touch '/icecrypt-src'
