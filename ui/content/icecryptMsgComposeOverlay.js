/*global Components: false, IcecryptLocale: false, IcecryptApp: false, Dialog: false, IcecryptTimer: false */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

/*global Thunderbird variables: */
/*global MimeBody: false, MimeUnknown: false, MimeMessageAttachment: false, gMsgCompose: false, getCurrentIdentity: false */
/*global msgHdrToMimeMessage: false, MimeMessage: false, MimeContainer: false, UpdateAttachmentBucket: false, gContentChanged: true */
/*global AddAttachments: false, AddAttachment: false, ChangeAttachmentBucketVisibility: false, GetResourceFromUri: false */
/*global Recipients2CompFields: false, Attachments2CompFields: false, DetermineConvertibility: false, gWindowLocked: false */
/*global CommandUpdate_MsgCompose: false, gSMFields: false, Sendlater3Composing: false*/

Components.utils.import("resource://icecrypt/glodaMime.jsm");
Components.utils.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Components.utils.import("resource://icecrypt/funcs.jsm"); /*global IcecryptFuncs: false */
Components.utils.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Components.utils.import("resource://icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */
Components.utils.import("resource://icecrypt/os.jsm"); /*global IcecryptOS: false */
Components.utils.import("resource://icecrypt/armor.jsm"); /*global IcecryptArmor: false */
Components.utils.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Components.utils.import("resource://icecrypt/files.jsm"); /*global IcecryptFiles: false */
Components.utils.import("resource://icecrypt/data.jsm"); /*global IcecryptData: false */
Components.utils.import("resource://icecrypt/app.jsm"); /*global IcecryptApp: false */
Components.utils.import("resource://icecrypt/dialog.jsm"); /*global IcecryptDialog: false */
Components.utils.import("resource://icecrypt/timer.jsm"); /*global IcecryptTimer: false */
Components.utils.import("resource://icecrypt/windows.jsm"); /* global: IcecryptWindows: false */
Components.utils.import("resource://icecrypt/events.jsm"); /*global IcecryptEvents: false */
Components.utils.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */
Components.utils.import("resource://icecrypt/uris.jsm"); /*global IcecryptURIs: false */
Components.utils.import("resource://icecrypt/constants.jsm"); /*global IcecryptConstants: false */
Components.utils.import("resource://icecrypt/passwords.jsm"); /*global IcecryptPassword: false */
Components.utils.import("resource://icecrypt/rules.jsm"); /*global IcecryptRules: false */
Components.utils.import("resource://icecrypt/mime.jsm"); /*global IcecryptMime: false */
Components.utils.import("resource://icecrypt/clipboard.jsm"); /*global IcecryptClipboard: false */

try {
  Components.utils.import("resource:///modules/MailUtils.js"); /*global MailUtils: false */
}
catch (ex) {}


if (!Icecrypt) var Icecrypt = {};

const IOSERVICE_CONTRACTID = "@mozilla.org/network/io-service;1";
const LOCAL_FILE_CONTRACTID = "@mozilla.org/file/local;1";

Icecrypt.msg = {
  editor: null,
  dirty: null,
  processed: null,
  timeoutId: null,
  sendPgpMime: false,
  sendMode: null, // the current default for sending a message (0, SIGN, ENCRYPT, or SIGN|ENCRYPT)
  sendModeDirty: false, // send mode or final send options changed?

  // processed reasons for encryption:
  reasonEncrypted: "",
  reasonSigned: "",

  // encrypt/sign/pgpmime according to rules?
  // (1:ENIG_UNDEF(undef/maybe), 0:ENIG_NEVER(never/forceNo), 2:ENIG_ALWAYS(always/forceYes),
  //  22:ENIG_AUTO_ALWAYS, 99:ENIG_CONFLICT(conflict))
  encryptByRules: IcecryptConstants.ENIG_UNDEF,
  signByRules: IcecryptConstants.ENIG_UNDEF,
  pgpmimeByRules: IcecryptConstants.ENIG_UNDEF,

  // forced to encrypt/sign/pgpmime?
  // (1:ENIG_UNDEF(undef/maybe), 0:ENIG_NEVER(never/forceNo), 2:ENIG_ALWAYS(always/forceYes))
  encryptForced: IcecryptConstants.ENIG_UNDEF,
  signForced: IcecryptConstants.ENIG_UNDEF,
  pgpmimeForced: IcecryptConstants.ENIG_UNDEF,

  finalSignDependsOnEncrypt: false, // does signing finally depends on encryption mode?

  // resulting final encrypt/sign/pgpmime mode:
  //  (-1:ENIG_FINAL_UNDEF, 0:ENIG_FINAL_NO, 1:ENIG_FINAL_YES, 10:ENIG_FINAL_FORCENO, 11:ENIG_FINAL_FORCEYES, 99:ENIG_FINAL_CONFLICT)
  statusEncrypted: IcecryptConstants.ENIG_FINAL_UNDEF,
  statusSigned: IcecryptConstants.ENIG_FINAL_UNDEF,
  statusPGPMime: IcecryptConstants.ENIG_FINAL_UNDEF,
  statusEncryptedInStatusBar: null, // last statusEncyrpted when processing status buttons
  // to find possible broken promise of encryption

  // processed strings to signal final encrypt/sign/pgpmime state:
  statusEncryptedStr: "???",
  statusSignedStr: "???",
  statusPGPMimeStr: "???",
  statusInlinePGPStr: "???",
  statusAttachOwnKey: "???",

  sendProcess: false,
  nextCommandId: null,
  composeBodyReady: false,
  identity: null,
  enableRules: null,
  modifiedAttach: null,
  lastFocusedWindow: null,
  determineSendFlagId: null,
  trustAllKeys: false,
  protectHeaders: false,
  attachOwnKeyObj: {
    appendAttachment: false,
    attachedObj: null,
    attachedKey: null
  },

  compFieldsEnig_CID: "@mozdev.org/icecrypt/composefields;1",

  saveDraftError: 0,


  composeStartup: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.composeStartup\n");

    function delayedProcessFinalState() {
      IcecryptTimer.setTimeout(function _f() {
          Icecrypt.msg.processFinalState();
          Icecrypt.msg.updateStatusBar();
        },
        50);
    }

    function addSecurityListener(itemId, func) {
      let s = document.getElementById(itemId);
      if (s) {
        s.addEventListener("command", func);
      }
      else {
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: addSecurityListener - cannot find element " + itemId + "\n");
      }
    }

    // Relabel/hide SMIME button and menu item
    var smimeButton = document.getElementById("button-security");

    if (smimeButton) {
      smimeButton.setAttribute("label", "S/MIME");
    }

    var enigButton = document.getElementById("button-icecrypt-send");

    var msgId = document.getElementById("msgIdentityPopup");
    if (msgId) {
      msgId.addEventListener("command", Icecrypt.msg.setIdentityCallback);
    }

    var subj = document.getElementById("msgSubject");
    subj.addEventListener('focus', Icecrypt.msg.fireSendFlags);
    //subj.setAttribute('onfocus', "Icecrypt.msg.fireSendFlags()");

    // listen to S/MIME changes to potentially display "conflict" message
    addSecurityListener("menu_securitySign1", delayedProcessFinalState);
    addSecurityListener("menu_securitySign2", delayedProcessFinalState);
    addSecurityListener("menu_securityEncryptRequire1", delayedProcessFinalState);
    addSecurityListener("menu_securityEncryptRequire2", delayedProcessFinalState);

    this.msgComposeReset(false); // false => not closing => call setIdentityDefaults()
    this.composeOpen();
    this.processFinalState();
    this.updateStatusBar();
  },


  composeUnload: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.composeUnload\n");
    //if (gMsgCompose)
    //  gMsgCompose.UnregisterStateListener(Icecrypt.composeStateListener);
  },


  handleClick: function(event, modifyType) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.handleClick\n");
    switch (event.button) {
      case 2:
        // do not process the event any futher
        // needed on Windows to prevent displaying the context menu
        event.preventDefault();
        this.doPgpButton();
        break;
      case 0:
        this.doPgpButton(modifyType);
        break;
    }
  },


  setIdentityCallback: function(elementId) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.setIdentityCallback: elementId=" + elementId + "\n");

    IcecryptTimer.setTimeout(function _f() {
        Icecrypt.msg.setIdentityDefaults();
      },
      50);
  },


  /* return whether the account specific setting key is enabled or disabled
   */
  getAccDefault: function(key) {
    //IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.getAccDefault: identity="+this.identity.key+"("+this.identity.email+") key="+key+"\n");

    var enabled = this.identity.getBoolAttribute("enablePgp");
    if (key == "enabled") {
      return enabled;
    }

    if (enabled) {
      var res = null;
      switch (key) {
        case 'sign':
          res = (this.identity.getIntAttribute("defaultSigningPolicy") > 0); // converts int property to bool property
          break;
        case 'encrypt':
          res = (this.identity.getIntAttribute("defaultEncryptionPolicy") > 0); // converts int property to bool property
          break;
        case 'pgpMimeMode':
          res = this.identity.getBoolAttribute(key);
          break;
        case 'signIfNotEnc':
          res = this.identity.getBoolAttribute("pgpSignPlain");
          break;
        case 'signIfEnc':
          res = this.identity.getBoolAttribute("pgpSignEncrypted");
          break;
        case 'attachPgpKey':
          res = this.identity.getBoolAttribute(key);
          break;
      }
      //IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.getAccDefault:   "+key+"="+res+"\n");
      return res;
    }
    else {
      // every detail is disabled if OpenPGP in general is disabled:
      switch (key) {
        case 'sign':
        case 'encrypt':
        case 'signIfNotEnc':
        case 'signIfEnc':
        case 'pgpMimeMode':
        case 'attachPgpKey':
          return false;
      }
    }

    // should not be reached
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.getAccDefault:   internal error: invalid key '" + key + "'\n");
    return null;
  },


  setIdentityDefaults: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.setIdentityDefaults\n");

    this.identity = getCurrentIdentity();
    if (this.getAccDefault("enabled")) {
      IcecryptFuncs.getSignMsg(this.identity); // convert old acc specific to new acc specific options
    }
    else {
      // reset status strings in menu to useful defaults
      this.statusEncryptedStr = IcecryptLocale.getString("encryptNo");
      this.statusSignedStr = IcecryptLocale.getString("signNo", [""]);
      this.statusPGPMimeStr = IcecryptLocale.getString("pgpmimeYes");
      this.statusInlinePGPStr = IcecryptLocale.getString("pgpmimeNo");
      this.statusAttachOwnKey = IcecryptLocale.getString("attachOwnKeyNo");
    }

    // reset default send settings, unless we have changed them already
    if (!this.sendModeDirty) {
      this.processAccountSpecificDefaultOptions();
      this.determineSendFlags(); // important to use identity specific settings
      this.processFinalState();
      this.updateStatusBar();
    }
  },


  // set the current default for sending a message
  // depending on the identity
  processAccountSpecificDefaultOptions: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.processAccountSpecificDefaultOptions\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;

    this.sendMode = 0;
    if (!this.getAccDefault("enabled")) {
      return;
    }

    if (this.getAccDefault("encrypt")) {
      this.sendMode |= ENCRYPT;
      this.reasonEncrypted = IcecryptLocale.getString("reasonEnabledByDefault");
    }
    if (this.getAccDefault("sign")) {
      this.sendMode |= SIGN;
      this.reasonSigned = IcecryptLocale.getString("reasonEnabledByDefault");
    }

    this.sendPgpMime = this.getAccDefault("pgpMimeMode");
    this.attachOwnKeyObj.appendAttachment = this.getAccDefault("attachPgpKey");
    this.setOwnKeyStatus();
    this.attachOwnKeyObj.attachedObj = null;
    this.attachOwnKeyObj.attachedKey = null;

    this.finalSignDependsOnEncrypt = (this.getAccDefault("signIfEnc") || this.getAccDefault("signIfNotEnc"));
  },

  /**
   * Get a mail URL from a uriSpec
   *
   * @param uriSpec: String - URI of the desired message
   *
   * @return Object: nsIURL or nsIMsgMailNewsUrl object
   */
  getUrlFromUriSpec: function(uriSpec) {
    try {
      if (!uriSpec)
        return null;

      let messenger = Components.classes["@mozilla.org/messenger;1"].getService(Components.interfaces.nsIMessenger);
      let msgService = messenger.messageServiceFromURI(uriSpec);

      let urlObj = {};
      msgService.GetUrlForUri(uriSpec, urlObj, null);

      let url = urlObj.value;

      if (url.scheme == "file") {
        return url;
      }
      else {
        return url.QueryInterface(Components.interfaces.nsIMsgMailNewsUrl);
      }

    }
    catch (ex) {
      return null;
    }
  },

  getOriginalMsgUri: function() {
    let draftId = gMsgCompose.compFields.draftId;
    let msgUri = null;

    if (typeof(draftId) == "string" && draftId.length > 0) {
      // original message is draft
      msgUri = draftId.replace(/\?.*$/, "");
    }
    else if (typeof(gMsgCompose.originalMsgURI) == "string" && gMsgCompose.originalMsgURI.length > 0) {
      // original message is a "true" mail
      msgUri = gMsgCompose.originalMsgURI;
    }

    return msgUri;
  },

  getMsgHdr: function(msgUri) {
    if (!msgUri) {
      msgUri = this.getOriginalMsgUri();
    }
    let messenger = Components.classes["@mozilla.org/messenger;1"].getService(Components.interfaces.nsIMessenger);
    return messenger.messageServiceFromURI(msgUri).messageURIToMsgHdr(msgUri);
  },

  getMsgProperties: function(msgUri, draft) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: Icecrypt.msg.getMsgProperties:\n");
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    let properties = 0;
    let self = this;

    try {
      let msgHdr = this.getMsgHdr(msgUri);
      if (msgHdr) {
        let msgUrl = this.getUrlFromUriSpec(msgUri);
        properties = msgHdr.getUint32Property("icecrypt");
        try {
          IcecryptMime.getMimeTreeFromUrl(msgUrl.spec, false, function _cb(mimeMsg) {
            if (draft) {
              self.setDraftOptions(mimeMsg);
            }
            else {
              self.checkMimeStructure(mimeMsg);
            }
          });
        }
        catch (ex) {
          IcecryptLog.DEBUG("icecryptMessengerOverlay.js: Icecrypt.msg.getMsgProperties: excetion in getMimeTreeFromUrl\n");
        }
      }
    }
    catch (ex) {
      IcecryptLog.DEBUG("icecryptMessengerOverlay.js: Icecrypt.msg.getMsgProperties: got exception '" + ex.toString() + "'\n");
    }

    if (IcecryptURIs.isEncryptedUri(msgUri)) {
      properties |= nsIIcecrypt.DECRYPTION_OKAY;
    }

    return properties;
  },

  checkMimeStructure: function(mimeMsg) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.checkMimeStructure\n");

    if (mimeMsg.fullContentType.search(/^multipart\/encrypted.*protocol="?application\/pgp-encrypted?"/i) === 0) return;

    function getEncryptedSubPart(mimeMsg) {
      if (mimeMsg.fullContentType.search(/^multipart\/encrypted.*protocol="?application\/pgp-encrypted?"/i) === 0) return true;
      if (mimeMsg.fullContentType.search(/^message\/rfc822/i) === 0) return false;

      for (let i in mimeMsg.subParts) {
        if (getEncryptedSubPart(mimeMsg.subParts[i])) return true;
      }

      return false;
    }

    for (let i in mimeMsg.subParts) {
      if (getEncryptedSubPart(mimeMsg.subParts[i])) {
        this.displayPartialEncryptedWarning();
        return;
      }
    }
  },

  /**
   * Display a warning message if we are replying to or forwarding
   * a partially decrypted email
   */
  displayPartialEncryptedWarning: function() {

    this.notifyUser(1, IcecryptLocale.getString("msgCompose.partiallyEncrypted.short"), "notifyPartialDecrypt", IcecryptLocale.getString("msgCompose.partiallyEncrypted.long"));
  },

  setDraftOptions: function(msg, mimeMsg) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.setDraftOptions\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    var stat = "";
    if (mimeMsg && mimeMsg.headers["x-icecrypt-draft-status"]) {
      stat = String(mimeMsg.headers["x-icecrypt-draft-status"]);
    }
    else {
      return;
    }

    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.setDraftOptions: draftStatus: " + stat + "\n");

    if (stat.substr(0, 1) == "N") {
      // new style drafts (Icecrypt 1.7)

      var enc = "final-encryptDefault";
      switch (Number(stat.substr(1, 1))) {
        case IcecryptConstants.ENIG_NEVER:
          enc = "final-encryptNo";
          break;
        case IcecryptConstants.ENIG_ALWAYS:
          enc = "final-encryptYes";
      }

      var sig = "final-signDefault";
      switch (Number(stat.substr(2, 1))) {
        case IcecryptConstants.ENIG_NEVER:
          sig = "final-signNo";
          break;
        case IcecryptConstants.ENIG_ALWAYS:
          sig = "final-signYes";
      }

      var pgpMime = "final-pgpmimeDefault";
      switch (Number(stat.substr(3, 1))) {
        case IcecryptConstants.ENIG_NEVER:
          pgpMime = "final-pgpmimeNo";
          break;
        case IcecryptConstants.ENIG_ALWAYS:
          pgpMime = "final-pgpmimeYes";
      }

      Icecrypt.msg.setFinalSendMode(enc);
      Icecrypt.msg.setFinalSendMode(sig);
      Icecrypt.msg.setFinalSendMode(pgpMime);

      if (stat.substr(4, 1) == "1") Icecrypt.msg.attachOwnKeyObj.appendAttachment = true;
    }
    else {
      // drafts from older versions of Icecrypt
      var flags = Number(stat);
      if (flags & nsIIcecrypt.SEND_SIGNED) Icecrypt.msg.setFinalSendMode('final-signYes');
      if (flags & nsIIcecrypt.SEND_ENCRYPTED) Icecrypt.msg.setFinalSendMode('final-encryptYes');
      if (flags & nsIIcecrypt.SEND_ATTACHMENT) Icecrypt.msg.attachOwnKeyObj.appendAttachment = true;
    }
    Icecrypt.msg.setOwnKeyStatus();
  },


  composeOpen: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.composeOpen\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;

    var msgFlags;
    var msgUri = null;
    var msgIsDraft = false;

    this.determineSendFlagId = null;
    this.disableSmime = false;
    this.saveDraftError = 0;
    this.protectHeaders = IcecryptPrefs.getPref("protectHeaders");
    this.enableUndoEncryption(false);

    this.displayProtectHeadersStatus();

    var toobarElem = document.getElementById("composeToolbar2");
    if (toobarElem && (IcecryptOS.getOS() == "Darwin")) {
      toobarElem.setAttribute("platform", "macos");
    }

    // check rules for status bar icons on each change of the recipients
    var adrCol = document.getElementById("addressCol2#1"); // recipients field
    if (adrCol) {
      let attr = adrCol.getAttribute("oninput");
      adrCol.setAttribute("oninput", attr + "; Icecrypt.msg.addressOnChange();");
      attr = adrCol.getAttribute("onchange");
      adrCol.setAttribute("onchange", attr + "; Icecrypt.msg.addressOnChange();");
      adrCol.setAttribute("observes", "icecrypt-bc-sendprocess");
    }
    adrCol = document.getElementById("addressCol1#1"); // to/cc/bcc/... field
    if (adrCol) {
      let attr = adrCol.getAttribute("oncommand");
      adrCol.setAttribute("oncommand", attr + "; Icecrypt.msg.addressOnChange();");
      adrCol.setAttribute("observes", "icecrypt-bc-sendprocess");
    }

    var draftId = gMsgCompose.compFields.draftId;

    if (IcecryptPrefs.getPref("keepSettingsForReply") && (!(this.sendMode & ENCRYPT)) || (typeof(draftId) == "string" && draftId.length > 0)) {
      if (typeof(draftId) == "string" && draftId.length > 0) {
        // original message is draft
        msgUri = draftId.replace(/\?.*$/, "");
        msgIsDraft = true;
      }
      else if (typeof(gMsgCompose.originalMsgURI) == "string" && gMsgCompose.originalMsgURI.length > 0) {
        // original message is a "true" mail
        msgUri = gMsgCompose.originalMsgURI;
      }

      if (msgUri) {
        msgFlags = this.getMsgProperties(msgUri, msgIsDraft);
        if (!msgIsDraft) {
          if (msgFlags & nsIIcecrypt.DECRYPTION_OKAY) {
            IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.composeOpen: has encrypted originalMsgUri\n");
            IcecryptLog.DEBUG("originalMsgURI=" + gMsgCompose.originalMsgURI + "\n");
            this.setSendMode('encrypt');

            // TODO: disable S/MIME in case it is enabled

            this.disableSmime = true;
          }
          else if (msgFlags & (nsIIcecrypt.GOOD_SIGNATURE |
              nsIIcecrypt.BAD_SIGNATURE |
              nsIIcecrypt.UNVERIFIED_SIGNATURE)) {
            this.setSendMode('sign');
          }
        }
        this.removeAttachedKey();
      }
    }

    // check for attached signature files and remove them
    var bucketList = document.getElementById("attachmentBucket");
    if (bucketList.hasChildNodes()) {
      var node = bucketList.firstChild;
      let nodeNumber = 0;
      while (node) {
        if (node.attachment.contentType == "application/pgp-signature") {
          if (!this.findRelatedAttachment(bucketList, node)) {
            node = bucketList.removeItemAt(nodeNumber);
            // Let's release the attachment object held by the node else it won't go away until the window is destroyed
            node.attachment = null;
          }
        }
        else {
          ++nodeNumber;
        }
        node = node.nextSibling;
      }
      if (!bucketList.hasChildNodes()) {
        try {
          // TB only
          UpdateAttachmentBucket(false);
        }
        catch (ex) {}
      }
    }

    try {
      // TB only
      UpdateAttachmentBucket(bucketList.hasChildNodes());
    }
    catch (ex) {}

    this.processFinalState();
    this.updateStatusBar();
  },


  // check if an signature is related to another attachment
  findRelatedAttachment: function(bucketList, node) {

    // check if filename ends with .sig
    if (node.attachment.name.search(/\.sig$/i) < 0) return null;

    var relatedNode = bucketList.firstChild;
    var findFile = node.attachment.name.toLowerCase();
    var baseAttachment = null;
    while (relatedNode) {
      if (relatedNode.attachment.name.toLowerCase() + ".sig" == findFile) baseAttachment = relatedNode.attachment;
      relatedNode = relatedNode.nextSibling;
    }
    return baseAttachment;
  },

  msgComposeReopen: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.msgComposeReopen\n");
    this.msgComposeReset(false); // false => not closing => call setIdentityDefaults()
    this.composeOpen();
    this.fireSendFlags();

    IcecryptTimer.setTimeout(function _f() {
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay: re-determine send flags\n");
      try {
        this.determineSendFlags();
        this.processFinalState();
        this.updateStatusBar();
      }
      catch (ex) {
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay: re-determine send flags - ERROR: " + ex.toString() + "\n");
      }
    }.bind(Icecrypt.msg), 1000);
  },


  msgComposeClose: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.msgComposeClose\n");

    var ioServ;
    try {
      // we should delete the original temporary files of the encrypted or signed
      // inline PGP attachments (the rest is done automatically)
      if (this.modifiedAttach) {
        ioServ = Components.classes[IOSERVICE_CONTRACTID].getService(Components.interfaces.nsIIOService);
        if (!ioServ)
          return;

        for (var i in this.modifiedAttach) {
          if (this.modifiedAttach[i].origTemp) {
            IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.msgComposeClose: deleting " + this.modifiedAttach[i].origUrl + "\n");
            var fileUri = ioServ.newURI(this.modifiedAttach[i].origUrl, null, null);
            var fileHandle = Components.classes[LOCAL_FILE_CONTRACTID].createInstance(Components.interfaces.nsIFile);
            fileHandle.initWithPath(fileUri.path);
            if (fileHandle.exists()) fileHandle.remove(false);
          }
        }
        this.modifiedAttach = null;
      }

    }
    catch (ex) {
      IcecryptLog.ERROR("icecryptMsgComposeOverlay.js: ECSL.ComposeProcessDone: could not delete all files:\n" + ex.toString() + "\n");
    }

    this.msgComposeReset(true); // true => closing => don't call setIdentityDefaults()
  },


  msgComposeReset: function(closing) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.msgComposeReset\n");

    this.dirty = 0;
    this.processed = null;
    this.timeoutId = null;

    this.modifiedAttach = null;
    this.sendMode = 0;
    this.sendModeDirty = false;
    this.reasonEncrypted = "";
    this.reasonSigned = "";
    this.encryptByRules = IcecryptConstants.ENIG_UNDEF;
    this.signByRules = IcecryptConstants.ENIG_UNDEF;
    this.pgpmimeByRules = IcecryptConstants.ENIG_UNDEF;
    this.signForced = IcecryptConstants.ENIG_UNDEF;
    this.encryptForced = IcecryptConstants.ENIG_UNDEF;
    this.pgpmimeForced = IcecryptConstants.ENIG_UNDEF;
    this.finalSignDependsOnEncrypt = false;
    this.statusSigned = IcecryptConstants.ENIG_FINAL_UNDEF;
    this.statusEncrypted = IcecryptConstants.ENIG_FINAL_UNDEF;
    this.statusPGPMime = IcecryptConstants.ENIG_FINAL_UNDEF;
    this.statusEncryptedStr = "???";
    this.statusSignedStr = "???";
    this.statusPGPMimeStr = "???";
    this.statusInlinePGPStr = "???";
    this.statusAttachOwnKey = "???";
    this.enableRules = true;
    this.identity = null;
    this.sendProcess = false;
    this.trustAllKeys = false;

    if (!closing) {
      this.setIdentityDefaults();
    }
  },


  initRadioMenu: function(prefName, optionIds) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: Icecrypt.msg.initRadioMenu: " + prefName + "\n");

    var encryptId;

    var prefValue = IcecryptPrefs.getPref(prefName);

    if (prefValue >= optionIds.length)
      return;

    var menuItem = document.getElementById("icecrypt_" + optionIds[prefValue]);
    if (menuItem)
      menuItem.setAttribute("checked", "true");
  },


  usePpgMimeOption: function(value) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: Icecrypt.msg.usePpgMimeOption: " + value + "\n");

    IcecryptPrefs.setPref("usePGPMimeOption", value);

    return true;
  },

  togglePgpMime: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.togglePgpMime\n");

    this.sendPgpMime = !this.sendPgpMime;
  },

  tempTrustAllKeys: function() {
    this.trustAllKeys = !this.trustAllKeys;
  },

  toggleAttachOwnKey: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.toggleAttachOwnKey\n");
    IcecryptCore.getService(window); // make sure Icecrypt is loaded and working

    this.attachOwnKeyObj.appendAttachment = !this.attachOwnKeyObj.appendAttachment;

    this.setOwnKeyStatus();
  },

  toggleProtectHeaders: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.toggleProtectHeaders\n");
    IcecryptCore.getService(window); // make sure Icecrypt is loaded and working

    this.protectHeaders = !this.protectHeaders;

    this.displayProtectHeadersStatus();
  },

  displayProtectHeadersStatus: function() {
    let bc = document.getElementById("icecrypt-bc-protectHdr");

    if (this.protectHeaders) {
      bc.setAttribute("checked", "true");
    }
    else {
      bc.removeAttribute("checked");
    }
  },

  /***
   * set broadcaster to display whether the own key is attached or not
   */

  setOwnKeyStatus: function() {
    let bc = document.getElementById("icecrypt-bc-attach");
    let attachIcon = document.getElementById("button-icecrypt-attach");

    if (this.allowAttachOwnKey() === 0) {
      this.statusAttachOwnKey = IcecryptLocale.getString("attachOwnKeyDisabled");
    }
    else {
      if (this.attachOwnKeyObj.appendAttachment) {
        bc.setAttribute("addPubkey", "true");
        bc.setAttribute("checked", "true");
        this.statusAttachOwnKey = IcecryptLocale.getString("attachOwnKeyYes");
      }
      else {
        bc.setAttribute("addPubkey", "false");
        bc.removeAttribute("checked");
        this.statusAttachOwnKey = IcecryptLocale.getString("attachOwnKeyNo");
      }
    }

    if (attachIcon)
      attachIcon.setAttribute("tooltiptext", this.statusAttachOwnKey);

  },

  attachOwnKey: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.attachOwnKey:\n");

    var userIdValue;

    if (this.identity.getIntAttribute("pgpKeyMode") > 0) {
      userIdValue = this.identity.getCharAttribute("pgpkeyId");

      if (this.attachOwnKeyObj.attachedKey && (this.attachOwnKeyObj.attachedKey != userIdValue)) {
        // remove attached key if user ID changed
        this.removeAttachedKey();
      }

      if (!this.attachOwnKeyObj.attachedKey) {
        var attachedObj = this.extractAndAttachKey([userIdValue]);
        if (attachedObj) {
          this.attachOwnKeyObj.attachedObj = attachedObj;
          this.attachOwnKeyObj.attachedKey = userIdValue;
        }
      }
    }
    else {
      IcecryptLog.ERROR("icecryptMsgComposeOverlay.js: Icecrypt.msg.attachOwnKey: trying to attach unknown own key!\n");
    }
  },

  attachKey: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.attachKey: \n");

    var resultObj = {};
    var inputObj = {};
    inputObj.dialogHeader = IcecryptLocale.getString("keysToExport");
    inputObj.options = "multisel,allowexpired,nosending";
    if (this.trustAllKeys) {
      inputObj.options += ",trustallkeys";
    }
    var userIdValue = "";

    window.openDialog("chrome://icecrypt/content/icecryptKeySelection.xul", "", "dialog,modal,centerscreen,resizable", inputObj, resultObj);
    try {
      if (resultObj.cancelled) return;
      this.extractAndAttachKey(resultObj.userList);
    }
    catch (ex) {
      // cancel pressed -> do nothing
      return;
    }
  },

  extractAndAttachKey: function(uid) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.attachKey: \n");
    var icecryptSvc = IcecryptCore.getService(window);
    if (!icecryptSvc)
      return null;

    var tmpDir = IcecryptFiles.getTempDir();
    var tmpFile;
    try {
      tmpFile = Components.classes[LOCAL_FILE_CONTRACTID].createInstance(Components.interfaces.nsIFile);
      tmpFile.initWithPath(tmpDir);
      if (!(tmpFile.isDirectory() && tmpFile.isWritable())) {
        IcecryptDialog.alert(window, IcecryptLocale.getString("noTempDir"));
        return null;
      }
    }
    catch (ex) {
      IcecryptLog.writeException("icecryptMsgComposeOverlay.js: Icecrypt.msg.extractAndAttachKey", ex);
    }
    tmpFile.append("key.asc");
    tmpFile.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0x180); // equal 0600

    // save file
    var exitCodeObj = {};
    var errorMsgObj = {};

    IcecryptKeyRing.extractKey(false, uid.join(" "), tmpFile /*.path */ , exitCodeObj, errorMsgObj);
    if (exitCodeObj.value !== 0) {
      IcecryptDialog.alert(window, errorMsgObj.value);
      return null;
    }

    // create attachment
    var ioServ = Components.classes[IOSERVICE_CONTRACTID].getService(Components.interfaces.nsIIOService);
    var tmpFileURI = ioServ.newFileURI(tmpFile);
    var keyAttachment = Components.classes["@mozilla.org/messengercompose/attachment;1"].createInstance(Components.interfaces.nsIMsgAttachment);
    keyAttachment.url = tmpFileURI.spec;
    if ((uid.length == 1) && (uid[0].search(/^(0x)?[a-fA-F0-9]+$/) === 0)) {
      keyAttachment.name = "0x" + uid[0].substr(-8, 8) + ".asc";
    }
    else {
      keyAttachment.name = "pgpkeys.asc";
    }
    keyAttachment.temporary = true;
    keyAttachment.contentType = "application/pgp-keys";

    // add attachment to msg
    this.addAttachment(keyAttachment);

    try {
      // TB only
      ChangeAttachmentBucketVisibility(false);
    }
    catch (ex) {}
    gContentChanged = true;
    return keyAttachment;
  },

  addAttachment: function(attachment) {
    if (typeof(AddAttachment) == "undefined") {
      // TB >= 24
      AddAttachments([attachment]);
    }
    else {
      // SeaMonkey
      AddAttachment(attachment);
    }
  },

  enableUndoEncryption: function(newStatus) {
    let eue = document.getElementById("icecrypt_undo_encryption");

    if (newStatus) {
      eue.removeAttribute("disabled");
    }
    else
      eue.setAttribute("disabled", "true");
  },


  /**
   *  undo the encryption or signing; get back the original (unsigned/unencrypted) text
   *
   * useEditorUndo |Number|:   > 0  use undo function of editor |n| times
   *                           0: replace text with original text
   */
  undoEncryption: function(useEditorUndo) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.undoEncryption:\n");
    if (this.processed) {
      if (useEditorUndo) {
        IcecryptTimer.setTimeout(function _f() {
          Icecrypt.msg.editor.undo(useEditorUndo);
        }, 10);
      }
      else {
        this.replaceEditorText(this.processed.origText);
        this.enableUndoEncryption(false);
      }
      this.processed = null;

    }
    else {
      this.decryptQuote(true);
    }

    var node;
    var nodeNumber;
    var bucketList = document.getElementById("attachmentBucket");
    if (this.modifiedAttach && bucketList && bucketList.hasChildNodes()) {
      // undo inline encryption of attachments
      for (var i = 0; i < this.modifiedAttach.length; i++) {
        node = bucketList.firstChild;
        nodeNumber = -1;
        while (node) {
          ++nodeNumber;
          if (node.attachment.url == this.modifiedAttach[i].newUrl) {
            if (this.modifiedAttach[i].encrypted) {
              node.attachment.url = this.modifiedAttach[i].origUrl;
              node.attachment.name = this.modifiedAttach[i].origName;
              node.attachment.temporary = this.modifiedAttach[i].origTemp;
              node.attachment.contentType = this.modifiedAttach[i].origCType;
            }
            else {
              node = bucketList.removeItemAt(nodeNumber);
              // Let's release the attachment object held by the node else it won't go away until the window is destroyed
              node.attachment = null;
            }
            // delete encrypted file
            try {
              this.modifiedAttach[i].newFile.remove(false);
            }
            catch (ex) {}

            node = null; // next attachment please
          }
          else {
            node = node.nextSibling;
          }
        }
      }
    }

    this.removeAttachedKey();
  },


  removeAttachedKey: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.removeAttachedKey: \n");

    var bucketList = document.getElementById("attachmentBucket");
    var node = bucketList.firstChild;

    if (bucketList && bucketList.hasChildNodes() && this.attachOwnKeyObj.attachedObj) {
      // undo attaching own key
      var nodeNumber = -1;
      while (node) {
        ++nodeNumber;
        if (node.attachment.url == this.attachOwnKeyObj.attachedObj.url) {
          node = bucketList.removeItemAt(nodeNumber);
          // Let's release the attachment object held by the node else it won't go away until the window is destroyed
          node.attachment = null;
          this.attachOwnKeyObj.attachedObj = null;
          this.attachOwnKeyObj.attachedKey = null;
          node = null; // exit loop
        }
        else {
          node = node.nextSibling;
        }
      }
      if (!bucketList.hasChildNodes()) {
        try {
          // TB only
          ChangeAttachmentBucketVisibility(true);
        }
        catch (ex) {}
      }
    }
  },


  resetUpdatedFields: function() {
    this.removeAttachedKey();

    // reset subject
    if (gMsgCompose.compFields.securityInfo instanceof Components.interfaces.nsIEnigMsgCompFields) {
      let si = gMsgCompose.compFields.securityInfo.QueryInterface(Components.interfaces.nsIEnigMsgCompFields);
      if (si.originalSubject) {
        gMsgCompose.compFields.subject = si.originalSubject;
      }
    }
  },


  replaceEditorText: function(text) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.replaceEditorText:\n");

    this.editorSelectAll();
    // Overwrite text in clipboard for security
    // (Otherwise plaintext will be available in the clipbaord)

    if (this.editor.textLength > 0) {
      this.editorInsertText("Icecrypt");
    }
    else {
      this.editorInsertText(" ");
    }

    this.editorSelectAll();
    this.editorInsertText(text);
  },


  getMsgFolderFromUri: function(uri, checkFolderAttributes) {
    let msgfolder = null;
    if (typeof MailUtils != 'undefined') {
      return MailUtils.getFolderForURI(uri, checkFolderAttributes);
    }
    try {
      // Postbox, older versions of TB
      let resource = GetResourceFromUri(uri);
      msgfolder = resource.QueryInterface(Components.interfaces.nsIMsgFolder);
      if (checkFolderAttributes) {
        if (!(msgfolder && (msgfolder.parent || msgfolder.isServer))) {
          msgfolder = null;
        }
      }
    }
    catch (ex) {
      //IcecryptLog.DEBUG("failed to get the folder resource\n");
    }
    return msgfolder;
  },


  goAccountManager: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.goAccountManager:\n");
    IcecryptCore.getService(window);
    var currentId = null;
    var server = null;
    try {
      currentId = getCurrentIdentity();
      var amService = Components.classes["@mozilla.org/messenger/account-manager;1"].getService();
      var servers, folderURI;
      try {
        // Gecko >= 20
        servers = amService.getServersForIdentity(currentId);
        folderURI = servers.queryElementAt(0, Components.interfaces.nsIMsgIncomingServer).serverURI;
      }
      catch (ex) {
        servers = amService.GetServersForIdentity(currentId);
        folderURI = servers.GetElementAt(0).QueryInterface(Components.interfaces.nsIMsgIncomingServer).serverURI;
      }

      server = this.getMsgFolderFromUri(folderURI, true).server;
    }
    catch (ex) {}
    window.openDialog("chrome://icecrypt/content/am-enigprefs-edit.xul", "", "dialog,modal,centerscreen", {
      identity: currentId,
      account: server
    });
    this.setIdentityDefaults();
  },


  doPgpButton: function(what) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.doPgpButton: what=" + what + "\n");

    // Note: For the toolbar button this is indirectly triggered:
    //       - the menu items trigger nextCommand()
    //       - because afterwards doPgpButton('') is always called (for whatever reason)
    if (!what) {
      what = this.nextCommandId;
    }
    this.nextCommandId = "";
    IcecryptCore.getService(window); // try to access Icecrypt to launch the wizard if needed

    // ignore settings for this account?
    try {
      if (!this.getAccDefault("enabled")) {
        if (IcecryptDialog.confirmDlg(window, IcecryptLocale.getString("configureNow"),
            IcecryptLocale.getString("msgCompose.button.configure"))) {
          // configure account settings for the first time
          this.goAccountManager();
          if (!this.identity.getBoolAttribute("enablePgp")) {
            return;
          }
        }
        else {
          return;
        }
      }
    }
    catch (ex) {}

    switch (what) {
      case 'sign':
      case 'encrypt':
      case 'toggle-sign':
      case 'toggle-encrypt':
        this.setSendMode(what);
        break;

        // menu entries:
      case 'final-signDefault':
      case 'final-signYes':
      case 'final-signNo':
      case 'final-encryptDefault':
      case 'final-encryptYes':
      case 'final-encryptNo':
      case 'final-pgpmimeDefault':
      case 'final-pgpmimeYes':
      case 'final-pgpmimeNo':
      case 'toggle-final-sign':
      case 'toggle-final-encrypt':
      case 'toggle-final-mime':
        this.setFinalSendMode(what);
        break;

      case 'togglePGPMime':
        this.togglePgpMime();
        break;

      case 'toggleRules':
        this.toggleRules();
        break;

      case 'trustKeys':
        this.tempTrustAllKeys();
        break;

      case 'nothing':
        break;

      case 'displaySecuritySettings':
        this.displaySecuritySettings();
        break;
      default:
        this.displaySecuritySettings();
    }

  },


  nextCommand: function(what) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.nextCommand: what=" + what + "\n");
    this.nextCommandId = what;
  },


  // changes the DEFAULT sendMode
  // - also called internally for saved emails
  setSendMode: function(sendMode) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.setSendMode: sendMode=" + sendMode + "\n");
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;

    var origSendMode = this.sendMode;
    switch (sendMode) {
      case 'sign':
        this.sendMode |= SIGN;
        break;
      case 'encrypt':
        this.sendMode |= ENCRYPT;
        break;
      case 'toggle-sign':
        if (this.sendMode & SIGN) {
          this.sendMode &= ~SIGN;
        }
        else {
          this.sendMode |= SIGN;
        }
        break;
      case 'toggle-encrypt':
        if (this.sendMode & ENCRYPT) {
          this.sendMode &= ~ENCRYPT;
        }
        else {
          this.sendMode |= ENCRYPT;
        }
        break;
      default:
        IcecryptDialog.alert(window, "Icecrypt.msg.setSendMode - unexpected value: " + sendMode);
        break;
    }
    // sendMode changed ?
    // - sign and send are internal initializations
    if (!this.sendModeDirty && (this.sendMode != origSendMode) && sendMode != 'sign' && sendMode != 'encrypt') {
      this.sendModeDirty = true;
    }
    this.processFinalState();
    this.updateStatusBar();
  },


  // changes the FINAL sendMode
  // - triggered by the user interface
  setFinalSendMode: function(sendMode) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.setFinalSendMode: sendMode=" + sendMode + "\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;

    switch (sendMode) {

      // menu entries for final settings:

      case 'final-encryptDefault':
        // switch encryption to "use defaults & rules"
        if (this.encryptForced != IcecryptConstants.ENIG_UNDEF) { // if encrypt/noencrypt forced
          this.encryptForced = IcecryptConstants.ENIG_UNDEF; // back to defaults/rules
        }
        break;
      case 'final-encryptYes':
        // switch encryption to "force encryption"
        if (this.encryptForced != IcecryptConstants.ENIG_ALWAYS) { // if not forced to encrypt
          this.encryptForced = IcecryptConstants.ENIG_ALWAYS; // force to encrypt
        }
        break;
      case 'final-encryptNo':
        // switch encryption to "force no to encrypt"
        if (this.encryptForced != IcecryptConstants.ENIG_NEVER) { // if not forced not to encrypt
          this.encryptForced = IcecryptConstants.ENIG_NEVER; // force not to encrypt
        }
        break;

      case 'final-signDefault':
        // switch signing to "use defaults & rules"
        if (this.signForced != IcecryptConstants.ENIG_UNDEF) { // if sign/nosign forced
          // re-init if signing depends on encryption if this was broken before
          this.finalSignDependsOnEncrypt = (this.getAccDefault("signIfEnc") || this.getAccDefault("signIfNotEnc"));
          this.signForced = IcecryptConstants.ENIG_UNDEF; // back to defaults/rules
        }
        break;
      case 'final-signYes':
        if (this.signForced != IcecryptConstants.ENIG_ALWAYS) { // if not forced to sign
          this.signingNoLongerDependsOnEnc();
          this.signForced = IcecryptConstants.ENIG_ALWAYS; // force to sign
        }
        break;
      case 'final-signNo':
        if (this.signForced != IcecryptConstants.ENIG_NEVER) { // if not forced not to sign
          this.signingNoLongerDependsOnEnc();
          this.signForced = IcecryptConstants.ENIG_NEVER; // force not to sign
        }
        break;

      case 'final-pgpmimeDefault':
        if (this.pgpmimeForced != IcecryptConstants.ENIG_UNDEF) { // if any PGP mode forced
          this.pgpmimeForced = IcecryptConstants.ENIG_UNDEF; // back to defaults/rules
        }
        break;
      case 'final-pgpmimeYes':
        if (this.pgpmimeForced != IcecryptConstants.ENIG_ALWAYS) { // if not forced to PGP/Mime
          this.pgpmimeForced = IcecryptConstants.ENIG_ALWAYS; // force to PGP/Mime
        }
        break;
      case 'final-pgpmimeNo':
        if (this.pgpmimeForced != IcecryptConstants.ENIG_NEVER) { // if not forced not to PGP/Mime
          this.pgpmimeForced = IcecryptConstants.ENIG_NEVER; // force not to PGP/Mime
        }
        break;

        // status bar buttons:
        // - can only switch to force or not to force sign/enc

      case 'toggle-final-sign':
        this.signingNoLongerDependsOnEnc();
        switch (this.statusSigned) {
          case IcecryptConstants.ENIG_FINAL_NO:
          case IcecryptConstants.ENIG_FINAL_FORCENO:
            this.signForced = IcecryptConstants.ENIG_ALWAYS; // force to sign
            break;
          case IcecryptConstants.ENIG_FINAL_YES:
          case IcecryptConstants.ENIG_FINAL_FORCEYES:
            this.signForced = IcecryptConstants.ENIG_NEVER; // force not to sign
            break;
          case IcecryptConstants.ENIG_FINAL_CONFLICT:
            this.signForced = IcecryptConstants.ENIG_NEVER;
            break;
        }
        break;

      case 'toggle-final-encrypt':
        switch (this.statusEncrypted) {
          case IcecryptConstants.ENIG_FINAL_NO:
          case IcecryptConstants.ENIG_FINAL_FORCENO:
            this.encryptForced = IcecryptConstants.ENIG_ALWAYS; // force to encrypt
            break;
          case IcecryptConstants.ENIG_FINAL_YES:
          case IcecryptConstants.ENIG_FINAL_FORCEYES:
            this.encryptForced = IcecryptConstants.ENIG_NEVER; // force not to encrypt
            break;
          case IcecryptConstants.ENIG_FINAL_CONFLICT:
            this.encryptForced = IcecryptConstants.ENIG_NEVER;
            break;
        }
        break;

      case 'toggle-final-mime':
        switch (this.statusPGPMime) {
          case IcecryptConstants.ENIG_FINAL_NO:
          case IcecryptConstants.ENIG_FINAL_FORCENO:
            this.pgpmimeForced = IcecryptConstants.ENIG_ALWAYS; // force PGP/MIME
            break;
          case IcecryptConstants.ENIG_FINAL_YES:
          case IcecryptConstants.ENIG_FINAL_FORCEYES:
            this.pgpmimeForced = IcecryptConstants.ENIG_NEVER; // force Inline-PGP
            break;
          case IcecryptConstants.ENIG_FINAL_CONFLICT:
            this.pgpmimeForced = IcecryptConstants.ENIG_NEVER;
            break;
        }
        break;

      default:
        IcecryptDialog.alert(window, "Icecrypt.msg.setFinalSendMode - unexpected value: " + sendMode);
        break;
    }

    // this is always a send mode change (only toggle effects)
    this.sendModeDirty = true;

    this.processFinalState();
    this.updateStatusBar();
  },


  // key function to process the final encrypt/sign/pgpmime state from all settings
  // sendFlags: contains the sendFlags if the message is really processed. Optional, can be null
  // - uses as INPUT:
  //   - this.sendMode
  //   - this.encryptByRules, this.signByRules, pgpmimeByRules
  //   - this.encryptForced, this.encryptSigned
  // - uses as OUTPUT:
  //   - this.statusEncrypt, this.statusSign, this.statusPGPMime
  processFinalState: function(sendFlags) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.processFinalState()\n");
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;


    var encFinally = null;
    var encReason = "";
    var signFinally = null;
    var signReason = "";
    var pgpmimeFinally = null;

    // process resulting encrypt mode
    if (this.encryptForced == IcecryptConstants.ENIG_NEVER) { // force not to encrypt?
      encFinally = IcecryptConstants.ENIG_FINAL_FORCENO;
      encReason = IcecryptLocale.getString("reasonManuallyForced");
    }
    else if (this.encryptForced == IcecryptConstants.ENIG_ALWAYS) { // force to encrypt?
      encFinally = IcecryptConstants.ENIG_FINAL_FORCEYES;
      encReason = IcecryptLocale.getString("reasonManuallyForced");
    }
    else switch (this.encryptByRules) {
      case IcecryptConstants.ENIG_NEVER:
        encFinally = IcecryptConstants.ENIG_FINAL_NO;
        encReason = IcecryptLocale.getString("reasonByRecipientRules");
        break;
      case IcecryptConstants.ENIG_UNDEF:
        if (this.sendMode & ENCRYPT) {
          encFinally = IcecryptConstants.ENIG_FINAL_YES;
          if (this.getAccDefault("encrypt")) {
            encReason = IcecryptLocale.getString("reasonEnabledByDefault");
          }
        }
        else {
          encFinally = IcecryptConstants.ENIG_FINAL_NO;
        }
        break;
      case IcecryptConstants.ENIG_ALWAYS:
        encFinally = IcecryptConstants.ENIG_FINAL_YES;
        encReason = IcecryptLocale.getString("reasonByRecipientRules");
        break;
      case IcecryptConstants.ENIG_AUTO_ALWAYS:
        encFinally = IcecryptConstants.ENIG_FINAL_YES;
        encReason = IcecryptLocale.getString("reasonByAutoEncryption");
        break;
      case IcecryptConstants.ENIG_CONFLICT:
        encFinally = IcecryptConstants.ENIG_FINAL_CONFLICT;
        encReason = IcecryptLocale.getString("reasonByConflict");
        break;
    }
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js:   encrypt=" + ((this.sendMode & ENCRYPT) !== 0) + " encryptByRules=" + this.encryptByRules + " encFinally=" + encFinally + "\n");
    IcecryptLog.DEBUG("                                encReason=" + encReason + "\n");

    // process resulting sign mode
    if (this.signForced == IcecryptConstants.ENIG_NEVER) { // force not to sign?
      signFinally = IcecryptConstants.ENIG_FINAL_FORCENO;
      signReason = IcecryptLocale.getString("reasonManuallyForced");
    }
    else if (this.signForced == IcecryptConstants.ENIG_ALWAYS) { // force to sign?
      signFinally = IcecryptConstants.ENIG_FINAL_FORCEYES;
      signReason = IcecryptLocale.getString("reasonManuallyForced");
    }
    else switch (this.signByRules) {
      case IcecryptConstants.ENIG_NEVER:
        signFinally = IcecryptConstants.ENIG_FINAL_NO;
        signReason = IcecryptLocale.getString("reasonByRecipientRules");
        break;
      case IcecryptConstants.ENIG_UNDEF:
        if (this.sendMode & SIGN) {
          signFinally = IcecryptConstants.ENIG_FINAL_YES;
          if (this.getAccDefault("sign")) {
            signReason = IcecryptLocale.getString("reasonEnabledByDefault");
          }
        }
        else {
          signFinally = IcecryptConstants.ENIG_FINAL_NO;
        }
        break;
      case IcecryptConstants.ENIG_ALWAYS:
        signFinally = IcecryptConstants.ENIG_FINAL_YES;
        signReason = IcecryptLocale.getString("reasonByRecipientRules");
        break;
      case IcecryptConstants.ENIG_CONFLICT:
        signFinally = IcecryptConstants.ENIG_FINAL_CONFLICT;
        signReason = IcecryptLocale.getString("reasonByConflict");
        break;
    }
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js:   signed=" + ((this.sendMode & SIGN) !== 0) + " signByRules=" + this.signByRules + " signFinally=" + signFinally + "\n");
    IcecryptLog.DEBUG("                                signReason=" + signReason + "\n");

    // process option to finally sign if encrypted/unencrypted
    // (unless rules force not to sign)
    //var derivedFromEncMode = false;
    if (this.finalSignDependsOnEncrypt) {
      if (this.signByRules == IcecryptConstants.ENIG_UNDEF) { // if final sign mode not clear yet
        //derivedFromEncMode = true;
        switch (encFinally) {
          case IcecryptConstants.ENIG_FINAL_YES:
          case IcecryptConstants.ENIG_FINAL_FORCEYES:
            if (this.getAccDefault("signIfEnc")) {
              signFinally = IcecryptConstants.ENIG_FINAL_YES;
              signReason = IcecryptLocale.getString("reasonByEncryptionMode");
            }
            break;
          case IcecryptConstants.ENIG_FINAL_NO:
          case IcecryptConstants.ENIG_FINAL_FORCENO:
            if (this.getAccDefault("signIfNotEnc")) {
              signFinally = IcecryptConstants.ENIG_FINAL_YES;
              signReason = IcecryptLocale.getString("reasonByEncryptionMode");
            }
            break;
          case IcecryptConstants.ENIG_FINAL_CONFLICT:
            if (this.getAccDefault("signIfEnc") && this.getAccDefault("signIfNotEnc")) {
              signFinally = IcecryptConstants.ENIG_FINAL_YES;
              signReason = IcecryptLocale.getString("reasonByEncryptionMode");
            }
            else {
              signFinally = IcecryptConstants.ENIG_FINAL_CONFLICT;
            }
            break;
        }
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js:   derived signFinally=" + signFinally + "\n");
        IcecryptLog.DEBUG("                                signReason=" + signReason + "\n");
      }
    }

    if (gSMFields) {
      if (gSMFields.signMessage || gSMFields.requireEncryptMessage) {
        if (IcecryptPrefs.getPref("mimePreferPgp") == 2) {
          encFinally = IcecryptConstants.ENIG_FINAL_SMIME_DISABLED;
          encReason = IcecryptLocale.getString("reasonSmimeConflict");
          signFinally = IcecryptConstants.ENIG_FINAL_SMIME_DISABLED;
          signReason = IcecryptLocale.getString("reasonSmimeConflict");

        }
      }
    }


    // process resulting PGP/MIME mode
    if (this.pgpmimeForced == IcecryptConstants.ENIG_NEVER) { // force not to PGP/Mime?
      pgpmimeFinally = IcecryptConstants.ENIG_FINAL_FORCENO;
    }
    else if (this.pgpmimeForced == IcecryptConstants.ENIG_ALWAYS) { // force to PGP/Mime?
      pgpmimeFinally = IcecryptConstants.ENIG_FINAL_FORCEYES;
    }
    else switch (this.pgpmimeByRules) {
      case IcecryptConstants.ENIG_NEVER:
        pgpmimeFinally = IcecryptConstants.ENIG_FINAL_NO;
        break;
      case IcecryptConstants.ENIG_UNDEF:
        pgpmimeFinally = ((this.sendPgpMime || (this.sendMode & nsIIcecrypt.SEND_PGP_MIME)) ? IcecryptConstants.ENIG_FINAL_YES : IcecryptConstants.ENIG_FINAL_NO);
        break;
      case IcecryptConstants.ENIG_ALWAYS:
        pgpmimeFinally = IcecryptConstants.ENIG_FINAL_YES;
        break;
      case IcecryptConstants.ENIG_CONFLICT:
        pgpmimeFinally = IcecryptConstants.ENIG_FINAL_CONFLICT;
        break;
    }
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js:   pgpmimeByRules=" + this.pgpmimeByRules + " pgpmimeFinally=" + pgpmimeFinally + "\n");

    this.statusEncrypted = encFinally;
    this.statusSigned = signFinally;
    this.statusPGPMime = pgpmimeFinally;
    this.reasonEncrypted = encReason;
    this.reasonSigned = signReason;
  },


  // process icon/strings of status bar buttons and menu entries according to final encrypt/sign/pgpmime status
  // - uses as INPUT:
  //   - this.statusEncrypt, this.statusSign, this.statusPGPMime
  // - uses as OUTPUT:
  //   - resulting icon symbols
  //   - this.statusEncryptStr, this.statusSignStr, this.statusPGPMimeStr, this.statusInlinePGPStr, this.statusAttachOwnKey
  updateStatusBar: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.updateStatusBar()\n");
    this.statusEncryptedInStatusBar = this.statusEncrypted; // to double check broken promise for encryption

    if (!this.identity) {
      this.identity = getCurrentIdentity();
    }

    var toolbarTxt = document.getElementById("icecrypt-toolbar-text");
    var encBroadcaster = document.getElementById("icecrypt-bc-encrypt");
    var signBroadcaster = document.getElementById("icecrypt-bc-sign");
    var attachBroadcaster = document.getElementById("icecrypt-bc-attach");

    // icecrypt disabled for this identity?:
    if (!this.getAccDefault("enabled")) {
      // hide icons if icecrypt not enabled
      encBroadcaster.removeAttribute("encrypted");
      encBroadcaster.setAttribute("disabled", "true");
      signBroadcaster.removeAttribute("signed");
      signBroadcaster.setAttribute("disabled", "true");
      attachBroadcaster.setAttribute("disabled", "true");
      if (toolbarTxt) {
        toolbarTxt.value = IcecryptLocale.getString("msgCompose.toolbarTxt.disabled");
        toolbarTxt.removeAttribute("class");
      }
      return;
    }
    encBroadcaster.removeAttribute("disabled");
    signBroadcaster.removeAttribute("disabled");
    attachBroadcaster.removeAttribute("disabled");

    // process resulting icon symbol and status strings for encrypt mode
    var encSymbol = null;
    var doEncrypt = false;
    var encStr = null;
    switch (this.statusEncrypted) {
      case IcecryptConstants.ENIG_FINAL_FORCENO:
        encSymbol = "forceNo";
        encStr = IcecryptLocale.getString("encryptMessageNorm");
        break;
      case IcecryptConstants.ENIG_FINAL_FORCEYES:
        doEncrypt = true;
        encSymbol = "forceYes";
        encStr = IcecryptLocale.getString("encryptMessageNorm");
        break;
      case IcecryptConstants.ENIG_FINAL_NO:
        encSymbol = "inactiveNone";
        encStr = IcecryptLocale.getString("encryptMessageAuto");
        break;
      case IcecryptConstants.ENIG_FINAL_YES:
        doEncrypt = true;
        encSymbol = "activeNone";
        encStr = IcecryptLocale.getString("encryptMessageAuto");
        break;
      case IcecryptConstants.ENIG_FINAL_CONFLICT:
        encSymbol = "inactiveConflict";
        encStr = IcecryptLocale.getString("encryptMessageAuto");
        break;
    }
    var encReasonStr = null;
    if (doEncrypt) {
      if (this.reasonEncrypted && this.reasonEncrypted !== "") {
        encReasonStr = IcecryptLocale.getString("encryptOnWithReason", [this.reasonEncrypted]);
      }
      else {
        encReasonStr = IcecryptLocale.getString("encryptOn");
      }
    }
    else {
      if (this.reasonEncrypted && this.reasonEncrypted !== "") {
        encReasonStr = IcecryptLocale.getString("encryptOffWithReason", [this.reasonEncrypted]);
      }
      else {
        encReasonStr = IcecryptLocale.getString("encryptOff");
      }
    }
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js:   encSymbol=" + encSymbol + "  encReasonStr=" + encReasonStr + "\n");

    // update encrypt icon and tooltip/menu-text
    encBroadcaster.setAttribute("encrypted", encSymbol);
    var encIcon = document.getElementById("button-icecrypt-encrypt");
    if (encIcon) {
      encIcon.setAttribute("tooltiptext", encReasonStr);
    }
    this.statusEncryptedStr = encStr;
    this.setChecked("icecrypt-bc-encrypt", doEncrypt);

    // process resulting icon symbol for sign mode
    var signSymbol = null;
    var doSign = false;
    var signStr = "";
    switch (this.statusSigned) {
      case IcecryptConstants.ENIG_FINAL_FORCENO:
        signSymbol = "forceNo";
        signStr = IcecryptLocale.getString("signMessageNorm");
        signReasonStr = IcecryptLocale.getString("signOffWithReason", [this.reasonSigned]);
        break;
      case IcecryptConstants.ENIG_FINAL_FORCEYES:
        doSign = true;
        signSymbol = "forceYes";
        signStr = IcecryptLocale.getString("signMessageNorm");
        signReasonStr = IcecryptLocale.getString("signOnWithReason", [this.reasonSigned]);
        break;
      case IcecryptConstants.ENIG_FINAL_NO:
        signSymbol = "inactiveNone";
        signStr = IcecryptLocale.getString("signMessageAuto");
        signReasonStr = IcecryptLocale.getString("signOffWithReason", [this.reasonSigned]);
        break;
      case IcecryptConstants.ENIG_FINAL_YES:
        doSign = true;
        signSymbol = "activeNone";
        signStr = IcecryptLocale.getString("signMessageAuto");
        signReasonStr = IcecryptLocale.getString("signOnWithReason", [this.reasonSigned]);
        break;
      case IcecryptConstants.ENIG_FINAL_CONFLICT:
        signSymbol = "inactiveConflict";
        signStr = IcecryptLocale.getString("signMessageAuto");
        signReasonStr = IcecryptLocale.getString("signOffWithReason", [this.reasonSigned]);
        break;
    }
    var signReasonStr = null;
    if (doSign) {
      if (this.reasonSigned && this.reasonSigned !== "") {
        signReasonStr = IcecryptLocale.getString("signOnWithReason", [this.reasonSigned]);
      }
      else {
        signReasonStr = signStr;
      }
    }
    else {
      if (this.reasonSigned && this.reasonSigned !== "") {
        signReasonStr = IcecryptLocale.getString("signOffWithReason", [this.reasonSigned]);
      }
      else {
        signReasonStr = signStr;
      }
    }
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js:   signSymbol=" + signSymbol + "  signReasonStr=" + signReasonStr + "\n");

    // update sign icon and tooltip/menu-text
    signBroadcaster.setAttribute("signed", signSymbol);
    var signIcon = document.getElementById("button-icecrypt-sign");
    if (signIcon) {
      signIcon.setAttribute("tooltiptext", signReasonStr);
    }
    this.statusSignedStr = signStr;
    this.setChecked("icecrypt-bc-sign", doSign);

    // process resulting toolbar message
    var toolbarMsg = "";
    if (doSign && doEncrypt) {
      toolbarMsg = IcecryptLocale.getString("msgCompose.toolbarTxt.signAndEncrypt");
    }
    else if (doSign) {
      toolbarMsg = IcecryptLocale.getString("msgCompose.toolbarTxt.signOnly");
    }
    else if (doEncrypt) {
      toolbarMsg = IcecryptLocale.getString("msgCompose.toolbarTxt.encryptOnly");
    }
    else {
      toolbarMsg = IcecryptLocale.getString("msgCompose.toolbarTxt.noEncryption");
    }

    if (gSMFields) {
      if (gSMFields.signMessage || gSMFields.requireEncryptMessage) {

        // Determine if user wants to encrypt drafts
        let doEncryptDrafts = this.identity.getBoolAttribute("autoEncryptDrafts");

        switch (IcecryptPrefs.getPref("mimePreferPgp")) {
          case 0:
            // prefer OpenPGP over S/MIME
            if (doSign || doEncrypt) {
              toolbarMsg += " " + IcecryptLocale.getString("msgCompose.toolbarTxt.smimeOff");
            }
            break;
          case 1:
            // ask user
            if (doSign || doEncrypt) {
              toolbarMsg += " " + IcecryptLocale.getString("msgCompose.toolbarTxt.smime");
            }
            if (doEncryptDrafts) {
              toolbarMsg += " " + IcecryptLocale.getString("msgCompose.toolbarTxt.smimeNoDraftEncryption");
            }
            break;
          case 2:
            // prefer S/MIME over OpenPGP
            encBroadcaster.setAttribute("disabled", "true");
            signBroadcaster.setAttribute("disabled", "true");
            toolbarMsg = IcecryptLocale.getString("msgCompose.toolbarTxt.smimeSignOrEncrypt");
            if (doEncryptDrafts) {
              toolbarMsg += " " + IcecryptLocale.getString("msgCompose.toolbarTxt.smimeNoDraftEncryption");
            }
            break;

        }
      }
    }

    if (toolbarTxt) {
      toolbarTxt.value = toolbarMsg;

      if (gMsgCompose.compFields.securityInfo) {
        let si = gMsgCompose.compFields.securityInfo.QueryInterface(Components.interfaces.nsIMsgSMIMECompFields);

        if (!doSign && !doEncrypt &&
          !(gMsgCompose.compFields.securityInfo instanceof Components.interfaces.nsIMsgSMIMECompFields &&
            (si.signMessage || si.requireEncryptMessage))) {
          toolbarTxt.setAttribute("class", "icecryptStrong");
        }
        else {
          toolbarTxt.removeAttribute("class");
        }
      }
      else {
        toolbarTxt.removeAttribute("class");
      }
    }

    // update pgp mime/inline PGP menu-text
    if (this.statusPGPMime == IcecryptConstants.ENIG_FINAL_YES) {
      this.statusPGPMimeStr = IcecryptLocale.getString("pgpmimeAuto");
    }
    else {
      this.statusPGPMimeStr = IcecryptLocale.getString("pgpmimeNormal");
    }

    if (this.statusPGPMime == IcecryptConstants.ENIG_FINAL_NO) {
      this.inlinePGPStr = IcecryptLocale.getString("inlinePGPAuto");
    }
    else {
      this.inlinePGPStr = IcecryptLocale.getString("inlinePGPNormal");
    }

    if (this.allowAttachOwnKey() === 1) {
      attachBroadcaster.removeAttribute("disabled");
    }
    else {
      attachBroadcaster.setAttribute("disabled", "true");
    }

  },

  /**
   * determine if own key may be attached.
   * @result: Number:
   *          -1: account not enabled for Icecrypt
   *           0: account enabled but key mode set to "by Email address"
   *           1: account enabled; key specified
   */
  allowAttachOwnKey: function() {

    let allow = -1;

    if (this.getAccDefault("enabled")) {
      allow = 0;
      if (this.identity.getIntAttribute("pgpKeyMode") > 0) {
        let keyIdValue = this.identity.getCharAttribute("pgpkeyId");
        if (keyIdValue.search(/^ *(0x)?[0-9a-fA-F]* *$/) === 0) {
          allow = 1;
        }
      }
    }

    return allow;
  },

  /* compute whether to sign/encrypt according to current rules and sendMode
   * - without any interaction, just to process resulting status bar icons
   */
  determineSendFlags: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.focusChange: Icecrypt.msg.determineSendFlags\n");
    this.statusEncryptedInStatusBar = null; // to double check broken promise for encryption

    if (!this.identity) {
      this.identity = getCurrentIdentity();
    }

    if (this.getAccDefault("enabled")) {
      var compFields = gMsgCompose.compFields;

      if (!Icecrypt.msg.composeBodyReady) {
        compFields = Components.classes["@mozilla.org/messengercompose/composefields;1"].createInstance(Components.interfaces.nsIMsgCompFields);
      }
      Recipients2CompFields(compFields);
      gMsgCompose.expandMailingLists();

      // process list of to/cc email addresses
      // - bcc email addresses are ignored, when processing whether to sign/encrypt
      var toAddrList = [];
      var arrLen = {};
      var recList;
      if (compFields.to.length > 0) {
        recList = compFields.splitRecipients(compFields.to, true, arrLen);
        this.addRecipients(toAddrList, recList);
      }
      if (compFields.cc.length > 0) {
        recList = compFields.splitRecipients(compFields.cc, true, arrLen);
        this.addRecipients(toAddrList, recList);
      }

      this.encryptByRules = IcecryptConstants.ENIG_UNDEF;
      this.signByRules = IcecryptConstants.ENIG_UNDEF;
      this.pgpmimeByRules = IcecryptConstants.ENIG_UNDEF;

      // process rules
      if (toAddrList.length > 0 && IcecryptPrefs.getPref("assignKeysByRules")) {
        var matchedKeysObj = {};
        var flagsObj = {};
        if (IcecryptRules.mapAddrsToKeys(toAddrList.join(", "),
            false, // no interaction if not all addrs have a key
            window,
            matchedKeysObj, // resulting matching keys
            flagsObj)) { // resulting flags (0/1/2/3 for each type)
          this.encryptByRules = flagsObj.encrypt;
          this.signByRules = flagsObj.sign;
          this.pgpmimeByRules = flagsObj.pgpMime;

          if (matchedKeysObj.value && matchedKeysObj.value.length > 0) {
            // replace addresses with results from rules
            toAddrList = matchedKeysObj.value.split(", ");
          }
        }
      }

      // if not clear whether to encrypt yet, check whether automatically-send-encrypted applies
      if (toAddrList.length > 0 && this.encryptByRules == IcecryptConstants.ENIG_UNDEF && IcecryptPrefs.getPref("autoSendEncrypted") == 1) {
        var validKeyList = Icecrypt.hlp.validKeysForAllRecipients(toAddrList.join(", "));
        if (validKeyList) {
          this.encryptByRules = IcecryptConstants.ENIG_AUTO_ALWAYS;
        }
      }
    }

    // process and signal new resulting state
    this.processFinalState();
    this.updateStatusBar();
    this.determineSendFlagId = null;

  },

  setChecked: function(elementId, checked) {
    let elem = document.getElementById(elementId);
    if (elem) {
      if (checked) {
        elem.setAttribute("checked", "true");
      }
      else
        elem.removeAttribute("checked");
    }
  },

  setMenuSettings: function(postfix) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.setMenuSettings: postfix=" + postfix + "\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;

    var elem = document.getElementById("icecrypt_compose_sign_item" + postfix);
    if (elem) {
      elem.setAttribute("label", this.statusSignedStr);
      switch (this.statusSigned) {
        case IcecryptConstants.ENIG_FINAL_YES:
        case IcecryptConstants.ENIG_FINAL_FORCEYES:
          elem.setAttribute("checked", "true");
          break;
        default:
          elem.setAttribute("checked", "false");
      }
    }

    elem = document.getElementById("icecrypt_compose_encrypt_item" + postfix);
    if (elem) {
      elem.setAttribute("label", this.statusEncryptedStr);
      switch (this.statusEncrypted) {
        case IcecryptConstants.ENIG_FINAL_YES:
        case IcecryptConstants.ENIG_FINAL_FORCEYES:
          elem.setAttribute("checked", "true");
          break;
        default:
          elem.setAttribute("checked", "false");
      }
    }

    elem = document.getElementById("icecrypt_compose_pgpmime_item" + postfix);
    if (elem) {
      elem.setAttribute("label", this.statusPGPMimeStr);

      switch (this.statusPGPMime) {
        case IcecryptConstants.ENIG_FINAL_YES:
        case IcecryptConstants.ENIG_FINAL_FORCEYES:
          elem.setAttribute("checked", "true");
          break;
        default:
          elem.setAttribute("checked", "false");
      }
    }

    elem = document.getElementById("icecrypt_compose_inline_item" + postfix);
    if (elem) {
      elem.setAttribute("label", this.inlinePGPStr);

      switch (this.statusPGPMime) {
        case IcecryptConstants.ENIG_FINAL_NO:
        case IcecryptConstants.ENIG_FINAL_FORCENO:
        case IcecryptConstants.ENIG_FINAL_CONFLICT:
        case IcecryptConstants.ENIG_FINAL_UNDEF:
          elem.setAttribute("checked", "true");
          break;
        default:
          elem.setAttribute("checked", "false");
      }

    }


    /*
        this.setChecked("icecrypt_final_encryptDefault"+postfix, this.encryptForced == IcecryptConstants.ENIG_UNDEF);
        this.setChecked("icecrypt_final_encryptYes"+postfix, this.encryptForced == IcecryptConstants.ENIG_ALWAYS);
        this.setChecked("icecrypt_final_encryptNo"+postfix, this.encryptForced == IcecryptConstants.ENIG_NEVER);
        this.setChecked("icecrypt_final_signDefault"+postfix, this.signForced == IcecryptConstants.ENIG_UNDEF);
        this.setChecked("icecrypt_final_signYes"+postfix, this.signForced == IcecryptConstants.ENIG_ALWAYS);
        this.setChecked("icecrypt_final_signNo"+postfix, this.signForced == IcecryptConstants.ENIG_NEVER);
        this.setChecked("icecrypt_final_pgpmimeDefault"+postfix, this.pgpmimeForced == IcecryptConstants.ENIG_UNDEF);
        this.setChecked("icecrypt_final_pgpmimeYes"+postfix, this.pgpmimeForced == IcecryptConstants.ENIG_ALWAYS);
        this.setChecked("icecrypt_final_pgpmimeNo"+postfix, this.pgpmimeForced == IcecryptConstants.ENIG_NEVER);
    */

    let menuElement = document.getElementById("icecrypt_insert_own_key");
    if (menuElement) {
      if (this.identity.getIntAttribute("pgpKeyMode") > 0) {
        menuElement.setAttribute("checked", this.attachOwnKeyObj.appendAttachment.toString());
        menuElement.removeAttribute("disabled");
      }
      else {
        menuElement.setAttribute("disabled", "true");
      }
    }
  },

  displaySecuritySettings: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.displaySecuritySettings\n");

    if (this.statusEncrypted == IcecryptConstants.ENIG_FINAL_SMIME_DISABLED) {
      IcecryptDialog.alert(window, IcecryptLocale.getString("msgCompose.toolbarTxt.smimeConflict"));
      return;
    }

    var inputObj = {
      statusEncrypted: this.statusEncrypted,
      statusSigned: this.statusSigned,
      statusPGPMime: this.statusPGPMime,
      success: false,
      resetDefaults: false
    };
    window.openDialog("chrome://icecrypt/content/icecryptEncryptionDlg.xul", "", "dialog,modal,centerscreen", inputObj);

    if (!inputObj.success) return; // Cancel pressed

    if (inputObj.resetDefaults) {
      // reset everything to defaults
      this.encryptForced = IcecryptConstants.ENIG_UNDEF;
      this.signForced = IcecryptConstants.ENIG_UNDEF;
      this.pgpmimeForced = IcecryptConstants.ENIG_UNDEF;
      this.finalSignDependsOnEncrypt = true;
    }
    else {
      if (this.signForced != inputObj.sign) {
        this.dirty = 2;
        this.signForced = inputObj.sign;
        this.finalSignDependsOnEncrypt = false;
      }

      if (this.encryptForced != inputObj.encrypt || this.pgpmimeForced != inputObj.pgpmime) {
        this.dirty = 2;
      }

      this.encryptForced = inputObj.encrypt;
      this.pgpmimeForced = inputObj.pgpmime;
    }

    this.processFinalState();
    this.updateStatusBar();
  },


  signingNoLongerDependsOnEnc: function() {
    if (this.finalSignDependsOnEncrypt) {
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.signingNoLongerDependsOnEnc(): unbundle final signing\n");
      this.finalSignDependsOnEncrypt = false;

      IcecryptDialog.alertPref(window, IcecryptLocale.getString("signIconClicked"), "displaySignWarn");
    }
  },


  confirmBeforeSend: function(toAddrStr, gpgKeys, sendFlags, isOffline) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.confirmBeforeSend: sendFlags=" + sendFlags + "\n");
    // get confirmation before sending message

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;

    // get wording for message status (e.g. " SIGNED ENCRYPTED")
    var msgStatus = "";
    if (sendFlags & (ENCRYPT | SIGN)) {
      if (sendFlags & nsIIcecrypt.SEND_PGP_MIME) {
        msgStatus += " " + IcecryptLocale.getString("statPGPMIME");
      }
      if (sendFlags & SIGN) {
        msgStatus += " " + IcecryptLocale.getString("statSigned");
      }
      if (sendFlags & ENCRYPT) {
        msgStatus += " " + IcecryptLocale.getString("statEncrypted");
      }
    }
    else {
      msgStatus += " " + IcecryptLocale.getString("statPlain");
    }

    // create message
    var msgConfirm = "";
    if (isOffline || sendFlags & nsIIcecrypt.SEND_LATER) {
      msgConfirm = IcecryptLocale.getString("offlineSave", [msgStatus, IcecryptFuncs.stripEmail(toAddrStr).replace(/,/g, ", ")]);
    }
    else {
      msgConfirm = IcecryptLocale.getString("onlineSend", [msgStatus, IcecryptFuncs.stripEmail(toAddrStr).replace(/,/g, ", ")]);
    }

    // add list of keys
    if (sendFlags & ENCRYPT) {
      gpgKeys = gpgKeys.replace(/^, /, "").replace(/, $/, "");

      // make gpg keys unique
      let keyList = gpgKeys.split(/[, ]+/).reduce(function _f(p, key) {
        if (p.indexOf(key) < 0) p.push(key);
        return p;
      }, []);

      msgConfirm += "\n\n" + IcecryptLocale.getString("encryptKeysNote", [keyList.join(", ")]);
    }

    return IcecryptDialog.confirmDlg(window, msgConfirm,
      IcecryptLocale.getString((isOffline || sendFlags & nsIIcecrypt.SEND_LATER) ?
        "msgCompose.button.save" : "msgCompose.button.send"));
  },


  addRecipients: function(toAddrList, recList) {
    for (var i = 0; i < recList.length; i++) {
      toAddrList.push(IcecryptFuncs.stripEmail(recList[i].replace(/[",]/g, "")));
    }
  },

  setDraftStatus: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.setDraftStatus - enabling draft mode\n");

    // Draft Status:
    // N (for new style) plus String of 4 numbers:
    // 1: encryption
    // 2: signing
    // 3: PGP/MIME
    // 4: attach own key

    var draftStatus = "N" + this.encryptForced + this.signForced + this.pgpmimeForced +
      (this.attachOwnKeyObj.appendAttachment ? "1" : "0");

    this.setAdditionalHeader("X-Icecrypt-Draft-Status", draftStatus);
  },


  getSenderUserId: function() {
    var userIdValue = null;

    if (this.identity.getIntAttribute("pgpKeyMode") > 0) {
      userIdValue = this.identity.getCharAttribute("pgpkeyId");

      if (!userIdValue) {

        var mesg = IcecryptLocale.getString("composeSpecifyEmail");

        var valueObj = {
          value: userIdValue
        };

        if (IcecryptDialog.promptValue(window, mesg, valueObj)) {
          userIdValue = valueObj.value;
        }
      }

      if (userIdValue) {
        this.identity.setCharAttribute("pgpkeyId", userIdValue);

      }
      else {
        this.identity.setIntAttribute("pgpKeyMode", 0);
      }
    }

    if (typeof(userIdValue) != "string") {
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.getSenderUserId: type of userIdValue=" + typeof(userIdValue) + "\n");
      userIdValue = this.identity.email;
    }
    return userIdValue;
  },


  /* process rules and find keys for passed email addresses
   * This is THE core method to prepare sending encryptes emails.
   * - it processes the recipient rules (if not disabled)
   * - it
   *
   * @sendFlags:    Longint - all current combined/processed send flags (incl. optSendFlags)
   * @optSendFlags: Longint - may only be SEND_ALWAYS_TRUST or SEND_ENCRYPT_TO_SELF
   * @gotSendFlags: Longint - initial sendMode of encryptMsg() (0 or SIGN or ENCRYPT or SIGN|ENCRYPT)
   * @fromAddr:     String - from email
   * @toAddrList:   Array  - both to and cc receivers
   * @bccAddrList:  Array  - bcc receivers
   * @return:       Object:
   *                - sendFlags (Longint)
   *                - toAddrStr  comma separated string of unprocessed to/cc emails
   *                - bccAddrStr comma separated string of unprocessed to/cc emails
   *                or null (cancel sending the email)
   */
  keySelection: function(icecryptSvc, sendFlags, optSendFlags, gotSendFlags, fromAddr, toAddrList, bccAddrList) {
    IcecryptLog.DEBUG("=====> keySelection()\n");
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.keySelection()\n");
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;

    var toAddrStr = toAddrList.join(", ");
    var bccAddrStr = bccAddrList.join(", ");

    // NOTE: If we only have bcc addresses, we currently do NOT process rules and select keys at all
    //       This is GOOD because sending keys for bcc addresses makes bcc addresses visible
    //       (thus compromising the concept of bcc)
    //       THUS, we disable encryption even though all bcc receivers might want to have it encrypted.
    if (toAddrStr.length === 0) {
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.keySelection(): skip key selection because we neither have \"to\" nor \"cc\" addresses\n");

      if (this.statusPGPMime == IcecryptConstants.ENIG_FINAL_YES ||
        this.statusPGPMime == IcecryptConstants.ENIG_FINAL_FORCEYES) {
        sendFlags |= nsIIcecrypt.SEND_PGP_MIME;
      }
      else if (this.statusPGPMime == IcecryptConstants.ENIG_FINAL_NO ||
        this.statusPGPMime == IcecryptConstants.ENIG_FINAL_FORCENO ||
        this.statusPGPMime == IcecryptConstants.ENIG_FINAL_CONFLICT) {
        sendFlags &= ~nsIIcecrypt.SEND_PGP_MIME;
      }

      return {
        sendFlags: sendFlags,
        toAddrStr: toAddrStr,
        bccAddrStr: bccAddrStr,
      };
    }

    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.keySelection(): toAddrStr=\"" + toAddrStr + "\" bccAddrStr=\"" + bccAddrStr + "\"\n");

    // force add-rule dialog for each missing key?:
    var forceRecipientSettings = false;
    // if keys are ONLY assigned by rules, force add-rule dialog for each missing key
    if (IcecryptPrefs.getPref("assignKeysByRules") &&
      !IcecryptPrefs.getPref("assignKeysByEmailAddr") &&
      !IcecryptPrefs.getPref("assignKeysManuallyIfMissing") &&
      !IcecryptPrefs.getPref("assignKeysManuallyAlways")) {
      forceRecipientSettings = true;
    }

    // REPEAT 1 or 2 times:
    // NOTE: The only way to call this loop twice is to come to the "continue;" statement below,
    //       which forces a second iteration (with forceRecipientSettings==true)
    var doRulesProcessingAgain;
    do {
      doRulesProcessingAgain = false;

      // process rules if not disabled
      // - enableRules: rules not temporarily disabled
      // REPLACES email addresses by keys in its result !!!
      var refreshKeyList = true;
      if (IcecryptPrefs.getPref("assignKeysByRules") && this.enableRules) {
        let result = this.processRules(forceRecipientSettings, sendFlags, optSendFlags, toAddrStr, bccAddrStr);
        if (!result) {
          return null;
        }
        sendFlags = result.sendFlags;
        optSendFlags = result.optSendFlags;
        toAddrStr = result.toAddr; // replace email addresses with rules by the corresponding keys
        bccAddrStr = result.bccAddr; // replace email addresses with rules by the corresponding keys
        refreshKeyList = !result.didRefreshKeyList; // if key list refreshed we don't have to do it again
      }

      // if encryption is requested for the email:
      // - encrypt test message for default encryption
      // - might trigger a second iteration through this loop
      //   - if during its dialog for manual key selection "create per-recipient rules" is pressed
      //   to force manual settings for missing keys
      // LEAVES remaining email addresses not covered by rules as they are
      if (sendFlags & ENCRYPT) {
        let result = this.encryptTestMessage(icecryptSvc, sendFlags, optSendFlags,
          fromAddr, toAddrStr, bccAddrStr, bccAddrList, refreshKeyList);
        if (!result) {
          return null;
        }
        sendFlags = result.sendFlags;
        toAddrStr = result.toAddrStr;
        bccAddrStr = result.bccAddrStr;
        if (result.doRulesProcessingAgain) { // start rule processing again ?
          doRulesProcessingAgain = true;
          if (result.createNewRule) {
            forceRecipientSettings = true;
          }
        }
      }
    } while (doRulesProcessingAgain);

    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.keySelection(): return toAddrStr=\"" + toAddrStr + "\" bccAddrStr=\"" + bccAddrStr + "\"\n");
    IcecryptLog.DEBUG("  <=== keySelection()");
    return {
      sendFlags: sendFlags,
      toAddrStr: toAddrStr,
      bccAddrStr: bccAddrStr,
    };
  },


  /* Determine if S/MIME or OpenPGP should be used
   *
   * return: Boolean:
   *   - true:  use OpenPGP
   *   - false: use S/MIME
   *   - null:  dialog aborted - cancel sending
   */
  preferPgpOverSmime: function(sendFlags) {

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    if (gMsgCompose.compFields.securityInfo instanceof Components.interfaces.nsIMsgSMIMECompFields &&
      (sendFlags & (nsIIcecrypt.SEND_SIGNED | nsIIcecrypt.SEND_ENCRYPTED))) {

      let si = gMsgCompose.compFields.securityInfo.QueryInterface(Components.interfaces.nsIMsgSMIMECompFields);
      if (si.requireEncryptMessage || si.signMessage) {

        if (sendFlags & nsIIcecrypt.SAVE_MESSAGE) {
          // use S/MIME if it's enabled for saving drafts
          return false;
        }
        else {
          var promptSvc = IcecryptDialog.getPromptSvc();
          var prefAlgo = IcecryptPrefs.getPref("mimePreferPgp");
          if (prefAlgo == 1) {
            var checkedObj = {
              value: null
            };
            prefAlgo = promptSvc.confirmEx(window,
              IcecryptLocale.getString("enigConfirm"),
              IcecryptLocale.getString("pgpMime_sMime.dlg.text"), (promptSvc.BUTTON_TITLE_IS_STRING * promptSvc.BUTTON_POS_0) +
              (promptSvc.BUTTON_TITLE_CANCEL * promptSvc.BUTTON_POS_1) +
              (promptSvc.BUTTON_TITLE_IS_STRING * promptSvc.BUTTON_POS_2),
              IcecryptLocale.getString("pgpMime_sMime.dlg.pgpMime.button"),
              null,
              IcecryptLocale.getString("pgpMime_sMime.dlg.sMime.button"),
              IcecryptLocale.getString("dlgKeepSetting"),
              checkedObj);
            if (checkedObj.value && (prefAlgo === 0 || prefAlgo == 2)) IcecryptPrefs.setPref("mimePreferPgp", prefAlgo);
          }
          switch (prefAlgo) {
            case 0:
              // use OpenPGP and not S/MIME
              si.requireEncryptMessage = false;
              si.signMessage = false;
              return true;
            case 2:
              // use S/MIME and not OpenPGP
              return false;
            case 1:
              return null;
            default:
              // cancel or ESC pressed
              return null;
          }
        }
      }
    }

    return true;
  },


  /* process rules
   *
   * @forceRecipientSetting: force manual selection for each missing key?
   * @sendFlags:    INPUT/OUTPUT all current combined/processed send flags (incl. optSendFlags)
   * @optSendFlags: INPUT/OUTPUT may only be SEND_ALWAYS_TRUST or SEND_ENCRYPT_TO_SELF
   * @toAddrStr:    INPUT/OUTPUT comma separated string of keys and unprocessed to/cc emails
   * @bccAddrStr:   INPUT/OUTPUT comma separated string of keys and unprocessed bcc emails
   * @return:       { sendFlags, toAddr, bccAddr }
   *                or null (cancel sending the email)
   */
  processRules: function(forceRecipientSettings, sendFlags, optSendFlags, toAddrStr, bccAddrStr) {
    IcecryptLog.DEBUG("=====> processRules()\n");
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.processRules(): toAddrStr=\"" + toAddrStr + "\" bccAddrStr=\"" + bccAddrStr + "\" forceRecipientSettings=" +
      forceRecipientSettings + "\n");

    // process defaults
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;
    var didRefreshKeyList = false; // return value to signal whether the key list was refreshed

    // get keys for to and cc addresses:
    // - matchedKeysObj will contain the keys and the remaining toAddrStr elements
    var matchedKeysObj = {}; // returned value for matched keys
    var flagsObj = {}; // returned value for flags
    if (!IcecryptRules.mapAddrsToKeys(toAddrStr,
        forceRecipientSettings, // true => start dialog for addrs without any key
        window,
        matchedKeysObj,
        flagsObj)) {
      return null;
    }
    if (matchedKeysObj.value) {
      toAddrStr = matchedKeysObj.value;
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.processRules(): after mapAddrsToKeys() toAddrStr=\"" + toAddrStr + "\"\n");
    }
    this.encryptByRules = flagsObj.encrypt;
    this.signByRules = flagsObj.sign;
    this.pgpmimeByRules = flagsObj.pgpMime;

    // if not clear whether to encrypt yet, check whether automatically-send-encrypted applies
    // - check whether bcc is empty here? if (bccAddrStr.length === 0)
    if (toAddrStr.length > 0 && this.encryptByRules == IcecryptConstants.ENIG_UNDEF && IcecryptPrefs.getPref("autoSendEncrypted") == 1) {
      var validKeyList = Icecrypt.hlp.validKeysForAllRecipients(toAddrStr);
      if (validKeyList) {
        this.encryptByRules = IcecryptConstants.ENIG_AUTO_ALWAYS;
        toAddrStr = validKeyList.join(", ");
      }
    }

    // process final state
    this.processFinalState(sendFlags);

    // final handling of conflicts:
    // - pgpMime conflicts always result into pgpMime = 0/'never'
    if (this.statusPGPMime == IcecryptConstants.ENIG_FINAL_CONFLICT) {
      this.statusPGPMime = IcecryptConstants.ENIG_FINAL_NO;
    }
    // - encrypt/sign conflicts result into result 0/'never'
    //   with possible dialog to give a corresponding feedback
    var conflictFound = false;
    if (this.statusEncrypted == IcecryptConstants.ENIG_FINAL_CONFLICT) {
      this.statusEncrypted = IcecryptConstants.ENIG_FINAL_NO;
      conflictFound = true;
    }
    if (this.statusSigned == IcecryptConstants.ENIG_FINAL_CONFLICT) {
      this.statusSigned = IcecryptConstants.ENIG_FINAL_NO;
      conflictFound = true;
    }
    if (conflictFound) {
      if (!Icecrypt.hlp.processConflicts(this.statusEncrypted == IcecryptConstants.ENIG_FINAL_YES || this.statusEncrypted == IcecryptConstants.ENIG_FINAL_FORCEYES,
          this.statusSigned == IcecryptConstants.ENIG_FINAL_YES || this.statusSigned == IcecryptConstants.ENIG_FINAL_FORCEYES)) {
        return null;
      }
    }

    // process final sendMode
    //  ENIG_FINAL_CONFLICT no longer possible
    switch (this.statusEncrypted) {
      case IcecryptConstants.ENIG_FINAL_NO:
      case IcecryptConstants.ENIG_FINAL_FORCENO:
        sendFlags &= ~ENCRYPT;
        break;
      case IcecryptConstants.ENIG_FINAL_YES:
      case IcecryptConstants.ENIG_FINAL_FORCEYES:
        sendFlags |= ENCRYPT;
        break;
    }
    switch (this.statusSigned) {
      case IcecryptConstants.ENIG_FINAL_NO:
      case IcecryptConstants.ENIG_FINAL_FORCENO:
        sendFlags &= ~SIGN;
        break;
      case IcecryptConstants.ENIG_FINAL_YES:
      case IcecryptConstants.ENIG_FINAL_FORCEYES:
        sendFlags |= SIGN;
        break;
    }
    switch (this.statusPGPMime) {
      case IcecryptConstants.ENIG_FINAL_NO:
      case IcecryptConstants.ENIG_FINAL_FORCENO:
        sendFlags &= ~nsIIcecrypt.SEND_PGP_MIME;
        break;
      case IcecryptConstants.ENIG_FINAL_YES:
      case IcecryptConstants.ENIG_FINAL_FORCEYES:
        sendFlags |= nsIIcecrypt.SEND_PGP_MIME;
        break;
    }

    // get keys according to rules for bcc addresses:
    // - matchedKeysObj will contain the keys and the remaining bccAddrStr elements
    // - NOTE: bcc recipients are ignored when in general computing whether to sign or encrypt or pgpMime
    if (!IcecryptRules.mapAddrsToKeys(bccAddrStr,
        forceRecipientSettings, // true => start dialog for addrs without any key
        window,
        matchedKeysObj,
        flagsObj)) {
      return null;
    }
    if (matchedKeysObj.value) {
      bccAddrStr = matchedKeysObj.value;
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.processRules(): after mapAddrsToKeys() bccAddrStr=\"" + bccAddrStr + "\"\n");
    }

    IcecryptLog.DEBUG("  <=== processRules()\n");
    return {
      sendFlags: sendFlags,
      optSendFlags: optSendFlags,
      toAddr: toAddrStr,
      bccAddr: bccAddrStr,
      didRefreshKeyList: didRefreshKeyList,
    };
  },


  /* encrypt a test message to see whether we have all necessary keys
   *
   * @sendFlags:    all current combined/processed send flags (incl. optSendFlags)
   * @optSendFlags: may only be SEND_ALWAYS_TRUST or SEND_ENCRYPT_TO_SELF
   * @fromAddr:     from email
   * @toAddrStr:    comma separated string of keys and unprocessed to/cc emails
   * @bccAddrStr:   comma separated string of keys and unprocessed bcc emails
   * @bccAddrList:  bcc receivers
   * @return:       doRulesProcessingAgain: start with rule processing once more
   *                or null (cancel sending the email)
   */
  encryptTestMessage: function(icecryptSvc, sendFlags, optSendFlags, fromAddr, toAddrStr, bccAddrStr, bccAddrList, refresh) {
    IcecryptLog.DEBUG("=====> encryptTestMessage()\n");
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;

    var testCipher = null;
    var testExitCodeObj = {};
    var testStatusFlagsObj = {};
    var testErrorMsgObj = {};

    // get keys for remaining email addresses
    // - NOTE: This should not be necessary; however, in GPG there is a problem:
    //         Only the first key found for an email is used.
    //         If this is invalid, no other keys are tested.
    //         Thus, WE make it better here in icecrypt until the bug is fixed.
    var details = {}; // will contain msgList[] afterwards
    if (IcecryptPrefs.getPref("assignKeysByEmailAddr")) {
      var validKeyList = Icecrypt.hlp.validKeysForAllRecipients(toAddrStr, details);
      if (validKeyList) {
        toAddrStr = validKeyList.join(", ");
      }
    }

    // encrypt test message for test recipients
    var testPlain = "Test Message";
    var testUiFlags = nsIIcecrypt.UI_TEST;
    var testSendFlags = nsIIcecrypt.SEND_TEST | ENCRYPT | optSendFlags;
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptTestMessage(): call encryptMessage() for fromAddr=\"" + fromAddr + "\" toAddrStr=\"" + toAddrStr + "\" bccAddrStr=\"" +
      bccAddrStr + "\"\n");
    testCipher = icecryptSvc.encryptMessage(window, testUiFlags, testPlain,
      fromAddr, toAddrStr, bccAddrStr,
      testSendFlags,
      testExitCodeObj,
      testStatusFlagsObj,
      testErrorMsgObj);

    if (testStatusFlagsObj.value & (nsIIcecrypt.INVALID_RECIPIENT | nsIIcecrypt.NO_SECKEY)) {
      // check if own key is invalid
      IcecryptDialog.alert(window, testErrorMsgObj.value);
      return null;
    }

    // if
    // - "always ask/manually" (even if all keys were found) or
    // - unless "ask for missing keys":
    //   - we have an invalid recipient or
    //   - we could not resolve any/all keys
    //     (due to disabled "assignKeysByEmailAddr"" or multiple keys with same trust for a recipient)
    // start the dialog for user selected keys
    if (IcecryptPrefs.getPref("assignKeysManuallyAlways") ||
      (((testStatusFlagsObj.value & nsIIcecrypt.INVALID_RECIPIENT) ||
          toAddrStr.indexOf('@') >= 0) &&
        IcecryptPrefs.getPref("assignKeysManuallyIfMissing")) ||
      (details && details.errArray && details.errArray.length > 0)
    ) {

      // check for invalid recipient keys
      var resultObj = {};
      var inputObj = {};
      inputObj.toAddr = toAddrStr;
      inputObj.invalidAddr = Icecrypt.hlp.getInvalidAddress(testErrorMsgObj.value);
      if (details && details.errArray && details.errArray.length > 0) {
        inputObj.errArray = details.errArray;
      }

      // prepare dialog options:
      inputObj.options = "multisel";
      if (IcecryptPrefs.getPref("assignKeysByRules")) {
        inputObj.options += ",rulesOption"; // enable button to create per-recipient rule
      }
      if (IcecryptPrefs.getPref("assignKeysManuallyAlways")) {
        inputObj.options += ",noforcedisp";
      }
      if (!(sendFlags & SIGN)) {
        inputObj.options += ",unsigned";
      }
      if (this.trustAllKeys) {
        inputObj.options += ",trustallkeys";
      }
      if (sendFlags & nsIIcecrypt.SEND_LATER) {
        let sendLaterLabel = IcecryptLocale.getString("sendLaterCmd.label");
        inputObj.options += ",sendlabel=" + sendLaterLabel;
      }
      inputObj.options += ",";
      inputObj.dialogHeader = IcecryptLocale.getString("recipientsSelectionHdr");

      // perform key selection dialog:
      window.openDialog("chrome://icecrypt/content/icecryptKeySelection.xul", "", "dialog,modal,centerscreen,resizable", inputObj, resultObj);

      // process result from key selection dialog:
      try {
        // CANCEL:
        if (resultObj.cancelled) {
          return null;
        }


        // repeat checking of rules etc. (e.g. after importing new key)
        if (resultObj.repeatEvaluation) {
          // THIS is the place that triggers a second iteration
          let returnObj = {
            doRulesProcessingAgain: true,
            createNewRule: false,
            sendFlags: sendFlags,
            toAddrStr: toAddrStr,
            bccAddrStr: bccAddrStr,
          };

          // "Create per recipient rule(s)":
          if (resultObj.perRecipientRules && this.enableRules) {
            // do an extra round because the user wants to set a PGP rule
            returnObj.createNewRule = true;
          }

          return returnObj;
        }

        // process OK button:
        if (resultObj.encrypt) {
          sendFlags |= ENCRYPT; // should anyway be set
          if (bccAddrList.length > 0) {
            toAddrStr = "";
            bccAddrStr = resultObj.userList.join(", ");
          }
          else {
            toAddrStr = resultObj.userList.join(", ");
            bccAddrStr = "";
          }
        }
        else {
          // encryption explicitely turned off
          sendFlags &= ~ENCRYPT;
          // counts as forced non-encryption
          // (no internal error if different state was processed before)
          this.statusEncrypted = IcecryptConstants.ENIG_FINAL_NO;
          this.statusEncryptedInStatusBar = IcecryptConstants.ENIG_FINAL_NO;
        }
        if (resultObj.sign) {
          sendFlags |= SIGN;
        }
        else {
          sendFlags &= ~SIGN;
        }
        testCipher = "ok";
        testExitCodeObj.value = 0;
      }
      catch (ex) {
        // cancel pressed -> don't send mail
        return null;
      }
    }
    // If test encryption failed and never ask manually, turn off default encryption
    if ((!testCipher || (testExitCodeObj.value !== 0)) &&
      !IcecryptPrefs.getPref("assignKeysManuallyIfMissing") &&
      !IcecryptPrefs.getPref("assignKeysManuallyAlways")) {
      sendFlags &= ~ENCRYPT;
      this.statusEncrypted = IcecryptConstants.ENIG_FINAL_NO;
      this.statusEncryptedInStatusBar = IcecryptConstants.ENIG_FINAL_NO;
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptTestMessage: No default encryption because test failed\n");
    }
    IcecryptLog.DEBUG("  <=== encryptTestMessage()");
    return {
      doRulesProcessingAgain: false,
      createNewRule: false,
      sendFlags: sendFlags,
      toAddrStr: toAddrStr,
      bccAddrStr: bccAddrStr,
    };
  },

  /* Manage the wrapping of inline signed mails
   *
   * @wrapresultObj: Result:
   * @wrapresultObj.cancelled, true if send operation is to be cancelled, else false
   * @wrapresultObj.usePpgMime, true if message send option was changed to PGP/MIME, else false
   */

  wrapInLine: function(wrapresultObj) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: WrapInLine\n");
    wrapresultObj.cancelled = false;
    wrapresultObj.usePpgMime = false;
    try {
      const dce = Components.interfaces.nsIDocumentEncoder;
      var wrapper = gMsgCompose.editor.QueryInterface(Components.interfaces.nsIEditorMailSupport);
      var editor = gMsgCompose.editor.QueryInterface(Components.interfaces.nsIPlaintextEditor);
      var encoderFlags = dce.OutputFormatted | dce.OutputLFLineBreak;

      var wrapWidth = this.getMailPref("mailnews.wraplength");
      if (wrapWidth > 0 && wrapWidth < 68 && editor.wrapWidth > 0) {
        if (IcecryptDialog.confirmDlg(window, IcecryptLocale.getString("minimalLineWrapping", [wrapWidth]))) {
          wrapWidth = 68;
          IcecryptPrefs.getPrefRoot().setIntPref("mailnews.wraplength", wrapWidth);
        }
      }

      if (wrapWidth && editor.wrapWidth > 0) {
        // First use standard editor wrap mechanism:
        editor.wrapWidth = wrapWidth - 2;
        wrapper.rewrap(true);
        editor.wrapWidth = wrapWidth;

        // Now get plaintext from editor
        var wrapText = this.editorGetContentAs("text/plain", encoderFlags);

        // split the lines into an array
        wrapText = wrapText.split(/\r\n|\r|\n/g);

        var i = 0;
        var excess = 0;
        // inspect all lines of mail text to detect if we still have excessive lines which the "standard" editor wrapper leaves
        for (i = 0; i < wrapText.length; i++) {
          if (wrapText[i].length > wrapWidth) {
            excess = 1;
          }
        }

        if (excess) {
          IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Excess lines detected\n");
          var resultObj = {};
          window.openDialog("chrome://icecrypt/content/icecryptWrapSelection.xul", "", "dialog,modal,centerscreen", resultObj);
          try {
            if (resultObj.cancelled) {
              // cancel pressed -> do not send, return instead.
              wrapresultObj.cancelled = true;
              return;
            }
          }
          catch (ex) {
            // cancel pressed -> do not send, return instead.
            wrapresultObj.cancelled = true;
            return;
          }

          var quote = "";
          var limitedLine = "";
          var restOfLine = "";

          var WrapSelect = resultObj.Select;
          switch (WrapSelect) {
            case "0": // Selection: Force rewrap
              for (i = 0; i < wrapText.length; i++) {
                if (wrapText[i].length > wrapWidth) {

                  // If the current line is too long, limit it hard to wrapWidth and insert the rest as the next line into wrapText array
                  limitedLine = wrapText[i].slice(0, wrapWidth);
                  restOfLine = wrapText[i].slice(wrapWidth);

                  // We should add quotes at the beginning of "restOfLine", if limitedLine is a quoted line
                  // However, this would be purely academic, because limitedLine will always be "standard"-wrapped
                  // by the editor-rewrapper at the space between quote sign (>) and the quoted text.

                  wrapText.splice(i, 1, limitedLine, restOfLine);
                }
              }
              break;
            case "1": // Selection: Send as is
              break;
            case "2": // Selection: Use MIME
              wrapresultObj.usePpgMime = true;
              break;
            case "3": // Selection: Edit manually -> do not send, return instead.
              wrapresultObj.cancelled = true;
              return;
          } //switch
        }
        // Now join all lines together again and feed it back into the compose editor.
        var newtext = wrapText.join("\n");
        this.replaceEditorText(newtext);
      }
    }
    catch (ex) {
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Exception while wrapping=" + ex + "\n");
    }

  },

  // Save draft message. We do not want most of the other processing for encrypted mails here...
  saveDraftMessage: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: saveDraftMessage()\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    let doEncrypt = this.getAccDefault("enabled") && this.identity.getBoolAttribute("autoEncryptDrafts");

    this.setDraftStatus();

    if (!doEncrypt) {
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: drafts disabled\n");

      try {
        if (gMsgCompose.compFields.securityInfo instanceof Components.interfaces.nsIEnigMsgCompFields) {
          gMsgCompose.compFields.securityInfo.sendFlags = 0;
        }
      }
      catch (ex) {}

      return true;
    }

    let sendFlags = nsIIcecrypt.SEND_PGP_MIME | nsIIcecrypt.SEND_ENCRYPTED | nsIIcecrypt.SAVE_MESSAGE | nsIIcecrypt.SEND_ALWAYS_TRUST;

    let fromAddr = this.identity.email;
    let userIdValue = this.getSenderUserId();
    if (userIdValue) {
      fromAddr = userIdValue;
    }

    let icecryptSvc = IcecryptCore.getService(window);
    if (!icecryptSvc) return true;

    let useIcecrypt = this.preferPgpOverSmime(sendFlags);

    if (useIcecrypt === null) return false; // dialog aborted
    if (useIcecrypt === false) return true; // use S/MIME

    // Try to save draft

    var testCipher = null;
    var testExitCodeObj = {};
    var testStatusFlagsObj = {};
    var testErrorMsgObj = {};

    // encrypt test message for test recipients
    var testPlain = "Test Message";
    var testUiFlags = nsIIcecrypt.UI_TEST;
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.saveDraft(): call encryptMessage() for fromAddr=\"" + fromAddr + "\"\n");
    testCipher = icecryptSvc.encryptMessage(null, testUiFlags, testPlain,
      fromAddr, fromAddr, "",
      sendFlags | nsIIcecrypt.SEND_TEST,
      testExitCodeObj,
      testStatusFlagsObj,
      testErrorMsgObj);

    if (testStatusFlagsObj.value & (nsIIcecrypt.INVALID_RECIPIENT | nsIIcecrypt.NO_SECKEY)) {
      // check if own key is invalid
      if (testErrorMsgObj.value && testErrorMsgObj.value.length > 0) {
        ++this.saveDraftError;
        if (this.saveDraftError === 1) {
          this.notifyUser(3, IcecryptLocale.getString("msgCompose.cannotSaveDraft"), "saveDraftFailed",
            testErrorMsgObj.value);
        }
        return false;
      }
    }

    let newSecurityInfo;

    try {
      if (gMsgCompose.compFields.securityInfo instanceof Components.interfaces.nsIEnigMsgCompFields) {
        newSecurityInfo = gMsgCompose.compFields.securityInfo;
      }
      else {
        throw "dummy";
      }
    }
    catch (ex) {
      try {
        newSecurityInfo = Components.classes[this.compFieldsEnig_CID].createInstance(Components.interfaces.nsIEnigMsgCompFields);
        if (newSecurityInfo) {
          let oldSecurityInfo = gMsgCompose.compFields.securityInfo;
          newSecurityInfo.init(oldSecurityInfo);
          gMsgCompose.compFields.securityInfo = newSecurityInfo;
        }
      }
      catch (ex2) {
        IcecryptLog.writeException("icecryptMsgComposeOverlay.js: Icecrypt.msg.saveDraftMessage", ex);
        return false;
      }
    }

    newSecurityInfo.sendFlags = sendFlags;
    newSecurityInfo.UIFlags = 0;
    newSecurityInfo.senderEmailAddr = fromAddr;
    newSecurityInfo.recipients = fromAddr;
    newSecurityInfo.bccRecipients = "";
    this.dirty = true;

    return true;
  },

  encryptMsg: function(msgSendType) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: msgSendType=" + msgSendType + ", Icecrypt.msg.sendMode=" + this.sendMode + ", Icecrypt.msg.statusEncrypted=" + this.statusEncrypted +
      "\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;
    const CiMsgCompDeliverMode = Components.interfaces.nsIMsgCompDeliverMode;
    var promptSvc = IcecryptDialog.getPromptSvc();
    var newSecurityInfo;

    var gotSendFlags = this.sendMode;
    // here we process the final state:
    if (this.statusEncrypted == IcecryptConstants.ENIG_FINAL_YES ||
      this.statusEncrypted == IcecryptConstants.ENIG_FINAL_FORCEYES) {
      gotSendFlags |= ENCRYPT;
    }
    else if (this.statusEncrypted == IcecryptConstants.ENIG_FINAL_FORCENO) {
      gotSendFlags &= ~ENCRYPT;
    }

    if (this.statusSigned == IcecryptConstants.ENIG_FINAL_YES ||
      this.statusSigned == IcecryptConstants.ENIG_FINAL_FORCEYES) {
      gotSendFlags |= SIGN;
    }
    else if (this.statusSigned == IcecryptConstants.ENIG_FINAL_FORCENO) {
      gotSendFlags &= ~SIGN;
    }

    var sendFlags = 0;

    switch (msgSendType) {
      case CiMsgCompDeliverMode.Later:
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: adding SEND_LATER\n");
        sendFlags |= nsIIcecrypt.SEND_LATER;
        break;
      case CiMsgCompDeliverMode.SaveAsDraft:
      case CiMsgCompDeliverMode.SaveAsTemplate:
      case CiMsgCompDeliverMode.AutoSaveAsDraft:
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: detected save draft\n");

        // saving drafts is simpler and works differently than the rest of Icecrypt.
        // All rules except account-settings are ignored.
        return this.saveDraftMessage();
    }

    this.unsetAdditionalHeader("x-icecrypt-draft-status");

    var msgCompFields = gMsgCompose.compFields;
    var newsgroups = msgCompFields.newsgroups; // Check if sending to any newsgroups

    if (msgCompFields.to === "" &&
      msgCompFields.cc === "" &&
      msgCompFields.bcc === "" &&
      newsgroups === "") {
      // don't attempt to send message if no recipient specified
      var bundle = document.getElementById("bundle_composeMsgs");
      IcecryptDialog.alert(window, bundle.getString("12511"));
      return false;
    }

    if (gotSendFlags & SIGN) sendFlags |= SIGN;
    if (gotSendFlags & ENCRYPT) sendFlags |= ENCRYPT;

    this.identity = getCurrentIdentity();
    var encryptIfPossible = false;

    if (gWindowLocked) {
      IcecryptDialog.alert(window, IcecryptLocale.getString("windowLocked"));
      return false;
    }

    if (this.dirty) {
      // make sure the sendFlags are reset before the message is processed
      // (it may have been set by a previously cancelled send operation!)
      try {
        if (gMsgCompose.compFields.securityInfo instanceof Components.interfaces.nsIEnigMsgCompFields) {
          gMsgCompose.compFields.securityInfo.sendFlags = 0;
          gMsgCompose.compFields.securityInfo.originalSubject = gMsgCompose.compFields.subject;
        }
        else if (!gMsgCompose.compFields.securityInfo) {
          throw "dummy";
        }
      }
      catch (ex) {
        try {
          newSecurityInfo = Components.classes[this.compFieldsEnig_CID].createInstance(Components.interfaces.nsIEnigMsgCompFields);
          if (newSecurityInfo) {
            newSecurityInfo.sendFlags = 0;
            newSecurityInfo.originalSubject = gMsgCompose.compFields.subject;

            gMsgCompose.compFields.securityInfo = newSecurityInfo;
          }
        }
        catch (ex2) {
          IcecryptLog.writeException("icecryptMsgComposeOverlay.js: Icecrypt.msg.attachKey", ex);
        }
      }
    }
    this.dirty = 1;

    var icecryptSvc = IcecryptCore.getService(window);
    if (!icecryptSvc) {
      var msg = IcecryptLocale.getString("sendUnencrypted");
      if (IcecryptCore.getIcecryptService() && IcecryptCore.getIcecryptService().initializationError) {
        msg = IcecryptCore.getIcecryptService().initializationError + "\n\n" + msg;
      }

      return IcecryptDialog.confirmDlg(window, msg, IcecryptLocale.getString("msgCompose.button.send"));
    }

    try {

      this.modifiedAttach = null;

      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: currentId=" + this.identity +
        ", " + this.identity.email + "\n");
      var fromAddr = this.identity.email;

      var pgpEnabled = this.getAccDefault("enabled");

      if (!pgpEnabled) return true;

      var optSendFlags = 0;
      var inlineEncAttach = false;

      // request or preference to always accept (even non-authenticated) keys?
      if (this.trustAllKeys) {
        optSendFlags |= nsIIcecrypt.SEND_ALWAYS_TRUST;
      }
      else {
        var acceptedKeys = IcecryptPrefs.getPref("acceptedKeys");
        switch (acceptedKeys) {
          case 0: // accept valid/authenticated keys only
            break;
          case 1: // accept all but revoked/disabled/expired keys
            optSendFlags |= nsIIcecrypt.SEND_ALWAYS_TRUST;
            break;
          default:
            IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: INVALID VALUE for acceptedKeys: \"" + acceptedKeys + "\"\n");
            break;
        }
      }

      if (IcecryptPrefs.getPref("encryptToSelf")) {
        optSendFlags |= nsIIcecrypt.SEND_ENCRYPT_TO_SELF;
      }

      sendFlags |= optSendFlags;

      var userIdValue = this.getSenderUserId();
      if (userIdValue) {
        fromAddr = userIdValue;
      }

      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg:gMsgCompose=" + gMsgCompose + "\n");

      var toAddrList = [];
      var bccAddrList = [];

      var splitRecipients;
      var arrLen = {};
      var recList;
      splitRecipients = msgCompFields.splitRecipients;

      if (msgCompFields.to.length > 0) {
        recList = splitRecipients(msgCompFields.to, true, arrLen);
        this.addRecipients(toAddrList, recList);
      }

      if (msgCompFields.cc.length > 0) {
        recList = splitRecipients(msgCompFields.cc, true, arrLen);
        this.addRecipients(toAddrList, recList);
      }

      // special handling of bcc:
      // - note: bcc and encryption is a problem
      // - but bcc to the sender is fine
      if (msgCompFields.bcc.length > 0) {
        recList = splitRecipients(msgCompFields.bcc, true, arrLen);

        var bccLC = IcecryptFuncs.stripEmail(msgCompFields.bcc).toLowerCase();
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: BCC: " + bccLC + "\n");

        var selfBCC = this.identity.email && (this.identity.email.toLowerCase() == bccLC);

        if (selfBCC) {
          IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: Self BCC\n");
          this.addRecipients(toAddrList, recList);

        }
        else if (sendFlags & ENCRYPT) {
          // BCC and encryption

          if (encryptIfPossible) {
            sendFlags &= ~ENCRYPT;
            IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: No default encryption because of BCC\n");
          }
          else {
            var dummy = {
              value: null
            };

            var hideBccUsers = promptSvc.confirmEx(window,
              IcecryptLocale.getString("enigConfirm"),
              IcecryptLocale.getString("sendingHiddenRcpt"), (promptSvc.BUTTON_TITLE_IS_STRING * promptSvc.BUTTON_POS_0) +
              (promptSvc.BUTTON_TITLE_CANCEL * promptSvc.BUTTON_POS_1) +
              (promptSvc.BUTTON_TITLE_IS_STRING * promptSvc.BUTTON_POS_2),
              IcecryptLocale.getString("sendWithShownBcc"),
              null,
              IcecryptLocale.getString("sendWithHiddenBcc"),
              null,
              dummy);
            switch (hideBccUsers) {
              case 2:
                this.addRecipients(bccAddrList, recList);
                this.addRecipients(toAddrList, recList);
                break;
              case 0:
                this.addRecipients(toAddrList, recList);
                break;
              case 1:
                return false;
            }
          }
        }
      }

      if (newsgroups) {
        toAddrList.push(newsgroups);

        if (sendFlags & ENCRYPT) {

          if (!encryptIfPossible) {
            if (!IcecryptPrefs.getPref("encryptToNews")) {
              IcecryptDialog.alert(window, IcecryptLocale.getString("sendingNews"));
              return false;
            }
            else if (!IcecryptDialog.confirmPref(window,
                IcecryptLocale.getString("sendToNewsWarning"),
                "warnOnSendingNewsgroups",
                IcecryptLocale.getString("msgCompose.button.send"))) {
              return false;
            }
          }
          else {
            sendFlags &= ~ENCRYPT;
            IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: No default encryption because of newsgroups\n");
          }
        }
      }

      var usePGPMimeOption = IcecryptPrefs.getPref("usePGPMimeOption");

      if (this.sendPgpMime) {
        // Use PGP/MIME
        sendFlags |= nsIIcecrypt.SEND_PGP_MIME;
      }

      var result = this.keySelection(icecryptSvc,
        sendFlags, // all current combined/processed send flags (incl. optSendFlags)
        optSendFlags, // may only be SEND_ALWAYS_TRUST or SEND_ENCRYPT_TO_SELF
        gotSendFlags, // initial sendMode (0 or SIGN or ENCRYPT or SIGN|ENCRYPT)
        fromAddr, toAddrList, bccAddrList);
      if (!result) {
        return false;
      }

      var toAddrStr;
      var bccAddrStr;
      sendFlags = result.sendFlags;
      toAddrStr = result.toAddrStr;
      bccAddrStr = result.bccAddrStr;

      var useIcecrypt = this.preferPgpOverSmime(sendFlags);

      if (useIcecrypt === null) return false; // dialog aborted
      if (useIcecrypt === false) {
        // use S/MIME
        sendFlags = 0;
        return true;
      }

      if (this.attachOwnKeyObj.appendAttachment) this.attachOwnKey();

      var bucketList = document.getElementById("attachmentBucket");
      var hasAttachments = ((bucketList && bucketList.hasChildNodes()) || gMsgCompose.compFields.attachVCard);

      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: hasAttachments = " + hasAttachments + "\n");

      if (hasAttachments &&
        (sendFlags & (ENCRYPT | SIGN)) &&
        !(sendFlags & nsIIcecrypt.SEND_PGP_MIME)) {

        let inputObj = {
          pgpMimePossible: true,
          inlinePossible: true,
          restrictedScenario: false,
          reasonForCheck: ""
        };
        // init reason for dialog to be able to use the right labels
        if (sendFlags & ENCRYPT) {
          if (sendFlags & SIGN) {
            inputObj.reasonForCheck = "encryptAndSign";
          }
          else {
            inputObj.reasonForCheck = "encrypt";
          }
        }
        else {
          if (sendFlags & SIGN) {
            inputObj.reasonForCheck = "sign";
          }
        }

        // determine if attachments are all local files (currently the only
        // supported kind of attachments)
        var node = bucketList.firstChild;
        while (node) {
          if (node.attachment.url.substring(0, 7) != "file://") {
            inputObj.inlinePossible = false;
          }
          node = node.nextSibling;
        }

        if (inputObj.pgpMimePossible || inputObj.inlinePossible) {
          let resultObj = {
            selected: IcecryptPrefs.getPref("encryptAttachments")
          };

          //skip or not
          var skipCheck = IcecryptPrefs.getPref("encryptAttachmentsSkipDlg");
          if (skipCheck == 1) {
            if ((resultObj.selected == 2 && inputObj.pgpMimePossible === false) || (resultObj.selected == 1 && inputObj.inlinePossible === false)) {
              //add var to disable remember box since we're dealing with restricted scenarios...
              inputObj.restrictedScenario = true;
              resultObj.selected = -1;
              window.openDialog("chrome://icecrypt/content/icecryptAttachmentsDialog.xul", "", "dialog,modal,centerscreen", inputObj, resultObj);
            }
          }
          else {
            resultObj.selected = -1;
            window.openDialog("chrome://icecrypt/content/icecryptAttachmentsDialog.xul", "", "dialog,modal,centerscreen", inputObj, resultObj);
          }
          if (resultObj.selected < 0) {
            // dialog cancelled
            return false;
          }
          else if (resultObj.selected == 1) {
            // encrypt attachments
            inlineEncAttach = true;
          }
          else if (resultObj.selected == 2) {
            // send as PGP/MIME
            sendFlags |= nsIIcecrypt.SEND_PGP_MIME;
          }
          else if (resultObj.selected == 3) {
            // cancel the encryption/signing for the whole message
            sendFlags &= ~ENCRYPT;
            sendFlags &= ~SIGN;
          }
        }
        else {
          if (sendFlags & ENCRYPT) {
            if (!IcecryptDialog.confirmDlg(window,
                IcecryptLocale.getString("attachWarning"),
                IcecryptLocale.getString("msgCompose.button.send")))
              return false;
          }
        }
      }

      var usingPGPMime = (sendFlags & nsIIcecrypt.SEND_PGP_MIME) &&
        (sendFlags & (ENCRYPT | SIGN));

      // ----------------------- Rewrapping code, taken from function "encryptInline"

      // Check wrapping, if sign only and inline and plaintext
      if ((sendFlags & SIGN) && !(sendFlags & ENCRYPT) && !(usingPGPMime) && !(gMsgCompose.composeHTML)) {
        var wrapresultObj = {};

        this.wrapInLine(wrapresultObj);

        if (wrapresultObj.usePpgMime) {
          sendFlags |= nsIIcecrypt.SEND_PGP_MIME;
          usingPGPMime = nsIIcecrypt.SEND_PGP_MIME;
        }
        if (wrapresultObj.cancelled) {
          return false;
        }
      }

      var uiFlags = nsIIcecrypt.UI_INTERACTIVE;

      if (usingPGPMime)
        uiFlags |= nsIIcecrypt.UI_PGP_MIME;

      if ((sendFlags & (ENCRYPT | SIGN)) && usingPGPMime) {
        // Use PGP/MIME
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: Using PGP/MIME, flags=" + sendFlags + "\n");

        var oldSecurityInfo = gMsgCompose.compFields.securityInfo;

        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: oldSecurityInfo = " + oldSecurityInfo + "\n");

        if (!oldSecurityInfo) {
          try {
            newSecurityInfo = oldSecurityInfo.QueryInterface(Components.interfaces.nsIEnigMsgCompFields);
          }
          catch (ex) {}
        }

        if (!newSecurityInfo) {
          newSecurityInfo = Components.classes[this.compFieldsEnig_CID].createInstance(Components.interfaces.nsIEnigMsgCompFields);

          if (!newSecurityInfo)
            throw Components.results.NS_ERROR_FAILURE;

          newSecurityInfo.init(oldSecurityInfo);
          gMsgCompose.compFields.securityInfo = newSecurityInfo;
        }

        newSecurityInfo.originalSubject = gMsgCompose.compFields.subject;
        newSecurityInfo.originalReferences = gMsgCompose.compFields.references;

        if (this.protectHeaders) {
          sendFlags |= nsIIcecrypt.ENCRYPT_HEADERS;

          if (sendFlags & ENCRYPT) {
            gMsgCompose.compFields.subject = IcecryptFuncs.getProtectedSubjectText();
            gMsgCompose.compFields.references = "";
          }

        }

        newSecurityInfo.sendFlags = sendFlags;
        newSecurityInfo.UIFlags = uiFlags;
        newSecurityInfo.senderEmailAddr = fromAddr;
        newSecurityInfo.recipients = toAddrStr;
        newSecurityInfo.bccRecipients = bccAddrStr;

        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: securityInfo = " + newSecurityInfo + "\n");

      }
      else if (!this.processed && (sendFlags & (ENCRYPT | SIGN))) {
        // use inline PGP

        var sendInfo = {
          sendFlags: sendFlags,
          inlineEncAttach: inlineEncAttach,
          fromAddr: fromAddr,
          toAddr: toAddrStr,
          bccAddr: bccAddrStr,
          uiFlags: uiFlags,
          bucketList: bucketList
        };

        if (!this.encryptInline(sendInfo)) {
          return false;
        }
      }

      var ioService = Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService);
      // EnigSend: Handle both plain and encrypted messages below
      var isOffline = (ioService && ioService.offline);

      // update the list of attachments
      Attachments2CompFields(msgCompFields);

      // process whether final confirmation is necessary
      var confirm = false;
      var conf = IcecryptPrefs.getPref("confirmBeforeSending");
      switch (conf) {
        case 0: // never
          confirm = false;
          break;
        case 1: // always
          confirm = true;
          break;
        case 2: // if send encrypted
          confirm = ((sendFlags & ENCRYPT) == ENCRYPT);
          break;
        case 3: // if send unencrypted
          confirm = ((sendFlags & ENCRYPT) === 0);
          break;
        case 4: // if encryption changed due to rules
          confirm = ((sendFlags & ENCRYPT) != (this.sendMode & ENCRYPT));
          break;
      }

      // double check that no internal error did result in broken promise of encryption
      // - if NOT send encrypted
      //   - although encryption was
      //     - the recent processed resulting encryption status or
      //     - was signaled in the status bar but is not the outcome now
      if ((sendFlags & ENCRYPT) === 0 &&
        (this.statusEncrypted == IcecryptConstants.ENIG_FINAL_YES ||
          this.statusEncrypted == IcecryptConstants.ENIG_FINAL_FORCEYES ||
          this.statusEncryptedInStatusBar == IcecryptConstants.ENIG_FINAL_YES ||
          this.statusEncryptedInStatusBar == IcecryptConstants.ENIG_FINAL_FORCEYES)) {
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: promised encryption did not succeed\n");
        if (!IcecryptDialog.confirmDlg(window,
            IcecryptLocale.getString("msgCompose.internalEncryptionError"),
            IcecryptLocale.getString("msgCompose.button.sendAnyway"))) {
          return false; // cancel sending
        }
        // without canceling sending, force firnal confirmation
        confirm = true;
      }

      // perform confirmation dialog if necessary/requested
      if (confirm) {
        if (!this.confirmBeforeSend(toAddrList.join(", "), toAddrStr + ", " + bccAddrStr, sendFlags, isOffline)) {
          if (this.processed) {
            this.undoEncryption(0);
          }
          else {
            this.removeAttachedKey();
          }
          return false;
        }
      }
      else if ((sendFlags & nsIIcecrypt.SEND_WITH_CHECK) &&
        !this.messageSendCheck()) {
        // Abort send
        if (this.processed) {
          this.undoEncryption(0);
        }
        else {
          this.removeAttachedKey();
        }

        return false;
      }

      if (msgCompFields.characterSet != "ISO-2022-JP") {
        if ((usingPGPMime &&
            ((sendFlags & (ENCRYPT | SIGN)))) || ((!usingPGPMime) && (sendFlags & ENCRYPT))) {
          try {
            // make sure plaintext is not changed to 7bit
            if (typeof(msgCompFields.forceMsgEncoding) == "boolean") {
              msgCompFields.forceMsgEncoding = true;
              IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: enabled forceMsgEncoding\n");
            }
          }
          catch (ex) {}
        }
      }
    }
    catch (ex) {
      IcecryptLog.writeException("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg", ex);
      let msg = IcecryptLocale.getString("signFailed");
      if (IcecryptCore.getIcecryptService() && IcecryptCore.getIcecryptService().initializationError) {
        msg += "\n" + IcecryptCore.getIcecryptService().initializationError;
      }
      return IcecryptDialog.confirmDlg(window, msg, IcecryptLocale.getString("msgCompose.button.sendUnencrypted"));
    }

    // The encryption process for PGP/MIME messages follows "here". It's
    // called automatically from nsMsgCompose->sendMsg().
    // registration for this is dome in chrome.manifest

    return true;
  },

  encryptInline: function(sendInfo) {
    // sign/encrpyt message using inline-PGP

    const dce = Components.interfaces.nsIDocumentEncoder;
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;

    var icecryptSvc = IcecryptCore.getService(window);
    if (!icecryptSvc) return false;

    if (gMsgCompose.composeHTML) {
      var errMsg = IcecryptLocale.getString("hasHTML");
      IcecryptDialog.alertCount(window, "composeHtmlAlertCount", errMsg);
    }

    try {
      var convert = DetermineConvertibility();
      if (convert == Components.interfaces.nsIMsgCompConvertible.No) {
        if (!IcecryptDialog.confirmDlg(window,
            IcecryptLocale.getString("strippingHTML"),
            IcecryptLocale.getString("msgCompose.button.sendAnyway"))) {
          return false;
        }
      }
    }
    catch (ex) {
      IcecryptLog.writeException("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptInline", ex);
    }

    try {
      if (this.getMailPref("mail.strictly_mime")) {
        if (IcecryptDialog.confirmPref(window,
            IcecryptLocale.getString("quotedPrintableWarn"), "quotedPrintableWarn")) {
          IcecryptPrefs.getPrefRoot().setBoolPref("mail.strictly_mime", false);
        }
      }
    }
    catch (ex) {}


    var sendFlowed;
    try {
      sendFlowed = this.getMailPref("mailnews.send_plaintext_flowed");
    }
    catch (ex) {
      sendFlowed = true;
    }
    var encoderFlags = dce.OutputFormatted | dce.OutputLFLineBreak;

    var wrapper = gMsgCompose.editor.QueryInterface(Components.interfaces.nsIEditorMailSupport);
    var editor = gMsgCompose.editor.QueryInterface(Components.interfaces.nsIPlaintextEditor);
    var wrapWidth = 72;

    if (!(sendInfo.sendFlags & ENCRYPT)) {
      // signed messages only
      if (gMsgCompose.composeHTML) {
        // enforce line wrapping here
        // otherwise the message isn't signed correctly
        try {
          wrapWidth = this.getMailPref("editor.htmlWrapColumn");

          if (wrapWidth > 0 && wrapWidth < 68 && gMsgCompose.wrapLength > 0) {
            if (IcecryptDialog.confirmDlg(window, IcecryptLocale.getString("minimalLineWrapping", [wrapWidth]))) {
              IcecryptPrefs.getPrefRoot().setIntPref("editor.htmlWrapColumn", 68);
            }
          }
          if (IcecryptPrefs.getPref("wrapHtmlBeforeSend")) {
            if (wrapWidth) {
              editor.wrapWidth = wrapWidth - 2; // prepare for the worst case: a 72 char's long line starting with '-'
              wrapper.rewrap(false);
            }
          }
        }
        catch (ex) {}
      }
      else {
        // plaintext: Wrapping code has been moved to superordinate function encryptMsg to enable interactive format switch
      }
    }

    var exitCodeObj = {};
    var statusFlagsObj = {};
    var errorMsgObj = {};
    var exitCode;

    // Get plain text
    // (Do we need to set the nsIDocumentEncoder.* flags?)
    var origText = this.editorGetContentAs("text/plain",
      encoderFlags);
    if (!origText) origText = "";

    if (origText.length > 0) {
      // Sign/encrypt body text

      var escText = origText; // Copy plain text for possible escaping

      if (sendFlowed && !(sendInfo.sendFlags & ENCRYPT)) {
        // Prevent space stuffing a la RFC 2646 (format=flowed).

        //IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: escText["+encoderFlags+"] = '"+escText+"'\n");

        escText = escText.replace(/^From /gm, "~From ");
        escText = escText.replace(/^>/gm, "|");
        escText = escText.replace(/^[ \t]+$/gm, "");
        escText = escText.replace(/^ /gm, "~ ");

        //IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: escText = '"+escText+"'\n");
        // Replace plain text and get it again
        this.replaceEditorText(escText);

        escText = this.editorGetContentAs("text/plain", encoderFlags);
      }

      // Replace plain text and get it again (to avoid linewrapping problems)
      this.replaceEditorText(escText);

      escText = this.editorGetContentAs("text/plain", encoderFlags);

      //IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: escText["+encoderFlags+"] = '"+escText+"'\n");

      // Encrypt plaintext
      var charset = this.editorGetCharset();
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptMsg: charset=" + charset + "\n");

      // Encode plaintext to charset from unicode
      var plainText = (sendInfo.sendFlags & ENCRYPT) ?
        IcecryptData.convertFromUnicode(origText, charset) :
        IcecryptData.convertFromUnicode(escText, charset);

      var cipherText = icecryptSvc.encryptMessage(window, sendInfo.uiFlags, plainText,
        sendInfo.fromAddr, sendInfo.toAddr, sendInfo.bccAddr,
        sendInfo.sendFlags,
        exitCodeObj, statusFlagsObj,
        errorMsgObj);

      exitCode = exitCodeObj.value;

      //IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: cipherText = '"+cipherText+"'\n");
      if (cipherText && (exitCode === 0)) {
        // Encryption/signing succeeded; overwrite plaintext

        if (gMsgCompose.composeHTML) {
          // workaround for Thunderbird bug (TB adds an extra space in front of the text)
          cipherText = "\n" + cipherText;
        }
        else
          cipherText = cipherText.replace(/\r\n/g, "\n");

        if ((sendInfo.sendFlags & ENCRYPT) && charset &&
          (charset.search(/^us-ascii$/i) !== 0)) {
          // Add Charset armor header for encrypted blocks
          cipherText = cipherText.replace(/(-----BEGIN PGP MESSAGE----- *)(\r?\n)/, "$1$2Charset: " + charset + "$2");
        }

        // Decode ciphertext from charset to unicode and overwrite
        this.replaceEditorText(IcecryptData.convertToUnicode(cipherText, charset));
        this.enableUndoEncryption(true);

        // Save original text (for undo)
        this.processed = {
          "origText": origText,
          "charset": charset
        };

      }
      else {
        // Restore original text
        this.replaceEditorText(origText);
        this.enableUndoEncryption(false);

        if (sendInfo.sendFlags & (ENCRYPT | SIGN)) {
          // Encryption/signing failed

          /*if (statusFlagsObj.statusMsg) {
            // check if own key is invalid
            let s = new RegExp("^(\\[GNUPG:\\] )?INV_(RECP|SGNR) [0-9]+ \\<?" + sendInfo.fromAddr + "\\>?", "m");
            if (statusFlagsObj.statusMsg.search(s) >= 0) {
              errorMsgObj.value += "\n\n" + IcecryptLocale.getString("keyError.resolutionAction");
            }
          }*/

          this.sendAborted(window, errorMsgObj);
          return false;
        }
      }
    }

    if (sendInfo.inlineEncAttach) {
      // encrypt attachments
      this.modifiedAttach = [];
      exitCode = this.encryptAttachments(sendInfo.bucketList, this.modifiedAttach,
        window, sendInfo.uiFlags, sendInfo.fromAddr, sendInfo.toAddr, sendInfo.bccAddr,
        sendInfo.sendFlags, errorMsgObj);
      if (exitCode !== 0) {
        this.modifiedAttach = null;
        this.sendAborted(window, errorMsgObj);
        if (this.processed) {
          this.undoEncryption(0);
        }
        else {
          this.removeAttachedKey();
        }
        return false;
      }
    }
    return true;
  },


  sendAborted: function(window, errorMsgObj) {
    if (errorMsgObj && errorMsgObj.value) {
      var txt = errorMsgObj.value;
      var txtLines = txt.split(/\r?\n/);
      var errorMsg = "";
      for (var i = 0; i < txtLines.length; ++i) {
        var line = txtLines[i];
        var tokens = line.split(/ /);
        // process most important business reasons for invalid recipient (and sender) errors:
        if (tokens.length == 3 && (tokens[0] == "INV_RECP" || tokens[0] == "INV_SGNR")) {
          var reason = tokens[1];
          var key = tokens[2];
          if (reason == "10") {
            errorMsg += IcecryptLocale.getString("keyNotTrusted", [key]) + "\n";
          }
          else if (reason == "1") {
            errorMsg += IcecryptLocale.getString("keyNotFound", [key]) + "\n";
          }
          else if (reason == "4") {
            errorMsg += IcecryptLocale.getString("keyRevoked", [key]) + "\n";
          }
          else if (reason == "5") {
            errorMsg += IcecryptLocale.getString("keyExpired", [key]) + "\n";
          }
        }
      }
      if (errorMsg !== "") {
        txt = errorMsg + "\n" + txt;
      }
      IcecryptDialog.alert(window, IcecryptLocale.getString("sendAborted") + txt);
    }
    else {
      IcecryptDialog.alert(window, IcecryptLocale.getString("sendAborted") + "\n" +
        IcecryptLocale.getString("msgCompose.internalError"));
    }
  },


  getMailPref: function(prefName) {
    let prefRoot = IcecryptPrefs.getPrefRoot();

    var prefValue = null;
    try {
      var prefType = prefRoot.getPrefType(prefName);
      // Get pref value
      switch (prefType) {
        case prefRoot.PREF_BOOL:
          prefValue = prefRoot.getBoolPref(prefName);
          break;

        case prefRoot.PREF_INT:
          prefValue = prefRoot.getIntPref(prefName);
          break;

        case prefRoot.PREF_STRING:
          prefValue = prefRoot.getCharPref(prefName);
          break;

        default:
          prefValue = undefined;
          break;
      }

    }
    catch (ex) {
      // Failed to get pref value
      IcecryptLog.ERROR("icecryptMsgComposeOverlay.js: Icecrypt.msg.getMailPref: unknown prefName:" + prefName + " \n");
    }

    return prefValue;
  },

  messageSendCheck: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.messageSendCheck\n");

    try {
      var warn = this.getMailPref("mail.warn_on_send_accel_key");

      if (warn) {
        var checkValue = {
          value: false
        };
        var bundle = document.getElementById("bundle_composeMsgs");
        var buttonPressed = IcecryptDialog.getPromptSvc().confirmEx(window,
          bundle.getString('sendMessageCheckWindowTitle'),
          bundle.getString('sendMessageCheckLabel'), (IcecryptDialog.getPromptSvc().BUTTON_TITLE_IS_STRING * IcecryptDialog.getPromptSvc().BUTTON_POS_0) +
          (IcecryptDialog.getPromptSvc().BUTTON_TITLE_CANCEL * IcecryptDialog.getPromptSvc().BUTTON_POS_1),
          bundle.getString('sendMessageCheckSendButtonLabel'),
          null, null,
          bundle.getString('CheckMsg'),
          checkValue);
        if (buttonPressed !== 0) {
          return false;
        }
        if (checkValue.value) {
          IcecryptPrefs.getPrefRoot().setBoolPref("mail.warn_on_send_accel_key", false);
        }
      }
    }
    catch (ex) {}

    return true;
  },


  /**
   * set non-standard message Header
   * (depending on TB version)
   *
   * hdr: String: header type (e.g. X-Icecrypt-Version)
   * val: String: header data (e.g. 1.2.3.4)
   */
  setAdditionalHeader: function(hdr, val) {
    if ("otherRandomHeaders" in gMsgCompose.compFields) {
      // TB <= 36
      gMsgCompose.compFields.otherRandomHeaders += hdr + ": " + val + "\r\n";
    }
    else {
      gMsgCompose.compFields.setHeader(hdr, val);
    }
  },

  unsetAdditionalHeader: function(hdr) {
    if ("otherRandomHeaders" in gMsgCompose.compFields) {
      // TB <= 36
      let h = gMsgCompose.compFields.otherRandomHeaders;
      let r = new RegExp("^(" + hdr + ":)(.*)$", "im");
      let m = h.replace(r, "").replace(/(\r\n)+/, "\r\n");
      gMsgCompose.compFields.otherRandomHeaders = m;
    }
    else {
      gMsgCompose.compFields.deleteHeader(hdr);
    }
  },

  modifyCompFields: function() {

    const HEADERMODE_KEYID = 0x01;
    const HEADERMODE_URL = 0x10;

    try {

      if (!this.identity) {
        this.identity = getCurrentIdentity();
      }

      if (this.identity.getBoolAttribute("enablePgp")) {
        if (IcecryptPrefs.getPref("addHeaders")) {
          this.setAdditionalHeader("X-Icecrypt-Version", IcecryptApp.getVersion());
        }
        var pgpHeader = "";
        var openPgpHeaderMode = this.identity.getIntAttribute("openPgpHeaderMode");

        if (openPgpHeaderMode & HEADERMODE_KEYID) {

          let key = IcecryptKeyRing.getKeyById(this.identity.getCharAttribute("pgpkeyId"));
          if (key && key.fpr && key.fpr.length > 0) {
            pgpHeader += "id=" + key.fpr;
          }
        }
        if (openPgpHeaderMode & HEADERMODE_URL) {
          if (pgpHeader.indexOf("=") > 0) pgpHeader += ";\r\n\t";
          pgpHeader += "url=" + this.identity.getCharAttribute("openPgpUrlName");
        }
        if (pgpHeader.length > 0) {
          this.setAdditionalHeader("OpenPGP", pgpHeader);
        }
      }
    }
    catch (ex) {
      IcecryptLog.writeException("icecryptMsgComposeOverlay.js: Icecrypt.msg.modifyCompFields", ex);
    }
  },

  /**
   * Handle the 'compose-send-message' event from TB
   */
  sendMessageListener: function(event) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.sendMessageListener\n");

    // Do nothing if a compatible version of the "SendLater" addon is installed.
    // SendLater will call handleSendMessageEvent when needed.

    try {
      if (typeof(Sendlater3Composing.callIcecrypt) === "function") {
        return;
      }
    }
    catch (ex) {}

    Icecrypt.msg.handleSendMessageEvent(event);
  },

  /**
   * Perform handling of the compose-send-message' event from TB (or SendLater)
   */
  handleSendMessageEvent: function(event) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.handleSendMessageEvent\n");
    let msgcomposeWindow = document.getElementById("msgcomposeWindow");
    let sendMsgType = Number(msgcomposeWindow.getAttribute("msgtype"));

    if (!(this.sendProcess && sendMsgType == Components.interfaces.nsIMsgCompDeliverMode.AutoSaveAsDraft)) {
      this.sendProcess = true;
      let bc = document.getElementById("icecrypt-bc-sendprocess");

      try {
        this.modifyCompFields();
        bc.setAttribute("disabled", "true");
        if (!this.encryptMsg(sendMsgType)) {
          this.resetUpdatedFields();
          event.preventDefault();
          event.stopPropagation();
        }
      }
      catch (ex) {}
      bc.removeAttribute("disabled");
    }
    else {
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.sendMessageListener: sending in progress - autosave aborted\n");
      event.preventDefault();
      event.stopPropagation();
    }
    this.sendProcess = false;
  },


  // encrypt attachments when sending inline PGP mails
  // It's quite a hack: the attachments are stored locally
  // and the attachments list is modified to pick up the
  // encrypted file(s) instead of the original ones.
  encryptAttachments: function(bucketList, newAttachments, window, uiFlags,
    fromAddr, toAddr, bccAddr, sendFlags,
    errorMsgObj) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptAttachments\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const SIGN = nsIIcecrypt.SEND_SIGNED;
    const ENCRYPT = nsIIcecrypt.SEND_ENCRYPTED;

    var ioServ;
    var fileTemplate;
    errorMsgObj.value = "";

    try {
      ioServ = Components.classes[IOSERVICE_CONTRACTID].getService(Components.interfaces.nsIIOService);
      if (!ioServ)
        return -1;

    }
    catch (ex) {
      return -1;
    }

    var tmpDir = IcecryptFiles.getTempDir();
    var extAppLauncher = Components.classes["@mozilla.org/mime;1"].
    getService(Components.interfaces.nsPIExternalAppLauncher);

    try {
      fileTemplate = Components.classes[LOCAL_FILE_CONTRACTID].createInstance(Components.interfaces.nsIFile);
      fileTemplate.initWithPath(tmpDir);
      if (!(fileTemplate.isDirectory() && fileTemplate.isWritable())) {
        errorMsgObj.value = IcecryptLocale.getString("noTempDir");
        return -1;
      }
      fileTemplate.append("encfile");
    }
    catch (ex) {
      errorMsgObj.value = IcecryptLocale.getString("noTempDir");
      return -1;
    }
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.encryptAttachments tmpDir=" + tmpDir + "\n");
    var icecryptSvc = IcecryptCore.getService(window);
    if (!icecryptSvc)
      return null;

    var exitCodeObj = {};
    var statusFlagsObj = {};

    var node = bucketList.firstChild;
    while (node) {
      var origUrl = node.attachment.url;
      if (origUrl.substring(0, 7) != "file://") {
        // this should actually never happen since it is pre-checked!
        errorMsgObj.value = "The attachment '" + node.attachment.name + "' is not a local file";
        return -1;
      }

      // transform attachment URL to platform-specific file name
      var origUri = ioServ.newURI(origUrl, null, null);
      var origFile = origUri.QueryInterface(Components.interfaces.nsIFileURL);
      if (node.attachment.temporary) {
        try {
          var origLocalFile = Components.classes[LOCAL_FILE_CONTRACTID].createInstance(Components.interfaces.nsIFile);
          origLocalFile.initWithPath(origFile.file.path);
          extAppLauncher.deleteTemporaryFileOnExit(origLocalFile);
        }
        catch (ex) {}
      }

      var newFile = fileTemplate.clone();
      var txtMessage;
      try {
        newFile.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0x180);
        txtMessage = icecryptSvc.encryptAttachment(window, fromAddr, toAddr, bccAddr, sendFlags,
          origFile.file, newFile,
          exitCodeObj, statusFlagsObj,
          errorMsgObj);
      }
      catch (ex) {}

      if (exitCodeObj.value !== 0) {
        return exitCodeObj.value;
      }

      var fileInfo = {
        origFile: origFile,
        origUrl: node.attachment.url,
        origName: node.attachment.name,
        origTemp: node.attachment.temporary,
        origCType: node.attachment.contentType
      };

      // transform platform specific new file name to file:// URL
      var newUri = ioServ.newFileURI(newFile);
      fileInfo.newUrl = newUri.asciiSpec;
      fileInfo.newFile = newFile;
      fileInfo.encrypted = (sendFlags & ENCRYPT);

      newAttachments.push(fileInfo);
      node = node.nextSibling;
    }

    var i = 0;
    if (sendFlags & ENCRYPT) {
      // if we got here, all attachments were encrpted successfully,
      // so we replace their names & urls
      node = bucketList.firstChild;

      while (node) {
        node.attachment.url = newAttachments[i].newUrl;
        node.attachment.name += IcecryptPrefs.getPref("inlineAttachExt");
        node.attachment.contentType = "application/octet-stream";
        node.attachment.temporary = true;

        ++i;
        node = node.nextSibling;
      }
    }
    else {
      // for inline signing we need to add new attachments for every
      // signed file
      for (i = 0; i < newAttachments.length; i++) {
        // create new attachment
        var fileAttachment = Components.classes["@mozilla.org/messengercompose/attachment;1"].createInstance(Components.interfaces.nsIMsgAttachment);
        fileAttachment.temporary = true;
        fileAttachment.url = newAttachments[i].newUrl;
        fileAttachment.name = newAttachments[i].origName + IcecryptPrefs.getPref("inlineSigAttachExt");

        // add attachment to msg
        this.addAttachment(fileAttachment);
      }

    }
    return 0;
  },

  toggleAttribute: function(attrName) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.toggleAttribute('" + attrName + "')\n");

    var menuElement = document.getElementById("icecrypt_" + attrName);

    var oldValue = IcecryptPrefs.getPref(attrName);
    IcecryptPrefs.setPref(attrName, !oldValue);
  },

  toggleAccountAttr: function(attrName) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.toggleAccountAttr('" + attrName + "')\n");

    var oldValue = this.identity.getBoolAttribute(attrName);
    this.identity.setBoolAttribute(attrName, !oldValue);

  },

  toggleRules: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.toggleRules: Icecrypt.msg.enableRules=" + Icecrypt.msg.enableRules + "\n");
    this.enableRules = !this.enableRules;
  },

  decryptQuote: function(interactive) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.decryptQuote: " + interactive + "\n");
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;
    const CT = Components.interfaces.nsIMsgCompType;

    if (gWindowLocked || this.processed)
      return;

    var icecryptSvc = IcecryptCore.getService(window);
    if (!icecryptSvc)
      return;

    const dce = Components.interfaces.nsIDocumentEncoder;
    var encoderFlags = dce.OutputFormatted | dce.OutputLFLineBreak;

    var docText = this.editorGetContentAs("text/plain", encoderFlags);

    var blockBegin = docText.indexOf("-----BEGIN PGP ");
    if (blockBegin < 0)
      return;

    // Determine indentation string
    var indentBegin = docText.substr(0, blockBegin).lastIndexOf("\n");
    var indentStr = docText.substring(indentBegin + 1, blockBegin);

    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.decryptQuote: indentStr='" + indentStr + "'\n");

    var beginIndexObj = {};
    var endIndexObj = {};
    var indentStrObj = {};
    var blockType = IcecryptArmor.locateArmoredBlock(docText, 0, indentStr, beginIndexObj, endIndexObj, indentStrObj);
    if ((blockType != "MESSAGE") && (blockType != "SIGNED MESSAGE"))
      return;

    var beginIndex = beginIndexObj.value;
    var endIndex = endIndexObj.value;

    var head = docText.substr(0, beginIndex);
    var tail = docText.substr(endIndex + 1);

    var pgpBlock = docText.substr(beginIndex, endIndex - beginIndex + 1);
    var indentRegexp;

    if (indentStr) {
      if (indentStr == "> ") {
        // replace ">> " with "> > " to allow correct quoting
        pgpBlock = pgpBlock.replace(/^>>/gm, "> >");
      }

      // Delete indentation
      indentRegexp = new RegExp("^" + indentStr, "gm");

      pgpBlock = pgpBlock.replace(indentRegexp, "");
      //tail     =     tail.replace(indentRegexp, "");

      if (indentStr.match(/[ \t]*$/)) {
        indentStr = indentStr.replace(/[ \t]*$/gm, "");
        indentRegexp = new RegExp("^" + indentStr + "$", "gm");

        pgpBlock = pgpBlock.replace(indentRegexp, "");
      }


      // Handle blank indented lines
      pgpBlock = pgpBlock.replace(/^[ \t]*>[ \t]*$/gm, "");
      //tail     =     tail.replace(/^[ \t]*>[ \t]*$/g, "");

      // Trim leading space in tail
      tail = tail.replace(/^\s*\n/m, "\n");
    }

    if (tail.search(/\S/) < 0) {
      // No non-space characters in tail; delete it
      tail = "";
    }

    //IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.decryptQuote: pgpBlock='"+pgpBlock+"'\n");

    var charset = this.editorGetCharset();
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.decryptQuote: charset=" + charset + "\n");

    // Encode ciphertext from unicode to charset
    var cipherText = IcecryptData.convertFromUnicode(pgpBlock, charset);

    if ((!this.getMailPref("mailnews.reply_in_default_charset")) && (blockType == "MESSAGE")) {
      // set charset according to PGP block, if available (encrypted messages only)
      cipherText = cipherText.replace(/\r\n/g, "\n");
      cipherText = cipherText.replace(/\r/g, "\n");
      let cPos = cipherText.search(/\nCharset: .+\n/i);
      if (cPos < cipherText.search(/\n\n/)) {
        var charMatch = cipherText.match(/\n(Charset: )(.+)\n/i);
        if (charMatch && charMatch.length > 2) {
          charset = charMatch[2];
          gMsgCompose.SetDocumentCharset(charset);
        }
      }
    }

    // Decrypt message
    var signatureObj = {};
    signatureObj.value = "";
    var exitCodeObj = {};
    var statusFlagsObj = {};
    var userIdObj = {};
    var keyIdObj = {};
    var sigDetailsObj = {};
    var errorMsgObj = {};
    var blockSeparationObj = {};
    var encToDetailsObj = {};

    var uiFlags = nsIIcecrypt.UI_UNVERIFIED_ENC_OK;

    var plainText = icecryptSvc.decryptMessage(window, uiFlags, cipherText,
      signatureObj, exitCodeObj, statusFlagsObj,
      keyIdObj, userIdObj, sigDetailsObj,
      errorMsgObj, blockSeparationObj, encToDetailsObj);

    // Decode plaintext from charset to unicode
    plainText = IcecryptData.convertToUnicode(plainText, charset).replace(/\r\n/g, "\n");
    if (IcecryptPrefs.getPref("keepSettingsForReply")) {
      if (statusFlagsObj.value & nsIIcecrypt.DECRYPTION_OKAY)
        this.setSendMode('encrypt');
    }

    var exitCode = exitCodeObj.value;

    if (exitCode !== 0) {
      // Error processing
      var errorMsg = errorMsgObj.value;

      var statusLines = errorMsg.split(/\r?\n/);

      var displayMsg;
      if (statusLines && statusLines.length) {
        // Display only first ten lines of error message
        while (statusLines.length > 10)
          statusLines.pop();

        displayMsg = statusLines.join("\n");

        if (interactive)
          IcecryptDialog.alert(window, displayMsg);
      }
    }

    if (blockType == "MESSAGE" && exitCode === 0 && plainText.length === 0) {
      plainText = " ";
    }

    if (!plainText) {
      if (blockType != "SIGNED MESSAGE")
        return;

      // Extract text portion of clearsign block
      plainText = IcecryptArmor.extractSignaturePart(pgpBlock, nsIIcecrypt.SIGNATURE_TEXT);
    }

    const nsIMsgCompType = Components.interfaces.nsIMsgCompType;
    var doubleDashSeparator = IcecryptPrefs.getPref("doubleDashSeparator");
    if (gMsgCompose.type != nsIMsgCompType.Template &&
      gMsgCompose.type != nsIMsgCompType.Draft &&
      doubleDashSeparator) {
      var signOffset = plainText.search(/[\r\n]-- +[\r\n]/);

      if (signOffset < 0 && blockType == "SIGNED MESSAGE") {
        signOffset = plainText.search(/[\r\n]--[\r\n]/);
      }

      if (signOffset > 0) {
        // Strip signature portion of quoted message
        plainText = plainText.substr(0, signOffset + 1);
      }
    }

    var clipBoard = Components.classes["@mozilla.org/widget/clipboard;1"].
    getService(Components.interfaces.nsIClipboard);
    var data;
    if (clipBoard.supportsSelectionClipboard()) {
      // get the clipboard contents for selected text (X11)
      data = IcecryptClipboard.getClipboardContent(window, Components.interfaces.nsIClipboard.kSelectionClipboard);
    }

    // Replace encrypted quote with decrypted quote (destroys selection clipboard on X11)
    this.editorSelectAll();

    //IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.decryptQuote: plainText='"+plainText+"'\n");

    if (head)
      this.editorInsertText(head);

    var quoteElement;

    if (indentStr) {
      quoteElement = this.editorInsertAsQuotation(plainText);

    }
    else {
      this.editorInsertText(plainText);
    }

    if (tail)
      this.editorInsertText(tail);

    if (statusFlagsObj.value & nsIIcecrypt.DECRYPTION_OKAY) {
      let hLines = (head.search(/[^\s>]/) < 0 ? 0 : 1);

      if (hLines > 0) {
        switch (gMsgCompose.type) {
          case CT.Reply:
          case CT.ReplyAll:
          case CT.ReplyToSender:
          case CT.ReplyToGroup:
          case CT.ReplyToSenderAndGroup:
          case CT.ReplyToList:
            {
              // if head contains at most 1 line of text, we assume it's the
              // header above the quote (e.g. XYZ wrote:)

              let h = head.split(/\r?\n/);
              hLines = -1;

              for (let i = 0; i < h.length; i++) {
                if (h[i].search(/[^\s>]/) >= 0) hLines++;
              }
            }
        }
      }
      if (hLines > 0 || tail.search(/[^\s>]/) >= 0) {
        this.displayPartialEncryptedWarning();
      }
    }


    if (clipBoard.supportsSelectionClipboard()) {
      // restore the clipboard contents for selected text (X11)
      IcecryptClipboard.setClipboardContent(data, clipBoard.kSelectionClipboard);
    }

    if (interactive)
      return;

    // Position cursor
    var replyOnTop = 1;
    try {
      replyOnTop = this.identity.replyOnTop;
    }
    catch (ex) {}

    if (!indentStr || !quoteElement) replyOnTop = 1;

    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.decryptQuote: replyOnTop=" + replyOnTop + ", quoteElement=" + quoteElement + "\n");

    var nsISelectionController = Components.interfaces.nsISelectionController;

    if (this.editor.selectionController) {
      var selection = this.editor.selectionController;
      selection.completeMove(false, false); // go to start;

      switch (replyOnTop) {
        case 0:
          // Position after quote
          this.editor.endOfDocument();
          if (tail) {
            for (let cPos = 0; cPos < tail.length; cPos++) {
              selection.characterMove(false, false); // move backwards
            }
          }
          break;

        case 2:
          // Select quote

          if (head) {
            for (let cPos = 0; cPos < head.length; cPos++) {
              selection.characterMove(true, false);
            }
          }
          selection.completeMove(true, true);
          if (tail) {
            for (let cPos = 0; cPos < tail.length; cPos++) {
              selection.characterMove(false, true); // move backwards
            }
          }
          break;

        default:
          // Position at beginning of document

          if (this.editor) {
            this.editor.beginningOfDocument();
          }
      }

      this.editor.selectionController.scrollSelectionIntoView(nsISelectionController.SELECTION_NORMAL,
        nsISelectionController.SELECTION_ANCHOR_REGION,
        true);
    }

    this.processFinalState();
    this.updateStatusBar();
  },

  editorInsertText: function(plainText) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.editorInsertText\n");
    if (this.editor) {
      var mailEditor;
      try {
        mailEditor = this.editor.QueryInterface(Components.interfaces.nsIEditorMailSupport);
        mailEditor.insertTextWithQuotations(plainText);
      }
      catch (ex) {
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.editorInsertText: no mail editor\n");
        this.editor.insertText(plainText);
      }
    }
  },

  editorInsertAsQuotation: function(plainText) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.editorInsertAsQuotation\n");
    if (this.editor) {
      var mailEditor;
      try {
        mailEditor = this.editor.QueryInterface(Components.interfaces.nsIEditorMailSupport);
      }
      catch (ex) {}

      if (!mailEditor)
        return 0;

      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.editorInsertAsQuotation: mailEditor=" + mailEditor + "\n");

      mailEditor.insertAsQuotation(plainText);

      return 1;
    }
    return 0;
  },

  /**
   * Display a notification to the user at the bottom of the window
   *
   * @param priority: Number    - Priority of the message [1 = high (error) ... 3 = low (info)]
   * @param msgText: String     - Text to be displayed in notification bar
   * @param messageId: String   - Unique message type identification
   * @param detailsText: String - optional text to be displayed by clicking on "Details" button.
   *                              if null or "", then the Detail button will no be displayed.
   */
  notifyUser: function(priority, msgText, messageId, detailsText) {
    let notif = document.getElementById("attachmentNotificationBox");
    let prio;

    switch (priority) {
      case 1:
        prio = notif.PRIORITY_CRITICAL_MEDIUM;
        break;
      case 3:
        prio = notif.PRIORITY_INFO_MEDIUM;
        break;
      default:
        prio = notif.PRIORITY_WARNING_MEDIUM;
    }

    let buttonArr = [];

    if (detailsText && detailsText.length > 0) {
      buttonArr.push({
        accessKey: IcecryptLocale.getString("msgCompose.detailsButton.accessKey"),
        label: IcecryptLocale.getString("msgCompose.detailsButton.label"),
        callback: function(aNotificationBar, aButton) {
          IcecryptDialog.alert(window, detailsText);
        }
      });
    }
    notif.appendNotification(msgText, messageId, null, prio, buttonArr);
  },

  editorSelectAll: function() {
    if (this.editor) {
      this.editor.selectAll();
    }
  },

  editorGetCharset: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.editorGetCharset\n");
    return this.editor.documentCharacterSet;
  },

  editorGetContentAs: function(mimeType, flags) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.editorGetContentAs\n");
    if (this.editor) {
      return this.editor.outputToString(mimeType, flags);
    }
  },

  addrOnChangeTimer: null,

  addressOnChange: function(element) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.addressOnChange\n");
    if (!this.addrOnChangeTimer) {
      var self = this;
      this.addrOnChangeTimer = IcecryptTimer.setTimeout(function _f() {
        self.fireSendFlags();
        self.addrOnChangeTimer = null;
      }, 200);
    }
  },

  focusChange: function() {
    // call original TB function
    CommandUpdate_MsgCompose();

    var focusedWindow = top.document.commandDispatcher.focusedWindow;

    // we're just setting focus to where it was before
    if (focusedWindow == Icecrypt.msg.lastFocusedWindow) {
      // skip
      return;
    }

    Icecrypt.msg.lastFocusedWindow = focusedWindow;

    Icecrypt.msg.fireSendFlags();
  },

  fireSendFlags: function() {
    try {
      IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: Icecrypt.msg.fireSendFlags\n");
      if (!this.determineSendFlagId) {
        this.determineSendFlagId = IcecryptEvents.dispatchEvent(
          function _sendFlagWrapper() {
            Icecrypt.msg.determineSendFlags();
          },
          0);
      }
    }
    catch (ex) {}
  }
};


Icecrypt.composeStateListener = {
  NotifyComposeFieldsReady: function() {
    // Note: NotifyComposeFieldsReady is only called when a new window is created (i.e. not in case a window object is reused).
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: ECSL.NotifyComposeFieldsReady\n");

    try {
      Icecrypt.msg.editor = gMsgCompose.editor.QueryInterface(Components.interfaces.nsIEditor);
    }
    catch (ex) {}

    if (!Icecrypt.msg.editor)
      return;

    function enigDocStateListener() {}

    enigDocStateListener.prototype = {
      QueryInterface: function(iid) {
        if (!iid.equals(Components.interfaces.nsIDocumentStateListener) &&
          !iid.equals(Components.interfaces.nsISupports))
          throw Components.results.NS_ERROR_NO_INTERFACE;

        return this;
      },

      NotifyDocumentCreated: function() {
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: EDSL.NotifyDocumentCreated\n");
      },

      NotifyDocumentWillBeDestroyed: function() {
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: EDSL.enigDocStateListener.NotifyDocumentWillBeDestroyed\n");
      },

      NotifyDocumentStateChanged: function(nowDirty) {
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: EDSL.enigDocStateListener.NotifyDocumentStateChanged\n");
      }
    };

    var docStateListener = new enigDocStateListener();

    Icecrypt.msg.editor.addDocumentStateListener(docStateListener);
  },

  ComposeProcessDone: function(aResult) {
    // Note: called after a mail was sent (or saved)
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: ECSL.ComposeProcessDone: " + aResult + "\n");

    if (aResult != Components.results.NS_OK) {
      if (Icecrypt.msg.processed) {
        Icecrypt.msg.undoEncryption(4);
      }
      Icecrypt.msg.removeAttachedKey();
    }

    // ensure that securityInfo is set back to S/MIME flags (especially required if draft was saved)
    if (gSMFields) gMsgCompose.compFields.securityInfo = gSMFields;
  },

  NotifyComposeBodyReady: function() {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: ECSL.ComposeBodyReady\n");

    var isEmpty, isEditable;

    isEmpty = Icecrypt.msg.editor.documentIsEmpty;
    isEditable = Icecrypt.msg.editor.isDocumentEditable;
    Icecrypt.msg.composeBodyReady = true;

    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: EDSL.ComposeBodyReady: isEmpty=" + isEmpty + ", isEditable=" + isEditable + "\n");

    //FIXME
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: EDSL.ComposeBodyReady: disableSmime=" + Icecrypt.msg.disableSmime + "\n");

    if (Icecrypt.msg.disableSmime) {
      if (gMsgCompose && gMsgCompose.compFields && gMsgCompose.compFields.securityInfo) {
        let si = gMsgCompose.compFields.securityInfo.QueryInterface(Components.interfaces.nsIMsgSMIMECompFields);
        si.signMessage = false;
        si.requireEncryptMessage = false;
      }
      else {
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: EDSL.ComposeBodyReady: could not disable S/MIME\n");
      }
    }

    if (!isEditable || isEmpty)
      return;

    if (!Icecrypt.msg.timeoutId && !Icecrypt.msg.dirty) {
      Icecrypt.msg.timeoutId = IcecryptTimer.setTimeout(function() {
          Icecrypt.msg.decryptQuote(false);

        },
        0);
    }

  },

  SaveInFolderDone: function(folderURI) {
    //IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: ECSL.SaveInFolderDone\n");
  }
};


window.addEventListener("load",
  function _icecrypt_composeStartup(event) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: got load event\n");

    Icecrypt.msg.composeStartup(event);
  },
  false);

window.addEventListener("unload",
  function _icecrypt_composeUnload(event) {
    Icecrypt.msg.composeUnload(event);
  },
  false);

// Handle recycled windows
// TB < 47
window.addEventListener('compose-window-close',
  function _icecrypt_msgComposeClose(event) {
    Icecrypt.msg.msgComposeClose(event);
  },
  true);

// TB >= 48
window.addEventListener('compose-window-unload',
  function _icecrypt_msgComposeDestory(event) {
    Icecrypt.msg.msgComposeClose(event);
  },
  true);

// TB < 47 only
window.addEventListener('compose-window-reopen',
  function _icecrypt_msgComposeReopen(event) {
    Icecrypt.msg.msgComposeReopen(event);
  },
  true);

// Listen to message sending event
window.addEventListener('compose-send-message',
  function _icecrypt_sendMessageListener(event) {
    Icecrypt.msg.sendMessageListener(event);
  },
  true);

window.addEventListener('compose-window-init',
  function _icecrypt_composeWindowInit(event) {
    IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: _icecrypt_composeWindowInit\n");
    gMsgCompose.RegisterStateListener(Icecrypt.composeStateListener);
    Icecrypt.msg.composeBodyReady = false;
  },
  true);
