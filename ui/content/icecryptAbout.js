/*global Components: false, IcecryptApp: false, IcecryptWindows: false */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

// Uses: chrome://icecrypt/content/icecryptCommon.js
//       chrome://icecrypt/content/icecryptBuildDate.js


"use strict";

/* global IcecryptLog: false, IcecryptLocale: false, IcecryptCore: false, IcecryptGpgAgent: false */

/* global EnigBuildDate: false, EnigGetHttpUri: false, EnigOpenUrlExternally: false */

function enigAboutLoad() {
  IcecryptLog.DEBUG("icecryptAbout.js: enigAboutLoad\n");

  var contentFrame = IcecryptWindows.getFrame(window, "contentFrame");
  if (!contentFrame)
    return;

  var enigVersion = IcecryptApp.getVersion() + " (" + EnigBuildDate + ")";
  var versionElement = contentFrame.document.getElementById('version');
  if (versionElement)
    versionElement.firstChild.data = IcecryptLocale.getString("usingVersion", enigVersion);

  var icecryptSvc = IcecryptCore.getService();

  var agentStr;
  if (icecryptSvc) {
    agentStr = IcecryptLocale.getString("usingAgent", [IcecryptGpgAgent.agentType, IcecryptGpgAgent.agentPath.path]);
  }
  else {
    agentStr = IcecryptLocale.getString("agentError");

    if (icecryptSvc && icecryptSvc.initializationError)
      agentStr += "\n" + icecryptSvc.initializationError;
  }

  var agentElement = contentFrame.document.getElementById('agent');
  if (agentElement)
    agentElement.firstChild.data = agentStr;

}


function contentAreaClick(event) {
  let uri = EnigGetHttpUri(event);
  if (uri) {
    EnigOpenUrlExternally(uri);
    event.preventDefault();

    return false;
  }

  return true;
}
