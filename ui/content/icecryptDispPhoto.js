/*global Components: false */

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

Components.utils.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */
Components.utils.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Components.utils.import("resource://icecrypt/trust.jsm"); /*global IcecryptTrust: false */
Components.utils.import("resource://icecrypt/windows.jsm"); /*global IcecryptWindows: false */

var gRepaintCount = 0;
var gKeyId;

/* imports from icecryptCommon.js: */
/* global EnigGetTrustLabel: false */


function appendUid(uidStr) {
  let uidCont = document.getElementById("uidContainer");
  let l = document.createElement("label");
  l.setAttribute("value", uidStr);
  uidCont.appendChild(l);
}

function onLoad() {
  window.addEventListener("MozAfterPaint", resizeDlg, false);

  let key = IcecryptKeyRing.getKeyById(window.arguments[0].keyId);
  gKeyId = key.keyId;

  document.getElementById("photoImage").setAttribute("src", window.arguments[0].photoUri);
  for (let su of key.userIds) {
    if (su.type === "uid") {
      appendUid(su.userId);
    }
  }
  document.getElementById("keyId").setAttribute("value", IcecryptLocale.getString("keyId") + ": 0x" + gKeyId);
  document.getElementById("keyValidity").setAttribute("value", IcecryptTrust.getTrustLabel(key.keyTrust));
}

function resizeDlg(event) {
  ++gRepaintCount;
  window.sizeToContent();
  if (gRepaintCount > 3) {
    removeListener();
  }
}

function removeListener() {
  window.removeEventListener("MozAfterPaint", resizeDlg, false);
}

function displayKeyProps() {
  IcecryptWindows.openKeyDetails(window, gKeyId, false);
}
