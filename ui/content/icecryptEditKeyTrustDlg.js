/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* global Components: false */

"use strict";

Components.utils.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Components.utils.import("resource://icecrypt/keyEditor.jsm"); /*global IcecryptKeyEditor: false */
Components.utils.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Components.utils.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Components.utils.import("resource://icecrypt/dialog.jsm"); /*global IcecryptDialog: false */
Components.utils.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */

var gKeyList = [];

function onLoad() {
  // set current key trust if only one key is changed
  var icecryptSvc = IcecryptCore.getService(window);
  if (!icecryptSvc)
    return;

  var errorMsgObj = {};
  var exitCodeObj = {};

  try {
    window.arguments[1].refresh = false;
    var currTrust = -1;
    var lastTrust = -1;

    gKeyList = [];
    let k = window.arguments[0].keyId;

    for (let i in k) {
      let o = IcecryptKeyRing.getKeyById(k[i]);
      if (o) {
        gKeyList.push(o);
      }
    }

    if (gKeyList.length > 0) {
      for (let i = 0; i < gKeyList.length; i++) {
        currTrust = (("-nmfuq").indexOf(gKeyList[i].ownerTrust) % 5) + 1;
        if (lastTrust == -1) lastTrust = currTrust;
        if (currTrust != lastTrust) {
          currTrust = -1;
          break;
        }
      }
    }
    if (currTrust > 0) {
      var t = document.getElementById("trustLevel" + currTrust.toString());
      document.getElementById("trustLevelGroup").selectedItem = t;
    }
  }
  catch (ex) {}

  var keyIdList = document.getElementById("keyIdList");

  for (let i = 0; i < gKeyList.length; i++) {
    var keyId = gKeyList[i].userId + " - 0x" + gKeyList[i].keyId.substr(-8, 8);
    keyIdList.appendItem(keyId);
  }
}

function processNextKey(index) {
  IcecryptLog.DEBUG("icecryptEditKeyTrust: processNextKey(" + index + ")\n");

  var t = document.getElementById("trustLevelGroup");

  IcecryptKeyEditor.setKeyTrust(window,
    gKeyList[index].keyId,
    Number(t.selectedItem.value),
    function(exitCode, errorMsg) {
      if (exitCode !== 0) {
        IcecryptDialog.alert(window, IcecryptLocale.getString("setKeyTrustFailed") + "\n\n" + errorMsg);
        window.close();
        return;
      }
      else {
        window.arguments[1].refresh = true;
      }

      ++index;
      if (index >= gKeyList.length)
        window.close();
      else {
        processNextKey(index);
      }
    });
}

function onAccept() {
  processNextKey(0);

  return false;
}
