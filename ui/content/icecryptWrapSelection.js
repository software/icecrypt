/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

// Uses: chrome://icecrypt/content/icecryptCommon.js

/* global IcecryptLog: false */


"use strict";

function onLoad() {
  IcecryptLog.DEBUG("icecryptwrapSelection.js: onLoad\n");
  window.arguments[0].cancelled = true;
  window.arguments[0].Select = "";
}

function onAccept() {
  IcecryptLog.DEBUG("icecryptwrapSelection.js: onAccept\n");
  let wrapSelect = document.getElementById("wrapSelectGroup");
  IcecryptLog.DEBUG("icecryptwrapSelection.js: onAccept, selected value='" + wrapSelect.value + "'\n");
  if (wrapSelect.value !== "") {
    window.arguments[0].Select = wrapSelect.value;
    window.arguments[0].cancelled = false;
    IcecryptLog.DEBUG("icecryptwrapSelection.js: onAccept, setting return value, disable cancel\n");
  }
  else {
    IcecryptLog.DEBUG("icecryptwrapSelection.js: onAccept, enable cancel\n");
    window.arguments[0].cancelled = true;
  }
}

function onCancel() {
  IcecryptLog.DEBUG("icecryptwrapSelection.js: onCancel, enable cancel\n");
  window.arguments[0].cancelled = true;
}
