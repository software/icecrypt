/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

/* global Components: false */

Components.utils.import("resource://icecrypt/funcs.jsm"); /* global IcecryptFuncs: false */
Components.utils.import("resource://icecrypt/keyEditor.jsm"); /* global IcecryptKeyEditor: false */
Components.utils.import("resource://icecrypt/locale.jsm"); /* global IcecryptLocale: false */
Components.utils.import("resource://icecrypt/data.jsm"); /* global IcecryptData: false */
Components.utils.import("resource://icecrypt/dialog.jsm"); /* global IcecryptDialog: false */
Components.utils.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */
Components.utils.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Components.utils.import("resource://icecrypt/windows.jsm"); /*global IcecryptWindows: false */

var gUserId;
var gIcecryptUid;

function onLoad() {
  window.arguments[1].refresh = false;
  reloadUidList();
  var keyId = gUserId + " - 0x" + window.arguments[0].keyId.substr(-8, 8);
  document.getElementById("keyId").value = keyId;

  if (!window.arguments[0].ownKey) {
    document.getElementById("addUid").setAttribute("disabled", "true");
    document.getElementById("setPrimary").setAttribute("disabled", "true");
    document.getElementById("revokeUid").setAttribute("disabled", "true");
  }
}

function appendUid(uidList, uidObj, uidNum) {
  let uidTxt;
  let uidType = uidObj.type;
  if (uidType === "uat") {
    if (uidObj.userId.indexOf("1 ") === 0) {
      uidTxt = IcecryptLocale.getString("userAtt.photo");
    }
    else return;
  }
  else {
    uidTxt = uidObj.userId;
    if (!gIcecryptUid) {
      gIcecryptUid = uidTxt;
    }
  }

  if (uidObj.keyTrust === "r") {
    uidTxt += " - " + IcecryptLocale.getString("keyValid.revoked");
    uidType = uidType.replace(/^./, "r");
  }
  let item = uidList.appendItem(uidTxt, uidType + ":" + String(uidNum));
  if (uidObj.keyTrust == "r") {
    item.setAttribute("class", "icecryptUidInactive");
  }
}

function reloadUidList() {
  var uidList = document.getElementById("uidList");
  while (uidList.getRowCount() > 0) {
    uidList.removeItemAt(0);
  }

  var icecryptSvc = IcecryptCore.getService();
  if (!icecryptSvc)
    return;

  var keyObj = IcecryptKeyRing.getKeyById(window.arguments[0].keyId);
  if (keyObj) {
    gUserId = keyObj.userId;

    for (var i = 0; i < keyObj.userIds.length; i++) {
      appendUid(uidList, keyObj.userIds[i], i + 1);
    }
  }

  uidSelectCb();
}

function handleDblClick() {
  var uidList = document.getElementById("uidList");
  if (uidList.selectedCount > 0) {
    var selValue = uidList.selectedItem.value;
    var uidType = selValue.substr(0, 3);
    if (uidType == "uat" || uidType == "rat") {
      IcecryptWindows.showPhoto(window, window.arguments[0].keyId, gIcecryptUid);
    }
  }
}

function uidSelectCb() {
  var uidList = document.getElementById("uidList");
  var selValue;

  if (uidList.selectedCount > 0) {
    selValue = uidList.selectedItem.value;
  }
  else {
    selValue = "uid:1";
  }
  if (window.arguments[0].ownKey) {
    var uidType = selValue.substr(0, 3);
    if (uidType == "uat" || uidType == "rat" || uidType == "rid" || selValue.substr(4) == "1") {
      document.getElementById("setPrimary").setAttribute("disabled", "true");
    }
    else {
      document.getElementById("setPrimary").removeAttribute("disabled");
    }
    if (selValue.substr(4) == "1") {
      document.getElementById("deleteUid").setAttribute("disabled", "true");
      document.getElementById("revokeUid").setAttribute("disabled", "true");
    }
    else {
      document.getElementById("deleteUid").removeAttribute("disabled");
      if (uidType == "rid" || uidType == "rat") {
        document.getElementById("revokeUid").setAttribute("disabled", "true");
      }
      else {
        document.getElementById("revokeUid").removeAttribute("disabled");
      }
    }
  }
  else {
    if (selValue.substr(4) == "1") {
      document.getElementById("deleteUid").setAttribute("disabled", "true");
    }
    else {
      document.getElementById("deleteUid").removeAttribute("disabled");
    }
  }
}

function addUid() {
  var inputObj = {
    keyId: "0x" + window.arguments[0].keyId,
    userId: gIcecryptUid
  };
  var resultObj = {
    refresh: false
  };
  window.openDialog("chrome://icecrypt/content/icecryptAddUidDlg.xul",
    "", "dialog,modal,centerscreen", inputObj, resultObj);
  window.arguments[1].refresh = resultObj.refresh;
  reloadUidList();
}

function setPrimaryUid() {
  var icecryptSvc = IcecryptCore.getService();
  if (!icecryptSvc)
    return;

  var errorMsgObj = {};
  var uidList = document.getElementById("uidList");
  if (uidList.selectedItem.value.substr(0, 3) == "uid") {

    IcecryptKeyEditor.setPrimaryUid(window,
      "0x" + window.arguments[0].keyId,
      uidList.selectedItem.value.substr(4),
      function _cb(exitCode, errorMsg) {
        if (exitCode === 0) {
          IcecryptDialog.alert(window, IcecryptLocale.getString("changePrimUidOK"));
          window.arguments[1].refresh = true;
          reloadUidList();
        }
        else
          IcecryptDialog.alert(window, IcecryptLocale.getString("changePrimUidFailed") + "\n\n" + errorMsg);
      });
  }
}

function revokeUid() {
  var icecryptSvc = IcecryptCore.getService();
  if (!icecryptSvc)
    return;
  var uidList = document.getElementById("uidList");
  if (!IcecryptDialog.confirmDlg(window, IcecryptLocale.getString("revokeUidQuestion", uidList.selectedItem.label))) return;
  if (uidList.selectedItem.value.substr(4) != "1") {
    IcecryptKeyEditor.revokeUid(window,
      "0x" + window.arguments[0].keyId,
      uidList.selectedItem.value.substr(4),
      function _cb(exitCode, errorMsg) {
        if (exitCode === 0) {
          IcecryptDialog.alert(window, IcecryptLocale.getString("revokeUidOK", uidList.selectedItem.label));
          window.arguments[1].refresh = true;
          reloadUidList();
        }
        else
          IcecryptDialog.alert(window, IcecryptLocale.getString("revokeUidFailed", uidList.selectedItem.label) + "\n\n" + errorMsg);
      });
  }
}

function deleteUid() {
  var icecryptSvc = IcecryptCore.getService();
  if (!icecryptSvc)
    return;
  var uidList = document.getElementById("uidList");
  if (!IcecryptDialog.confirmDlg(window, IcecryptLocale.getString("deleteUidQuestion", uidList.selectedItem.label))) return;
  if (uidList.selectedItem.value.substr(4) != "1") {
    IcecryptKeyEditor.deleteUid(window,
      "0x" + window.arguments[0].keyId,
      uidList.selectedItem.value.substr(4),
      function _cb(exitCode, errorMsg) {
        if (exitCode === 0) {
          IcecryptDialog.alert(window, IcecryptLocale.getString("deleteUidOK", uidList.selectedItem.label));
          window.arguments[1].refresh = true;
          reloadUidList();
        }
        else
          IcecryptDialog.alert(window, IcecryptLocale.getString("deleteUidFailed", uidList.selectedItem.label) + "\n\n" + errorMsg);
      });
  }
}
