/*global Components: false */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

/**
 * helper functions for message composition
 */

Components.utils.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Components.utils.import("resource://icecrypt/funcs.jsm"); /*global IcecryptFuncs: false */
Components.utils.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Components.utils.import("resource://icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */
Components.utils.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Components.utils.import("resource://icecrypt/dialog.jsm"); /*global IcecryptDialog: false */
Components.utils.import("resource://icecrypt/gpg.jsm"); /*global IcecryptGpg: false */
Components.utils.import("resource://icecrypt/trust.jsm"); /*global IcecryptTrust: false */
Components.utils.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */
Components.utils.import("resource://icecrypt/constants.jsm"); /*global IcecryptConstants: false */

if (!Icecrypt) var Icecrypt = {};

Icecrypt.hlp = {

  /* try to find valid key to passed email addresses (or keys)
   * @return: list of all found key (with leading "0x") or null
   *          details in details parameter
   */
  validKeysForAllRecipients: function(emailsOrKeys, details) {
    IcecryptLog.DEBUG("=====> validKeysForAllRecipients()\n");
    IcecryptLog.DEBUG("icecryptMsgComposeHelper.js: validKeysForAllRecipients(): emailsOrKeys='" + emailsOrKeys + "'\n");

    // use helper to see when we enter and leave this function
    let resultingArray = this.doValidKeysForAllRecipients(emailsOrKeys, details);

    IcecryptLog.DEBUG("icecryptMsgComposeHelper.js: validKeysForAllRecipients(): return '" + resultingArray + "'\n");
    IcecryptLog.DEBUG("  <=== validKeysForAllRecipients()\n");
    return resultingArray;
  },


  // helper for validKeysForAllRecipients()
  doValidKeysForAllRecipients: function(emailsOrKeys, details) {
    IcecryptLog.DEBUG("icecryptMsgComposeHelper.js: doValidKeysForAllRecipients(): emailsOrKeys='" + emailsOrKeys + "'\n");

    // check which keys are accepted
    let minTrustLevel;
    let acceptedKeys = IcecryptPrefs.getPref("acceptedKeys");
    switch (acceptedKeys) {
      case 0: // accept valid/authenticated keys only
        minTrustLevel = "f"; // first value for trusted keys
        break;
      case 1: // accept all but revoked/disabled/expired keys
        minTrustLevel = "?"; // value between invalid and unknown keys
        break;
      default:
        IcecryptLog.DEBUG("icecryptMsgComposeOverlay.js: doValidKeysForAllRecipients(): return null (INVALID VALUE for acceptedKeys: \"" + acceptedKeys + "\")\n");
        return null;
    }

    IcecryptLog.DEBUG("icecryptMsgComposeHelper.js: doValidKeysForAllRecipients(): find keys with minTrustLevel=\"" + minTrustLevel + "\"\n");

    let keyMissing;
    let resultingArray = []; // resulting key list (if all valid)
    try {
      // create array of address elements (email or key)
      let addresses = IcecryptFuncs.stripEmail(emailsOrKeys).split(',');

      // resolve GnuPG groups
      let gpgGroups = IcecryptGpg.getGpgGroups();
      for (let i = 0; i < addresses.length; i++) {
        let addr = addresses[i].toLowerCase();
        for (let j = 0; j < gpgGroups.length; j++) {
          if (addr == gpgGroups[j].alias.toLowerCase() ||
            "<" + addr + ">" == gpgGroups[j].alias.toLowerCase()) {
            // replace address with keylist
            let grpList = gpgGroups[j].keylist.split(/;/);
            addresses[i] = grpList[0];
            for (let k = 1; k < grpList.length; k++) {
              addresses.push(grpList[k]);
            }
          }
        }
      }

      // resolve all the email addresses if possible:
      keyMissing = IcecryptKeyRing.getValidKeysForAllRecipients(addresses, minTrustLevel, details, resultingArray);
    }
    catch (ex) {
      IcecryptLog.DEBUG("icecryptMsgComposeHelper.js: doValidKeysForAllRecipients(): return null (exception: " + ex.description + ")\n");
      return null;
    }
    if (keyMissing) {
      IcecryptLog.DEBUG("icecryptMsgComposeHelper.js: doValidKeysForAllRecipients(): return null (key missing)\n");
      return null;
    }
    IcecryptLog.DEBUG("icecryptMsgComposeHelper.js: doValidKeysForAllRecipients(): return \"" + resultingArray + "\"\n");
    return resultingArray;
  },


  /**
   * processConflicts
   * - handle sign/encrypt/pgpMime conflicts if any
   * - NOTE: conflicts result into disabling the feature (0/never)
   * Input parameters:
   *  @encrypt: email would currently get encrypted
   *  @sign:    email would currently get signed
   * @return:  false if error occurred or processing was canceled
   */
  processConflicts: function(encrypt, sign) {
    // process message about whether we still sign/encrypt
    let msg = "";
    msg += "\n- " + IcecryptLocale.getString(encrypt ? "encryptYes" : "encryptNo");
    msg += "\n- " + IcecryptLocale.getString(sign ? "signYes" : "signNo");
    if (IcecryptPrefs.getPref("warnOnRulesConflict") == 2) {
      IcecryptPrefs.setPref("warnOnRulesConflict", 0);
    }
    if (!IcecryptDialog.confirmPref(window, IcecryptLocale.getString("rulesConflict", [msg]), "warnOnRulesConflict")) {
      return false;
    }
    return true;
  },


  /**
   * determine invalid recipients as returned from GnuPG
   *
   * @gpgMsg: output from GnuPG
   *
   * @return: space separated list of invalid addresses
   */
  getInvalidAddress: function(gpgMsg) {
    IcecryptLog.DEBUG("icecryptMsgComposeHelper.js: getInvalidAddress(): gpgMsg=\"" + gpgMsg + "\"\n\n");
    var invalidAddr = [];
    var lines = gpgMsg.split(/[\n\r]+/);
    for (var i = 0; i < lines.length; i++) {
      var m = lines[i].match(/^(INV_RECP \d+ )(.*)$/);
      if (m && m.length == 3) {
        invalidAddr.push(IcecryptFuncs.stripEmail(m[2].toLowerCase()));
      }
    }
    return invalidAddr.join(" ");
  }

};
