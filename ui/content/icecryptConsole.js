/*global Components: false */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

Components.utils.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false*/
Components.utils.import("resource://icecrypt/pipeConsole.jsm"); /*global IcecryptConsole: false */
Components.utils.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Components.utils.import("resource://icecrypt/data.jsm"); /*global IcecryptData: false */
Components.utils.import("resource://icecrypt/windows.jsm"); /*global IcecryptWindows: false */

/* global goUpdateCommand: false */

var gConsoleIntervalId;

function consoleLoad() {
  IcecryptLog.DEBUG("icecryptConsole.js: consoleLoad\n");

  top.controllers.insertControllerAt(0, CommandController);

  IcecryptCore.getService(window);

  // Refresh console every 2 seconds
  gConsoleIntervalId = window.setInterval(refreshConsole, 2000);
  updateData();
}

function consoleUnload() {
  IcecryptLog.DEBUG("icecryptConsole.js: consoleUnload\n");

  // Cancel console refresh
  if (window.consoleIntervalId) {
    window.clearInterval(gConsoleIntervalId);
    gConsoleIntervalId = null;
  }
}

function refreshConsole() {
  //IcecryptLog.DEBUG("icecryptConsole.js: refreshConsole():\n");

  if (IcecryptConsole.hasNewData()) {
    IcecryptLog.DEBUG("icecryptConsole.js: refreshConsole(): hasNewData\n");

    updateData();
  }

  return false;
}

function updateData() {
  //IcecryptLog.DEBUG("icecryptConsole.js: updateData():\n");

  var contentFrame = IcecryptWindows.getFrame(window, "contentFrame");
  if (!contentFrame)
    return;

  var consoleElement = contentFrame.document.getElementById('console');

  consoleElement.firstChild.data = IcecryptData.convertToUnicode(IcecryptConsole.getData(), "utf-8");

  if (!contentFrame.mouseDownState)
    contentFrame.scrollTo(0, 9999);
}


function icecryptConsoleCopy() {
  var selText = getSelectionStr();

  IcecryptLog.DEBUG("icecryptConsole.js: icecryptConsoleCopy: selText='" + selText + "'\n");

  if (selText) {
    var clipHelper = Components.classes["@mozilla.org/widget/clipboardhelper;1"].createInstance(Components.interfaces.nsIClipboardHelper);

    clipHelper.copyString(selText);
  }

  return true;
}

function getSelectionStr() {
  try {
    var contentFrame = IcecryptWindows.getFrame(window, "contentFrame");

    var sel = contentFrame.getSelection();
    return sel.toString();

  }
  catch (ex) {
    return "";
  }
}

function isItemSelected() {
  IcecryptLog.DEBUG("icecryptConsole.js: isItemSelected\n");
  return getSelectionStr() !== "";
}

function UpdateCopyMenu() {
  IcecryptLog.DEBUG("icecryptConsole.js: UpdateCopyMenu\n");
  goUpdateCommand("cmd_copy");
}

var CommandController = {
  isCommandEnabled: function(aCommand) {
    switch (aCommand) {
      case "cmd_copy":
        return isItemSelected();
      default:
        return false;
    }
  },

  supportsCommand: function(aCommand) {
    switch (aCommand) {
      case "cmd_copy":
        return true;
      default:
        return false;
    }
  },

  doCommand: function(aCommand) {
    switch (aCommand) {
      case "cmd_copy":
        icecryptConsoleCopy();
        break;
      default:
        break;
    }
  },

  onEvent: function(aEvent) {}
};
