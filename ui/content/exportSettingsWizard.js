/*global Components: false, document: false, window: false */
/*jshint -W097 */
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */


"use strict";

const Cc = Components.classes;
const Ci = Components.interfaces;
const Cu = Components.utils;

Cu.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Cu.import("resource://icecrypt/dialog.jsm"); /*global IcecryptDialog: false */
Cu.import("resource://icecrypt/files.jsm"); /*global IcecryptFiles: false */
Cu.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Cu.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */
Cu.import("resource://icecrypt/configBackup.jsm"); /*global IcecryptConfigBackup: false */
Cu.import("resource://icecrypt/gpgAgent.jsm"); /*global IcecryptGpgAgent: false */
Cu.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Cu.import("resource://icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */

var osUtils = {};
Components.utils.import("resource://gre/modules/FileUtils.jsm", osUtils);

var gWorkFile = {
  file: null
};

function getWizard() {
  return document.getElementById("overallWizard");
}

function enableNext(status) {
  let wizard = getWizard();
  wizard.canAdvance = status;
}


function onCancel() {
  return true;
}

function browseExportFile(referencedId) {
  var filePath = IcecryptDialog.filePicker(window, IcecryptLocale.getString("specifyExportFile"),
    "", true, "*.zip", IcecryptLocale.getString("defaultBackupFileName") + ".zip", [IcecryptLocale.getString("icecryptSettings"), "*.zip"]);

  if (filePath) {

    if (filePath.exists()) filePath.normalize();

    if ((filePath.exists() && !filePath.isDirectory() && filePath.isWritable()) ||
      (!filePath.exists() && filePath.parent.isWritable())) {
      document.getElementById(referencedId).value = filePath.path;
      gWorkFile.file = filePath;
    }
    else {
      IcecryptDialog.alert(window, IcecryptLocale.getString("cannotWriteToFile", filePath.path));
    }
  }

  enableNext(gWorkFile.file !== null);
}

function doExport(tmpDir) {

  let exitCodeObj = {},
    errorMsgObj = {};

  let keyRingFile = tmpDir.clone();
  keyRingFile.append("keyring.asc");

  IcecryptLog.DEBUG("importExportWizard: doExport - temp file: " + keyRingFile.path + "\n");

  IcecryptKeyRing.extractKey(true, null, keyRingFile, exitCodeObj, errorMsgObj);
  if (exitCodeObj.value !== 0) {
    IcecryptDialog.alert(window, IcecryptLocale.getString("dataExportError"));
    return false;
  }

  let otFile = tmpDir.clone();
  otFile.append("ownertrust.txt");
  IcecryptKeyRing.extractOwnerTrust(otFile, exitCodeObj, errorMsgObj);
  if (exitCodeObj.value !== 0) {
    IcecryptDialog.alert(window, IcecryptLocale.getString("dataExportError"));
    return false;
  }

  let prefsFile = tmpDir.clone();
  prefsFile.append("prefs.json");
  if (IcecryptConfigBackup.backupPrefs(prefsFile) !== 0) {
    IcecryptDialog.alert(window, IcecryptLocale.getString("dataExportError"));
    return false;
  }

  let homeDir = IcecryptGpgAgent.getGpgHomeDir();
  let gpgConfgFile = null;
  let zipW = IcecryptFiles.createZipFile(gWorkFile.file);

  zipW.addEntryFile("keyring.asc", Ci.nsIZipWriter.COMPRESSION_DEFAULT, keyRingFile, false);
  zipW.addEntryFile("ownertrust.txt", Ci.nsIZipWriter.COMPRESSION_DEFAULT, otFile, false);
  zipW.addEntryFile("prefs.json", Ci.nsIZipWriter.COMPRESSION_DEFAULT, prefsFile, false);

  if (homeDir) {
    gpgConfgFile = new osUtils.FileUtils.File(homeDir);
    gpgConfgFile.append("gpg.conf");
  }

  if (gpgConfgFile && gpgConfgFile.exists()) {
    zipW.addEntryFile("gpg.conf", Ci.nsIZipWriter.COMPRESSION_DEFAULT, gpgConfgFile, false);
  }
  zipW.close();

  tmpDir.remove(true);
  document.getElementById("doneMessage").removeAttribute("hidden");

  return true;
}

function exportFailed() {
  let wizard = getWizard();
  wizard.getButton("cancel").removeAttribute("disabled");
  wizard.canRewind = true;
  document.getElementById("errorMessage").removeAttribute("hidden");

  return false;
}

function startExport() {
  IcecryptLog.DEBUG("importExportWizard: doExport\n");
  document.getElementById("errorMessage").setAttribute("hidden", "true");

  let wizard = getWizard();
  wizard.canAdvance = false;
  wizard.canRewind = false;
  wizard.getButton("finish").setAttribute("disabled", "true");

  let svc = IcecryptCore.getService();
  if (!svc) return exportFailed();

  if (!gWorkFile.file) return exportFailed();

  let tmpDir = IcecryptFiles.createTempSubDir("enig-exp", true);

  wizard.getButton("cancel").setAttribute("disabled", "true");
  document.getElementById("spinningWheel").removeAttribute("hidden");

  let retVal = false;

  try {
    retVal = doExport(tmpDir);
  }
  catch (ex) {}

  // stop spinning the wheel
  document.getElementById("spinningWheel").setAttribute("hidden", "true");

  if (retVal) {
    wizard.getButton("finish").removeAttribute("disabled");
    wizard.canAdvance = true;
  }
  else {
    exportFailed();
  }

  return retVal;
}

function checkAdditionalParam() {
  let param = IcecryptPrefs.getPref("agentAdditionalParam");

  if (param) {
    if (param.search(/--(homedir|trustdb-name|options)/) >= 0 || param.search(/--(primary-|secret-)?keyring/) >= 0) {
      IcecryptDialog.alert(null, IcecryptLocale.getString("homedirParamNotSUpported"));
      return false;
    }
  }
  return true;
}

function onLoad() {
  enableNext(false);

  if (!checkAdditionalParam()) {
    window.close();
  }
}
