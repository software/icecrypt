/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

"use strict";

/*global Components: false */

Components.utils.import("resource://icecrypt/log.jsm"); /* global IcecryptLog: false */


window.addEventListener("load", function _icecrypt_msgPrintLoad() {
    IcecryptLog.DEBUG("icecryptMsgPrintOverlay.js: enigMsgPrintLoad\n");

    // functionality to be added ...
  },
  false);

window.addEventListener("unload", function _icecrypt_msgPrintUnload() {
    IcecryptLog.DEBUG("icecryptMsgPrintOverlay.js: enigMsgPrintUnload\n");

    // functionality to be added ...
  },
  false);
