/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* global Components: false */


"use strict";
Components.utils.import("resource://icecrypt/core.jsm"); /* global IcecryptCore: false */
Components.utils.import("resource://icecrypt/keyEditor.jsm"); /* global IcecryptKeyEditor: false */
Components.utils.import("resource://icecrypt/locale.jsm"); /* global IcecryptLocale: false */
Components.utils.import("resource://icecrypt/data.jsm"); /* global IcecryptData: false */
Components.utils.import("resource://icecrypt/dialog.jsm"); /* global IcecryptDialog: false */

function onAccept() {
  var name = document.getElementById("addUid_name");
  var email = document.getElementById("addUid_email");

  if ((email.value.search(/^ *$/) === 0) || (name.value.search(/^ *$/) === 0)) {
    IcecryptDialog.alert(window, IcecryptLocale.getString("addUidDlg.nameOrEmailError"));
    return false;
  }
  if (name.value.replace(/ *$/, "").length < 5) {
    IcecryptDialog.alert(window, IcecryptLocale.getString("addUidDlg.nameMinLengthError"));
    return false;
  }
  if (email.value.search(/.@./) < 0) {
    IcecryptDialog.alert(window, IcecryptLocale.getString("addUidDlg.invalidEmailError"));
    return false;
  }

  var icecryptSvc = IcecryptCore.getService();
  if (!icecryptSvc) {
    IcecryptDialog.alert(window, IcecryptLocale.getString("accessError"));
    return true;
  }

  IcecryptKeyEditor.addUid(window,
    window.arguments[0].keyId,
    IcecryptData.convertFromUnicode(name.value),
    IcecryptData.convertFromUnicode(email.value),
    "", // user id comment
    function _addUidCb(exitCode, errorMsg) {
      if (exitCode !== 0) {
        IcecryptDialog.alert(window, IcecryptLocale.getString("addUidFailed") + "\n\n" + errorMsg);
      }
      else {
        window.arguments[1].refresh = true;
        IcecryptDialog.alert(window, IcecryptLocale.getString("addUidOK"));
      }
      window.close();
    });

  return false;
}
