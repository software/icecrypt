/*global Components: false */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";


Components.utils.import("resource://icecrypt/funcs.jsm"); /*global IcecryptFuncs: false */
Components.utils.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Components.utils.import("resource://icecrypt/keyEditor.jsm"); /*global IcecryptKeyEditor: false */
Components.utils.import("resource://icecrypt/key.jsm"); /*global IcecryptKey: false */
Components.utils.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */
Components.utils.import("resource://icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */
Components.utils.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Components.utils.import("resource://icecrypt/data.jsm"); /*global IcecryptData: false */
Components.utils.import("resource://icecrypt/dialog.jsm"); /*global IcecryptDialog: false */
Components.utils.import("resource://icecrypt/time.jsm"); /*global IcecryptTime: false */
Components.utils.import("resource://icecrypt/events.jsm"); /*global IcecryptEvents: false */
Components.utils.import("resource://icecrypt/card.jsm"); /*global IcecryptCard: false */

var gCardData = {};

function onLoad() {
  var icecryptSvc = IcecryptCore.getService(window);
  if (!icecryptSvc) {
    IcecryptEvents.dispatchEvent(failWithError, 0, IcecryptLocale.getString("accessError"));
    return;
  }
  var exitCodeObj = {};
  var errorMsgObj = {};

  var dryRun = false;
  try {
    dryRun = IcecryptPrefs.getPref("dryRun");
  }
  catch (ex) {}

  var cardStr = IcecryptCard.getCardStatus(exitCodeObj, errorMsgObj);
  if (exitCodeObj.value === 0) {
    var statusList = cardStr.split(/[\r\n]+/);
    for (var i = 0; i < statusList.length; i++) {
      var l = statusList[i].split(/:/);
      switch (l[0]) {
        case "name":
          setValue("firstname", IcecryptData.convertGpgToUnicode(l[1]));
          setValue(l[0], IcecryptData.convertGpgToUnicode(l[2]));
          break;
        case "vendor":
          setValue(l[0], IcecryptData.convertGpgToUnicode(l[2].replace(/\\x3a/ig, ":")));
          break;
        case "sex":
        case "forcepin":
          var selItem = document.getElementById("card_" + l[0] + "_" + l[1]);
          document.getElementById("card_" + l[0]).selectedItem = selItem;
          gCardData[l[0]] = l[1];
          break;
        case "pinretry":
        case "maxpinlen":
          setValue(l[0], l[1] + " / " + l[2] + " / " + l[3]);
          break;
        case "fpr":
          setValue("key_fpr_1", IcecryptKey.formatFpr(l[1]));
          setValue("key_fpr_2", IcecryptKey.formatFpr(l[2]));
          setValue("key_fpr_3", IcecryptKey.formatFpr(l[3]));
          break;
        case "fprtime":
          setValue("key_created_1", IcecryptTime.getDateTime(l[1], true, false));
          setValue("key_created_2", IcecryptTime.getDateTime(l[2], true, false));
          setValue("key_created_3", IcecryptTime.getDateTime(l[3], true, false));
          break;
        default:
          if (l[0]) {
            setValue(l[0], IcecryptData.convertGpgToUnicode(l[1].replace(/\\x3a/ig, ":")));
          }
      }
    }
  }
  else {
    if (!dryRun) {
      IcecryptEvents.dispatchEvent(failWithError, 0, errorMsgObj.value);
    }
  }
  return;
}

function failWithError(errorMsg) {
  IcecryptDialog.alert(window, errorMsg);
  window.close();
}


function setValue(attrib, value) {
  var elem = document.getElementById("card_" + attrib);
  if (elem) {
    elem.value = value;
  }
  gCardData[attrib] = value;
}

function getValue(attrib) {
  var elem = document.getElementById("card_" + attrib);
  if (elem) {
    return elem.value;
  }
  else {
    return "";
  }
}

function getSelection(attrib) {
  var elem = document.getElementById("card_" + attrib);
  if (elem) {
    return elem.selectedItem.value;
  }
  else {
    return "";
  }
}

function doEditData() {
  document.getElementById("bcEditMode").removeAttribute("readonly");
  document.getElementById("bcEnableMode").removeAttribute("disabled");
}

function doReset() {
  document.getElementById("bcEditMode").setAttribute("readonly", "true");
  document.getElementById("bcEnableMode").setAttribute("disabled", "true");
  onLoad();
}

function doSaveChanges() {
  document.getElementById("bcEditMode").setAttribute("readonly", "true");
  document.getElementById("bcEnableMode").setAttribute("disabled", "true");

  var icecryptSvc = IcecryptCore.getService(window);
  if (!icecryptSvc) {
    IcecryptDialog.alert(window, IcecryptLocale.getString("accessError"));
    window.close();
    return;
  }

  var forcepin = (getSelection("forcepin") == gCardData.forcepin ? 0 : 1);
  var dialogname = getValue("name");
  var dialogfirstname = getValue("firstname");
  if ((dialogname.search(/^[A-Za-z0-9.,?_ -]*$/) !== 0) || (dialogfirstname.search(/^[A-Za-z0-9.,?_ -]*$/) !== 0)) {
    IcecryptDialog.alert(window, IcecryptLocale.getString("Carddetails.NoASCII"));
    onLoad();
    doEditData();
  }
  else {
    IcecryptKeyEditor.cardAdminData(window,
      IcecryptData.convertFromUnicode(dialogname),
      IcecryptData.convertFromUnicode(dialogfirstname),
      getValue("lang"),
      getSelection("sex"),
      IcecryptData.convertFromUnicode(getValue("url")),
      getValue("login"),
      forcepin,
      function _cardAdminCb(exitCode, errorMsg) {
        if (exitCode !== 0) {
          IcecryptDialog.alert(window, errorMsg);
        }

        onLoad();
      });
  }
}

function engmailGenerateCardKey() {
  window.openDialog("chrome://icecrypt/content/icecryptGenCardKey.xul",
    "", "dialog,modal,centerscreen");

  IcecryptKeyRing.clearCache();
  onLoad();
}

function icecryptAdminPin() {
  window.openDialog("chrome://icecrypt/content/icecryptSetCardPin.xul",
    "", "dialog,modal,centerscreen");
  onLoad();
}
