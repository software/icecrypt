/*global Components: false */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

/* global IcecryptData: false, IcecryptApp: false, IcecryptDialog: false, IcecryptTimer: false, IcecryptWindows: false, IcecryptTime: false */
/* global IcecryptLocale: false, IcecryptLog: false, XPCOMUtils: false, IcecryptPrefs: false */

/* globals from Thunderbird: */
/* global ReloadMessage: false, gDBView: false, gSignatureStatus: false, gEncryptionStatus: false, showMessageReadSecurityInfo: false */
/* global gFolderDisplay: false, messenger: false, currentAttachments: false, msgWindow: false, ChangeMailLayout: false, MsgToggleMessagePane: false */
/* global currentHeaderData: false, gViewAllHeaders: false, gExpandedHeaderList: false, goDoCommand: false, HandleSelectedAttachments: false */
/* global statusFeedback: false */

Components.utils.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Components.utils.import("resource://icecrypt/funcs.jsm"); /* global IcecryptFuncs: false */
Components.utils.import("resource://icecrypt/mimeVerify.jsm"); /* global IcecryptVerify: false */
Components.utils.import("resource://icecrypt/fixExchangeMsg.jsm"); /* global IcecryptFixExchangeMsg: false */
Components.utils.import("resource://icecrypt/log.jsm");
Components.utils.import("resource://icecrypt/prefs.jsm");
Components.utils.import("resource://icecrypt/os.jsm"); /* global IcecryptOS: false */
Components.utils.import("resource://icecrypt/locale.jsm");
Components.utils.import("resource://icecrypt/files.jsm"); /* global IcecryptFiles: false */
Components.utils.import("resource://icecrypt/key.jsm"); /* global IcecryptKey: false */
Components.utils.import("resource://icecrypt/data.jsm");
Components.utils.import("resource://icecrypt/app.jsm");
Components.utils.import("resource://icecrypt/dialog.jsm");
Components.utils.import("resource://icecrypt/timer.jsm");
Components.utils.import("resource://icecrypt/windows.jsm");
Components.utils.import("resource://icecrypt/time.jsm");
Components.utils.import("resource://icecrypt/decryptPermanently.jsm"); /* global IcecryptDecryptPermanently: false */
Components.utils.import("resource://icecrypt/streams.jsm"); /*global IcecryptStreams: false */
Components.utils.import("resource://icecrypt/events.jsm"); /*global IcecryptEvents: false */
Components.utils.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */
Components.utils.import("resource://icecrypt/attachment.jsm"); /*global IcecryptAttachment: false */
Components.utils.import("resource://icecrypt/constants.jsm"); /*global IcecryptConstants: false */
Components.utils.import("resource://icecrypt/passwords.jsm"); /*global IcecryptPassword: false */
Components.utils.import("resource://icecrypt/expiry.jsm"); /*global IcecryptExpiry: false */
Components.utils.import("resource://icecrypt/uris.jsm"); /*global IcecryptURIs: false */
Components.utils.import("resource://icecrypt/protocolHandler.jsm"); /*global IcecryptProtocolHandler: false */
Components.utils.import("resource://icecrypt/mime.jsm"); /*global IcecryptMime: false */

if (!Icecrypt) var Icecrypt = {};

Icecrypt.getIcecryptSvc = function() {
  return IcecryptCore.getService(window);
};

const IOSERVICE_CONTRACTID = "@mozilla.org/network/io-service;1";
const LOCAL_FILE_CONTRACTID = "@mozilla.org/file/local;1";

Icecrypt.msg = {
  createdURIs: [],
  decryptedMessage: null,
  securityInfo: null,
  lastSaveDir: "",
  messagePane: null,
  noShowReload: false,
  decryptButton: null,
  savedHeaders: null,
  removeListener: false,
  enableExperiments: false,
  headersList: ["content-transfer-encoding",
    "x-icecrypt-version", "x-pgp-encoding-format"
  ],
  buggyExchangeEmailContent: null, // for HACK for MS-EXCHANGE-Server Problem
  buggyMailType: null,

  messengerStartup: function() {

    // private function to overwrite attributes
    function overrideAttribute(elementIdList, attrName, prefix, suffix) {
      for (var index = 0; index < elementIdList.length; index++) {
        var elementId = elementIdList[index];
        var element = document.getElementById(elementId);
        if (element) {
          try {
            var oldValue = element.getAttribute(attrName);
            IcecryptLog.DEBUG("icecryptMessengerOverlay.js: overrideAttribute " + attrName + ": oldValue=" + oldValue + "\n");
            var newValue = prefix + elementId + suffix;

            element.setAttribute(attrName, newValue);
          }
          catch (ex) {}
        }
        else {
          IcecryptLog.DEBUG("icecryptMessengerOverlay.js: *** UNABLE to override id=" + elementId + "\n");
        }
      }
    }

    Icecrypt.msg.messagePane = document.getElementById("messagepane");

    if (!Icecrypt.msg.messagePane) return; // TB on Mac OS X calls this twice -- once far too early

    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: Startup\n");

    // Override SMIME ui
    var viewSecurityCmd = document.getElementById("cmd_viewSecurityStatus");
    if (viewSecurityCmd) {
      viewSecurityCmd.setAttribute("oncommand", "Icecrypt.msg.viewSecurityInfo(null, true);");
    }

    // Override print command
    var printElementIds = ["cmd_print", "cmd_printpreview", "key_print", "button-print",
      "mailContext-print", "mailContext-printpreview"
    ];

    overrideAttribute(printElementIds, "oncommand",
      "Icecrypt.msg.msgPrint('", "');");

    Icecrypt.msg.overrideLayoutChange();

    Icecrypt.msg.savedHeaders = null;

    Icecrypt.msg.decryptButton = document.getElementById("button-icecrypt-decrypt");

    IcecryptTimer.setTimeout(function _f() {
      let msg = IcecryptExpiry.keyExpiryCheck();

      if (msg && msg.length > 0) {
        IcecryptDialog.alert(window, msg);
      }

    }.bind(Icecrypt.msg), 60000); // 1 minute

    // Need to add event listener to Icecrypt.msg.messagePane to make it work
    // Adding to msgFrame doesn't seem to work
    Icecrypt.msg.messagePane.addEventListener("unload", Icecrypt.msg.messageFrameUnload.bind(Icecrypt.msg), true);

    // override double clicking attachments, but fall back to existing handler if present
    var attListElem = document.getElementById("attachmentList");
    if (attListElem) {
      var newHandler = "Icecrypt.msg.enigAttachmentListClick('attachmentList', event)";
      var oldHandler = attListElem.getAttribute("onclick");
      if (oldHandler)
        newHandler = "if (!" + newHandler + ") {" + oldHandler + "}";
      attListElem.setAttribute("onclick", newHandler);
    }

    var treeController = {
      supportsCommand: function(command) {
        // IcecryptLog.DEBUG("icecryptMessengerOverlay.js: treeCtrl: supportsCommand: "+command+"\n");
        switch (command) {
          case "button_icecrypt_decrypt":
            return true;
        }
        return false;
      },
      isCommandEnabled: function(command) {
        // IcecryptLog.DEBUG("icecryptMessengerOverlay.js: treeCtrl: isCommandEnabled: "+command+"\n");
        try {
          if (gFolderDisplay.messageDisplay.visible) {
            if (gFolderDisplay.selectedCount != 1) Icecrypt.hdrView.statusBarHide();
            return (gFolderDisplay.selectedCount == 1);
          }
          Icecrypt.hdrView.statusBarHide();
        }
        catch (ex) {}
        return false;
      },
      doCommand: function(command) {
        //IcecryptLog.DEBUG("icecryptMessengerOverlay.js: treeCtrl: doCommand: "+command+"\n");
        // nothing
      },
      onEvent: function(event) {
        // IcecryptLog.DEBUG("icecryptMessengerOverlay.js: treeCtrl: onEvent: "+command+"\n");
        // nothing
      }
    };

    top.controllers.appendController(treeController);

    if (IcecryptPrefs.getPref("configuredVersion") === "") {
      IcecryptPrefs.setPref("configuredVersion", IcecryptApp.getVersion());
      IcecryptWindows.openSetupWizard(window, false);
    }
  },

  viewSecurityInfo: function(event, displaySmimeMsg) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: viewSecurityInfo\n");

    if (event && event.button !== 0)
      return;

    if (gSignatureStatus >= 0 || gEncryptionStatus >= 0) {
      showMessageReadSecurityInfo();
    }
    else {
      if (Icecrypt.msg.securityInfo)
        this.viewOpenpgpInfo();
      else
        showMessageReadSecurityInfo();
    }
  },

  viewOpenpgpInfo: function() {
    if (Icecrypt.msg.securityInfo) {
      IcecryptDialog.longAlert(window, IcecryptLocale.getString("securityInfo") + Icecrypt.msg.securityInfo.statusInfo);
    }
  },


  messageReload: function(noShowReload) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageReload: " + noShowReload + "\n");

    Icecrypt.msg.noShowReload = noShowReload;

    ReloadMessage();
  },


  reloadCompleteMsg: function() {
    gDBView.reloadMessageWithAllParts();
  },


  setAttachmentReveal: function(attachmentList) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: setAttachmentReveal\n");

    var revealBox = document.getElementById("icecryptRevealAttachments");
    if (revealBox) {
      // there are situations when evealBox is not yet present
      revealBox.setAttribute("hidden", !attachmentList ? "true" : "false");
    }
  },


  messageCleanup: function() {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageCleanup\n");

    var icecryptBox = document.getElementById("icecryptBox");

    if (icecryptBox && !icecryptBox.collapsed) {
      icecryptBox.setAttribute("collapsed", "true");

      var statusText = document.getElementById("expandedIcecryptStatusText");

      if (statusText)
        statusText.value = "";
    }

    document.getElementById("icecryptBrokenExchangeBox").setAttribute("collapsed", "true");

    this.setAttachmentReveal(null);

    if (Icecrypt.msg.createdURIs.length) {
      // Cleanup messages belonging to this window (just in case)
      var icecryptSvc = Icecrypt.getIcecryptSvc();
      if (icecryptSvc) {
        IcecryptLog.DEBUG("icecryptMessengerOverlay.js: Cleanup: Deleting messages\n");
        for (var index = 0; index < Icecrypt.msg.createdURIs.length; index++) {
          icecryptSvc.deleteMessageURI(Icecrypt.msg.createdURIs[index]);
        }
        Icecrypt.msg.createdURIs = [];
      }
    }

    Icecrypt.msg.decryptedMessage = null;
    Icecrypt.msg.securityInfo = null;
  },

  messageFrameUnload: function() {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageFrameUnload\n");

    if (Icecrypt.msg.noShowReload) {
      Icecrypt.msg.noShowReload = false;

    }
    else {
      Icecrypt.msg.savedHeaders = null;

      Icecrypt.msg.messageCleanup();
    }
  },

  overrideLayoutChange: function() {
    // Icecrypt needs to listen to some layout changes in order to decrypt
    // messages in case the user changes the layout
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: overrideLayoutChange\n");
    var viewTypeElementIds = ["messagePaneVertical",
      "messagePaneClassic",
      "messagePaneWide"
    ];
    var i;
    for (i = 0; i < viewTypeElementIds.length; i++) {
      let elementId = viewTypeElementIds[i];
      let element = document.getElementById(elementId);
      if (element) {
        try {
          var oldValue = element.getAttribute("oncommand").replace(/;/g, "");
          var arg = oldValue.replace(/^(.*)(\(.*\))/, "$2");
          element.setAttribute("oncommand", "Icecrypt.msg.changeMailLayout" + arg);
        }
        catch (ex) {}
      }
    }

    var toggleMsgPaneElementIds = ["cmd_toggleMessagePane"];
    for (i = 0; i < toggleMsgPaneElementIds.length; i++) {
      let elementId = toggleMsgPaneElementIds[i];
      let element = document.getElementById(elementId);
      if (element) {
        try {
          element.setAttribute("oncommand", "Icecrypt.msg.toggleMessagePane()");
        }
        catch (ex) {}
      }
    }
  },

  changeMailLayout: function(viewType) {
    // call the original layout change 1st
    ChangeMailLayout(viewType);

    // This event requires that we re-subscribe to these events!
    Icecrypt.msg.messagePane.addEventListener("unload", Icecrypt.msg.messageFrameUnload.bind(Icecrypt.msg), true);
    this.messageReload(false);
  },

  toggleMessagePane: function() {
    Icecrypt.hdrView.statusBarHide();
    MsgToggleMessagePane(true);

    var button = document.getElementById("button_icecrypt_decrypt");
    if (gFolderDisplay.messageDisplay.visible) {
      button.removeAttribute("disabled");
    }
    else {
      button.setAttribute("disabled", "true");
    }
  },

  getCurrentMsgUriSpec: function() {
    try {
      if (gFolderDisplay.selectedMessages.length != 1)
        return "";

      var uriSpec = gFolderDisplay.selectedMessageUris[0];
      //IcecryptLog.DEBUG("icecryptMessengerOverlay.js: getCurrentMsgUriSpec: uriSpec="+uriSpec+"\n");

      return uriSpec;

    }
    catch (ex) {
      return "";
    }
  },

  getCurrentMsgUrl: function() {
    var uriSpec = this.getCurrentMsgUriSpec();
    return this.getUrlFromUriSpec(uriSpec);
  },

  getUrlFromUriSpec: function(uriSpec) {
    try {
      if (!uriSpec)
        return null;

      var msgService = messenger.messageServiceFromURI(uriSpec);

      var urlObj = {};
      msgService.GetUrlForUri(uriSpec, urlObj, msgWindow);

      var url = urlObj.value;

      if (url.scheme == "file") {
        return url;
      }
      else {
        return url.QueryInterface(Components.interfaces.nsIMsgMailNewsUrl);
      }

    }
    catch (ex) {
      return null;
    }
  },

  updateOptionsDisplay: function() {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: updateOptionsDisplay: \n");
    var optList = ["autoDecrypt"];

    for (let j = 0; j < optList.length; j++) {
      let menuElement = document.getElementById("icecrypt_" + optList[j]);
      menuElement.setAttribute("checked", IcecryptPrefs.getPref(optList[j]) ? "true" : "false");

      menuElement = document.getElementById("icecrypt_" + optList[j] + "2");
      if (menuElement)
        menuElement.setAttribute("checked", IcecryptPrefs.getPref(optList[j]) ? "true" : "false");
    }

    optList = ["decryptverify"];
    for (let j = 0; j < optList.length; j++) {
      let menuElement = document.getElementById("icecrypt_" + optList[j]);
      if (Icecrypt.msg.decryptButton && Icecrypt.msg.decryptButton.disabled) {
        menuElement.setAttribute("disabled", "true");
      }
      else {
        menuElement.removeAttribute("disabled");
      }

      menuElement = document.getElementById("icecrypt_" + optList[j] + "2");
      if (menuElement) {
        if (Icecrypt.msg.decryptButton && Icecrypt.msg.decryptButton.disabled) {
          menuElement.setAttribute("disabled", "true");
        }
        else {
          menuElement.removeAttribute("disabled");
        }
      }
    }
  },

  displayMainMenu: function(menuPopup) {

    function traverseTree(currentElement, func) {
      if (currentElement) {
        func(currentElement);
        if (currentElement.id)
          IcecryptLog.DEBUG("traverseTree: " + currentElement.id + "\n");

        // Traverse the tree
        var i = 0;
        var currentElementChild = currentElement.childNodes[i];
        while (currentElementChild) {
          // Recursively traverse the tree structure of the child node
          traverseTree(currentElementChild, func);
          i++;
          currentElementChild = currentElement.childNodes[i];
        }
      }
    }

    var p = menuPopup.parentNode;
    var a = document.getElementById("menu_IcecryptPopup");
    var c = a.cloneNode(true);
    p.removeChild(menuPopup);


    traverseTree(c, function _updNode(node) {
      if (node.id && node.id.length > 0) node.id += "2";
    });
    p.appendChild(c);

  },

  toggleAttribute: function(attrName) {
    IcecryptLog.DEBUG("icecryptMsgessengerOverlay.js: toggleAttribute('" + attrName + "')\n");

    var menuElement = document.getElementById("icecrypt_" + attrName);

    var oldValue = IcecryptPrefs.getPref(attrName);
    IcecryptPrefs.setPref(attrName, !oldValue);

    this.updateOptionsDisplay();

    if (attrName == "autoDecrypt")
      this.messageReload(false);
  },

  messageImport: function(event) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageImport: " + event + "\n");

    return this.messageParse(!event, true, "", this.getCurrentMsgUriSpec());
  },

  /***
   * check that handler for multipart/signed is set to Icecrypt.
   * if handler is different, change it and reload message
   *
   * @return: - true if handler is OK
   *          - false if handler was changed and message is reloaded
   */
  checkPgpmimeHandler: function() {
    if (IcecryptVerify.currentCtHandler !== IcecryptConstants.MIME_HANDLER_PGPMIME) {
      IcecryptVerify.registerContentTypeHandler();
      this.messageReload();
      return false;
    }

    return true;
  },

  // callback function for automatic decryption
  messageAutoDecrypt: function(event) {
    Icecrypt.msg.messageDecrypt(event, true);
  },

  // analyse message header and decrypt/verify message
  messageDecrypt: function(event, isAuto) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageDecrypt: " + event + "\n");

    this.mimeParts = null;

    var cbObj = {
      event: event,
      isAuto: isAuto
    };

    if (!isAuto) {
      IcecryptVerify.setManualUri(this.getCurrentMsgUriSpec());
    }

    let contentType = "text/plain";
    if ('content-type' in currentHeaderData) contentType = currentHeaderData['content-type'].headerValue;


    // don't parse message if we know it's a PGP/MIME message
    if (((contentType.search(/^multipart\/signed(;|$)/i) === 0) && (contentType.search(/application\/pgp-signature/i) > 0)) ||
      ((contentType.search(/^multipart\/encrypted(;|$)/i) === 0) && (contentType.search(/application\/pgp-encrypted/i) > 0))) {
      this.messageDecryptCb(event, isAuto, null);
      return;
    }

    try {
      IcecryptMime.getMimeTreeFromUrl(this.getCurrentMsgUrl().spec, false,
        function _cb(mimeMsg) {
          Icecrypt.msg.messageDecryptCb(event, isAuto, mimeMsg);
        });
    }
    catch (ex) {
      IcecryptLog.DEBUG("icecryptMessengerOverlay.js: enigMessageDecrypt: cannot use MsgHdrToMimeMessage\n");
      this.messageDecryptCb(event, isAuto, null);
    }
  },

  /***
   * walk through the (sub-) mime tree and determine PGP/MIME encrypted and signed message parts
   *
   * @param mimePart:  parent object to walk through
   * @param resultObj: object containing two arrays. The resultObj must be pre-initialized by the caller
   *                    - encrypted
   *                    - signed
   */
  enumerateMimeParts: function(mimePart, resultObj) {
    IcecryptLog.DEBUG("enumerateMimeParts: partNum=\"" + mimePart.partNum + "\"\n");
    IcecryptLog.DEBUG("                    " + mimePart.fullContentType + "\n");
    IcecryptLog.DEBUG("                    " + mimePart.subParts.length + " subparts\n");

    try {
      var ct = mimePart.fullContentType;
      if (typeof(ct) == "string") {
        ct = ct.replace(/[\r\n]/g, " ");
        if (ct.search(/multipart\/signed.*application\/pgp-signature/i) >= 0) {
          resultObj.signed.push(mimePart.partNum);
        }
        else if (ct.search(/application\/pgp-encrypted/i) >= 0)
          resultObj.encrypted.push(mimePart.partNum);
      }
    }
    catch (ex) {
      // catch exception if no headers or no content-type defined.
    }

    var i;
    for (i in mimePart.subParts) {
      this.enumerateMimeParts(mimePart.subParts[i], resultObj);
    }
  },

  messageDecryptCb: function(event, isAuto, mimeMsg) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageDecryptCb:\n");

    this.buggyExchangeEmailContent = null; // reinit HACK for MS-EXCHANGE-Server Problem

    let icecryptSvc;
    let contentType = "";

    try {
      if (!mimeMsg) {
        IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageDecryptCb: mimeMsg is null\n");
        try {
          contentType = currentHeaderData['content-type'].headerValue;
        }
        catch (ex) {
          contentType = "text/plain";
        }
        mimeMsg = {
          partNum: "1",
          headers: {
            has: function() {
              return false;
            },
            contentType: {
              type: contentType,
              mediatype: "",
              subtype: ""
            }
          },
          fullContentType: contentType,
          body: "",
          parent: null,
          subParts: []
        };
      }

      // Copy selected headers
      Icecrypt.msg.savedHeaders = {};

      if (!mimeMsg.fullContentType) {
        mimeMsg.fullContentType = "text/plain";
      }
      this.mimeParts = mimeMsg;

      Icecrypt.msg.savedHeaders["content-type"] = mimeMsg.fullContentType;

      for (var index = 0; index < Icecrypt.msg.headersList.length; index++) {
        var headerName = Icecrypt.msg.headersList[index];
        var headerValue = "";

        if (mimeMsg.headers.has(headerName)) {
          let h = mimeMsg.headers.get(headerName);
          if (Array.isArray(h)) {
            headerValue = h.join("");
          }
          else
            headerValue = h;
        }
        Icecrypt.msg.savedHeaders[headerName] = headerValue;
        IcecryptLog.DEBUG("icecryptMessengerOverlay.js: header " + headerName + ": '" + headerValue + "'\n");
      }

      var msgSigned = null;
      var msgEncrypted = null;
      var resultObj = {
        encrypted: [],
        signed: []
      };

      if (mimeMsg.subParts.length > 0) {
        this.enumerateMimeParts(mimeMsg, resultObj);
        IcecryptLog.DEBUG("icecryptMessengerOverlay.js: embedded objects: " + resultObj.encrypted.join(", ") + " / " + resultObj.signed.join(", ") + "\n");

        msgSigned = resultObj.signed.length > 0;
        msgEncrypted = resultObj.encrypted.length > 0;

        // HACK for Zimbra OpenPGP Zimlet
        // Zimbra illegally changes attachment content-type to application/pgp-encrypted which interfers with below
        // see https://sourceforge.net/p/enigmail/bugs/600/

        try {

          if (mimeMsg.subParts.length > 1 &&
            mimeMsg.headers.has("x-mailer") && mimeMsg.headers.get("x-mailer")[0].indexOf("ZimbraWebClient") >= 0 &&
            mimeMsg.subParts[0].fullContentType.indexOf("text/plain") >= 0 &&
            mimeMsg.fullContentType.indexOf("multipart/mixed") >= 0 &&
            mimeMsg.subParts[1].fullContentType.indexOf("application/pgp-encrypted") >= 0) {
            this.messageParse(event, false, Icecrypt.msg.savedHeaders["content-transfer-encoding"], this.getCurrentMsgUriSpec());
            return;
          }

        }
        catch (ex) {}


        // HACK for MS-EXCHANGE-Server Problem:
        // check for possible bad mime structure due to buggy exchange server:
        // - multipart/mixed Container with
        //   - application/pgp-encrypted Attachment with name "PGPMIME Versions Identification"
        //   - application/octet-stream Attachment with name "encrypted.asc" having the encrypted content in base64
        // - see:
        //   - http://www.mozilla-icecrypt.org/forum/viewtopic.php?f=4&t=425
        //   - http://sourceforge.net/p/icecrypt/forum/support/thread/4add2b69/

        // iPGMail produces a similar broken structure, see here:
        //   - https://sourceforge.net/p/enigmail/forum/support/thread/afc9c246/#5de7

        if (mimeMsg.subParts.length == 3 &&
          mimeMsg.fullContentType.search(/multipart\/mixed/i) >= 0 &&
          mimeMsg.subParts[0].fullContentType.search(/multipart\/encrypted/i) < 0 &&
          mimeMsg.subParts[0].fullContentType.search(/text\/(plain|html)/i) >= 0 &&
          mimeMsg.subParts[1].fullContentType.search(/application\/pgp-encrypted/i) >= 0) {
          if (mimeMsg.subParts[1].fullContentType.search(/multipart\/encrypted/i) < 0 &&
            mimeMsg.subParts[1].fullContentType.search(/PGP\/?MIME Versions? Identification/i) >= 0 &&
            mimeMsg.subParts[2].fullContentType.search(/application\/octet-stream/i) >= 0 &&
            mimeMsg.subParts[2].fullContentType.search(/encrypted.asc/i) >= 0) {
            this.buggyMailType = "exchange";
          }
          else {
            this.buggyMailType = "iPGMail";
          }

          // signal that the structure matches to save the content later on
          IcecryptLog.DEBUG("icecryptMessengerOverlay: messageDecryptCb: enabling MS-Exchange hack\n");
          this.buggyExchangeEmailContent = "???";

          this.buggyMailHeader();
          return;
        }
      }

      var contentEncoding = "";
      var xIcecryptVersion = "";
      var msgUriSpec = this.getCurrentMsgUriSpec();
      var encrypedMsg;

      if (Icecrypt.msg.savedHeaders) {
        contentType = Icecrypt.msg.savedHeaders["content-type"];
        contentEncoding = Icecrypt.msg.savedHeaders["content-transfer-encoding"];
        xIcecryptVersion = Icecrypt.msg.savedHeaders["x-icecrypt-version"];
      }

      if (msgSigned || msgEncrypted) {
        // PGP/MIME messages
        icecryptSvc = Icecrypt.getIcecryptSvc();
        if (!icecryptSvc)
          return;

        if (!Icecrypt.msg.checkPgpmimeHandler()) return;

        if (isAuto && (!IcecryptPrefs.getPref("autoDecrypt"))) {

          if (IcecryptVerify.getManualUri() != this.getCurrentMsgUriSpec()) {
            // decryption set to manual
            Icecrypt.hdrView.updateHdrIcons(IcecryptConstants.POSSIBLE_PGPMIME, 0, // exitCode, statusFlags
              "", "", // keyId, userId
              "", // sigDetails
              IcecryptLocale.getString("possiblyPgpMime"), // infoMsg
              null, // blockSeparation
              "", // encToDetails
              null); // xtraStatus
          }
        }
        else if (!isAuto) {
          Icecrypt.msg.messageReload(false);
        }
        return;
      }

      // inline-PGP messages
      if (!isAuto || IcecryptPrefs.getPref("autoDecrypt")) {
        this.messageParse(!event, false, contentEncoding, msgUriSpec);
      }
    }
    catch (ex) {
      IcecryptLog.writeException("icecryptMessengerOverlay.js: messageDecryptCb", ex);
    }
  },

  // display header about reparing buggy MS-Exchange messages
  buggyMailHeader: function() {
    let headerSink = msgWindow.msgHeaderSink.securityInfo.QueryInterface(Components.interfaces.nsIEnigMimeHeaderSink);


    let uriStr = IcecryptURIs.createMessageURI(this.getCurrentMsgUrl(),
      "message/rfc822",
      "",
      "??",
      false);

    let ph = new IcecryptProtocolHandler();
    let uri = ph.newURI(uriStr, "", "");
    headerSink.updateSecurityStatus("", 0, 0, "", "", "", "", "", uri, "", "1");
  },

  messageParse: function(interactive, importOnly, contentEncoding, msgUriSpec) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageParse: " + interactive + "\n");
    var msgFrame = IcecryptWindows.getFrame(window, "messagepane");
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: msgFrame=" + msgFrame + "\n");

    var bodyElement = msgFrame.document.getElementsByTagName("body")[0];
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: bodyElement=" + bodyElement + "\n");

    if (!bodyElement) return;

    let topElement = bodyElement;
    var findStr = /* interactive ? null : */ "-----BEGIN PGP";
    var msgText = null;
    var foundIndex = -1;

    if (bodyElement.firstChild) {
      let node = bodyElement.firstChild;
      while (node) {
        if (node.nodeName == "DIV") {
          foundIndex = node.textContent.indexOf(findStr);

          if (foundIndex >= 0) {
            if (node.textContent.indexOf(findStr + " LICENSE AUTHORIZATION") == foundIndex)
              foundIndex = -1;
          }
          if (foundIndex >= 0) {
            bodyElement = node;
            break;
          }
        }
        node = node.nextSibling;
      }
    }

    if (foundIndex >= 0) {
      if (Icecrypt.msg.savedHeaders["content-type"].search(/^text\/html/i) === 0) {
        let p = Components.classes["@mozilla.org/parserutils;1"].createInstance(Components.interfaces.nsIParserUtils);
        const de = Components.interfaces.nsIDocumentEncoder;
        msgText = p.convertToPlainText(topElement.innerHTML, de.OutputRaw | de.OutputBodyOnly, 0);
      }
      else {
        msgText = bodyElement.textContent;
      }
    }

    if (!msgText) {
      // No PGP content

      // but this might be caused by the HACK for MS-EXCHANGE-Server Problem
      // - so return only if:
      if (!this.buggyExchangeEmailContent || this.buggyExchangeEmailContent == "???") {
        return;
      }

      IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageParse: got buggyExchangeEmailContent = " + this.buggyExchangeEmailContent.substr(0, 50) + "\n");

      // fix the whole invalid email by replacing the contents by the decoded text
      // as plain inline format
      if (this.displayBuggyExchangeMail()) {
        return;
      }
      else {
        msgText = this.buggyExchangeEmailContent;
      }

      msgText = msgText.replace(/\r\n/g, "\n");
      msgText = msgText.replace(/\r/g, "\n");

      // content is in encrypted.asc part:
      let idx = msgText.search(/Content-Type: application\/octet-stream; name="encrypted.asc"/i);
      if (idx >= 0) {
        msgText = msgText.slice(idx);
      }
      // check whether we have base64 encoding
      var isBase64 = false;
      idx = msgText.search(/Content-Transfer-Encoding: base64/i);
      if (idx >= 0) {
        isBase64 = true;
      }
      // find content behind part header
      idx = msgText.search(/\n\n/);
      if (idx >= 0) {
        msgText = msgText.slice(idx);
      }
      // remove stuff behind content block (usually a final boundary row)
      idx = msgText.search(/\n\n--/);
      if (idx >= 0) {
        msgText = msgText.slice(0, idx + 1);
      }
      // decode base64 if it is encoded that way
      if (isBase64) {
        try {
          msgText = IcecryptData.decodeBase64(msgText);
        }
        catch (ex) {
          IcecryptLog.writeException("icecryptMessengerOverlay.js: decodeBase64() ", ex);
        }
        //IcecryptLog.DEBUG("nach base64 decode: \n" + msgText + "\n");
      }
    }

    var charset = msgWindow ? msgWindow.mailCharacterSet : "";

    // Encode ciphertext to charset from unicode
    msgText = IcecryptData.convertFromUnicode(msgText, charset);

    var mozPlainText = bodyElement.innerHTML.search(/class="moz-text-plain"/);

    if ((mozPlainText >= 0) && (mozPlainText < 40)) {
      // workaround for too much expanded emoticons in plaintext msg
      var r = new RegExp(/( )(;-\)|:-\)|;\)|:\)|:-\(|:\(|:-\\|:-P|:-D|:-\[|:-\*|>:o|8-\)|:-\$|:-X|=-O|:-!|O:-\)|:'\()( )/g);
      if (msgText.search(r) >= 0) {
        IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageParse: performing emoticons fixing\n");
        msgText = msgText.replace(r, "$2");
      }
    }

    // extract text preceeding and/or following armored block
    var head = "";
    var tail = "";
    if (findStr) {
      head = msgText.substring(0, msgText.indexOf(findStr)).replace(/^[\n\r\s]*/, "");
      head = head.replace(/[\n\r\s]*$/, "");
      var endStart = msgText.indexOf("-----END PGP");
      var nextLine = msgText.substring(endStart).search(/[\n\r]/);
      if (nextLine > 0) {
        tail = msgText.substring(endStart + nextLine).replace(/^[\n\r\s]*/, "");
      }
    }

    //IcecryptLog.DEBUG("icecryptMessengerOverlay.js: msgText='"+msgText+"'\n");

    var mailNewsUrl = this.getUrlFromUriSpec(msgUriSpec);

    var urlSpec = mailNewsUrl ? mailNewsUrl.spec : "";

    let retry = (charset != "UTF-8" ? 1 : 2);

    Icecrypt.msg.messageParseCallback(msgText, contentEncoding, charset, interactive,
      importOnly, urlSpec, "", retry, head, tail,
      msgUriSpec);
  },


  messageParseCallback: function(msgText, contentEncoding, charset, interactive,
    importOnly, messageUrl, signature, retry,
    head, tail, msgUriSpec) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageParseCallback: " + interactive + ", " + interactive + ", importOnly=" + importOnly + ", charset=" + charset + ", msgUrl=" +
      messageUrl +
      ", retry=" + retry + ", signature='" + signature + "'\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    if (!msgText)
      return;

    var icecryptSvc = Icecrypt.getIcecryptSvc();
    if (!icecryptSvc)
      return;

    var plainText;
    var exitCode;
    var newSignature = "";
    var statusFlags = 0;

    var errorMsgObj = {};
    var keyIdObj = {};
    var userIdObj = {};
    var sigDetailsObj = {};
    var encToDetailsObj = {};
    var blockSeparationObj = {
      value: ""
    };

    if (importOnly) {
      // Import public key
      exitCode = IcecryptKeyRing.importKey(window, true, msgText, "",
        errorMsgObj);

    }
    else {

      if (msgText.indexOf("\nCharset:") > 0) {
        // Check if character set needs to be overridden
        var startOffset = msgText.indexOf("-----BEGIN PGP ");

        if (startOffset >= 0) {
          var subText = msgText.substr(startOffset);

          subText = subText.replace(/\r\n/g, "\n");
          subText = subText.replace(/\r/g, "\n");

          var endOffset = subText.search(/\n\n/);
          if (endOffset > 0) {
            subText = subText.substr(0, endOffset) + "\n";

            let matches = subText.match(/\nCharset: *(.*) *\n/i);
            if (matches && (matches.length > 1)) {
              // Override character set
              charset = matches[1];
              IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageParseCallback: OVERRIDING charset=" + charset + "\n");
            }
          }
        }
      }

      var exitCodeObj = {};
      var statusFlagsObj = {};

      var signatureObj = {};
      signatureObj.value = signature;

      var uiFlags = interactive ? (nsIIcecrypt.UI_INTERACTIVE |
        nsIIcecrypt.UI_ALLOW_KEY_IMPORT |
        nsIIcecrypt.UI_UNVERIFIED_ENC_OK) : 0;


      plainText = icecryptSvc.decryptMessage(window, uiFlags, msgText,
        signatureObj, exitCodeObj, statusFlagsObj,
        keyIdObj, userIdObj, sigDetailsObj,
        errorMsgObj, blockSeparationObj, encToDetailsObj);

      //IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageParseCallback: plainText='"+plainText+"'\n");

      exitCode = exitCodeObj.value;
      newSignature = signatureObj.value;

      if (plainText === "" && exitCode === 0) {
        plainText = " ";
      }

      statusFlags = statusFlagsObj.value;

      IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageParseCallback: newSignature='" + newSignature + "'\n");
    }

    var errorMsg = errorMsgObj.value;

    if (importOnly) {
      if (interactive && errorMsg)
        IcecryptDialog.longAlert(window, errorMsg);
      return;
    }

    var displayedUriSpec = Icecrypt.msg.getCurrentMsgUriSpec();
    if (!msgUriSpec || (displayedUriSpec == msgUriSpec)) {
      Icecrypt.hdrView.updateHdrIcons(exitCode, statusFlags, keyIdObj.value, userIdObj.value,
        sigDetailsObj.value,
        errorMsg,
        null, // blockSeparation
        encToDetailsObj.value,
        null); // xtraStatus
    }

    var noSecondTry = nsIIcecrypt.GOOD_SIGNATURE |
      nsIIcecrypt.EXPIRED_SIGNATURE |
      nsIIcecrypt.EXPIRED_KEY_SIGNATURE |
      nsIIcecrypt.EXPIRED_KEY |
      nsIIcecrypt.REVOKED_KEY |
      nsIIcecrypt.NO_PUBKEY |
      nsIIcecrypt.NO_SECKEY |
      nsIIcecrypt.IMPORTED_KEY |
      nsIIcecrypt.MISSING_PASSPHRASE |
      nsIIcecrypt.BAD_PASSPHRASE |
      nsIIcecrypt.UNKNOWN_ALGO |
      nsIIcecrypt.DECRYPTION_OKAY |
      nsIIcecrypt.OVERFLOWED;

    if ((exitCode !== 0) && (!(statusFlags & noSecondTry))) {
      // Bad signature/armor
      if (retry == 1) {
        msgText = IcecryptData.convertFromUnicode(msgText, "UTF-8");
        Icecrypt.msg.messageParseCallback(msgText, contentEncoding, charset,
          interactive, importOnly, messageUrl,
          signature, retry + 1,
          head, tail, msgUriSpec);
        return;
      }
      else if (retry == 2) {
        // Try to verify signature by accessing raw message text directly
        // (avoid recursion by setting retry parameter to false on callback)
        newSignature = "";
        Icecrypt.msg.msgDirectDecrypt(interactive, importOnly, contentEncoding, charset,
          newSignature, 0, head, tail, msgUriSpec,
          Icecrypt.msg.messageParseCallback);
        return;
      }
      else if (retry == 3) {
        msgText = IcecryptData.convertToUnicode(msgText, "UTF-8");
        Icecrypt.msg.messageParseCallback(msgText, contentEncoding, charset, interactive,
          importOnly, messageUrl, null, retry + 1,
          head, tail, msgUriSpec);
        return;
      }
    }

    if (!plainText) {
      if (interactive && Icecrypt.msg.securityInfo && Icecrypt.msg.securityInfo.statusInfo)
        IcecryptDialog.longAlert(window, Icecrypt.msg.securityInfo.statusInfo);
      return;
    }

    if (retry >= 2) {
      plainText = IcecryptData.convertFromUnicode(IcecryptData.convertToUnicode(plainText, "UTF-8"), charset);
    }

    if (blockSeparationObj.value.indexOf(" ") >= 0) {
      var blocks = blockSeparationObj.value.split(/ /);
      var blockInfo = blocks[0].split(/:/);
      plainText = IcecryptData.convertFromUnicode(IcecryptLocale.getString("notePartEncrypted"), charset) +
        "\n\n" + plainText.substr(0, blockInfo[1]) + "\n\n" + IcecryptLocale.getString("noteCutMessage");
    }

    // Save decrypted message status, headers, and content
    var headerList = {
      "subject": "",
      "from": "",
      "date": "",
      "to": "",
      "cc": ""
    };

    var index, headerName;

    if (!gViewAllHeaders) {
      for (index = 0; index < headerList.length; index++) {
        headerList[index] = "";
      }

    }
    else {
      for (index = 0; index < gExpandedHeaderList.length; index++) {
        headerList[gExpandedHeaderList[index].name] = "";
      }

      for (headerName in currentHeaderData) {
        headerList[headerName] = "";
      }
    }

    for (headerName in headerList) {
      if (currentHeaderData[headerName])
        headerList[headerName] = currentHeaderData[headerName].headerValue;
    }

    // WORKAROUND
    if (headerList.cc == headerList.to)
      headerList.cc = "";

    var hasAttachments = currentAttachments && currentAttachments.length;
    var attachmentsEncrypted = true;

    for (index in currentAttachments) {
      if (!Icecrypt.msg.checkEncryptedAttach(currentAttachments[index])) {
        if (!Icecrypt.msg.checkSignedAttachment(currentAttachments, index)) attachmentsEncrypted = false;
      }
    }

    var msgRfc822Text = "";
    if (head || tail) {
      if (head) {
        // print a warning if the signed or encrypted part doesn't start
        // quite early in the message
        let matches = head.match(/(\n)/g);
        if (matches && matches.length > 10) {
          msgRfc822Text = IcecryptData.convertFromUnicode(IcecryptLocale.getString("notePartEncrypted"), charset) + "\n\n";
        }
        msgRfc822Text += head + "\n\n";
      }
      msgRfc822Text += IcecryptData.convertFromUnicode(IcecryptLocale.getString("beginPgpPart"), charset) + "\n\n";
    }
    msgRfc822Text += plainText;
    if (head || tail) {
      msgRfc822Text += "\n\n" + IcecryptData.convertFromUnicode(IcecryptLocale.getString("endPgpPart"), charset) + "\n\n" + tail;
    }

    Icecrypt.msg.decryptedMessage = {
      url: messageUrl,
      uri: msgUriSpec,
      headerList: headerList,
      hasAttachments: hasAttachments,
      attachmentsEncrypted: attachmentsEncrypted,
      charset: charset,
      plainText: msgRfc822Text
    };

    var msgFrame = IcecryptWindows.getFrame(window, "messagepane");
    var bodyElement = msgFrame.document.getElementsByTagName("body")[0];

    // don't display decrypted message if message selection has changed
    displayedUriSpec = Icecrypt.msg.getCurrentMsgUriSpec();
    if (msgUriSpec && displayedUriSpec && (displayedUriSpec != msgUriSpec)) return;


    // Create and load one-time message URI
    var messageContent = Icecrypt.msg.getDecryptedMessage("message/rfc822", false);

    Icecrypt.msg.noShowReload = true;
    var node;
    bodyElement = msgFrame.document.getElementsByTagName("body")[0];
    if (bodyElement.firstChild) {
      node = bodyElement.firstChild;
      var foundIndex = -1;
      var findStr = "-----BEGIN PGP";

      while (node) {
        if (node.nodeName == "DIV") {
          foundIndex = node.textContent.indexOf(findStr);

          if (foundIndex >= 0) {
            if (node.textContent.indexOf(findStr + " LICENSE AUTHORIZATION") == foundIndex)
              foundIndex = -1;
          }
          if (foundIndex >= 0) {
            node.innerHTML = IcecryptFuncs.formatPlaintextMsg(IcecryptData.convertToUnicode(messageContent, charset));
            return;
          }
        }
        node = node.nextSibling;
      }

      // if no <DIV> node is found, try with <PRE> (bug 24762)
      node = bodyElement.firstChild;
      foundIndex = -1;
      while (node) {
        if (node.nodeName == "PRE") {
          foundIndex = node.textContent.indexOf(findStr);

          if (foundIndex >= 0) {
            if (node.textContent.indexOf(findStr + " LICENSE AUTHORIZATION") == foundIndex)
              foundIndex = -1;
          }
          if (foundIndex >= 0) {
            node.innerHTML = IcecryptFuncs.formatPlaintextMsg(IcecryptData.convertToUnicode(messageContent, charset));
            return;
          }
        }
        node = node.nextSibling;
      }

      // HACK for MS-EXCHANGE-Server Problem:
      // - remove empty text/plain part
      //   and set message content as inner text
      // - missing:
      //   - signal in statusFlags so that we warn in Icecrypt.hdrView.updateHdrIcons()
      if (this.buggyExchangeEmailContent) {
        if (this.displayBuggyExchangeMail()) {
          return;
        }

        IcecryptLog.DEBUG("icecryptMessengerOverlay: messageParseCallback: got broken MS-Exchange mime message\n");
        messageContent = messageContent.replace(/^\s{0,2}Content-Transfer-Encoding: quoted-printable\s*Content-Type: text\/plain;\s*charset=windows-1252/i, "");
        node = bodyElement.firstChild;
        while (node) {
          if (node.nodeName == "DIV") {
            node.innerHTML = IcecryptFuncs.formatPlaintextMsg(IcecryptData.convertToUnicode(messageContent, charset));
            Icecrypt.hdrView.updateHdrIcons(exitCode, statusFlags, keyIdObj.value, userIdObj.value,
              sigDetailsObj.value,
              errorMsg,
              null, // blockSeparation
              encToDetailsObj.value,
              "buggyMailFormat");
            return;
          }
          node = node.nextSibling;
        }
      }

    }

    IcecryptLog.ERROR("icecryptMessengerOverlay.js: no node found to replace message display\n");

    return;
  },


  // check if an attachment could be signed
  checkSignedAttachment: function(attachmentObj, index) {
    var attachmentList;
    if (index !== null) {
      attachmentList = attachmentObj;
    }
    else {
      attachmentList = currentAttachments;
      for (let i = 0; i < attachmentList.length; i++) {
        if (attachmentList[i].url == attachmentObj.url) {
          index = i;
          break;
        }
      }
      if (index === null) return false;
    }

    var signed = false;
    var findFile;

    var attName = this.getAttachmentName(attachmentList[index]).toLowerCase().replace(/\+/g, "\\+");

    // check if filename is a signature
    if ((this.getAttachmentName(attachmentList[index]).search(/\.(sig|asc)$/i) > 0) ||
      (attachmentList[index].contentType.match(/^application\/pgp-signature/i))) {
      findFile = new RegExp(attName.replace(/\.(sig|asc)$/, ""));
    }
    else
      findFile = new RegExp(attName + ".(sig|asc)$");

    for (let i in attachmentList) {
      if ((i != index) &&
        (this.getAttachmentName(attachmentList[i]).toLowerCase().search(findFile) === 0))
        signed = true;
    }

    return signed;
  },

  /**
   * Fix broken PGP/MIME messages from MS-Exchange by replacing the broken original
   * message with a fixed copy.
   *
   * no return
   */
  fixBuggyExchangeMail: function() {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: fixBuggyExchangeMail:\n");

    function hideAndResetExchangePane() {
      document.getElementById("icecryptBrokenExchangeBox").setAttribute("collapsed", "true");
      document.getElementById("icecryptFixBrokenMessageProgress").setAttribute("collapsed", "true");
      document.getElementById("icecryptFixBrokenMessageButton").removeAttribute("collapsed");
    }

    document.getElementById("icecryptFixBrokenMessageButton").setAttribute("collapsed", "true");
    document.getElementById("icecryptFixBrokenMessageProgress").removeAttribute("collapsed");

    let msg = gFolderDisplay.messageDisplay.displayedMessage;

    let p = IcecryptFixExchangeMsg.fixExchangeMessage(msg, this.buggyMailType);
    p.then(
      function _success(msgKey) {
        // display message with given msgKey

        IcecryptLog.DEBUG("icecryptMessengerOverlay.js: fixBuggyExchangeMail: _success: msgKey=" + msgKey + "\n");

        if (msgKey) {
          let index = gFolderDisplay.view.dbView.findIndexFromKey(msgKey, true);
          IcecryptLog.DEBUG("  ** index = " + index + "\n");

          IcecryptTimer.setTimeout(function() {
            gFolderDisplay.view.dbView.selectMsgByKey(msgKey);
          }, 750);
        }

        hideAndResetExchangePane();
      }
    );
    p.catch(function _rejected() {
      IcecryptDialog.alert(window, IcecryptLocale.getString("fixBrokenExchangeMsg.failed"));
      hideAndResetExchangePane();
    });
  },

  /**
   * Attempt to work around bug with headers of MS-Exchange message.
   * Reload message content
   *
   * @return: true:  message displayed
   *          false: could not handle message
   */
  displayBuggyExchangeMail: function() {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: displayBuggyExchangeMail\n");
    let hdrs = Components.classes["@mozilla.org/messenger/mimeheaders;1"].createInstance(Components.interfaces.nsIMimeHeaders);
    hdrs.initialize(this.buggyExchangeEmailContent);
    let ct = hdrs.extractHeader("content-type", true);

    if (ct && ct.search(/^text\/plain/i) === 0) {
      let bi = this.buggyExchangeEmailContent.search(/\r?\n/);
      let boundary = this.buggyExchangeEmailContent.substr(2, bi - 2);
      let startMsg = this.buggyExchangeEmailContent.search(/\r?\n\r?\n/);
      let msgText;

      if (this.buggyMailType == "exchange") {
        msgText = 'Content-Type: multipart/encrypted; protocol="application/pgp-encrypted"; boundary="' + boundary + '"\r\n' +
          this.buggyExchangeEmailContent.substr(startMsg);
      }
      else {
        msgText = 'Content-Type: multipart/encrypted; protocol="application/pgp-encrypted"; boundary="' + boundary + '"\r\n' +
          "\r\n" + boundary + "\r\n" +
          "Content-Type: application/pgp-encrypted\r\n" +
          "Content-Description: PGP/MIME version identification\r\n\r\n" +
          "Version: 1\r\n\r\n" +
          this.buggyExchangeEmailContent.substr(startMsg).replace(/^Content-Type: +application\/pgp-encrypted/im,
            "Content-Type: application/octet-stream");

      }

      let icecryptSvc = Icecrypt.getIcecryptSvc();
      if (!icecryptSvc) return false;

      let uri = icecryptSvc.createMessageURI(this.getCurrentMsgUrl(),
        "message/rfc822",
        "",
        msgText,
        false);

      IcecryptVerify.setMsgWindow(msgWindow, null);
      messenger.loadURL(window, uri);

      // Thunderbird
      let atv = document.getElementById("attachmentView");
      if (atv) {
        atv.setAttribute("collapsed", "true");
      }

      // SeaMonkey
      let eab = document.getElementById("expandedAttachmentBox");
      if (eab) {
        eab.setAttribute("collapsed", "true");
      }

      return true;
    }

    return false;
  },

  // check if the attachment could be encrypted
  checkEncryptedAttach: function(attachment) {
    return (this.getAttachmentName(attachment).match(/\.(gpg|pgp|asc)$/i) ||
      (attachment.contentType.match(/^application\/pgp(-.*)?$/i)) &&
      (attachment.contentType.search(/^application\/pgp-signature/i) < 0));
  },

  getAttachmentName: function(attachment) {
    if ("name" in attachment) {
      // Thunderbird
      return attachment.name;
    }
    else
    // SeaMonkey
      return attachment.displayName;
  },

  escapeTextForHTML: function(text, hyperlink) {
    // Escape special characters
    if (text.indexOf("&") > -1)
      text = text.replace(/&/g, "&amp;");

    if (text.indexOf("<") > -1)
      text = text.replace(/</g, "&lt;");

    if (text.indexOf(">") > -1)
      text = text.replace(/>/g, "&gt;");

    if (text.indexOf("\"") > -1)
      text = text.replace(/"/g, "&quot;");

    if (!hyperlink)
      return text;

    // Hyperlink email addresses (we accept at most 1024 characters before and after the @)
    var addrs = text.match(/\b[A-Za-z0-9_+.-]+@[A-Za-z0-9.-]+\b/g);

    var newText, offset, loc;
    if (addrs && addrs.length) {
      newText = "";
      offset = 0;

      for (var j = 0; j < addrs.length; j++) {
        var addr = addrs[j];

        loc = text.indexOf(addr, offset);
        if (loc < offset)
          break;

        if (loc > offset)
          newText += text.substr(offset, loc - offset);

        // Strip any period off the end of address
        addr = addr.replace(/[.]$/, "");

        if (!addr.length)
          continue;

        newText += "<a href=\"mailto:" + addr + "\">" + addr + "</a>";

        offset = loc + addr.length;
      }

      newText += text.substr(offset, text.length - offset);

      text = newText;
    }

    // Hyperlink URLs (we don't accept URLS or more than 1024 characters length)
    var urls = text.match(/\b(http|https|ftp):\S{1,1024}\s/g);

    if (urls && urls.length) {
      newText = "";
      offset = 0;

      for (var k = 0; k < urls.length; k++) {
        var url = urls[k];

        loc = text.indexOf(url, offset);
        if (loc < offset)
          break;

        if (loc > offset)
          newText += text.substr(offset, loc - offset);

        // Strip delimiters off the end of URL
        url = url.replace(/\s$/, "");
        url = url.replace(/([),.']|&gt;|&quot;)$/, "");

        if (!url.length)
          continue;

        newText += "<a href=\"" + url + "\">" + url + "</a>";

        offset = loc + url.length;
      }

      newText += text.substr(offset, text.length - offset);

      text = newText;
    }

    return text;
  },

  getDecryptedMessage: function(contentType, includeHeaders) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: getDecryptedMessage: " + contentType + ", " + includeHeaders + "\n");

    if (!Icecrypt.msg.decryptedMessage)
      return "No decrypted message found!\n";

    var icecryptSvc = Icecrypt.getIcecryptSvc();
    if (!icecryptSvc)
      return "";

    var headerList = Icecrypt.msg.decryptedMessage.headerList;

    var statusLine = Icecrypt.msg.securityInfo ? Icecrypt.msg.securityInfo.statusLine : "";

    var contentData = "";

    var headerName;

    if (contentType == "message/rfc822") {
      // message/rfc822

      if (includeHeaders) {
        try {

          var msg = gFolderDisplay.selectedMessage;
          if (msg) {
            let msgHdr = {
              "From": msg.author,
              "Subject": msg.subject,
              "To": msg.recipients,
              "Cc": msg.ccList,
              "Date": IcecryptTime.getDateTime(msg.dateInSeconds, true, true)
            };


            if (gFolderDisplay.selectedMessageIsNews) {
              if (currentHeaderData.newsgroups) {
                msgHdr.Newsgroups = currentHeaderData.newsgroups.headerValue;
              }
            }

            for (let headerName in msgHdr) {
              if (msgHdr[headerName] && msgHdr[headerName].length > 0)
                contentData += headerName + ": " + msgHdr[headerName] + "\r\n";
            }

          }
        }
        catch (ex) {
          // the above seems to fail every now and then
          // so, here is the fallback
          for (let headerName in headerList) {
            let headerValue = headerList[headerName];
            contentData += headerName + ": " + headerValue + "\r\n";
          }
        }

        contentData += "Content-Type: text/plain";

        if (Icecrypt.msg.decryptedMessage.charset) {
          contentData += "; charset=" + Icecrypt.msg.decryptedMessage.charset;
        }

        contentData += "\r\n";
      }

      contentData += "\r\n";

      if (Icecrypt.msg.decryptedMessage.hasAttachments && (!Icecrypt.msg.decryptedMessage.attachmentsEncrypted)) {
        contentData += IcecryptData.convertFromUnicode(IcecryptLocale.getString("enigContentNote"), Icecrypt.msg.decryptedMessage.charset);
      }

      contentData += Icecrypt.msg.decryptedMessage.plainText;

    }
    else {
      // text/html or text/plain

      if (contentType == "text/html") {
        contentData += "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=" + Icecrypt.msg.decryptedMessage.charset + "\">\r\n";

        contentData += "<html><head></head><body>\r\n";
      }

      if (statusLine) {
        if (contentType == "text/html") {
          contentData += "<b>" + IcecryptLocale.getString("enigHeader") + "</b> " +
            this.escapeTextForHTML(statusLine, false) + "<br>\r\n<hr>\r\n";
        }
        else {
          contentData += IcecryptLocale.getString("enigHeader") + " " + statusLine + "\r\n\r\n";
        }
      }

      if (includeHeaders) {
        for (headerName in headerList) {
          let headerValue = headerList[headerName];

          if (headerValue) {
            if (contentType == "text/html") {
              contentData += "<b>" + this.escapeTextForHTML(headerName, false) + ":</b> " +
                this.escapeTextForHTML(headerValue, false) + "<br>\r\n";
            }
            else {
              contentData += headerName + ": " + headerValue + "\r\n";
            }
          }
        }
      }

      if (contentType == "text/html") {
        contentData += "<pre>" + this.escapeTextForHTML(Icecrypt.msg.decryptedMessage.plainText, false) + "</pre>\r\n";

        contentData += "</body></html>\r\n";

      }
      else {

        contentData += "\r\n" + Icecrypt.msg.decryptedMessage.plainText;
      }

      if (!(IcecryptOS.isDosLike())) {
        contentData = contentData.replace(/\r\n/g, "\n");
      }
    }

    return contentData;
  },


  msgDefaultPrint: function(elementId) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: this.msgDefaultPrint: " + elementId + "\n");

    goDoCommand(elementId.indexOf("printpreview") >= 0 ? "cmd_printpreview" : "cmd_print");
  },

  msgPrint: function(elementId) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: msgPrint: " + elementId + "\n");

    var contextMenu = (elementId.search("Context") > -1);

    if (!Icecrypt.msg.decryptedMessage || typeof(Icecrypt.msg.decryptedMessage) == "undefined") {
      this.msgDefaultPrint(elementId);
      return;
    }

    var mailNewsUrl = this.getCurrentMsgUrl();

    if (!mailNewsUrl) {
      this.msgDefaultPrint(elementId);
      return;
    }

    if (Icecrypt.msg.decryptedMessage.url != mailNewsUrl.spec) {
      Icecrypt.msg.decryptedMessage = null;
      this.msgDefaultPrint(elementId);
      return;
    }

    var icecryptSvc = Icecrypt.getIcecryptSvc();
    if (!icecryptSvc) {
      this.msgDefaultPrint(elementId);
      return;
    }

    // Note: Trying to print text/html content does not seem to work with
    //       non-ASCII chars
    var msgContent = this.getDecryptedMessage("message/rfc822", true);

    var uri = icecryptSvc.createMessageURI(Icecrypt.msg.decryptedMessage.url,
      "message/rfc822",
      "",
      msgContent,
      false);

    Icecrypt.msg.createdURIs.push(uri);

    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: msgPrint: uri=" + uri + "\n");

    var messageList = [uri];

    var printPreview = (elementId.indexOf("printpreview") >= 0);

    window.openDialog("chrome://messenger/content/msgPrintEngine.xul",
      "",
      "chrome,dialog=no,all,centerscreen",
      1, messageList, statusFeedback,
      printPreview, Components.interfaces.nsIMsgPrintEngine.MNAB_PRINTPREVIEW_MSG,
      window);

    return;
  },

  messageSave: function() {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageSave: \n");

    if (!Icecrypt.msg.decryptedMessage) {
      IcecryptDialog.alert(window, IcecryptLocale.getString("noDecrypted"));
      return;
    }

    var mailNewsUrl = this.getCurrentMsgUrl();

    if (!mailNewsUrl) {
      IcecryptDialog.alert(window, IcecryptLocale.getString("noMessage"));
      return;
    }

    if (Icecrypt.msg.decryptedMessage.url != mailNewsUrl.spec) {
      Icecrypt.msg.decryptedMessage = null;
      IcecryptDialog.alert(window, IcecryptLocale.getString("useButton"));
      return;
    }

    var saveFile = IcecryptDialog.filePicker(window, IcecryptLocale.getString("saveHeader"),
      Icecrypt.msg.lastSaveDir, true, "txt",
      null, ["Text files", "*.txt"]);
    if (!saveFile) return;

    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: messageSave: path=" + saveFile.path + "\n");

    if (saveFile.parent)
      Icecrypt.msg.lastSaveDir = IcecryptFiles.getFilePath(saveFile.parent);

    var textContent = this.getDecryptedMessage("text/plain", true);

    if (!IcecryptFiles.writeFileContents(saveFile.path, textContent, null)) {
      IcecryptDialog.alert(window, "Error in saving to file " + saveFile.path);
      return;
    }

    return;
  },

  msgDirectDecrypt: function(interactive, importOnly, contentEncoding, charset, signature,
    bufferSize, head, tail, msgUriSpec, callbackFunction) {
    IcecryptLog.WRITE("icecryptMessengerOverlay.js: msgDirectDecrypt: contentEncoding=" + contentEncoding + ", signature=" + signature + "\n");
    var mailNewsUrl = this.getCurrentMsgUrl();
    if (!mailNewsUrl)
      return;

    var callbackArg = {
      interactive: interactive,
      importOnly: importOnly,
      contentEncoding: contentEncoding,
      charset: charset,
      messageUrl: mailNewsUrl.spec,
      msgUriSpec: msgUriSpec,
      signature: signature,
      data: "",
      head: head,
      tail: tail,
      callbackFunction: callbackFunction
    };

    var msgSvc = messenger.messageServiceFromURI(msgUriSpec);

    var listener = {
      QueryInterface: XPCOMUtils.generateQI([Components.interfaces.nsIStreamListener]),
      onStartRequest: function() {
        this.data = "";
        this.inStream = Components.classes["@mozilla.org/scriptableinputstream;1"].
        createInstance(Components.interfaces.nsIScriptableInputStream);

      },
      onDataAvailable: function(req, sup, stream, offset, count) {
        this.inStream.init(stream);
        this.data += this.inStream.read(count);
      },
      onStopRequest: function() {
        var start = this.data.indexOf("-----BEGIN PGP");
        var end = this.data.indexOf("-----END PGP");

        if (start >= 0 && end > start) {
          var tStr = this.data.substr(end);
          var n = tStr.indexOf("\n");
          var r = tStr.indexOf("\r");
          var lEnd = -1;
          if (n >= 0 && r >= 0) {
            lEnd = Math.min(r, n);
          }
          else if (r >= 0) {
            lEnd = r;
          }
          else if (n >= 0)
            lEnd = n;

          if (lEnd >= 0) {
            end += lEnd;
          }

          callbackArg.data = this.data.substring(start, end + 1);
          IcecryptLog.DEBUG("icecryptMessengerOverlay.js: data: >" + callbackArg.data + "<\n");
          Icecrypt.msg.msgDirectCallback(callbackArg);
        }
      }
    };

    msgSvc.streamMessage(msgUriSpec,
      listener,
      msgWindow,
      null,
      false,
      null,
      false);

  },


  msgDirectCallback: function(callbackArg) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: msgDirectCallback: \n");

    var mailNewsUrl = Icecrypt.msg.getCurrentMsgUrl();
    var urlSpec = mailNewsUrl ? mailNewsUrl.spec : "";
    var newBufferSize = 0;

    var l = urlSpec.length;

    if (urlSpec.substr(0, l) != callbackArg.messageUrl.substr(0, l)) {
      IcecryptLog.ERROR("icecryptMessengerOverlay.js: msgDirectCallback: Message URL mismatch " + mailNewsUrl.spec + " vs. " + callbackArg.messageUrl + "\n");
      return;
    }

    var msgText = callbackArg.data;
    msgText = IcecryptData.convertFromUnicode(msgText, "UTF-8");

    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: msgDirectCallback: msgText='" + msgText + "'\n");

    var f = function(argList) {
      var msgText = argList[0];
      var cb = argList[1];
      cb.callbackFunction(msgText, cb.contentEncoding,
        cb.charset,
        cb.interactive,
        cb.importOnly,
        cb.messageUrl,
        cb.signature,
        3,
        cb.head,
        cb.tail,
        cb.msgUriSpec);
    };

    IcecryptEvents.dispatchEvent(f, 0, [msgText, callbackArg]);
  },


  verifyEmbeddedMsg: function(window, msgUrl, msgWindow, msgUriSpec, contentEncoding, event) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: verifyEmbeddedMsg: msgUrl" + msgUrl + "\n");

    var callbackArg = {
      data: "",
      window: window,
      msgUrl: msgUrl,
      msgWindow: msgWindow,
      msgUriSpec: msgUriSpec,
      contentEncoding: contentEncoding,
      event: event
    };

    var requestCallback = function _cb(data) {
      callbackArg.data = data;
      Icecrypt.msg.verifyEmbeddedCallback(callbackArg);
    };

    var bufferListener = IcecryptStreams.newStringStreamListener(requestCallback);

    var ioServ = Components.classes[IOSERVICE_CONTRACTID].getService(Components.interfaces.nsIIOService);

    var channel = ioServ.newChannelFromURI(msgUrl);

    channel.asyncOpen(bufferListener, msgUrl);
  },

  verifyEmbeddedCallback: function(callbackArg) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: verifyEmbeddedCallback: \n");


    // HACK for MS-EXCHANGE-Server Problem:
    // - now let's save the mail content for later processing
    if (this.buggyExchangeEmailContent == "???") {
      IcecryptLog.DEBUG("icecryptMessengerOverlay: verifyEmbeddedCallback: got broken MS-Exchange mime message\n");
      this.buggyExchangeEmailContent = callbackArg.data.replace(/^(\r?\n)*/, "");
      if (this.displayBuggyExchangeMail()) {
        return;
      }
    }

    // try inline PGP
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: verifyEmbeddedCallback: try inline PGP\n");

    Icecrypt.msg.messageParse(!callbackArg.event, false, callbackArg.contentEncoding, callbackArg.msgUriSpec);
  },


  revealAttachments: function(index) {
    if (!index) index = 0;

    if (index < currentAttachments.length) {
      this.handleAttachment("revealName/" + index.toString(), currentAttachments[index]);
    }
  },


  // handle a selected attachment (decrypt & open or save)
  handleAttachmentSel: function(actionType) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: handleAttachmentSel: actionType=" + actionType + "\n");
    var selectedAttachments;
    var anAttachment;

    // Thunderbird
    var contextMenu = document.getElementById('attachmentItemContext');

    if (contextMenu) {
      // Thunderbird
      selectedAttachments = contextMenu.attachments;
      anAttachment = selectedAttachments[0];
    }
    else {
      // SeaMonkey
      contextMenu = document.getElementById('attachmentListContext');
      selectedAttachments = document.getElementById('attachmentList').selectedItems;
      anAttachment = selectedAttachments[0].attachment;
    }

    switch (actionType) {
      case "saveAttachment":
      case "openAttachment":
      case "importKey":
      case "revealName":
        this.handleAttachment(actionType, anAttachment);
        break;
      case "verifySig":
        this.verifyDetachedSignature(anAttachment);
        break;
    }
  },

  /**
   * save the original file plus the signature file to disk and then verify the signature
   */
  verifyDetachedSignature: function(anAttachment) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: verifyDetachedSignature: url=" + anAttachment.url + "\n");

    var icecryptSvc = Icecrypt.getIcecryptSvc();
    if (!icecryptSvc) return;

    var origAtt, signatureAtt;

    if ((this.getAttachmentName(anAttachment).search(/\.sig$/i) > 0) ||
      (anAttachment.contentType.search(/^application\/pgp-signature/i) === 0)) {
      // we have the .sig file; need to know the original file;

      signatureAtt = anAttachment;
      var origName = this.getAttachmentName(anAttachment).replace(/\.sig$/i, "");

      for (let i = 0; i < currentAttachments.length; i++) {
        if (origName == this.getAttachmentName(currentAttachments[i])) {
          origAtt = currentAttachments[i];
          break;
        }
      }
    }
    else {
      // we have a supposedly original file; need to know the .sig file;

      origAtt = anAttachment;
      var sigName = this.getAttachmentName(anAttachment) + ".sig";

      for (let i = 0; i < currentAttachments.length; i++) {
        if (sigName == this.getAttachmentName(currentAttachments[i])) {
          signatureAtt = currentAttachments[i];
          break;
        }
      }
    }

    if (!signatureAtt) {
      IcecryptDialog.alert(window, IcecryptLocale.getString("attachment.noMatchToSignature", [this.getAttachmentName(origAtt)]));
      return;
    }
    if (!origAtt) {
      IcecryptDialog.alert(window, IcecryptLocale.getString("attachment.noMatchFromSignature", [this.getAttachmentName(signatureAtt)]));
      return;
    }

    // open
    var tmpDir = IcecryptFiles.getTempDir();
    var outFile1, outFile2;
    outFile1 = Components.classes[LOCAL_FILE_CONTRACTID].
    createInstance(Components.interfaces.nsIFile);
    outFile1.initWithPath(tmpDir);
    if (!(outFile1.isDirectory() && outFile1.isWritable())) {
      IcecryptDialog.alert(window, IcecryptLocale.getString("noTempDir"));
      return;
    }
    outFile1.append(this.getAttachmentName(origAtt));
    outFile1.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0x180); // equals 0800
    this.writeUrlToFile(origAtt.url, outFile1);

    outFile2 = Components.classes[LOCAL_FILE_CONTRACTID].
    createInstance(Components.interfaces.nsIFile);
    outFile2.initWithPath(tmpDir);
    outFile2.append(this.getAttachmentName(signatureAtt));
    outFile2.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0x180); // equals 0800
    this.writeUrlToFile(signatureAtt.url, outFile2);

    var statusFlagsObj = {};
    var errorMsgObj = {};
    var r = icecryptSvc.verifyAttachment(window, outFile1, outFile2, statusFlagsObj, errorMsgObj);

    if (r === 0)
      IcecryptDialog.alert(window, IcecryptLocale.getString("signature.verifiedOK", [this.getAttachmentName(origAtt)]) + "\n\n" + errorMsgObj.value);
    else
      IcecryptDialog.alert(window, IcecryptLocale.getString("signature.verifyFailed", [this.getAttachmentName(origAtt)]) + "\n\n" +
        errorMsgObj.value);

    outFile1.remove(false);
    outFile2.remove(false);
  },

  writeUrlToFile: function(srcUrl, outFile) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: writeUrlToFile: outFile=" + outFile.path + "\n");
    var ioServ = Components.classes[IOSERVICE_CONTRACTID].
    getService(Components.interfaces.nsIIOService);
    var msgUri = ioServ.newURI(srcUrl, null, null);
    var channel = ioServ.newChannelFromURI(msgUri);
    var istream = channel.open();

    var fstream = Components.classes["@mozilla.org/network/safe-file-output-stream;1"]
      .createInstance(Components.interfaces.nsIFileOutputStream);
    var buffer = Components.classes["@mozilla.org/network/buffered-output-stream;1"]
      .createInstance(Components.interfaces.nsIBufferedOutputStream);
    fstream.init(outFile, 0x04 | 0x08 | 0x20, 0x180, 0); // write, create, truncate
    buffer.init(fstream, 8192);

    while (istream.available() > 0) {
      buffer.writeFrom(istream, istream.available());
    }

    // Close the output streams
    if (buffer instanceof Components.interfaces.nsISafeOutputStream)
      buffer.finish();
    else
      buffer.close();

    if (fstream instanceof Components.interfaces.nsISafeOutputStream)
      fstream.finish();
    else
      fstream.close();

    // Close the input stream
    istream.close();
  },

  handleAttachment: function(actionType, anAttachment) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: handleAttachment: actionType=" + actionType + ", anAttachment(url)=" + anAttachment.url + "\n");

    var argumentsObj = {
      actionType: actionType,
      attachment: anAttachment,
      forceBrowser: false,
      data: ""
    };

    var f = function _cb(data) {
      argumentsObj.data = data;
      Icecrypt.msg.decryptAttachmentCallback([argumentsObj]);
    };

    var bufferListener = IcecryptStreams.newStringStreamListener(f);
    var ioServ = Components.classes[IOSERVICE_CONTRACTID].getService(Components.interfaces.nsIIOService);
    var msgUri = ioServ.newURI(argumentsObj.attachment.url, null, null);

    var channel = ioServ.newChannelFromURI(msgUri);
    channel.asyncOpen(bufferListener, msgUri);
  },

  setAttachmentName: function(attachment, newLabel, index) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: setAttachmentName (" + newLabel + "):\n");

    var attList = document.getElementById("attachmentList");
    if (attList) {
      var attNode = attList.firstChild;
      while (attNode) {
        // TB <= 9
        if (attNode.getAttribute("attachmentUrl") == attachment.url)
          attNode.setAttribute("label", newLabel);
        // TB >= 10
        if (attNode.getAttribute("name") == attachment.name)
          attNode.setAttribute("name", newLabel);
        attNode = attNode.nextSibling;
      }
    }

    if (typeof(attachment.displayName) == "undefined") {
      attachment.name = newLabel;
    }
    else
      attachment.displayName = newLabel;

    if (index && index.length > 0) {
      this.revealAttachments(parseInt(index, 10) + 1);
    }
  },

  decryptAttachmentCallback: function(cbArray) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: decryptAttachmentCallback:\n");

    var callbackArg = cbArray[0];
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    var exitCodeObj = {};
    var statusFlagsObj = {};
    var errorMsgObj = {};
    var exitStatus = -1;

    var icecryptSvc = Icecrypt.getIcecryptSvc();
    var outFile;
    var origFilename;
    var rawFileName = Icecrypt.msg.getAttachmentName(callbackArg.attachment).replace(/\.(asc|pgp|gpg)$/i, "");

    if (callbackArg.actionType != "importKey") {
      origFilename = IcecryptAttachment.getFileName(window, callbackArg.data);
      if (origFilename && origFilename.length > rawFileName.length) rawFileName = origFilename;
    }

    if (callbackArg.actionType == "saveAttachment") {
      outFile = IcecryptDialog.filePicker(window, IcecryptLocale.getString("saveAttachmentHeader"),
        Icecrypt.msg.lastSaveDir, true, "",
        rawFileName, null);
      if (!outFile) return;
    }
    else if (callbackArg.actionType.substr(0, 10) == "revealName") {
      if (origFilename && origFilename.length > 0) {
        Icecrypt.msg.setAttachmentName(callbackArg.attachment, origFilename + ".pgp", callbackArg.actionType.substr(11, 10));
      }
      Icecrypt.msg.setAttachmentReveal(null);
      return;
    }
    else {
      // open
      var tmpDir = IcecryptFiles.getTempDir();
      try {
        outFile = Components.classes[LOCAL_FILE_CONTRACTID].createInstance(Components.interfaces.nsIFile);
        outFile.initWithPath(tmpDir);
        if (!(outFile.isDirectory() && outFile.isWritable())) {
          errorMsgObj.value = IcecryptLocale.getString("noTempDir");
          return;
        }
        outFile.append(rawFileName);
        outFile.createUnique(Components.interfaces.nsIFile.NORMAL_FILE_TYPE, 0x180); // equals 0800
      }
      catch (ex) {
        errorMsgObj.value = IcecryptLocale.getString("noTempDir");
        return;
      }
    }

    if (callbackArg.actionType == "importKey") {
      var preview = IcecryptKey.getKeyListFromKeyBlock(callbackArg.data, errorMsgObj);

      if (errorMsgObj.value === "") {
        if (preview.length > 0) {
          if (preview.length == 1) {
            exitStatus = IcecryptDialog.confirmDlg(window, IcecryptLocale.getString("doImportOne", [preview[0].name, preview[0].id]));
          }
          else {
            exitStatus = IcecryptDialog.confirmDlg(window,
              IcecryptLocale.getString("doImportMultiple", [
                preview.map(function(a) {
                  return "\t" + a.name + " (" + a.id + ")";
                }).
                join("\n")
              ]));
          }

          if (exitStatus) {
            try {
              exitStatus = IcecryptKeyRing.importKey(parent, false, callbackArg.data, "", errorMsgObj);
            }
            catch (ex) {}

            if (exitStatus === 0) {
              var keyList = preview.map(function(a) {
                return a.id;
              });
              IcecryptDialog.keyImportDlg(window, keyList);
            }
            else {
              IcecryptDialog.alert(window, IcecryptLocale.getString("failKeyImport") + "\n" + errorMsgObj.value);
            }
          }
        }
        else {
          IcecryptDialog.alert(window, IcecryptLocale.getString("noKeyFound") + "\n" + errorMsgObj.value);
        }
      }
      else {
        IcecryptDialog.alert(window, IcecryptLocale.getString("previewFailed") + "\n" + errorMsgObj.value);
      }

      return;
    }

    exitStatus = icecryptSvc.decryptAttachment(window, outFile,
      Icecrypt.msg.getAttachmentName(callbackArg.attachment),
      callbackArg.data,
      exitCodeObj, statusFlagsObj,
      errorMsgObj);

    if ((!exitStatus) || exitCodeObj.value !== 0) {
      exitStatus = false;
      if ((statusFlagsObj.value & nsIIcecrypt.DECRYPTION_OKAY) &&
        (statusFlagsObj.value & nsIIcecrypt.UNVERIFIED_SIGNATURE)) {

        if (callbackArg.actionType == "openAttachment") {
          exitStatus = IcecryptDialog.confirmDlg(window, IcecryptLocale.getString("decryptOkNoSig"), IcecryptLocale.getString("msgOvl.button.contAnyway"));
        }
        else {
          IcecryptDialog.alert(window, IcecryptLocale.getString("decryptOkNoSig"));
        }
      }
      else {
        IcecryptDialog.alert(window, IcecryptLocale.getString("failedDecrypt") + "\n\n" + errorMsgObj.value);
        exitStatus = false;
      }
    }
    if (exitStatus) {
      if (statusFlagsObj.value & nsIIcecrypt.IMPORTED_KEY) {

        if (exitCodeObj.keyList) {
          let importKeyList = exitCodeObj.keyList.map(function(a) {
            return a.id;
          });
          IcecryptDialog.keyImportDlg(window, importKeyList);
        }
      }
      else if (statusFlagsObj.value & nsIIcecrypt.DISPLAY_MESSAGE) {
        HandleSelectedAttachments('open');
      }
      else if ((statusFlagsObj.value & nsIIcecrypt.DISPLAY_MESSAGE) ||
        (callbackArg.actionType == "openAttachment")) {
        var ioServ = Components.classes[IOSERVICE_CONTRACTID].getService(Components.interfaces.nsIIOService);
        var outFileUri = ioServ.newFileURI(outFile);
        var fileExt = outFile.leafName.replace(/(.*\.)(\w+)$/, "$2");
        if (fileExt && !callbackArg.forceBrowser) {
          var extAppLauncher = Components.classes["@mozilla.org/mime;1"].getService(Components.interfaces.nsPIExternalAppLauncher);
          extAppLauncher.deleteTemporaryFileOnExit(outFile);

          try {
            var mimeService = Components.classes["@mozilla.org/mime;1"].getService(Components.interfaces.nsIMIMEService);
            var fileMimeType = mimeService.getTypeFromFile(outFile);
            var fileMimeInfo = mimeService.getFromTypeAndExtension(fileMimeType, fileExt);

            fileMimeInfo.launchWithFile(outFile);
          }
          catch (ex) {
            // if the attachment file type is unknown, an exception is thrown,
            // so let it be handled by a browser window
            Icecrypt.msg.loadExternalURL(outFileUri.asciiSpec);
          }
        }
        else {
          // open the attachment using an external application
          Icecrypt.msg.loadExternalURL(outFileUri.asciiSpec);
        }
      }
    }
  },

  loadExternalURL: function(url) {
    if (IcecryptApp.isSuite()) {
      Icecrypt.msg.loadURLInNavigatorWindow(url, true);
    }
    else {
      messenger.launchExternalURL(url);
    }
  },

  // retrieves the most recent navigator window (opens one if need be)
  loadURLInNavigatorWindow: function(url, aOpenFlag) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: loadURLInNavigatorWindow: " + url + ", " + aOpenFlag + "\n");

    var navWindow;

    // if this is a browser window, just use it
    if ("document" in top) {
      var possibleNavigator = top.document.getElementById("main-window");
      if (possibleNavigator &&
        possibleNavigator.getAttribute("windowtype") == "navigator:browser")
        navWindow = top;
    }

    // if not, get the most recently used browser window
    if (!navWindow) {
      var wm;
      wm = Components.classes["@mozilla.org/appshell/window-mediator;1"].getService(
        Components.interfaces.nsIWindowMediator);
      navWindow = wm.getMostRecentWindow("navigator:browser");
    }

    if (navWindow) {

      if ("loadURI" in navWindow)
        navWindow.loadURI(url);
      else
        navWindow._content.location.href = url;

    }
    else if (aOpenFlag) {
      // if no browser window available and it's ok to open a new one, do so
      navWindow = window.open(url, "Icecrypt");
    }

    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: loadURLInNavigatorWindow: navWindow=" + navWindow + "\n");

    return navWindow;
  },

  // handle double click events on Attachments
  enigAttachmentListClick: function(elementId, event) {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: enigAttachmentListClick: event=" + event + "\n");

    var attachment = event.target.attachment;
    if (this.checkEncryptedAttach(attachment)) {
      if (event.button === 0 && event.detail == 2) { // double click
        this.handleAttachment("openAttachment", attachment);
        event.stopPropagation();
        return true;
      }
    }
    return false;
  },

  // create a decrypted copy of all selected messages in a target folder

  decryptToFolder: function(destFolder) {
    let msgHdrs = gFolderDisplay ? gFolderDisplay.selectedMessages : null;
    if (!msgHdrs || msgHdrs.length === 0) return;

    IcecryptDecryptPermanently.dispatchMessages(msgHdrs, destFolder.URI, false, false);
  },

  importAttachedKeys: function() {
    IcecryptLog.DEBUG("icecryptMessengerOverlay.js: importAttachedKeys\n");

    let keyFound = false;
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    for (let i in currentAttachments) {
      if (currentAttachments[i].contentType.search(/application\/pgp-keys/i) >= 0) {
        // found attached key
        this.handleAttachment("importKey", currentAttachments[i]);
        keyFound = true;
      }
    }

    return keyFound;
  },

  importKeyFromKeyserver: function() {
    var pubKeyId = "0x" + Icecrypt.msg.securityInfo.keyId;
    var inputObj = {
      searchList: [pubKeyId],
      autoKeyServer: IcecryptPrefs.getPref("autoKeyServerSelection") ? IcecryptPrefs.getPref("keyserver").split(/[ ,;]/g)[0] : null
    };
    var resultObj = {};
    IcecryptWindows.downloadKeys(window, inputObj, resultObj);


    if (resultObj.importedKeys > 0) {
      return true;
    }
  },

  // download or import keys
  handleUnknownKey: function() {
    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    let imported = false;
    // handline keys embedded in message body

    if (Icecrypt.msg.securityInfo.statusFlags & nsIIcecrypt.INLINE_KEY) {
      return Icecrypt.msg.messageDecrypt(null, false);
    }

    imported = this.importAttachedKeys();
    if (!imported) imported = this.importKeyFromKeyserver();

    if (imported) this.messageReload(false);
  }
};

window.addEventListener("load", Icecrypt.msg.messengerStartup.bind(Icecrypt.msg), false);
