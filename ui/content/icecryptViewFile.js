/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/* global Components: false */

"use strict";

Components.utils.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Components.utils.import("resource://icecrypt/dialog.jsm"); /*global IcecryptDialog: false */
Components.utils.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Components.utils.import("resource://icecrypt/files.jsm"); /*global IcecryptFiles: false */
Components.utils.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Components.utils.import("resource://icecrypt/windows.jsm"); /*global IcecryptWindows: false */
Components.utils.import("resource://icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */


var logFileData; // global definition of log file data to be able to save
// same data as displayed

function saveLogFile() {
  let fileObj = IcecryptDialog.filePicker(window, IcecryptLocale.getString("saveLogFile.title"), null,
    true, "txt");

  IcecryptFiles.writeFileContents(fileObj, logFileData, null);

}

function enigLoadPage() {
  IcecryptLog.DEBUG("icecryptHelp.js: enigLoadPage\n");
  IcecryptCore.getService();

  var contentFrame = IcecryptWindows.getFrame(window, "contentFrame");
  if (!contentFrame)
    return;

  var winOptions = getWindowOptions();

  if ("fileUrl" in winOptions) {
    contentFrame.document.location.href = winOptions.fileUrl;
  }

  if ("viewLog" in winOptions) {
    let cf = document.getElementById("contentFrame");
    cf.setAttribute("collapsed", "true");

    let cb = document.getElementById("contentBox");
    logFileData = IcecryptLog.getLogData(IcecryptCore.version, IcecryptPrefs);
    cb.value = logFileData;

    let cfb = document.getElementById("logFileBox");
    cfb.removeAttribute("collapsed");
  }

  if ("title" in winOptions) {
    document.getElementById("IcecryptViewFile").setAttribute("title", winOptions.title);
  }
}

function getWindowOptions() {
  var winOptions = [];
  if (window.location.search) {
    var optList = window.location.search.substr(1).split(/&/);
    for (var i = 0; i < optList.length; i++) {
      var anOption = optList[i].split(new RegExp("="));
      winOptions[anOption[0]] = unescape(anOption[1]);
    }
  }
  return winOptions;
}
