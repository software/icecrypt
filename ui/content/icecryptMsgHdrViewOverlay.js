/*global Components: false, IcecryptWindows: false, IcecryptLocale: false, IcecryptPrefs: false, IcecryptTime: false */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


"use strict";

/* globals from Thunderbird: */
/* global gFolderDisplay: false, currentAttachments: false, gSMIMEContainer: false, gSignedUINode: false, gEncryptedUINode: false */
/* global gDBView: false, msgWindow: false, messageHeaderSink: false: gMessageListeners: false, findEmailNodeFromPopupNode: true */
/* global gExpandedHeaderView: false, gMessageListeners: false, onShowAttachmentItemContextMenu: false, onShowAttachmentContextMenu: false */
/* global attachmentList: false, MailOfflineMgr: false, currentHeaderData: false, ContentTypeIsSMIME: false */

Components.utils.import("resource://icecrypt/core.jsm"); /*global IcecryptCore: false */
Components.utils.import("resource://icecrypt/funcs.jsm"); /*global IcecryptFuncs: false */
Components.utils.import("resource://icecrypt/mimeVerify.jsm"); /*global IcecryptVerify: false */
Components.utils.import("resource://icecrypt/log.jsm"); /*global IcecryptLog: false */
Components.utils.import("resource://icecrypt/prefs.jsm"); /*global IcecryptPrefs: false */
Components.utils.import("resource://icecrypt/locale.jsm"); /*global IcecryptLocale: false */
Components.utils.import("resource://icecrypt/windows.jsm"); /*global IcecryptWindows: false */
Components.utils.import("resource://icecrypt/dialog.jsm"); /*global IcecryptDialog: false */
Components.utils.import("resource://icecrypt/time.jsm"); /*global IcecryptTime: false */
Components.utils.import("resource://icecrypt/gpg.jsm"); /*global IcecryptGpg: false */
Components.utils.import("resource://icecrypt/key.jsm"); /*global IcecryptKey: false */
Components.utils.import("resource://icecrypt/keyRing.jsm"); /*global IcecryptKeyRing: false */
Components.utils.import("resource://icecrypt/uris.jsm"); /*global IcecryptURIs: false */
Components.utils.import("resource://icecrypt/constants.jsm"); /*global IcecryptConstants: false */
Components.utils.import("resource://icecrypt/data.jsm"); /*global IcecryptData: false */
Components.utils.import("resource://icecrypt/clipboard.jsm"); /*global IcecryptClipboard: false */
Components.utils.import("resource://icecrypt/mime.jsm"); /*global IcecryptMime: false */

if (!Icecrypt) var Icecrypt = {};

const EC = IcecryptCore;

Icecrypt.hdrView = {

  statusBar: null,
  icecryptBox: null,
  lastEncryptedMsgKey: null,


  hdrViewLoad: function() {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: this.hdrViewLoad\n");

    // Override SMIME ui
    var signedHdrElement = document.getElementById("signedHdrIcon");
    if (signedHdrElement) {
      signedHdrElement.setAttribute("onclick", "Icecrypt.msg.viewSecurityInfo(event, true);");
    }

    var encryptedHdrElement = document.getElementById("encryptedHdrIcon");
    if (encryptedHdrElement) {
      encryptedHdrElement.setAttribute("onclick", "Icecrypt.msg.viewSecurityInfo(event, true);");
    }

    this.statusBar = document.getElementById("icecrypt-status-bar");
    this.icecryptBox = document.getElementById("icecryptBox");

    var addrPopup = document.getElementById("emailAddressPopup");
    if (addrPopup) {
      var attr = addrPopup.getAttribute("onpopupshowing");
      attr = "IcecryptFuncs.collapseAdvanced(this, 'hidden'); " + attr;
      addrPopup.setAttribute("onpopupshowing", attr);
    }
  },


  statusBarHide: function() {
    try {
      this.statusBar.removeAttribute("signed");
      this.statusBar.removeAttribute("encrypted");
      this.icecryptBox.setAttribute("collapsed", "true");
      Icecrypt.msg.setAttachmentReveal(null);
      if (Icecrypt.msg.securityInfo) {
        Icecrypt.msg.securityInfo.statusFlags = 0;
        Icecrypt.msg.securityInfo.msgSigned = 0;
        Icecrypt.msg.securityInfo.msgEncrypted = 0;
      }

    }
    catch (ex) {}
  },

  // Match the userId from gpg to the sender's from address
  matchUidToSender: function(userId) {
    if (!gFolderDisplay.selectedMessage) {
      return userId;
    }

    var fromAddr = gFolderDisplay.selectedMessage.author;
    try {
      fromAddr = IcecryptFuncs.stripEmail(fromAddr);
    }
    catch (ex) {}

    var userIdList = userId.split(/\n/);
    try {
      let i;
      for (i = 0; i < userIdList.length; i++) {
        if (fromAddr.toLowerCase() == IcecryptFuncs.stripEmail(userIdList[i]).toLowerCase()) {
          userId = userIdList[i];
          break;
        }
      }
      if (i >= userIdList.length) userId = userIdList[0];
    }
    catch (ex) {
      userId = userIdList[0];
    }
    return userId;
  },


  setStatusText: function(txt) {
    let s = document.getElementById("icecryptStatusText");
    s.firstChild.data = txt;
  },

  updateHdrIcons: function(exitCode, statusFlags, keyId, userId, sigDetails, errorMsg, blockSeparation, encToDetails, xtraStatus, encMimePartNumber) {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: this.updateHdrIcons: exitCode=" + exitCode + ", statusFlags=" + statusFlags + ", keyId=" + keyId + ", userId=" + userId + ", " + errorMsg +
      "\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    this.statusBar = document.getElementById("icecrypt-status-bar");
    this.icecryptBox = document.getElementById("icecryptBox");

    if (gFolderDisplay.selectedMessageUris && gFolderDisplay.selectedMessageUris.length > 0) {
      this.lastEncryptedMsgKey = gFolderDisplay.selectedMessageUris[0];
    }

    if (!errorMsg) errorMsg = "";

    var replaceUid = null;
    if (userId && (userId.indexOf("\n") >= 0)) {
      replaceUid = this.matchUidToSender(userId);
    }
    else {
      replaceUid = userId;
    }

    if (Icecrypt.msg.savedHeaders && (Icecrypt.msg.savedHeaders["x-pgp-encoding-format"].search(/partitioned/i) === 0)) {
      if (currentAttachments && currentAttachments.length) {
        Icecrypt.msg.setAttachmentReveal(currentAttachments);
      }
    }

    if (userId && replaceUid) {
      // no EnigConvertGpgToUnicode() here; strings are already UTF-8
      replaceUid = replaceUid.replace(/\\[xe]3a/gi, ":");
      errorMsg = errorMsg.replace(userId, replaceUid);
    }

    var errorLines = "";
    var fullStatusInfo = "";

    if (exitCode == IcecryptConstants.POSSIBLE_PGPMIME) {
      exitCode = 0;
    }
    else {
      if (errorMsg) {
        // no EnigConvertGpgToUnicode() here; strings are already UTF-8
        errorLines = errorMsg.split(/\r?\n/);
        fullStatusInfo = errorMsg;
      }
    }

    if (errorLines && (errorLines.length > 22)) {
      // Retain only first twenty lines and last two lines of error message
      var lastLines = errorLines[errorLines.length - 2] + "\n" +
        errorLines[errorLines.length - 1] + "\n";

      while (errorLines.length > 20)
        errorLines.pop();

      errorMsg = errorLines.join("\n") + "\n...\n" + lastLines;
    }

    var statusInfo = "";
    var statusLine = "";
    var statusArr = [];

    if (statusFlags & nsIIcecrypt.NODATA) {
      if (statusFlags & nsIIcecrypt.PGP_MIME_SIGNED)
        statusFlags |= nsIIcecrypt.UNVERIFIED_SIGNATURE;

      if (statusFlags & nsIIcecrypt.PGP_MIME_ENCRYPTED)
        statusFlags |= nsIIcecrypt.DECRYPTION_INCOMPLETE;
    }

    if (!(statusFlags & nsIIcecrypt.PGP_MIME_ENCRYPTED)) {
      encMimePartNumber = "";
    }

    if (!IcecryptPrefs.getPref("displayPartiallySigned")) {
      if ((statusFlags & (nsIIcecrypt.PARTIALLY_PGP)) &&
        (statusFlags & (nsIIcecrypt.BAD_SIGNATURE))) {
        statusFlags &= ~(nsIIcecrypt.BAD_SIGNATURE | nsIIcecrypt.PARTIALLY_PGP);
        if (statusFlags === 0) {
          errorMsg = "";
          fullStatusInfo = "";
        }
      }
    }

    var msgSigned = (statusFlags & (nsIIcecrypt.BAD_SIGNATURE |
      nsIIcecrypt.GOOD_SIGNATURE |
      nsIIcecrypt.EXPIRED_KEY_SIGNATURE |
      nsIIcecrypt.EXPIRED_SIGNATURE |
      nsIIcecrypt.UNVERIFIED_SIGNATURE |
      nsIIcecrypt.REVOKED_KEY |
      nsIIcecrypt.EXPIRED_KEY_SIGNATURE |
      nsIIcecrypt.EXPIRED_SIGNATURE));
    var msgEncrypted = (statusFlags & (nsIIcecrypt.DECRYPTION_OKAY |
      nsIIcecrypt.DECRYPTION_INCOMPLETE |
      nsIIcecrypt.DECRYPTION_FAILED));

    if (msgSigned && (statusFlags & nsIIcecrypt.IMPORTED_KEY)) {
      statusFlags &= (~nsIIcecrypt.IMPORTED_KEY);
    }

    if (!(statusFlags & nsIIcecrypt.DECRYPTION_FAILED) &&
      ((!(statusFlags & (nsIIcecrypt.DECRYPTION_INCOMPLETE |
          nsIIcecrypt.UNVERIFIED_SIGNATURE |
          nsIIcecrypt.DECRYPTION_FAILED |
          nsIIcecrypt.BAD_SIGNATURE))) ||
        (statusFlags & nsIIcecrypt.DISPLAY_MESSAGE) &&
        !(statusFlags & nsIIcecrypt.UNVERIFIED_SIGNATURE)) &&
      !(statusFlags & nsIIcecrypt.IMPORTED_KEY)) {
      // normal exit / display message
      statusLine = errorMsg;
      statusInfo = statusLine;

      if (sigDetails) {
        var detailArr = sigDetails.split(/ /);

        let dateTime = IcecryptTime.getDateTime(detailArr[2], true, true);
        var txt = IcecryptLocale.getString("keyAndSigDate", [keyId.substr(-8, 8), dateTime]);
        statusArr.push(txt);
        statusInfo += "\n" + txt;
        var fpr = "";
        if (detailArr.length >= 10) {
          fpr = IcecryptKey.formatFpr(detailArr[9]);
        }
        else {
          fpr = IcecryptKey.formatFpr(detailArr[0]);
        }
        if (fpr) {
          statusInfo += "\n" + IcecryptLocale.getString("keyFpr", [fpr]);
        }
        if (detailArr.length > 7) {
          var signingAlg = IcecryptGpg.signingAlgIdToString(detailArr[6]);
          var hashAlg = IcecryptGpg.hashAlgIdToString(detailArr[7]);

          statusInfo += "\n\n" + IcecryptLocale.getString("usedAlgorithms", [signingAlg, hashAlg]);
        }
      }
      fullStatusInfo = statusInfo;

    }
    else {
      // no normal exit / don't display message
      // - process failed decryptions first because they imply bad signature handling
      if (statusFlags & nsIIcecrypt.BAD_PASSPHRASE) {
        statusInfo = IcecryptLocale.getString("badPhrase");
        statusLine = statusInfo + IcecryptLocale.getString("clickDecryptRetry");
      }
      else if (statusFlags & nsIIcecrypt.DECRYPTION_FAILED) {
        if (statusFlags & nsIIcecrypt.MISSING_MDC & !IcecryptPrefs.getPref("classicMode")) {
          statusInfo = IcecryptLocale.getString("missingMdcError");
          statusLine = statusInfo;
        }
        else if (statusFlags & nsIIcecrypt.MISSING_PASSPHRASE) {
          statusInfo = IcecryptLocale.getString("missingPassphrase");
          statusLine = statusInfo + IcecryptLocale.getString("clickDecryptRetry");
        }
        else if (statusFlags & nsIIcecrypt.NO_SECKEY) {
          statusInfo = IcecryptLocale.getString("needKey");
        }
        else {
          statusInfo = IcecryptLocale.getString("failedDecrypt");
        }
        statusLine = statusInfo + IcecryptLocale.getString("clickDetailsButton");
      }
      else if (statusFlags & nsIIcecrypt.UNVERIFIED_SIGNATURE) {
        statusInfo = IcecryptLocale.getString("unverifiedSig");
        if (keyId) {
          statusLine = statusInfo + IcecryptLocale.getString("clickImportButton");
        }
        else {
          statusLine = statusInfo + IcecryptLocale.getString("keyTypeUnsupported");
        }
      }
      else if (statusFlags & (nsIIcecrypt.BAD_SIGNATURE |
          nsIIcecrypt.EXPIRED_SIGNATURE |
          nsIIcecrypt.EXPIRED_KEY_SIGNATURE)) {
        statusInfo = IcecryptLocale.getString("unverifiedSig");
        statusLine = statusInfo + IcecryptLocale.getString("clickDetailsButton");
      }
      else if (statusFlags & nsIIcecrypt.DECRYPTION_INCOMPLETE) {
        statusInfo = IcecryptLocale.getString("incompleteDecrypt");
        statusLine = statusInfo + IcecryptLocale.getString("clickDetailsButton");
      }
      else if (statusFlags & nsIIcecrypt.IMPORTED_KEY) {
        statusLine = "";
        statusInfo = "";
        IcecryptDialog.alert(window, errorMsg);
      }
      else {
        statusInfo = IcecryptLocale.getString("failedDecryptVerify");
        statusLine = statusInfo + IcecryptLocale.getString("viewInfo");
      }
      // add key infos if available
      if (keyId) {
        var si = IcecryptLocale.getString("unverifiedSig"); // "Unverified signature"
        if (statusInfo === "") {
          statusInfo += si;
          statusLine = si + IcecryptLocale.getString("clickDetailsButton");
        }
        if (statusFlags & nsIIcecrypt.UNVERIFIED_SIGNATURE) {
          statusInfo += "\n" + IcecryptLocale.getString("keyNeeded", [keyId]); // "public key ... needed"
        }
        else {
          statusInfo += "\n" + IcecryptLocale.getString("keyUsed", [keyId]); // "public key ... used"
        }
      }
      statusInfo += "\n\n" + errorMsg;
    }

    if (statusFlags & nsIIcecrypt.DECRYPTION_OKAY ||
      (this.statusBar.getAttribute("encrypted") == "ok")) {
      var statusMsg;
      if (xtraStatus && xtraStatus == "buggyMailFormat") {
        statusMsg = IcecryptLocale.getString("decryptedMsgWithFormatError");
      }
      else {
        statusMsg = IcecryptLocale.getString("decryptedMsg");
      }
      if (!statusInfo) {
        statusInfo = statusMsg;
      }
      else {
        statusInfo = statusMsg + "\n" + statusInfo;
      }
      if (!statusLine) {
        statusLine = statusInfo;
      }
      else {
        statusLine = statusMsg + "; " + statusLine;
      }
    }

    if (IcecryptPrefs.getPref("displayPartiallySigned")) {
      if (statusFlags & nsIIcecrypt.PARTIALLY_PGP) {
        if (msgSigned && msgEncrypted) {
          statusLine = IcecryptLocale.getString("msgPart", [IcecryptLocale.getString("msgSignedAndEnc")]);
          statusLine += IcecryptLocale.getString("clickDetailsButton");
          statusInfo = IcecryptLocale.getString("msgPart", [IcecryptLocale.getString("msgSigned")]) +
            "\n" + statusInfo;
        }
        else if (msgEncrypted) {
          statusLine = IcecryptLocale.getString("msgPart", [IcecryptLocale.getString("msgEncrypted")]);
          statusLine += IcecryptLocale.getString("clickDetailsButton");
          statusInfo = IcecryptLocale.getString("msgPart", [IcecryptLocale.getString("msgEncrypted")]) +
            "\n" + statusInfo;
        }
        else if (msgSigned) {
          if (statusFlags & nsIIcecrypt.UNVERIFIED_SIGNATURE) {
            statusLine = IcecryptLocale.getString("msgPart", [IcecryptLocale.getString("msgSignedUnkownKey")]);
            if (keyId) {
              statusLine += IcecryptLocale.getString("clickImportButton");
            }
            else {
              statusLine += IcecryptLocale.getString("keyTypeUnsupported");
            }
          }
          else {
            statusLine = IcecryptLocale.getString("msgPart", [IcecryptLocale.getString("msgSigned")]);
            statusLine += IcecryptLocale.getString("clickDetailsButton");
          }
          statusInfo = IcecryptLocale.getString("msgPart", [IcecryptLocale.getString("msgSigned")]) +
            "\n" + statusInfo;
        }
      }
    }

    // if we have parsed ENC_TO entries, add them as status info
    if (encToDetails && encToDetails.length > 0) {
      statusInfo += "\n\n" + IcecryptLocale.getString("encryptKeysNote", [encToDetails]);
    }

    if (!statusLine) {
      return;
    }

    Icecrypt.msg.securityInfo = {
      statusFlags: statusFlags,
      keyId: keyId,
      userId: userId,
      statusLine: statusLine,
      msgSigned: msgSigned,
      statusArr: statusArr,
      statusInfo: statusInfo,
      fullStatusInfo: fullStatusInfo,
      blockSeparation: blockSeparation,
      xtraStatus: xtraStatus,
      encryptedMimePart: encMimePartNumber
    };

    this.displayStatusBar();
    this.updateMsgDb();

  },

  displayStatusBar: function() {
    const nsIIcecrypt = IcecryptConstants.nsIIcecrypt;

    let statusText = document.getElementById("icecryptStatusText");
    let expStatusText = document.getElementById("expandedIcecryptStatusText");
    let icon = document.getElementById("enigToggleHeaderView2");
    let bodyElement = document.getElementById("messagepanebox");

    let secInfo = Icecrypt.msg.securityInfo;
    let statusFlags = secInfo.statusFlags;

    if (secInfo.statusArr.length > 0) {
      expStatusText.value = secInfo.statusArr[0];
      expStatusText.setAttribute("state", "true");
      icon.removeAttribute("collapsed");
    }
    else {
      expStatusText.value = "";
      expStatusText.setAttribute("state", "false");
      icon.setAttribute("collapsed", "true");
    }

    if (secInfo.statusLine) {
      this.setStatusText(secInfo.statusLine + " ");
      this.icecryptBox.removeAttribute("collapsed");
      this.displayExtendedStatus(true);

      if ((secInfo.keyId && (statusFlags & nsIIcecrypt.UNVERIFIED_SIGNATURE)) ||
        (statusFlags & nsIIcecrypt.INLINE_KEY)) {
        document.getElementById("icecrypt_importKey").removeAttribute("hidden");
      }
      else {
        document.getElementById("icecrypt_importKey").setAttribute("hidden", "true");
      }

    }
    else {
      this.setStatusText("");
      this.icecryptBox.setAttribute("collapsed", "true");
      this.displayExtendedStatus(false);
    }

    if (!gSMIMEContainer)
      return;

    // Update icons and header-box css-class
    try {
      gSMIMEContainer.collapsed = false;
      gSignedUINode.collapsed = false;
      gEncryptedUINode.collapsed = false;

      if ((statusFlags & nsIIcecrypt.BAD_SIGNATURE) &&
        !(statusFlags & nsIIcecrypt.GOOD_SIGNATURE)) {
        // Display untrusted/bad signature icon
        gSignedUINode.setAttribute("signed", "unknown");
        this.icecryptBox.setAttribute("class", "expandedIcecryptBox icecryptHeaderBoxLabelSignatureUnknown");
        this.statusBar.setAttribute("signed", "unknown");
      }
      else if ((statusFlags & nsIIcecrypt.GOOD_SIGNATURE) &&
        (statusFlags & nsIIcecrypt.TRUSTED_IDENTITY) &&
        !(statusFlags & (nsIIcecrypt.REVOKED_KEY |
          nsIIcecrypt.EXPIRED_KEY_SIGNATURE |
          nsIIcecrypt.EXPIRED_SIGNATURE))) {
        // Display trusted good signature icon
        gSignedUINode.setAttribute("signed", "ok");
        this.icecryptBox.setAttribute("class", "expandedIcecryptBox icecryptHeaderBoxLabelSignatureOk");
        this.statusBar.setAttribute("signed", "ok");
        bodyElement.setAttribute("enigSigned", "ok");
      }
      else if (statusFlags & nsIIcecrypt.UNVERIFIED_SIGNATURE) {
        // Display unverified signature icon
        gSignedUINode.setAttribute("signed", "unknown");
        this.icecryptBox.setAttribute("class", "expandedIcecryptBox icecryptHeaderBoxLabelSignatureUnknown");
        this.statusBar.setAttribute("signed", "unknown");
      }
      else if (statusFlags & (nsIIcecrypt.REVOKED_KEY |
          nsIIcecrypt.EXPIRED_KEY_SIGNATURE |
          nsIIcecrypt.EXPIRED_SIGNATURE |
          nsIIcecrypt.GOOD_SIGNATURE)) {
        // Display unverified signature icon
        gSignedUINode.setAttribute("signed", "unknown");
        this.icecryptBox.setAttribute("class", "expandedIcecryptBox icecryptHeaderBoxLabelSignatureVerified");
        this.statusBar.setAttribute("signed", "unknown");
      }
      else if (statusFlags & nsIIcecrypt.INLINE_KEY) {
        this.icecryptBox.setAttribute("class", "expandedIcecryptBox icecryptHeaderBoxLabelSignatureUnknown");
      }
      else {
        this.icecryptBox.setAttribute("class", "expandedIcecryptBox icecryptHeaderBoxLabelNoSignature");
      }

      if (statusFlags & nsIIcecrypt.DECRYPTION_OKAY) {
        IcecryptURIs.rememberEncryptedUri(this.lastEncryptedMsgKey);

        // Display encrypted icon
        gEncryptedUINode.setAttribute("encrypted", "ok");
        this.statusBar.setAttribute("encrypted", "ok");
      }
      else if (statusFlags &
        (nsIIcecrypt.DECRYPTION_INCOMPLETE | nsIIcecrypt.DECRYPTION_FAILED)) {
        // Display un-encrypted icon
        gEncryptedUINode.setAttribute("encrypted", "notok");
        this.statusBar.setAttribute("encrypted", "notok");
        this.icecryptBox.setAttribute("class", "expandedIcecryptBox icecryptHeaderBoxLabelSignatureNotOk");
      }

      // special handling after trying to fix buggy mail format (see buggyExchangeEmailContent in code)
      if (secInfo.xtraStatus && secInfo.xtraStatus == "buggyMailFormat") {
        this.icecryptBox.setAttribute("class", "expandedIcecryptBox icecryptHeaderBoxLabelBuggyMailFormat");
      }

    }
    catch (ex) {
      IcecryptLog.writeException("displayStatusBar", ex);
    }
  },

  dispSecurityContext: function() {

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    try {
      if (Icecrypt.msg.securityInfo) {
        if ((Icecrypt.msg.securityInfo.statusFlags & nsIIcecrypt.NODATA) &&
          (Icecrypt.msg.securityInfo.statusFlags &
            (nsIIcecrypt.PGP_MIME_SIGNED | nsIIcecrypt.PGP_MIME_ENCRYPTED))) {
          document.getElementById("icecrypt_reloadMessage").removeAttribute("hidden");
        }
        else {
          document.getElementById("icecrypt_reloadMessage").setAttribute("hidden", "true");
        }
      }

      var optList = ["pgpSecurityInfo", "copySecurityInfo"];
      for (var j = 0; j < optList.length; j++) {
        var menuElement = document.getElementById("icecrypt_" + optList[j]);
        if (Icecrypt.msg.securityInfo) {
          menuElement.removeAttribute("disabled");
        }
        else {
          menuElement.setAttribute("disabled", "true");
        }
      }

      this.setSenderStatus("signSenderKey", "editSenderKeyTrust", "showPhoto", "dispKeyDetails");
    }
    catch (ex) {
      IcecryptLog.ERROR("error on displaying Security menu:\n" + ex.toString() + "\n");
    }
  },


  updateSendersKeyMenu: function() {
    this.setSenderStatus("keyMgmtSignKey",
      "keyMgmtKeyTrust",
      "keyMgmtShowPhoto",
      "keyMgmtDispKeyDetails",
      "importpublickey");
  },

  setSenderStatus: function(elemSign, elemTrust, elemPhoto, elemKeyProps, elemImportKey) {

    function setElemStatus(elemName, disabledValue) {
      document.getElementById("icecrypt_" + elemName).setAttribute("disabled", !disabledValue);

      let secondElem = document.getElementById("icecrypt_" + elemName + "2");
      if (secondElem) secondElem.setAttribute("disabled", !disabledValue);
    }

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    var photo = false;
    var sign = false;
    var trust = false;
    var unknown = false;
    var signedMsg = false;

    if (Icecrypt.msg.securityInfo) {
      if (Icecrypt.msg.securityInfo.statusFlags & nsIIcecrypt.PHOTO_AVAILABLE) {
        photo = true;
      }
      if (Icecrypt.msg.securityInfo.msgSigned) {
        signedMsg = true;
        if (!(Icecrypt.msg.securityInfo.statusFlags &
            (nsIIcecrypt.REVOKED_KEY | nsIIcecrypt.EXPIRED_KEY_SIGNATURE | nsIIcecrypt.UNVERIFIED_SIGNATURE))) {
          sign = true;
        }
        if (!(Icecrypt.msg.securityInfo.statusFlags & nsIIcecrypt.UNVERIFIED_SIGNATURE)) {
          trust = true;
        }

        if (Icecrypt.msg.securityInfo.statusFlags & nsIIcecrypt.UNVERIFIED_SIGNATURE) {
          unknown = true;
        }
      }
    }

    if (elemTrust) setElemStatus(elemTrust, trust);
    if (elemSign) setElemStatus(elemSign, sign);
    if (elemPhoto) setElemStatus(elemPhoto, photo);
    if (elemKeyProps) setElemStatus(elemKeyProps, (signedMsg && !unknown));
    if (elemImportKey) setElemStatus(elemImportKey, unknown);
  },

  editKeyExpiry: function() {
    IcecryptWindows.editKeyExpiry(window, [Icecrypt.msg.securityInfo.userId], [Icecrypt.msg.securityInfo.keyId]);
    gDBView.reloadMessageWithAllParts();
  },

  editKeyTrust: function() {
    let icecryptSvc = IcecryptCore.getService();
    let key = IcecryptKeyRing.getKeyById(Icecrypt.msg.securityInfo.keyId);

    IcecryptWindows.editKeyTrust(window, [Icecrypt.msg.securityInfo.userId], [key.keyId]);
    gDBView.reloadMessageWithAllParts();
  },

  signKey: function() {
    let icecryptSvc = IcecryptCore.getService();
    let key = IcecryptKeyRing.getKeyById(Icecrypt.msg.securityInfo.keyId);

    IcecryptWindows.signKey(window, Icecrypt.msg.securityInfo.userId, key.keyId, null);
    gDBView.reloadMessageWithAllParts();
  },


  msgHdrViewLoad: function(event) {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: this.msgHdrViewLoad\n");

    var listener = {
      icecryptBox: document.getElementById("icecryptBox"),
      onStartHeaders: function _listener_onStartHeaders() {
        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: _listener_onStartHeaders\n");

        try {

          Icecrypt.hdrView.statusBarHide();
          Icecrypt.msg.mimeParts = null;

          IcecryptVerify.setMsgWindow(msgWindow, Icecrypt.msg.getCurrentMsgUriSpec());

          Icecrypt.hdrView.setStatusText("");

          this.icecryptBox.setAttribute("class", "expandedIcecryptBox icecryptHeaderBoxLabelSignatureOk");

          var msgFrame = IcecryptWindows.getFrame(window, "messagepane");

          if (msgFrame) {
            IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: msgFrame=" + msgFrame + "\n");

            msgFrame.addEventListener("unload", Icecrypt.hdrView.messageUnload.bind(Icecrypt.hdrView), true);
            msgFrame.addEventListener("load", Icecrypt.msg.messageAutoDecrypt.bind(Icecrypt.msg), false);
          }

          Icecrypt.hdrView.forgetEncryptedMsgKey();

          if (messageHeaderSink) {
            try {
              messageHeaderSink.icecryptPrepSecurityInfo();
            }
            catch (ex) {}
          }
        }
        catch (ex) {}
      },

      onEndHeaders: function _listener_onEndHeaders() {
        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: _listener_onEndHeaders\n");
        try {
          Icecrypt.hdrView.statusBarHide();

          this.icecryptBox.setAttribute("class", "expandedIcecryptBox icecryptHeaderBoxLabelSignatureOk");
        }
        catch (ex) {}
      },

      beforeStartHeaders: function _listener_beforeStartHeaders() {
        return true;
      }
    };

    gMessageListeners.push(listener);
  },

  messageUnload: function() {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: this.messageUnload\n");
  },

  hdrViewUnload: function() {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: this.hdrViewUnLoad\n");
    this.forgetEncryptedMsgKey();
  },

  copyStatusInfo: function() {
    if (Icecrypt.msg.securityInfo) {
      IcecryptClipboard.setClipboardContent(Icecrypt.msg.securityInfo.statusInfo);
    }

  },

  showPhoto: function() {
    if (!Icecrypt.msg.securityInfo) return;

    let icecryptSvc = IcecryptCore.getService();
    let key = IcecryptKeyRing.getKeyById(Icecrypt.msg.securityInfo.keyId);

    IcecryptWindows.showPhoto(window, key.keyId, Icecrypt.msg.securityInfo.userId);
  },


  dispKeyDetails: function() {
    if (!Icecrypt.msg.securityInfo) return;

    let icecryptSvc = IcecryptCore.getService();
    let key = IcecryptKeyRing.getKeyById(Icecrypt.msg.securityInfo.keyId);

    IcecryptWindows.openKeyDetails(window, key.keyId, false);
  },

  createRuleFromAddress: function(emailAddressNode) {
    if (emailAddressNode) {
      if (typeof(findEmailNodeFromPopupNode) == "function") {
        emailAddressNode = findEmailNodeFromPopupNode(emailAddressNode, 'emailAddressPopup');
      }
      IcecryptWindows.createNewRule(window, emailAddressNode.getAttribute("emailAddress"));
    }
  },

  forgetEncryptedMsgKey: function() {
    if (Icecrypt.hdrView.lastEncryptedMsgKey) {
      IcecryptURIs.forgetEncryptedUri(Icecrypt.hdrView.lastEncryptedMsgKey);
      Icecrypt.hdrView.lastEncryptedMsgKey = null;
    }
  },

  msgHdrViewHide: function() {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: this.msgHdrViewHide\n");
    this.icecryptBox.setAttribute("collapsed", true);

    Icecrypt.msg.securityInfo = {
      statusFlags: 0,
      keyId: "",
      userId: "",
      statusLine: "",
      statusInfo: "",
      fullStatusInfo: "",
      encryptedMimePart: ""
    };

  },

  msgHdrViewUnhide: function(event) {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: this.msgHdrViewUnhide:\n");

    if (Icecrypt.msg.securityInfo && Icecrypt.msg.securityInfo.statusFlags !== 0) {
      this.icecryptBox.removeAttribute("collapsed");
    }
  },

  displayExtendedStatus: function(displayOn) {
    var expStatusText = document.getElementById("expandedIcecryptStatusText");
    if (displayOn && expStatusText.getAttribute("state") == "true") {
      if (expStatusText.getAttribute("display") == "true") {
        expStatusText.removeAttribute("collapsed");
      }
      else {
        expStatusText.setAttribute("collapsed", "true");
      }
    }
    else {
      expStatusText.setAttribute("collapsed", "true");
    }
  },

  toggleHeaderView: function() {
    var viewToggle = document.getElementById("enigToggleHeaderView2");
    var expandedText = document.getElementById("expandedIcecryptStatusText");
    var state = viewToggle.getAttribute("state");

    if (state == "true") {
      viewToggle.setAttribute("state", "false");
      viewToggle.setAttribute("class", "icecryptExpandViewButton");
      expandedText.setAttribute("display", "false");
      this.displayExtendedStatus(false);
    }
    else {
      viewToggle.setAttribute("state", "true");
      viewToggle.setAttribute("class", "icecryptCollapseViewButton");
      expandedText.setAttribute("display", "true");
      this.displayExtendedStatus(true);
    }
  },

  enigOnShowAttachmentContextMenu: function() {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: this.enigOnShowAttachmentContextMenu\n");
    // first, call the original function ...

    try {
      // Thunderbird
      onShowAttachmentItemContextMenu();
    }
    catch (ex) {
      // SeaMonkey
      onShowAttachmentContextMenu();
    }

    // then, do our own additional stuff ...

    // Thunderbird
    var contextMenu = document.getElementById('attachmentItemContext');
    var selectedAttachments = contextMenu.attachments;

    if (!contextMenu) {
      // SeaMonkey
      contextMenu = document.getElementById('attachmentListContext');
      selectedAttachments = attachmentList.selectedItems;
    }

    var decryptOpenMenu = document.getElementById('icecrypt_ctxDecryptOpen');
    var decryptSaveMenu = document.getElementById('icecrypt_ctxDecryptSave');
    var importMenu = document.getElementById('icecrypt_ctxImportKey');
    var verifyMenu = document.getElementById('icecrypt_ctxVerifyAtt');

    if (selectedAttachments.length > 0) {
      if (selectedAttachments[0].contentType.search(/^application\/pgp-keys/i) === 0) {
        importMenu.removeAttribute('disabled');
        decryptOpenMenu.setAttribute('disabled', true);
        decryptSaveMenu.setAttribute('disabled', true);
        verifyMenu.setAttribute('disabled', true);
      }
      else if (Icecrypt.msg.checkSignedAttachment(selectedAttachments[0], null)) {
        importMenu.setAttribute('disabled', true);
        decryptOpenMenu.setAttribute('disabled', true);
        decryptSaveMenu.setAttribute('disabled', true);
        verifyMenu.removeAttribute('disabled');
      }
      else if (Icecrypt.msg.checkEncryptedAttach(selectedAttachments[0])) {
        importMenu.setAttribute('disabled', true);
        decryptOpenMenu.removeAttribute('disabled');
        decryptSaveMenu.removeAttribute('disabled');
        verifyMenu.setAttribute('disabled', true);
        if (typeof(selectedAttachments[0].displayName) == "undefined") {
          if (!selectedAttachments[0].name) {
            selectedAttachments[0].name = "message.pgp";
          }
        }
        else
        if (!selectedAttachments[0].displayName) {
          selectedAttachments[0].displayName = "message.pgp";
        }
      }
      else {
        importMenu.setAttribute('disabled', true);
        decryptOpenMenu.setAttribute('disabled', true);
        decryptSaveMenu.setAttribute('disabled', true);
        verifyMenu.setAttribute('disabled', true);
      }
    }
    else {
      openMenu.setAttribute('disabled', true); /* global openMenu: false */
      saveMenu.setAttribute('disabled', true); /* global saveMenu: false */
      decryptOpenMenu.setAttribute('disabled', true);
      decryptSaveMenu.setAttribute('disabled', true);
      importMenu.setAttribute('disabled', true);
      verifyMenu.setAttribute('disabled', true);
    }
  },

  updateMsgDb: function() {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: this.updateMsgDb\n");
    var msg = gFolderDisplay.selectedMessage;
    if (!msg || !msg.folder) return;

    var msgHdr = msg.folder.GetMessageHeader(msg.messageKey);
    if (this.statusBar.getAttribute("encrypted") == "ok")
      Icecrypt.msg.securityInfo.statusFlags |= Components.interfaces.nsIIcecrypt.DECRYPTION_OKAY;
    msgHdr.setUint32Property("icecrypt", Icecrypt.msg.securityInfo.statusFlags);
  },

  enigCanDetachAttachments: function() {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: this.enigCanDetachAttachments\n");

    const nsIIcecrypt = Components.interfaces.nsIIcecrypt;

    var canDetach = true;
    if (Icecrypt.msg.securityInfo && (typeof(Icecrypt.msg.securityInfo.statusFlags) != "undefined")) {
      canDetach = ((Icecrypt.msg.securityInfo.statusFlags &
        (nsIIcecrypt.PGP_MIME_SIGNED | nsIIcecrypt.PGP_MIME_ENCRYPTED)) ? false : true);
    }
    return canDetach;
  },

  fillAttachmentListPopup: function(item) {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: Icecrypt.hdrView.fillAttachmentListPopup\n");
    FillAttachmentListPopup(item); /* global FillAttachmentListPopup: false */

    if (!this.enigCanDetachAttachments()) {
      for (var i = 0; i < item.childNodes.length; i++) {
        if (item.childNodes[i].className == "menu-iconic") {
          var mnu = item.childNodes[i].firstChild.firstChild;
          while (mnu) {
            if (mnu.getAttribute("oncommand").search(/(detachAttachment|deleteAttachment)/) >= 0) {
              mnu.setAttribute("disabled", true);
            }
            mnu = mnu.nextSibling;
          }
        }
      }
    }
  }

};

window.addEventListener("load", Icecrypt.hdrView.hdrViewLoad.bind(Icecrypt.hdrView), false);
addEventListener('messagepane-loaded', Icecrypt.hdrView.msgHdrViewLoad.bind(Icecrypt.hdrView), true);
addEventListener('messagepane-unloaded', Icecrypt.hdrView.hdrViewUnload.bind(Icecrypt.hdrView), true);
addEventListener('messagepane-hide', Icecrypt.hdrView.msgHdrViewHide.bind(Icecrypt.hdrView), true);
addEventListener('messagepane-unhide', Icecrypt.hdrView.msgHdrViewUnhide.bind(Icecrypt.hdrView), true);

////////////////////////////////////////////////////////////////////////////////
// THE FOLLOWING OVERRIDES CODE IN msgHdrViewOverlay.js
////////////////////////////////////////////////////////////////////////////////

// there is unfortunately no other way to add Icecrypt to the validator than this

function CanDetachAttachments() {
  var canDetach = !gFolderDisplay.selectedMessageIsNews &&
    (!gFolderDisplay.selectedMessageIsImap || MailOfflineMgr.isOnline());

  if (canDetach && ("content-type" in currentHeaderData)) {
    var contentType = currentHeaderData["content-type"].headerValue;

    canDetach = !ContentTypeIsSMIME(currentHeaderData["content-type"].headerValue);
  }
  return canDetach && Icecrypt.hdrView.enigCanDetachAttachments();
}


////////////////////////////////////////////////////////////////////////////////
// THE FOLLOWING EXTENDS CODE IN msgHdrViewOverlay.js
////////////////////////////////////////////////////////////////////////////////

if (messageHeaderSink) {
  messageHeaderSink.icecryptPrepSecurityInfo = function() {
    IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: icecryptPrepSecurityInfo\n");


    /// BEGIN EnigMimeHeaderSink definition
    function EnigMimeHeaderSink(innerSMIMEHeaderSink) {
      IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.innerSMIMEHeaderSink=" + innerSMIMEHeaderSink + "\n");
      this._smimeHeaderSink = innerSMIMEHeaderSink;
    }

    EnigMimeHeaderSink.prototype = {
      _smimeHeaderSink: null,

      workaroundMode: null,

      QueryInterface: function(iid) {
        //IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.QI: "+iid+"\n");
        if (iid.equals(Components.interfaces.nsIMsgSMIMEHeaderSink) &&
          this._smimeHeaderSink)
          return this;

        if (iid.equals(Components.interfaces.nsIEnigMimeHeaderSink) ||
          iid.equals(Components.interfaces.nsISupports))
          return this;

        throw Components.results.NS_NOINTERFACE;
      },

      /**
       * Determine message number and folder from mailnews URI
       *
       * @param url - nsIURI object
       *
       * @return Object:
       *    - msgNum: String - the message number, or "" if no URI Scheme fits
       *    - folder: String - the folder (or newsgroup) name
       */
      msgIdentificationFromUrl: function(url) {

        // sample URLs in Thunderbird
        // Local folder: mailbox:///some/path/to/folder?number=359360
        // IMAP: imap://user@host:port/fetch>some>path>111
        // NNTP: news://some.host/some.service.com?group=some.group.name&key=3510
        // also seen: e.g. mailbox:///som/path/to/folder?number=4455522&part=1.1.2&filename=test.eml
        // mailbox:///...?number=4455522&part=1.1.2&filename=test.eml&type=application/x-message-display&filename=test.eml
        // imap://user@host:port>UID>some>path>10?header=filter&emitter=js&examineEncryptedParts=true

        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: msgIdentificationFromUrl: url.path=" + url.path + "\n");

        let msgNum = "";
        let msgFolder = "";

        if (url.schemeIs("mailbox")) {
          msgNum = url.path.replace(/(.*[?&]number=)([0-9]+)([^0-9].*)?/, "$2");
          msgFolder = url.path.replace(/\?.*/, "");
        }
        else if (url.schemeIs("imap")) {
          let p = unescape(url.path);
          msgNum = p.replace(/(.*>)([0-9]+)([^0-9].*)?/, "$2");
          msgFolder = p.replace(/\?.*$/, "").replace(/>[^>]+$/, "");
        }
        else if (url.schemeIs("news")) {
          msgNum = url.path.replace(/(.*[?&]key=)([0-9]+)([^0-9].*)?/, "$2");
          msgFolder = url.path.replace(/(.*[?&]group=)([^&]+)(&.*)?/, "$2");
        }

        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: msgIdentificationFromUrl: msgNum=" + msgNum + " / folder=" + msgFolder + "\n");

        return {
          msgNum: msgNum,
          folder: msgFolder.toLowerCase()
        };
      },


      isCurrentMessage: function(uri) {
        let uriSpec = (uri ? uri.spec : null);

        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.isCurrentMessage: uri.spec=" + uriSpec + "\n");

        if (!uriSpec || uriSpec.search(/^icecrypt:/) === 0) {
          // we cannot compare if no URI given or if URI is Icecrypt-internal;
          // therefore assuming it's the current message
          return true;
        }

        let msgUriSpec = Icecrypt.msg.getCurrentMsgUriSpec();

        let currUrl = {};
        try {
          let messenger = Components.classes["@mozilla.org/messenger;1"].getService(Components.interfaces.nsIMessenger);
          let msgSvc = messenger.messageServiceFromURI(msgUriSpec);
          msgSvc.GetUrlForUri(msgUriSpec, currUrl, null);
        }
        catch (ex) {
          IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.isCurrentMessage: could not determine URL\n");
          currUrl.value = {
            host: "invalid",
            path: "/message",
            scheme: "icecrypt",
            spec: "icecrypt://invalid/message",
            schemeIs: function(s) {
              return s === this.scheme;
            }
          };
        }

        let currMsgId = this.msgIdentificationFromUrl(currUrl.value);
        let gotMsgId = this.msgIdentificationFromUrl(uri);

        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.isCurrentMessage: url=" + currUrl.value.spec + "\n");

        if (uri.host == currUrl.value.host &&
          currMsgId.folder === gotMsgId.folder &&
          currMsgId.msgNum === gotMsgId.msgNum) {
          IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.isCurrentMessage: true\n");
          return true;
        }

        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.isCurrentMessage: false\n");
        return false;
      },

      /**
       * Determine if a given MIME part number is a multipart/related message or a child thereof
       *
       * @param mimePart:      Object - The MIME Part object to evaluate from the MIME tree
       * @param searchPartNum: String - The part number to determine
       */
      isMultipartRelated: function(mimePart, searchPartNum) {
        if (searchPartNum.indexOf(mimePart.partNum) == 0 && mimePart.partNum.length <= searchPartNum.length) {
          if (mimePart.fullContentType.search(/^multipart\/related/i) === 0) return true;

          for (let i in mimePart.subParts) {
            if (this.isMultipartRelated(mimePart.subParts[i], searchPartNum)) return true;
          }
        }
        return false;
      },

      /**
       * Determine if a given mime part number should be displayed.
       * Returns true if one of these conditions is true:
       *  - this is the 1st displayed block of the message
       *  - the message part displayed corresonds to the decrypted part
       *
       * @param mimePartNumber: String - the MIME part number that was decrypted/verified
       * @param uriSpec:        String - the URI spec that is being displayed
       */
      displaySubPart: function(mimePartNumber, uriSpec) {
        if (!mimePartNumber) return true;
        let part = IcecryptMime.getMimePartNumber(uriSpec);

        if (part.length === 0) {
          // only display header if 1st message part
          if (mimePartNumber.search(/^1(\.1)*$/) < 0) return false;
        }
        else {
          let r = IcecryptFuncs.compareMimePartLevel(mimePartNumber, part);

          // analyzed mime part is contained in viewed message part
          if (r === 2) {
            if (mimePartNumber.substr(part.length).search(/^\.1(\.1)*$/) < 0) return false;
          }
          else if (r !== 0) return false;

          if (Icecrypt.msg.mimeParts) {
            if (this.isMultipartRelated(Icecrypt.msg.mimeParts, mimePartNumber)) return false;
          }
        }
        return true;
      },

      updateSecurityStatus: function(unusedUriSpec, exitCode, statusFlags, keyId, userId, sigDetails, errorMsg, blockSeparation, uri, encToDetails, mimePartNumber) {
        // unusedUriSpec is not used anymore. It is here becaue other addons rely on the same API

        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: updateSecurityStatus: mimePart=" + mimePartNumber + "\n");

        let uriSpec = (uri ? uri.spec : null);

        if (this.isCurrentMessage(uri)) {

          if (!this.displaySubPart(mimePartNumber, uriSpec)) return;
          if (this.hasUnauthenticatedParts(mimePartNumber)) {
            IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: updateSecurityStatus: found unauthenticated part\n");
            statusFlags |= Components.interfaces.nsIIcecrypt.PARTIALLY_PGP;
          }

          Icecrypt.hdrView.updateHdrIcons(exitCode, statusFlags, keyId, userId, sigDetails,
            errorMsg, blockSeparation, encToDetails,
            null, mimePartNumber);
        }

        if (uriSpec && uriSpec.search(/^icecrypt:message\//) === 0) {
          // display header for broken MS-Exchange message
          let ebeb = document.getElementById("icecryptBrokenExchangeBox");
          ebeb.removeAttribute("collapsed");
        }

        return;
      },

      /**
       * Determine if there are message parts that are not signed/encrypted
       *
       * @param mimePartNumber String - the MIME part number that was authenticated
       *
       * @return Boolean: true: there are siblings / false: no siblings
       */
      hasUnauthenticatedParts: function(mimePartNumber) {
        function hasSiblings(mimePart, searchPartNum, parent) {
          if (mimePart.partNum.indexOf(parent) == 0 && mimePart.partNum !== searchPartNum) return true;

          for (let i in mimePart.subParts) {
            if (hasSiblings(mimePart.subParts[i], searchPartNum, parent)) return true;
          }

          return false;
        }

        let parent = mimePartNumber.replace(/\.\d+$/, "");
        if (mimePartNumber.search(/\./) < 0) {
          parent = "";
        }

        if (mimePartNumber && Icecrypt.msg.mimeParts) {
          if (hasSiblings(Icecrypt.msg.mimeParts, mimePartNumber, parent)) return true;
        }

        return false;
      },


      modifyMessageHeaders: function(uri, headerData, mimePartNumber) {
        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.modifyMessageHeaders:\n");
        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: headerData= " + headerData + ", mimePart=" + mimePartNumber + "\n");

        function updateHdrBox(header, value) {
          let e = document.getElementById("expanded" + header + "Box");
          if (e) {
            e.headerValue = value;
          }
        }

        let uriSpec = (uri ? uri.spec : null);
        let hdr;
        try {
          hdr = JSON.parse(headerData);
        }
        catch (ex) {
          IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: modifyMessageHeaders: - no headers to display\n");
          return;
        }

        let msg = gFolderDisplay.selectedMessage;

        if (!msg) return;

        if (typeof(hdr) !== "object") return;
        if (!this.isCurrentMessage(uri) || gFolderDisplay.selectedMessages.length !== 1) return;

        if (!this.displaySubPart(mimePartNumber, uriSpec)) return;

        if ("subject" in hdr) {
          msg.subject = IcecryptData.convertFromUnicode(hdr.subject, "utf-8");
          updateHdrBox("subject", hdr.subject);
        }

        if ("date" in hdr) {
          msg.date = Date.parse(hdr.date) * 1000;
        }

        if ("newsgroups" in hdr) {
          updateHdrBox("newsgroups", hdr.newsgroups);
        }

        if ("followup-to" in hdr) {
          updateHdrBox("followup-to", hdr["followup-to"]);
        }

        if ("from" in hdr) {
          gExpandedHeaderView.from.outputFunction(gExpandedHeaderView.from, IcecryptData.convertFromUnicode(hdr.from, "utf-8"));
          msg.setStringProperty("Icecrypt-From", hdr.from);
        }

        if ("to" in hdr) {
          gExpandedHeaderView.to.outputFunction(gExpandedHeaderView.to, IcecryptData.convertFromUnicode(hdr.to, "utf-8"));
          msg.setStringProperty("Icecrypt-To", hdr.to);
        }

        if ("cc" in hdr) {
          gExpandedHeaderView.cc.outputFunction(gExpandedHeaderView.cc, IcecryptData.convertFromUnicode(hdr.cc, "utf-8"));
          msg.setStringProperty("Icecrypt-Cc", hdr.cc);
        }

        if ("reply-to" in hdr) {
          gExpandedHeaderView["reply-to"].outputFunction(gExpandedHeaderView["reply-to"], IcecryptData.convertFromUnicode(hdr["reply-to"], "utf-8"));
          msg.setStringProperty("Icecrypt-ReplyTo", hdr["reply-to"]);
        }

      },

      handleSMimeMessage: function(uri) {
        if (this.isCurrentMessage(uri)) {
          IcecryptVerify.unregisterContentTypeHandler();
          Icecrypt.msg.messageReload(false);
        }
      },

      maxWantedNesting: function() {
        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.maxWantedNesting:\n");
        return this._smimeHeaderSink.maxWantedNesting();
      },

      signedStatus: function(aNestingLevel, aSignatureStatus, aSignerCert) {
        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.signedStatus:\n");
        return this._smimeHeaderSink.signedStatus(aNestingLevel, aSignatureStatus, aSignerCert);
      },

      encryptionStatus: function(aNestingLevel, aEncryptionStatus, aRecipientCert) {
        IcecryptLog.DEBUG("icecryptMsgHdrViewOverlay.js: EnigMimeHeaderSink.encryptionStatus:\n");
        return this._smimeHeaderSink.encryptionStatus(aNestingLevel, aEncryptionStatus, aRecipientCert);
      }

    };
    /// END EnigMimeHeaderSink definition

    var innerSMIMEHeaderSink = null;
    var icecryptHeaderSink = null;

    try {
      innerSMIMEHeaderSink = this.securityInfo.QueryInterface(Components.interfaces.nsIMsgSMIMEHeaderSink);

      try {
        icecryptHeaderSink = innerSMIMEHeaderSink.QueryInterface(Components.interfaces.nsIEnigMimeHeaderSink);
      }
      catch (ex) {}
    }
    catch (ex) {}

    if (!icecryptHeaderSink) {
      this.securityInfo = new EnigMimeHeaderSink(innerSMIMEHeaderSink);
    }
  };
}
