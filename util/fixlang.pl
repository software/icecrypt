#!/usr/bin/perl
# check for missing entries in language specific dtd and properties files
# and add the english default for them

sub trim { # ($str)
  my $str = @_[0];

  $str =~ s/\s*$//;
  $str =~ s/^\s*//;

  return $str;
}

# Load DTD file
sub loaddtd { # ($file)
  my $file = @_[0];

  #print "+ Loading $file\n";
  my $tab={};
  my $line=0;


  my $fic;
  my $ind;
  my $val;

  open($fic, $file) || die "Could not open $file";
  my $prev=0;

  while (<$fic>) {
    my $buf = $_;
    ++$line;
    $buf =~ s/\n//;
    $buf =~ s/\r//;
    if (length(trim($buf)) == 0) {
      #print "+ empty\n";
    }
    elsif ($buf =~ /^<!--.*-->$'/i) {
      #print "+ comment\n";
    }
    elsif ($buf =~ /^<!ENTITY (.*)"(.*)">\s*$'/i) {
      $ind=trim($1);
      #print "+ Line  '$ind'\n";
      $val=$2;
      if ($ind eq "icecrypt.ruleEmail.tooltip"
          || $ind eq "icecrypt.noHushMailSupport.label"
          || $ind eq "icecrypt.noHushMailSupport.tooltip") {
        $val =~ s/\</&lt;/g;
        $val =~ s/\>/&gt;/g;
      }
      $tab->{$ind} = "$1\"$val\">";
      $prev=0;
    }
    elsif ($buf =~ /^<!ENTITY (.*)"(.*)$/i) {
      $ind=trim($1);
      #print "+ Start '$ind'\n";
      $tab->{$ind} = "$1\"$2";
      $prev=$ind;
    }
    elsif ($prev && $buf =~ /^(.*)">$/) {
      #print "+ End   '$prev'\n";
      $tab->{$prev} .= "\n$1\">";
      $prev=0;
    }
    elsif ($prev) {
      #print "+ Cont. '$prev'\n";
      $tab->{$prev} .= "\n$buf";
    }
    else {
      die ("- in $file on line $line: unknown ($buf). ABORT!\n");
    }
  }
  close($fic);

  return $tab;
}

# Load properties file
sub loadprop { # ($file)

  my $file = @_[0];

  #print "+ Loading $file\n";
  my $tab={};

  my $fic;
  my $ind;

  open($fic, $file) || die "Could not open $file";

  while (<$fic>) {
    my $buf = $_;
    $buf =~ s/\n//;
    $buf =~ s/\r//;

    if (length(trim($buf)) == 0) {
      #print "+ empty\n";
    }
    elsif ($buf =~ /^\s*#/) {
      #print "+ comments\n";
    }
    elsif ($buf =~ /^\s*([A-Za-z0-9._]+)\s*=\s*(.*)/) {
      #print "+ Value '$1'\n";
      $ind=trim($1);
      $tab->{$ind} = "$1=$2";
    }
    else {
      print ("\tIgnored ($buf) !\n");
    }
  }

  return $tab;
}


($#ARGV > 0) || die ("usage fixlang.pl fromdir destdir\n   fromdir: original en-US locale directory\n   destdir: locale lanugage dir\n");
my $from=$ARGV[0];
my $dest=$ARGV[1];

(-f "$from/icecrypt.dtd")        || die ("$from/icecrypt.dtd not found\n");
(-f "$dest/icecrypt.dtd")        || die ("$dest/icecrypt.dtd not found\n");
(-f "$from/icecrypt.properties") || die ("$from/icecrypt.properties not found\n");
(-f "$dest/icecrypt.properties") || die ("$dest/icecrypt.properties not found\n");

my $endtd = loaddtd("$from/icecrypt.dtd");
my $frdtd = loaddtd("$dest/icecrypt.dtd");

print "+ Writing $dest/icecrypt.dtd\n";
open(OUT, ">$dest/icecrypt.dtd.gen")  || die "Cannot write to $dest/icecrypt.dtd";

for my $ind (sort keys %$endtd) {

  if ($frdtd->{$ind}) {
    print OUT "<!ENTITY $frdtd->{$ind}\n";
  }
  else {
    # print "\tAdding missing $ind\n";
    print OUT "<!ENTITY $endtd->{$ind}\n";
  }
}

close(OUT);

my $enprop = loadprop("$from/icecrypt.properties");
my $frprop = loadprop("$dest/icecrypt.properties");

print "+ Writing $dest/icecrypt.properties\n";
open(OUT, ">$dest/icecrypt.properties.gen") || die "Cannot write to $dest/icecrypt.properties";
for my $ind (sort keys %$enprop) {
  if ($frprop->{$ind}) {
    print OUT "$frprop->{$ind}\n";
  } else {
    #print "\tAdding missing $ind\n";
    print OUT "$enprop->{$ind}\n";
  }
}
close(OUT);
